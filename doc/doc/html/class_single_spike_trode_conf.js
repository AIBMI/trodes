var class_single_spike_trode_conf =
[
    [ "color", "class_single_spike_trode_conf.html#ac79a7de4a082592f533cd0e8397bef07", null ],
    [ "filterOn", "class_single_spike_trode_conf.html#a0c4b34f727a056135a395ad4912dc5f1", null ],
    [ "highFilter", "class_single_spike_trode_conf.html#a295a14de18cb0d39b2e63fb49e4c3e71", null ],
    [ "hw_chan", "class_single_spike_trode_conf.html#a28f7328b6d4a44131181fe08d3442e2b", null ],
    [ "lfpChan", "class_single_spike_trode_conf.html#a9e85ce0304b0ce403a4ab6a999f47df7", null ],
    [ "lfpHighFilter", "class_single_spike_trode_conf.html#aeeaffb0a1e5050cc78cfd1d036963e99", null ],
    [ "lfpOn", "class_single_spike_trode_conf.html#a747329d503cf37d023e23688bf5567eb", null ],
    [ "lowFilter", "class_single_spike_trode_conf.html#a20643972a3d314452b8fcbc6459806fa", null ],
    [ "maxDisp", "class_single_spike_trode_conf.html#a488df54ae1243a1f458d2f99d72bc3ac", null ],
    [ "nTrode", "class_single_spike_trode_conf.html#a20030e03c3720021c3483824899f651a", null ],
    [ "refChan", "class_single_spike_trode_conf.html#a78f712f3eb5217ec9e99b3ebeb5d6ede", null ],
    [ "refNTrode", "class_single_spike_trode_conf.html#a73bdbce865a20e51f558f9ed10eded66", null ],
    [ "refOn", "class_single_spike_trode_conf.html#ab42355541673ffb3611fe42a4f531e06", null ],
    [ "streamingChannelLookup", "class_single_spike_trode_conf.html#a9bc7285d1e44e30193dbd65d2331e1c6", null ],
    [ "thresh", "class_single_spike_trode_conf.html#adc5db55e4915378c6d676f4c895c4467", null ],
    [ "thresh_rangeconvert", "class_single_spike_trode_conf.html#a115c566f1cc23e3c8d127fadc180fc56", null ]
];