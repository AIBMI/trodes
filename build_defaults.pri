#-------------------------------------------------
#
# Project include file for all Trodes projects:
#   Trodes app, modules and export scripts
#
#-------------------------------------------------

# Require minimum Qt versions for Windows and Mac/Linux
win32:{
MAJ_REQ = 5
MIN_REQ = 4
} else {
MAJ_REQ = 5
MIN_REQ = 5
}

lessThan(QT_MAJOR_VERSION, $$MAJ_REQ) | \
if(equals(QT_MAJOR_VERSION, $$MAJ_REQ):lessThan(QT_MINOR_VERSION, $$MIN_REQ)) {
    error("Building Trodes on $${QMAKE_HOST.os} requires Qt $${MAJ_REQ}.$${MIN_REQ} or greater, but Qt $$[QT_VERSION] was detected.")
}

QMAKE_MAC_SDK = macosx10.11

# save the top-level trodes repository path (same as location of this .pro file)
TRODES_REPO_DIR = $$PWD

# set command separator (using '&' on unix sends job to background, results
# in later commands running before first one is done).
win32:COMMAND_SEP = &
!win32:COMMAND_SEP = ;


# typedef the release destdir for each supported os
macx:       DESTDIR = $$TRODES_REPO_DIR/bin/macOS
win32:      DESTDIR = $$TRODES_REPO_DIR/bin/win32

INCLUDEPATH  += ../../Trodes/src-display


unix:!macx{

  # build executable in source tree, then install it with `make install`
  DESTDIR = $$_PRO_FILE_PWD_/build
  TRODES_BIN_INSTALL_DIR = $$TRODES_REPO_DIR/bin/linux

  # Install exectuables upon 'make install'
  target.path = $$TRODES_BIN_INSTALL_DIR 
  INSTALLS += target

  # set up Libraries install path and exectuable 'rpath' (all executables will
  # look here first for shared libs). In your .pro file, add files to
  # 'libraries.files' to mark them to be copied over.
  TRODES_LIB_SUBDIR = Libraries
  TRODES_LIB_INSTALL_DIR = $${TRODES_BIN_INSTALL_DIR}/$${TRODES_LIB_SUBDIR}
  QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/$${TRODES_LIB_SUBDIR}\''

  # Optionally deploy Qt libs and plugins from the build system alongside
  # Trodes. Enable this behavior by uncommenting the config line, or passing it
  # as an option to qmake:
  #
  #     qmake CONFIG+=DEPLOY_QT_ALL
  #     qmake CONFIG-=DEPLOY_QT_ALL


  # default to deploying Qt
  CONFIG+=DEPLOY_QT_ALL

  DEPLOY_QT_ALL {

    # Easiest way to deploy Qt shared libs on Linux is just to copy *all* the
    # *.so files and links. (Some Qt libs have dependencies on other Qt libs,
    # and both Ubuntu 14.04 and 16.04 come with Qt5 installed, which means you
    # can't always detect when you have not copied over all the libs or
    # plugins; this leads to using a mixture of libs from different releases.)
    #
    # NB: As of 5.5, the Qt plugin .so files expect the Qt shared libs to be at
    # a hardcoded relative location: "../../lib" (set via a RUNPATH on the plugin.so files),
    # so the libraries must be in a folder called 'lib', in the same directory as
    # the 'plugins' directory. It is also helpful to call the plugins folder
    # by its default name 'plugins' (otherwise would need to set this using
    # qt.conf):
    #
    # NB2: If you change the location of the plugins directory here, you must
    # also update the Resources/qt.conf file.
    TRODES_QTLIBS_SUBDIR = Qt/lib
    TRODES_QTPLUGINS_SUBDIR = Qt/plugins

    # Set up full install paths
    TRODES_QTLIBS_INSTALL_DIR = $${TRODES_BIN_INSTALL_DIR}/$${TRODES_QTLIBS_SUBDIR}
    TRODES_QTPLUGINS_INSTALL_DIR = $${TRODES_BIN_INSTALL_DIR}/$${TRODES_QTPLUGINS_SUBDIR}

    # use RPATH to tell all binaries to search our Qt install first
    QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/$${TRODES_QTLIBS_SUBDIR}\''
    ## Qt Plugins are found by Qt, nothing to do with OS, so no need to set rpath
    # QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/$${TRODES_QTPLUGINS_SUBDIR}\''


    # Create QtDeploy target to be built on 'make install'

    QtDeploy.path = / # dummy path needed to use INSTALLS, if only running commands

    # Copy over Qt libs and plugins
    # (Use 'rsync' instead of 'cp' to avoid lots of unnecessary copying and to
    # preserve symlinks, permissions, etc.)

    QtDeploy.commands += mkdir -p $$TRODES_QTLIBS_INSTALL_DIR ;
    QtDeploy.commands += rsync -a --info=NAME $$[QT_INSTALL_LIBS]/*.so* $$TRODES_QTLIBS_INSTALL_DIR ;
    QtDeploy.commands += mkdir -p $$TRODES_QTPLUGINS_INSTALL_DIR ;
    QtDeploy.commands += rsync -a --info=NAME $$[QT_INSTALL_PLUGINS]/* $$TRODES_QTPLUGINS_INSTALL_DIR ;

    # Copy over a qt.conf file so binaries can find the Plugins directory
#    QtDeploy.commands = rsync -a --info=NAME $${TRODES_REPO_DIR}/Resources/qt.conf $${TRODES_BIN_INSTALL_DIR} ;

    # Use Qt Resource System for the qt.conf file
    RESOURCES += \
        $$TRODES_REPO_DIR/Resources/qtconf.qrc

    # Remove any rpaths from our 3rd party libraries (since these 'pollute' the
    # library search path; e.g. libPythonQt typically has an rpath set to find Qt
    # libs at the compile location). Our libs will instead inherit the needed
    # search path from the executables. (Need to test for existence of chrpath first.)
    QtDeploy.commands += "command -v chrpath >/dev/null 2>&1 || { echo \"---> ---> \'chrpath\' command not found, install with \'apt-get install chrpath\'\" ; exit 1 ; } ;"
    QtDeploy.commands += "for lib in $${TRODES_LIB_INSTALL_DIR}/*.so*; do [ -f \$$lib ] || continue ; chrpath -d \$$lib; done ;"

    # message($$QtDeploy.commands)

    INSTALLS += QtDeploy


    # By default, Qmake includes the absolute path to the Qt libs used at
    # compile time, as an RPATH entry. Since we may be distributing this
    # software, we would like to suppress this, which we can do by unsetting the
    # QMAKE_RPATH variable:
    QMAKE_RPATH=

    # However this can break linking, since the linker uses -rpath entries to
    # resolve dependencies of shared libs. Fix it by passing the default libs
    # but only for linking purposes, (man ld, see '-rpath-link').
    QMAKE_LFLAGS += '-Wl,-rpath-link,$$[QT_INSTALL_LIBS]'

    # If all goes well (and you have the chrpath utility), you should see:
    #
    #   $ chrpath -l bin/linux/Trodes
    #   bin/linux/Trodes: RPATH=$ORIGIN/Libraries:$ORIGIN/Qt/lib
    #
    # (N.B. this could also be done on install using 'chrpath', but this tool
    # is not provided by default)

    # Let the user know that Trodes has been built to be used with a pre-deployed Qt:
    message('**Building $$TARGET to be used with a deployed Qt. Binaries may not work until you run \'make install\'**')


  }
}

# for debug builds, add "_d" extension to the destdir (not the target, since
# this complicates things with hard-coded executable names, like module
# loading)
CONFIG(debug, debug|release) {
  DESTDIR = $${DESTDIR}_d
}

# Store information about the which git commit the current instantiation is compiled from
# and store result in a preprocessor directive.
GIT_COMMAND = git --git-dir $$system_quote($$system_path($$TRODES_REPO_DIR/.git)) \
--work-tree $$system_quote($$system_path($$TRODES_REPO_DIR/)) describe --always --all \
--tags --dirty --long

#must not contain spaces
GIT_COMMIT = $$system($$GIT_COMMAND)
DEFINES += GIT_COMMIT=\\\"$$GIT_COMMIT\\\"

# Get date/time on which Qmake was run (usually compile time)
COMPILE_INFO = (Compiled using Qt version: $$QT_VERSION ($$[QT_INSTALL_PREFIX] on hostname: \'$$QMAKE_HOST.name\').)

# keep each project's build files in folders (useful when not doing shadow builds)
UI_DIR = $$_PRO_FILE_PWD_/build/ui
MOC_DIR = $$_PRO_FILE_PWD_/build/moc
OBJECTS_DIR = $$_PRO_FILE_PWD_/build/obj
RCC_DIR = $$_PRO_FILE_PWD_/build/rcc


# Add Qt libs directory to the @rpath list so that Mac apps will run 'out of the box'.
# NB: This is usually done by default, but if QMAKE_RPATHDIR is set elsewhere, then the Qt
# install dir is not added automatically, so we do it explicitly here.
macx:{
QMAKE_RPATHDIR += $$[QT_INSTALL_LIBS]
}

# Set up compiler warnings for gcc or MSVC
CONFIG += warn_on

!win32-msvc2015 {
    # For perf profiling
    QMAKE_CFLAGS += -fno-omit-frame-pointer
    QMAKE_CXXFLAGS += -fno-omit-frame-pointer

    # These flags block a bunch of warning messages but aren't present for msvc
    QMAKE_CFLAGS_WARN_ON += -Wno-unused-parameter
    QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter
} else {
    # MSVC equivalent
    QMAKE_CFLAGS_WARN_ON -= -w34100
    QMAKE_CXXFLAGS_WARN_ON -= -w34100
    QMAKE_CFLAGS += -wd4100 # Unreferenced formal parameter
    QMAKE_CXXFLAGS += -wd4100
}


# .pro file Debug messages (uncomment then run qmake to test)
#message('build_defaults.pri working directory: ' $$PWD)
#message('Working directory of the .pro file that includes us: ' $$_PRO_FILE_PWD_)
#message('Repo Git command: ' $$GIT_COMMAND)
#message('Repo Git Commit string: ' $$GIT_COMMIT)
#message($$_PRO_FILE_PWD_ $$DESTDIR)
#message($$UI_DIR)
#message($$TARGET)

