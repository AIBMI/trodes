#ifndef TRODESDATAINTERFACE_H
#define TRODESDATAINTERFACE_H

#include <QObject>
#include "trodesSocket.h"
#include <errno.h>

#ifndef WIN32
#define NATIVE_SOCKETS
#endif

#ifdef NATIVE_SOCKETS
#include <unistd.h>
#include <cstring>
#include <sys/socket.h> /* for socket(), connect(), send(), recv(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in, inet_addr(), and htons() */
#include <string.h>     /* for memset() */
#endif

class TrodesMessageInterface : public QObject
{
    Q_OBJECT
public:
    explicit TrodesMessageInterface(QString cfgFileName, QString srvAddr, QString srvPort, QObject *parent = 0);
    ~TrodesMessageInterface();

    TrodesModuleNetwork *trodesModuleInterface;
    QString configFileName;
    QString serverAddress;
    QString serverPort;

signals:
    void quitReceived();
    void statusMessage(QString);
    void newTimestamp(quint8 datatype, quint32 timestamp);
    void newContinuousMessage(QString);

public slots:
    void setup();
    void quitNow();

public:
    void Run();
    void timeRecevied(quint32);
    //void queryTimestamp();

};

class TrodesClientSocket : public QObject
{
    Q_OBJECT
public:
    explicit    TrodesClientSocket(quint8 dType, int uData, QTcpSocket *&sock, QObject *parent = 0);
    explicit    TrodesClientSocket(quint8 dType, int uData, QUdpSocket *&sock, QObject *parent = 0);
    explicit    TrodesClientSocket(quint8 dType, int uData, qint16 sockType, int sock, QObject *parent = 0);
    ~TrodesClientSocket();

    bool        waitForReadyRead(int msecs);

    qint16      socketType;
    QTcpSocket  *tcpSocket;
    QUdpSocket  *udpSocket;
    quint8      dataType;
    int         userData;

    int         nativeSock;
};



#ifdef NATIVE_SOCKETS

/*
   A TrodesNativeClient uses native socket calls for data communication

 */

class TrodesNativeClient : public QObject {
    Q_OBJECT

public:
    TrodesNativeClient(int sockType, QString hName, quint16 p, QObject *parent = 0);

    void connectToHost();
    bool isConnected();

    void setDataType(quint8 dataType, qint16 userData); //used to change the socket to a dedicated data line
    void setNTrodeId(int trodeId);
    void setNTrodeIndex(int index);
//    void setNTrodeChan(int trodeChan);
    void sendDecimationValue(quint16 dec);
    void turnOnDataStreaming();
    void turnOffDataStreaming();

    int getPort();
    QString getHostname();
    int getDescriptor();

private slots:

public slots:
    //Slots relating to socket operation
//    void disconnectFromHost();
//    void clientSocketError(QAbstractSocket::SocketError err);

signals:

private:
    int                 socketType; // TCPIP, UDP or LOCAL
    int                 sock;       // socket descriptor
    bool                connected;

    int                 socketDesc;  /* Socket descriptor */
    struct sockaddr_in  servAddr;    /* server address structure */
    QString             hostname;
    int                 port;

    int                 nTrodeId, nTrodeIndex;

    QAbstractSocket     *qSock;

    void sendMessage(quint8 dataType, QByteArray message);
    template<class T>
    void sendMessage(quint8 messageType, T message);

};

#endif



class TrodesDataInterface : public QObject
{
    Q_OBJECT
public:
    explicit TrodesDataInterface(QObject *parent = 0);
    ~TrodesDataInterface();

signals:
    void newTimestamp(quint8 datatype, quint32 timestamp);
    void newContinuousMessage(QString);
    void statusMessage(QString);

public slots:
    void connectDataClient(DataTypeSpec *da, int currentDataType);
    //void setupInterface(QUdpSocket *socket, qint16 userData)
//    void dataAvailableUpdated();
    void Run();

private:
    qint16                  socketType;
    QList<TrodesClientSocket*> socketList;
    //QList<QTcpSocket*>      tcpSocketList;
    //QList<QUdpSocket*>      udpSocketList;
    //QList<quint8>           dataTypeList;
    //QList<int>              userDataList;

    QByteArray dataBuffer;
    TrodesDataStream inStream;

    quint64 continuousDataBlockSize;
    quint64 headerSize;
    quint64 continuousBufferSize;
    quint64 blockContBufferSize;
    quint64 positionDataBufferSize;
    quint64 spikeDataBufferSize;
    quint64 dioDataBufferSize;

    quint64 minDataBufferSize;
    quint64 maxDataBufferSize;


    QVector<int16_t> blockData;

    QVector<int16_t> continuousData;
//    QVector<uint32_t> continuousTimestamps;
    QVector<long> continuousTimestamps;

private:
    TrodesClient *createClient(qint16 socketType, QString hostname, quint16 tcpPort, quint16 udpPort, int dataType, int userData);

#ifdef NATIVE_SOCKETS
    TrodesNativeClient *createNativeClient(qint16 socketType, QString hostname, quint16 tcpPort, quint16 udpPort, int dataType, int userData);
#endif


    void udpDataMessageReceived(TrodesClientSocket *client);
    void tcpDataMessageReceived(TrodesClientSocket *client);
#ifdef NATIVE_SOCKETS
    void nativeUdpDataMessageReceived(TrodesClientSocket *client);
    void nativeTcpDataMessageReceived(TrodesClientSocket *client);
#endif
    void processMessage(quint8 messageType, quint32 messageSize, TrodesDataStream &msgStream, int userData);


#ifdef NATIVE_SOCKETS
    int maxfd;
    fd_set              sockSet;     /* Set of socket descriptors for select() */
#endif

};


#endif // TRODESDATAINTERFACE_H
