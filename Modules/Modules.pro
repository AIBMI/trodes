TEMPLATE = subdirs

# Compiles faster if commented out. There are no actual dependencies
#CONFIG += ordered

SUBDIRS = cameraModule stateScript SimpleCommunicator SimpleMonitor/Source/SimpleMonitor.pro \
    workspaceGUI

# Add platform-specific modules below
macx {
SUBDIRS = cameraModule stateScript FSGui SimpleMonitor/Source/SimpleMonitor.pro
}
unix {
SUBDIRS += FSGui
}

win32 {
}
