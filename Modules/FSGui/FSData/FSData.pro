include(../../module_defaults.pri)

QT       += core

QT       -= gui

TARGET = FSData
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../../../Trodes/src-main/



SOURCES += \
    fsDataMain.cpp \
    fsSockets.cpp \
    fsProcessData.cpp \
    fsRTFilter.cpp

HEADERS += \
    fsDefines.h \
    stimControlGlobalVar.h \
    fsSocketDefines.h \
    ../../../Trodes/src-main/trodesSocketDefines.h \
    ../fsSharedStimControlDefines.h

