include(../module_defaults.pri)

QT       += core gui serialport network xml widgets

TARGET = stateScript
TEMPLATE = app

# Use Qt Resource System for images, etc
RESOURCES += \
    $$PWD/../../Resources/Images/buttons.qrc

#Directories that need to be copied to the install dir
MATLABSCRIPTDIR = $$PWD/matlabObserver/*
MATLABDESTDIR = $$DESTDIR/MatlabStateScriptObserver
win32:MATLABSCRIPTDIR ~= s,/,\\,g
win32:MATLABDESTDIR ~= s,/,\\,g
#mscripts.commands = $(MKDIR) $$MATLABDESTDIR $$COMMAND_SEP
#mscripts.commands += $(COPY) $$MATLABSCRIPTDIR $$MATLABDESTDIR
#QMAKE_EXTRA_TARGETS += mscripts
#POST_TARGETDEPS += mscripts

PYTHONSCRIPTDIR = $$PWD/pythonObserver/*
PYTHONDESTDIR = $$DESTDIR/PythonStateScriptObserver
win32:PYTHONSCRIPTDIR ~= s,/,\\,g
win32:PYTHONDESTDIR ~= s,/,\\,g
#pscripts.commands = $(MKDIR) $$PYTHONDESTDIR $$COMMAND_SEP
#pscripts.commands += $(COPY) $$PYTHONSCRIPTDIR $$PYTHONDESTDIR
#QMAKE_EXTRA_TARGETS += pscripts
#POST_TARGETDEPS += pscripts


#stuff specific for mac
macx {
INCLUDEPATH += ../../Trodes/Libraries/Mac
HEADERS    += ../../Trodes/Libraries/Mac/WinTypes.h
HEADERS    += ../../Trodes/Libraries/Mac/ftd2xx.h
LIBS       += /usr/local/lib/libftd2xx.dylib
ICON        = trodesMacIcon_ss.icns
OTHER_FILES += Info.plist
QMAKE_INFO_PLIST += Info.plist
}

unix:!macx {
INCLUDEPATH += ../../Trodes/Libraries/Linux
#INCLUDEPATH += /usr/local/include
LIBS += -L../../Trodes/Libraries/Linux #store ftdi lib here
HEADERS    += ../../Trodes/Libraries/Linux/WinTypes.h
HEADERS    += ../../Trodes/Libraries/Linux/ftd2xx.h

LIBS       += -lftd2xx
}

win32 {
RC_ICONS += trodesWindowsIcon_ss.ico
FTDX_LIBRARY_PATH = $${PWD}\..\..\Trodes\Libraries\Windows
INCLUDEPATH += $$PWD/../../Trodes/Libraries/Windows
HEADERS    += ftd2xx.h
#LIBS      += -L{PWD}\..\..\Trodes\Libraries\Windows
LIBS += -L$$FTDX_LIBRARY_PATH
LIBS      += -lftd2xx
#LIBS       += -L$$quote($$PWD../../Trodes/Libraries/Windows) -lftd2xx
}


INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config


SOURCES += ../../Trodes/src-display/sharedtrodesstyles.cpp
HEADERS += ../../Trodes/src-display/sharedtrodesstyles.h


SOURCES += main.cpp\
        mainwindow.cpp \
    console.cpp \
    scripteditor.cpp \
    highlighter.cpp \
    localcallbackwidget.cpp \
    dialogs.cpp \
    ../../Trodes/src-main/trodesSocket.cpp \
    ../../Trodes/src-config/configuration.cpp \
    ../../Trodes/src-main/trodesdatastructures.cpp \
    ../../Trodes/src-main/eventHandler.cpp \
    fileselectorwindow.cpp

HEADERS  += mainwindow.h \
    console.h \
    scripteditor.h \
    highlighter.h \
    localcallbackwidget.h \
    dialogs.h \
    ../../Trodes/src-main/trodesSocket.h \
    ../../Trodes/src-config/configuration.h \
    ../../Trodes/src-main/trodesdatastructures.h \
    ../../Trodes/src-main/eventHandler.h \
    fileselectorwindow.h

