#ifndef CUSTOMAPPLICATION_H
#define CUSTOMAPPLICATION_H

#include <QtGui>
#include "mainWindow.h"

class customApplication : public QApplication
{
Q_OBJECT
public:
    customApplication(int & argc, char **argv);
    virtual ~customApplication();
protected:
        bool event(QEvent *);
private:
        void loadFile(const QString &fileName);
        QSocketNotifier *pNot;

public:
MainWindow *win;

public slots:
    void stdinEvent();

};


/*
class StdinChecker : public QObject
{
Q_OBJECT
public:
    StdinChecker(QWidget *parent = 0);
    //virtual ~StdinChecker();

private:

   QSocketNotifier *pNot;
   QString currentLine;
   bool stopLoop;

public slots:
    void stdinEvent();

signals:
    void newLine(QString);

};*/

#endif // CUSTOMAPPLICATION_H
