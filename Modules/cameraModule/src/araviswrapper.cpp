/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "araviswrapper.h"

AravisWrapper* AravisWrapper::_instance = NULL;
AravisWrapper::AravisWrapper()
{
    camera_discovery();
	//_camera_list = new QVector<ArvCamera*>();
	//_stream_list = new QVector<ArvStream*>();
}

AravisWrapper* AravisWrapper::get_instance() {
	if(_instance == NULL) {
		_instance = new AravisWrapper();
	}
	return _instance;


}

AravisWrapper::~AravisWrapper() {
	//delete _camera_list;
	//delete _stream_list;
}

void AravisWrapper::camera_discovery() {


    arv_update_device_list();
    _n_devices = arv_get_n_devices();
    _device_names.resize(_n_devices);

    for (int i = 0; i < _n_devices; i ++) {
        _device_names[i] = arv_get_device_id(i);
        qDebug() << "Detected camera: " << _device_names[i];
    }
    qDebug() << _device_names.size() << endl;

}

GigECameraInterface* AravisWrapper::open_camera(int camera_num) {
	GigECameraInterface* gige_camera = NULL;
    if(_device_names.size() > camera_num) {
		qDebug() << "Opening camera number " << camera_num;
        gige_camera = open_camera(_device_names[camera_num]);
    }
	return gige_camera;
}

GigECameraInterface* AravisWrapper::open_camera(QString camera_name) {
	qDebug() << "Opening camera named " << camera_name;
    ArvCamera* camera = arv_camera_new(camera_name.toLocal8Bit().data());
    ArvStream* stream = arv_camera_create_stream(camera, NULL, NULL);
	return new GigECameraInterface(AravisWrapper::get_instance(), camera, stream);

}

GigECameraInterface::GigECameraInterface(AravisWrapper* arv_wrap, ArvCamera* camera, 
		ArvStream* stream) {
	_arv_ref = arv_wrap;
	_camera = camera;
	_stream = stream;
	_device = arv_camera_get_device(_camera);
	_gv_device = G_TYPE_CHECK_INSTANCE_CAST(_device,G_TYPE_OBJECT,ArvGvDevice);
	_configure_stream();
}

void GigECameraInterface::_configure_stream() {
	// Configure for Jumbo packets
	_set_packet_size(9000);
	arv_stream_set_emit_signals(_stream, TRUE);
	_payload = arv_camera_get_payload(_camera);
	qDebug() << "Payload: " << _payload;
	for ( int i; i < 50; i++) {
		arv_stream_push_buffer(_stream, arv_buffer_new(_payload, NULL));
	}
	
}

void GigECameraInterface::_set_packet_size(unsigned int packet_size) {
	arv_gv_device_set_packet_size(_gv_device, packet_size);
}	

//void GigECameraInterface::set_new_frame_callback(void (*callback)(ArvStream*, void*), void* data) {
void GigECameraInterface::set_new_frame_callback(void *callback, void* data) {
	_frame_callback = callback;
	_callback_data = data;
}

uchar* GigECameraInterface::get_current_frame() {
	uchar *buffer_data;
	uchar *buffer_data_ref;
	_buffer = arv_stream_pop_buffer(_stream);

	if(_buffer == NULL) {
		buffer_data == NULL;
	} else if (arv_buffer_get_status (_buffer) == ARV_BUFFER_STATUS_SUCCESS) {

		buffer_data= (uchar*) arv_buffer_get_data(_buffer, &buffer_size);
		//qDebug() << "Buffer Size: " << buffer_size;
		arv_buffer_get_image_region (_buffer, NULL, NULL, &width, &height);
		//qDebug() << "Image Region: " << width << " x " << height;
		arv_stream_push_buffer(_stream, arv_buffer_new(_payload, NULL));
	} else {
		buffer_data == NULL;
		// QByteArray buffer_data_qbyte(buffer_data, buffer_size);
		// //qDebug() << buffer_data_qbyte.toHex();
		// QFile file("./buffer_dump");
		// file.open(QIODevice::WriteOnly);
		// file.write(buffer_data_qbyte);
		// file.close();
	}
	return buffer_data;
}

int GigECameraInterface::get_current_height() {

	return height;
}


int GigECameraInterface::get_current_width() {
	return width;
}


void GigECameraInterface::start_aquisition() {
	g_signal_connect (_stream, "new-buffer", G_CALLBACK (_frame_callback), _callback_data);

	arv_camera_start_acquisition (_camera);
}

void GigECameraInterface::_new_buffer_callback(GigECameraInterface *camera) {
	qDebug() << "callback!";
	uchar* data = camera->get_current_frame();
	if(data != NULL) {
		int width = camera->get_current_width();
		int height = camera->get_current_height();
		qDebug() << width << " " << height; 
		emit camera->frame_ready(data, width, height);
		qDebug() << "Emitted";
	}
}
