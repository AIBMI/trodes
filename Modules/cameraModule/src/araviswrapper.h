/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ARAVISWRAPPER_H
#define ARAVISWRAPPER_H

#include <arv.h>
#include <string.h>
#include <iostream>
#include <stdio.h>

#include <glib-object.h>
#include <gtype.h>

#include <QString>
#include <QVector>
#include <QObject>
#include <QDebug>
#include <QCoreApplication>
#include <QBitmap>
#include <QSize>


class GigECameraInterface;

/**
 * A wrapper for the Aravis (Arv) open source genicam/GigE Vision 
 * drivers.
 *
 * A singleton class (not threadsafe) that wraps all C Aravis driver
 * calls.  Manages the creation of multiple cameras and video streams,
 * along with settings for each camera through the creation of
 * GigECameraInterfaces per camera.  Keep track of objects that
 * reference each individual camera, instead of executing global
 * C functions that require pointers to a particular camera and stream.
 */

class AravisWrapper 
{
public:
    static AravisWrapper* get_instance();
    void camera_discovery();
    GigECameraInterface* open_camera(int);
    GigECameraInterface* open_camera(QString);

private:
	static AravisWrapper *_instance;
    AravisWrapper();
    ~AravisWrapper();

	int _n_devices;
    QVector<QString> _device_names;
    ArvCamera *_camera;
    ArvStream *_stream;
	QVector<ArvCamera*> _camera_list;
	QVector<ArvStream*> _stream_list;
	AravisWrapper& operator=(AravisWrapper const&) {};
    //GigECameraInterface _create_gige_camera_interface()

};


class GigECameraInterface : public QObject
{
	Q_OBJECT
public:
	friend GigECameraInterface* AravisWrapper::open_camera(int num);
	friend GigECameraInterface* AravisWrapper::open_camera(QString name);
	uchar* get_current_frame();
	//void set_new_frame_callback(void (*)(ArvStream*,void*), void*);
	void set_new_frame_callback(void *, void*);
	int get_current_width();
	int get_current_height();
	void start_aquisition();

signals:
	void frame_ready(uchar* data, int width, int height);

private:
	int height;
	int width;
	size_t buffer_size;
    GigECameraInterface(AravisWrapper*, ArvCamera*, ArvStream*);
	//void (*_frame_callback)(ArvStream*, void*);
	void *_frame_callback;
	void* _callback_data;
	AravisWrapper* _arv_ref;
	ArvCamera* _camera;
	ArvStream* _stream;
	ArvDevice* _device;
	ArvGvDevice* _gv_device; //Pointer to GigE specific implementation
	ArvBuffer* _buffer;
	unsigned int _payload;
	void _configure_stream();
	void _set_packet_size(unsigned int);
	void _new_buffer_callback(GigECameraInterface *);

};

#endif // ARAVISWRAPPER_H
