/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "recordThread.h"
#include "globalObjects.h"


//Used to record data to disk.  Run as a separate thread.

RecordThread::RecordThread(QObject *parent)
    : QThread(parent)
    , fileOpen(false)
    , recording(false)
    , bytesWritten(0)  {

    pullTimer = new QTimer(this);
    connect(pullTimer, SIGNAL(timeout()), SLOT(pullTimerExpired()));
    pullTimer->start(100);
    saveMarker = 0;
    QObject::moveToThread(this);
    start();
}

RecordThread::~RecordThread() {

}

void RecordThread::run()
{
    exec();
}

int RecordThread::openFile(QString fileName) {

    if (!fileOpen) {

        file = new QFile;
        file->setFileName(fileName);
        if (file->exists()) {
            return -1;
        }
        writeTrodesConfig(fileName); //write the current configuration and settings to disk

        if (!file->open(QIODevice::Append)) {
            return -2;
        }

        bytesWritten = 0;
        outStream = new QDataStream(file); //link outStream to the file
        file->flush();
        fileOpen = true;

    }

    return 0;
}

void RecordThread::closeFile() {

    delete outStream;
    delete file;
    fileOpen = false;
}

void RecordThread::startRecord() {

    saveMarker = rawDataWritten;
    bufferLocation = rawData.writeIdx;
    recording = true;
    errorSignalEmitted = false;
}

void RecordThread::pauseRecord() {

    recording = false;
}

void RecordThread::setupSaveDisplayedChan() {

    if (globalConf->saveDisplayedChanOnly) {
        dataArray = new short[streamConf->nChanConfigured];
    }

}

void RecordThread::pullTimerExpired() {

    if (recording) {
       int packetsToSave = rawDataWritten-saveMarker;
       char *charPtr;
       while (packetsToSave > 0) {

           //Write the time stamp of the packet
           charPtr = (char*)(rawData.timestamps+bufferLocation);
           bytesWritten += outStream->writeRawData(charPtr,4);

           //Write the phase stamp of the packet
           charPtr = (char*)(rawData.data_f_ph+bufferLocation);
           bytesWritten += outStream->writeRawData(charPtr,2);

           //Write the fiber of the packet
           charPtr = (char*)(rawData.data_f+(bufferLocation*hardwareConf->NFIBER));
           bytesWritten += outStream->writeRawData(charPtr,hardwareConf->NFIBER*8);

           packetsToSave--;
           bufferLocation = (bufferLocation+1) % EEG_BUFFER_SIZE;
           saveMarker++;
       }
       file->flush();
    }
}

quint64 RecordThread::getBytesWritten() {
    return bytesWritten;
}

qint64 RecordThread::getBytesFree() {

    qint64 bytesfree = -1;
    if (fileOpen) {
        QStorageInfo driveInfo;
        QFileInfo fi(file->fileName());
        driveInfo.setPath(fi.absolutePath());
        bytesfree = driveInfo.bytesFree();
    }
    return bytesfree;

}

void RecordThread::endRecordThread() {
    pullTimer->stop();
    quit();
}
