#ifndef _SHARED_FUNCTIONS_H
#define _SHARED_FUNCTIONS_H

int TriggerOutput(int output);
int TriggerOutputs(int output);
int TriggerOutputs(int *bit, int *length, int *delay, int *percent,
	int n);
void ChangeOutput(int output, int raise);
void SendDigIOFSMessage(char *message, int len);

#endif

