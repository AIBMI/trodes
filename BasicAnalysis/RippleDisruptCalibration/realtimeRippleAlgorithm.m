function [threshtmp, v, m, s, lockout] = realtimeRippleAlgorithm(yt, rippleData, thresh, nostim_mask, peak_window)

% a = [1, -7.45735663369528, 28.7250866952453, -76.6576905574011, ...
%     159.121219449125, -271.335670775639, 391.351587611873, -485.982406239585, ...
%     525.205137459352, -496.514905259400, 411.014947844139, -297.145795944811, ...
%     186.329835613820, -100.094136315397, 45.1637435607148, -16.5828393019837, ...
%     4.68977600702455, -0.917188209533659, 0.0946362166342639];
% 
% b = [0.0232855416422778, -0.116923985094050, 0.267272526953982, -0.433074915275246, ...
%     0.637527665723657, -0.793642910169906, 0.752702952156722, -0.600794056710678, ...
%     0.383928961384643, 4.38452370247235e-15, -0.383928961384650, 0.600794056710682, ...
%     -0.752702952156724, 0.793642910169908, -0.637527665723659, 0.433074915275249, ...
%     -0.267272526953984, 0.116923985094051, -0.0232855416422780];
% 
% newa = fliplr(a(2:end));
% newb = fliplr(b);
% 
% % note that zero padding is essential
% %tmpdata = [zeros(100,1); double(e.data(1:100000))];
% tmpdata = [zeros(100,1); double(lfpData)];
% 
% l = length(b);
% y = zeros(size(tmpdata));
% for i = l:length(tmpdata)
%     y(i) = dot(newb, tmpdata(i-18:i)) - dot(newa, y(i-18:i-1));
% end
% 
% % remove the zero padding
% y = y(101:end);

y = rippleData;

lastval = zeros(peak_window,size(y,2));

tmpy = abs(y);
v = zeros(size(y));

m = zeros(size(y));
s = zeros(size(y));

lockout = zeros(size(y));
%m = zeros(1,size(y,2));
%s = zeros(1,size(y,2));

rt = [];
i = 2;
while (i <= size(y,1))
    dm = tmpy(i,:) - m(i-1,:);
    m(i,:) = m(i-1,:) + dm * 0.0001;
    ds = abs(tmpy(i,:) - m(i,:)) - s(i-1,:);
    s(i,:) = s(i-1,:) + ds * 0.0001;

    
    posgain = mean(lastval);
    lastval(1:peak_window-1,:) = lastval(2:peak_window,:);
    df = tmpy(i,:) - v(i-1,:);
    %	threshtmp2 = min(m{t}(i) + 2 * s{t}(i), 45);
    %	if (tmpy(i) > threshtmp)
    if (df > 0)
        gain = 1.2;
        v(i,:) = v(i-1,:) + df .* posgain;
    else
        gain = 0.2;
        v(i,:) = v(i-1,:) + df .* gain;
    end
    lastval(peak_window,:) = gain;
    
    %threshtmp = m(i,:) + thresh .* s(i,:);
    threshtmp = m(20000,:) + thresh .* s(20000,:);

        % lock in mean and std in 10000 samples
%     if(( i < 10000))
%         dm = tmpy(i,:) - m;
%         m = m + dm * 0.001;
%         ds = abs(tmpy(i,:) - m) - s;
%         s = s + ds * 0.001;
%         threshtmp = m + thresh .* s;
%         
    if ((i > 20000) && (sum(v(i,:) > threshtmp) > 2) && ~nostim_mask(i))
        rt = [rt yt(i,:)];
        tmpm = m(i,:);
        tmps = s(i,:);
        tmpy(i:i+374,:) = 0;
        lockout(i:i+374,:) = 1;
        i = i + 374;
        m(i,:) = tmpm;
        s(i,:) = tmps;
    end
    i = i + 1;
end
threshtmp = m(end,:) + thresh .* s(end,:);
sprintf('thresh = %d\n', threshtmp)
lockout = lockout(1:size(y,1),:);
m = m(1:size(y,1),:);
s = s(1:size(y,1),:);

end

