#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    setDefaultMenuEnabled(false); //the default options are turned off
    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to merge data from an SD card recording with simultaneuous Trodes recording. Output file is saved is same directory.\n");
    printf("Usage:  mergesdrecording -rec TRODESINPUTFILENAME -sd SDINPUTFILENAME -numchan NUMBEROFCHANNELS -mergeconf MERGEFILEWORKSPACE\n\n");
    //printf("Defaults:\n -outputrate -1 (full) \n -usespikefilters 1 \n -interp -1 (inf) \n -userefs 1 \n");

    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;

    QString sd_string = "";
    QString merge_workspace_string = "";
    QString numchan_string = "";


    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        } else if ((argumentList.at(optionInd).compare("-sd",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            sd_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-mergeconf",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            merge_workspace_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-numchan",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            numchan_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }

    if (sd_string.isEmpty()) {
        qDebug() << "No SD filename given.  Must include -sd <filename> in arguments.";
        argumentReadOk = false;
        return;
    } else {
        //Make sure the first file exists
        sdFileName = sd_string;

        QFileInfo fi(sdFileName);

        if (!fi.exists()) {
            qDebug() << "File could not be found: " << sdFileName;
            argumentReadOk = false;
        }

    }

    if (merge_workspace_string.isEmpty()) {
        qDebug() << "No merge workspace filename given.  Must include -mergeconf <filename> in arguments.";
        argumentReadOk = false;
        return;
    } else {
        //Make sure the first file exists
        mergedConfFileName = merge_workspace_string;

        QFileInfo fi(mergedConfFileName);

        if (!fi.exists()) {
            qDebug() << "File could not be found: " << mergedConfFileName;
            argumentReadOk = false;
        }

    }

    if (numchan_string.isEmpty()) {
        qDebug() << "Error. Must include -numchan NUMBEROFCHANNELS in arguments.";
        argumentReadOk = false;
        return;
    } else {
        bool ok;

        SDRecNumChan = numchan_string.toInt(&ok);
        if (!ok) {
            qDebug() << "Error. Must include -numchan NUMBEROFCHANNELS in arguments. NUMBEROFCHANNELS must be an integer multiple of 32.";
            argumentReadOk = false;
            return;
        }
        /*
        Below is the packet format for SD card recording.  Right now, we are hardcoding this format, but we may eventually want to use a config file.
        The total packet size is 14 bytes + 2*SDRecNumChan


        x55 (Sync byte)
        x68 (Data format byte - Note: MCU will replace this with Digital inputs)
        1 byte with valid flags (bit3 = acc_valid, bit2 = gyro_valid, bit1 = mag_valid, bit0 = rf_valid) (Note: at most one bit set)
        x00 (filler to make the packet byte count even)
        i2c_data_x[7:0]   (or rf_timestamp[7:0] if rf_valid)
        i2c_data_x[15:8]  (or rf_timestamp[15:8] if rf_valid)
        i2c_data_y[7:0]   (or rf_timestamp[23:16] if rf_valid)
        i2c_data_y[15:8]  (or rf_timestamp[31:24] if rf_valid)
        i2c_data_z[7:0]   (or x00 if rf_valid)
        i2c_data_z[15:8]  (or x00 if rf_valid)
        timestamp[7:0]
        timestamp[15:8]
        timestamp[23:16]
        timestamp[31:24]
        for (ch = 0, ch < 32, ch++) {
          sample0[7:0]
          sample0[15:8]
          sample1[7:0]
          sample1[15:8]
          sample2[7:0]
          sample2[15:8]
          sample3[7:0]
          sample3[15:8]
        }

        Note: i2c_data[x,y,z] = 0 if no valid flag set.
        */

        sdFilePacketSize = 2*SDRecNumChan + 14;
        sdFilePacketStartDataLoc = 14;
        sdFilePacketFlagByteLoc = 2;
        sdFilePacketSyncByteLoc = 4;
        sdFilePacketTimeByteLoc = 10;

    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "\nMerging files...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    createFilters();
    readMergeConfig();

    if (!openSDInputFile()) {
        return -1;
    }

    if (!openInputFile()) {
        return -1;
    }


    QFileInfo fi(recFileName);
    QString fileBaseName;
    fileBaseName = fi.completeBaseName()+"_merge";

    QString saveLocation = fi.absolutePath()+QString(QDir::separator());

    QList<QFile*> neuroFilePtrs;



    //Create an output file for the merged data
    //*****************************************

    neuroFilePtrs.push_back(new QFile);
    neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".rec"));
    if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
    neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    QFile finalConfFile;



    finalConfFile.setFileName(mergedConfFileName);
    if (!finalConfFile.open(QIODevice::ReadOnly)) {
        return -1;
    }

    QString configLine;
    bool foundEndOfConfig = false;
    int endPos = 0;
    int lastFCPos = 0;

    //Read in the configuratuion file that will be appended to the top of the merged file
    while (!finalConfFile.atEnd()) {
        configLine += finalConfFile.readLine();

        if (configLine.indexOf("</Configuration>") > -1) {
            foundEndOfConfig = true;
            endPos = finalConfFile.pos();
            break;
        } else {
            lastFCPos = finalConfFile.pos();
        }
        configLine = "";
    }

    if (!foundEndOfConfig) {
        qDebug() << "Error in reading mergeconf file.";
        return -1;
    }
    finalConfFile.seek(0);

    //Write the config section to the top of the merged data file
    neuroFilePtrs.last()->write(finalConfFile.read(endPos+1));

    finalConfFile.close();
    neuroFilePtrs.last()->flush();

    //************************************************

    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;


            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }
        }



        if (!(sdSyncValues.length() > 2)) {
            qDebug() << "Error: could not find at least two sync stamps in the SD recording file.";
            return -1;
        }



        bool trodesRFChannelIndFound = false;

        //There needs to be a device entry called 'RF'. We will use this data to synchronize the two files
        for (int deviceInd = 0; deviceInd < hardwareConf->devices.length(); deviceInd++) {
            for (int devChInd = 0; devChInd < hardwareConf->devices[deviceInd].channels.length(); devChInd++) {

                if (hardwareConf->devices[deviceInd].name == "RF" && hardwareConf->devices[deviceInd].channels[devChInd].idString == "RFsync")  {
                    trodesRFPacketByte = hardwareConf->devices[deviceInd].channels[devChInd].startByte;
                    trodesRFChannelIndFound = true;
                    break;

                }
            }
        }

        //Make sure the RF sync channel is defined in the .rec file workspace
        if (!trodesRFChannelIndFound) {
            qDebug() << "Error: could not find the RF channel in the .rec file.  Workspace must contain a device named RF with a channel named RFsync.";
            return -1;
        }

        //Compile a list of the devices from which to write data from
        deviceListToUse.clear();
        for (int i=0;i < sortedDeviceList.length();i++) {
            if (!(hardwareConf->devices[i].name == "RF") ) {
                deviceListToUse.push_back(i);
            }
        }

        bool firstSyncMatchFound = false; //We don't start saving data until we find the first sync stamp in the rec file matching the first sync stamp in the SD file
        int syncOffsetTime;
        int syncOffsetPackets;
        int currentSyncInd = 0; //used to keep track of which sync stamp we are on
        uint32_t packetsSinceLastRecSync = 0;

        //uint32_t currentSDSyncStamp = sdSyncValues[0];

        int packLengthAdjustmentofSD = 0;

        currentRecPacket = 0;
        currentSDPacket = 0;

        int numSyncsFound = 0;



        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {


            if (!readNextRecPacket()) {
                //If we have reached the end of the rec file, we are done
                qDebug() << "Rec file ended first.";
                break;
            }

            if (firstSyncMatchFound) {
                //The steps in this block are only done once a sync match has been found in both files in order to register them

                if (!readNextSDPacket()) {
                    //If we have reached the end of the SD file, we are done

                    qDebug() << "SD file ended first.";
                    break;
                }

                int jumpInRecTimeStamps = (currentTimeStamp-lastTimeStamp); // (1 if no data missing)
                int jumpInSDTimeStamps = (currentSDTimeStamp-lastSDTimeStamp);

                if (jumpInRecTimeStamps > 1) {
                    qDebug() << "Missing" << jumpInRecTimeStamps-1 << "rec file samples. Filling in with repeat data." ;

                    int fillPackets = jumpInRecTimeStamps;
                    while (fillPackets > jumpInSDTimeStamps) {

                        writeMergedPacket();
                        readNextSDPacket();
                        currentSDPacket++;
                        currentRecPacket++;
                        packetsSinceLastRecSync++;
                        currentTimeStamp++;
                        fillPackets--;
                    }
                }

                if (jumpInSDTimeStamps > 1) {
                    //There are missing packets in the SD recording.  To deal with this, we drop the same number of packets from the rec file.


                    int numberOfRecPacketsToDrop = jumpInSDTimeStamps-jumpInRecTimeStamps;
                    qDebug() << "SD recording has " << jumpInSDTimeStamps-1 << "packets dropped at timestamp" << lastSDTimeStamp;

                while (numberOfRecPacketsToDrop > 1) {
                    readNextRecPacket();
                    numberOfRecPacketsToDrop--;
                }

                }
            }



            if (currentRecSyncStamp > 0) {
                //A sync stamp is found in the rec file

                uint32_t sVal = currentRecSyncStamp & 0xFFFFFF; //The sync stamp is a 24 bit value
                //qDebug() << " Rec file sync: " << sVal;
                if (!firstSyncMatchFound) {

                    //The two files have not been registered yet, so here is where we first register the two files if we find a sync stamp match. Afterward, we rewind both files the same amount.
                    bool filesWereRewound = false;
                    for (int s = 0; s < sdSyncValues.length(); s++) {
                        if (sVal == sdSyncValues[s]) {
                            firstSyncMatchFound = true;
                            syncOffsetTime = (int64_t)currentTimeStamp - sdTimeDuringSync[s];
                            qDebug() << "Time lag between files: " << syncOffsetTime << "samples";


                            if (sdPacketDuringSync[s] >= currentRecPacket) {
                                //The SD recording started before the rec recording (usually the case)
                                //Seek to the beginning of the rec file and to the corresponding position in the SD file

                                syncOffsetPackets = currentRecPacket-sdPacketDuringSync[s];

                                sdFilePtr->seek((sdPacketDuringSync[s]-currentRecPacket)*sdFilePacketSize);
                                currentSDPacket = sdPacketDuringSync[s]-currentRecPacket;

                                filePtr->seek(filePtr->pos()-((currentRecPacket+1)*filePacketSize));
                                currentRecPacket = 0;


                            } else if (sdPacketDuringSync[s] < currentRecPacket) {
                                //The rec recording started before the SD recording

                                syncOffsetPackets = currentRecPacket-sdPacketDuringSync[s];
                                sdFilePtr->seek(0);
                                filePtr->seek(dataStartLocBytes + (currentRecPacket-sdPacketDuringSync[s])*filePacketSize);

                                currentRecPacket = currentRecPacket-sdPacketDuringSync[s];
                                currentSDPacket = 0;
                            }


                            currentSyncInd = s;
                            readNextRecPacket();
                            readNextSDPacket();
                            //lastTimeStamp = currentTimeStamp;
                            //lastSDTimeStamp = currentSDTimeStamp;
                            filesWereRewound = true;
                            break;

                        }
                    }

                    continue; //skip the rest of this loop

                    /*if (filesWereRewound) {


                        continue; //skip the rest of this loop
                    }*/

                }

                //If we made it here, we have reached a rec file sync stamp after writing to file has started
                //Now we need to check if there is a sync in the sd file at the same relative location

                for (int s = 0; s < sdSyncValues.length(); s++) {
                    if (sdSyncValues[s] == sVal) {
                        //A match sync was found in the sd recording
                        //qDebug() << "Sync from rec: " << currentRecPacket;
                        currentSyncInd = s;
                        numSyncsFound++;
                        packetsSinceLastRecSync = 0;
                        if (!(sdPacketDuringSync[s] == (currentRecPacket-syncOffsetPackets)+packLengthAdjustmentofSD)) {
                            //The sync in the SD card file is not in the expected place.  Either drift or dropped packets would cause this.

                            int numberOfPacketsOff = sdPacketDuringSync[s] - ((currentRecPacket-syncOffsetPackets)+packLengthAdjustmentofSD);

                            if (numberOfPacketsOff > 0) {
                                //Rec file is coming in slower than sd data, so we copy this rec file packet while
                                //reading in the drifted sd card packets until the drift is corrected.
                                if (numberOfPacketsOff > 0) {
                                    qDebug() << "SD file behind by" << numberOfPacketsOff << "samples at sync. Timestamp" << currentTimeStamp << "Adjusting.";
                                    //return -1;
                                }
                                while (numberOfPacketsOff > 0) {
                                    if (!readNextSDPacket()) break;
                                    currentSDPacket++;
                                    currentTimeStamp++;
                                    numberOfPacketsOff--;
                                    writeMergedPacket();

                                }



                                //qDebug() << "Rec file behind by" << numberOfPacketsOff;


                            } else {
                                //We deal with the reverse condition below.
                                //qDebug() << "Error: Can't figure out sync order.";
                            }
                        } else {

                            //currentSyncInd = s;
                        }
                        break;
                    }
                }


            } else {
                //No sync signal found in rec file. Check if a sync signal was found in the SD file.


                if ((firstSyncMatchFound) && ((currentSyncInd+1) <  sdPacketDuringSync.length()) && (sdPacketDuringSync[currentSyncInd+1] == currentSDPacket)) {
                    //The next SD sync signal is encountered before the next rec file sync signal,
                    //so the SD data is coming in slower than the rec data, so we skip over rec file packets until the drift is corrected.


                    //qDebug() << "Found lone SD sync at sync " << sdSyncValues[currentSyncInd+1] << currentRecPacket << currentSDPacket;
                    //qDebug() << "Sync from SD" << currentRecPacket;


                    currentSyncInd++;
                    numSyncsFound++;

                    bool foundNextRecSync = false;
                    int numStepsJumped = 0;


                    //Skip over the rec (dio, etc) data samples until we reach the same sync signal.
                    while (!foundNextRecSync) {
                        //Read in a packet of data to make sure everything looks good

                        if (!readNextRecPacket()) {
                            break;
                        }
                        currentRecPacket++;
                        uint32_t sVal = currentRecSyncStamp & 0xFFFFFF; //The sync stamp is a 24 bit value

                        if (sVal == sdSyncValues[currentSyncInd]) {
                            foundNextRecSync = true;
                            //qDebug() << " Rec file sync: " << sVal;
                            //qDebug() << "Sync from SD ahead by" << numStepsJumped << "Adjusting drift.";


                        } else {
                            numStepsJumped++;
                        }


                    }
                    if (numStepsJumped > 10) {
                        qDebug() << "Rec file behind by" << numStepsJumped << "samples at sync. Timestamp" << currentTimeStamp << "Adjusting.";
                        //return -1;
                    }


                }
            }

            if (firstSyncMatchFound) {

                writeMergedPacket();


                packetsSinceLastRecSync++;

                //Print the progress to stdout

                //printProgress();
                //Print progress (based on location in SD input file)
                if ((sdFilePtr->pos()%progressMarker) < lastProgressMod) {
                    //printf("\r%d%%",currentProgressMarker);
                    //fflush(stdout);
                    qDebug() << currentProgressMarker << "%";
                    currentProgressMarker = 100.0*((double)sdFilePtr->pos()/(double)sdFilePtr->size());
                    //currentProgressMarker+=10;
                }
                lastProgressMod = (sdFilePtr->pos()%progressMarker);

                pointsSinceLastLog = (pointsSinceLastLog+1)%decimation;
            }

            currentRecPacket++;
            currentSDPacket++;
            //lastTimeStamp = currentTimeStamp;
            //lastSDTimeStamp = currentSDTimeStamp;




        }
        qDebug() << "Final timestamp:" << currentTimeStamp;

        qDebug() << "\nNumber of sync points found in both files: " << numSyncsFound;
        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }



    return 0;
}

bool DATExportHandler::readNextRecPacket() {
    //Read in a packet of data to make sure everything looks good
    if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
        //We have reached the end of the rec file
        return false;
    }

    lastTimeStamp = currentTimeStamp;

    //Find the time stamp from rec file
    bufferPtr = buffer.data()+packetTimeLocation;
    tPtr = (uint32_t *)(bufferPtr);
    currentTimeStamp = *tPtr+startOffsetTime;

    bufferPtr = buffer.data() + trodesRFPacketByte;
    tPtr = (uint32_t *)(bufferPtr);
    currentRecSyncStamp = *tPtr;

    return true;

}

bool DATExportHandler::readNextSDPacket() {

    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        //We have reached the end of the sd file
        return false;
    }

    lastSDTimeStamp = currentSDTimeStamp;
    //Find the time stamp from sd file
    char *sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
    uint32_t *sdTimePtr = (uint32_t *)(sdBufferPtr);
    currentSDTimeStamp = *sdTimePtr;

    return true;

}

bool DATExportHandler::writeMergedPacket() {


    if (currentTimeStamp <= lastTimeStamp) {
        qDebug() << "Skipping non-increasing timestmap packet at time" << currentSDTimeStamp << "This is ok if at end of the merge.";
        return false;
    }

    neuroStreamPtrs.at(0)->writeRawData(buffer.data(),1); //We always have the the sync byte

    //Write data from approved devices
    for (int i=0; i < deviceListToUse.length(); i++) {
        neuroStreamPtrs.at(0)->writeRawData(buffer.data()+hardwareConf->devices[deviceListToUse[i]].byteOffset,hardwareConf->devices[deviceListToUse[i]].numBytes);

    }

    neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+2,8); //Sensor data


    *neuroStreamPtrs.at(0) << currentTimeStamp; //Write the timestamp from the trodes file


    //Write the neural data from the sd card
    char *dataStartLoc = sdFilePacketBuffer.data()+sdFilePacketStartDataLoc;
    for (int i=0; i < writeSDChannel.length();i++) {
        if (writeSDChannel.at(i)) {
            neuroStreamPtrs.at(0)->writeRawData(dataStartLoc+(2*i),2);
        }
    }
    //neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+sdFilePacketStartDataLoc,(SDRecNumChan*2));




    return true;
}


bool DATExportHandler::openSDInputFile() {
    sdFilePtr = new QFile;
    sdFilePtr->setFileName(sdFileName);

    //open the raw data file
    if (!sdFilePtr->open(QIODevice::ReadOnly)) {
        delete sdFilePtr;
        qDebug() << "Error: could not open sd recording file for reading.";
        return false;
    }

    sdFilePacketBuffer.resize(sdFilePacketSize*2); //we make the input buffer the size of two packets
    //dataLastTimePoint.resize(channelPacketLocations.size()); //Stores the last data point.
    //dataLastTimePoint.fill(0);
    //pointsSinceLastLog=-1;

    //----------------------------------

    //skip past the config header
    /*
    if (!filePtr->seek(dataStartLocBytes)) {
        delete filePtr;
        qDebug() << "Error seeking in file";
        return false;
    }*/


    char *sdBufferPtr;
    uint32_t *sdTimePtr;


    //Read in a packet of data to make sure everything looks good
    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        delete sdFilePtr;
        qDebug() << "Error: could not read from SD card recording file";
        return false;
    }

    qDebug() << "Finding sync packets in SD file...";

    //Seek back to begining of data
    sdFilePtr->seek(0);

    qint64 currentSDPacket = 0;
    //Find all of the sync stamps


    uint32_t ltime=0;
    uint32_t thistime=0;

    uint32_t lastSyncVal;

    while (!sdFilePtr->atEnd()) {
        //Seek to flag location of data packet
        if (!sdFilePtr->seek((currentSDPacket*sdFilePacketSize))) {
            break;
        }
        if (sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketStartDataLoc) == sdFilePacketStartDataLoc) {

            //Report dropped packets

            sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);
            thistime = *sdTimePtr;
            /*
            if (thistime < ltime) {
                qDebug() << "-" << ltime-thistime << "gap in data. Last time" << ltime << "Current time" << thistime << "Packet" << currentSDPacket;
            }
            else if (thistime-ltime > 1) {
                qDebug() << thistime-ltime << "gap in data at time. Last time" << ltime << "Current time" << thistime << "Packet" << currentSDPacket;
            }*/
            ltime = thistime;


            sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketFlagByteLoc;

            if (*sdFilePacketBuffer.data() != 0x55) {
                qDebug() << "Bad SD file packet found.";

                if (currentSDPacket > 0) {
                    sdFilePtr->seek(((currentSDPacket-1)*sdFilePacketSize));
                    qDebug() << "Packet size" << sdFilePacketSize;
                    char syncByteCheck;
                    for (int byteScan=0; byteScan < sdFilePacketSize*2; byteScan++) {
                        sdFilePtr->read(&syncByteCheck,1);
                        if (syncByteCheck == 0x55) {
                            qDebug() << "Sync at" << byteScan;
                        }
                    }


                }
                return false;


            }
            if (*sdBufferPtr == 1) {
                //The 0th bit is set in the flag byte, we have a sync value




                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketSyncByteLoc);
                uint32_t sVal = *sdTimePtr;
                sVal = sVal & 0xFFFFFF;
                //qDebug() << "SD sync: " << sVal;




                if (sdSyncValues.length() > 1) {
                    if (!((sVal == lastSyncVal+1) || (sVal == lastSyncVal+2))) {
                        //qDebug() << "Warning: skipped or non-increasing sync values in SD file found. Ignoring value." << sVal << lastSyncVal;
                        lastSyncVal = sVal;
                        currentSDPacket++;
                        continue;
                    }
                }

                sdSyncValues.push_back(sVal);
                lastSyncVal = sVal;

                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);
                sdTimeDuringSync.push_back(*sdTimePtr);
                sdPacketDuringSync.push_back(currentSDPacket);
            }

        } else {
            //qDebug() << "Could not read from file.";

        }
        currentSDPacket++;

        //sdFilePtr->

    }
    qDebug() << sdSyncValues.length() << "sync values found on SD file.";
    //qDebug() << sdPacketDuringSync;


    currentSDTimeStamp = 0;
    lastSDTimeStamp = 0;

    //Find first time stamp
    //sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
    //sdTimePtr = (uint32_t *)(sdBufferPtr);
    //currentSDTimeStamp = *sdTimePtr;
    //lastSDTimeStamp = currentSDTimeStamp-1;

    //Seek back to begining of data
    sdFilePtr->seek(0);


    return true;

}

int DATExportHandler::readMergeConfig() {
    //Read the config .xml file

    QString configFileName = mergedConfFileName;
    //int filePos = 0;
    //startOffsetTime = 0;



    QFileInfo fI(configFileName);

    QDomDocument doc("TrodesConf");

    QFile file;
    file.setFileName(fI.absoluteFilePath());
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << QString("Merge Rec file %1 not found").arg(fI.absoluteFilePath());
        return -1;
    }

    if (!doc.setContent(&file)) {
        file.close();
        qDebug("XML didn't read properly in merge workspace file.");
        return -1;
    }

    QDomElement root = doc.documentElement();
    if (root.tagName() != "Configuration") {
        qDebug("Configuration not root node.");
        return -1;
    }

    //nTrodeTable = new NTrodeTable();

    // create the global configuration object and recreate the hardwareConf if it exists.
    GlobalConfiguration *mergeGlobalConf = new GlobalConfiguration(NULL);
    QDomNodeList globalConfigList = root.elementsByTagName("GlobalConfiguration");
    if (globalConfigList.length() > 1) {
        qDebug() << "Config file error: multiple GlobalConfiguration sections found in merge workspace file.";
        return -7;
    }
    QDomNode globalnode = globalConfigList.item(0);
    int globalErrorCode = mergeGlobalConf->loadFromXML(globalnode);
    if (globalErrorCode != 0) {
        qDebug() << "configuration: nsParseTrodesConfig(): Failed to load global configuration err: " << globalErrorCode;
        return globalErrorCode;
    }

    hardwareConf->NCHAN = SDRecNumChan;

    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    // PARSE SPIKE CONFIGURATION
    QDomNodeList list = root.elementsByTagName("SpikeConfiguration");
    if (list.isEmpty() || list.length() > 1) {
        qDebug() << "Config file error: either no or multiple SpikeConfiguration sections found in merge workspace file.";
        return -5;
    }
    QDomNode spikenode = list.item(0);
    int autoNtrodeType = 0;
    autoNtrodeType = spikenode.toElement().attribute("autoPopulate", "0").toInt();

    SpikeConfiguration *mergeSpikeConf;
    if (autoNtrodeType > 0) {
        qDebug() << "Using auto population of nTrodes";
        int numTrodes = SDRecNumChan/autoNtrodeType;
        mergeSpikeConf = new SpikeConfiguration(NULL, numTrodes);
    } else {
        mergeSpikeConf = new SpikeConfiguration(NULL, spikenode.childNodes().length());
    }

    int spikeErrorCode = mergeSpikeConf->loadFromXML(spikenode);

    if (spikeErrorCode != 0) {
        return spikeErrorCode;
    }




    //If saveDisplayedChanOnly is set, we should only write the channels from the SD card that are configured in the merge workspace file.
    writeSDChannel.resize(SDRecNumChan);
    if (mergeGlobalConf->saveDisplayedChanOnly) {
        writeSDChannel.fill(false);
    } else {
        writeSDChannel.fill(true);
    }
    QList<int> sortedChannelList;
    //Gather all HW channels
    for (int n = 0; n < mergeSpikeConf->ntrodes.length(); n++) {



        for (int c = 0; c < mergeSpikeConf->ntrodes[n]->hw_chan.length(); c++) {
            bool channelalreadyused = false;

            int tempHWRead = mergeSpikeConf->ntrodes[n]->hw_chan[c];
            if (tempHWRead > SDRecNumChan) {
                return -1;
            }
            //make sure there is not a repeat channel
            for (int ch=0;ch<sortedChannelList.length();ch++) {
                if (sortedChannelList[ch]==tempHWRead) {
                    channelalreadyused = true;
                }
            }
            //qDebug() << "Read" << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << tempHWRead;
            if (!channelalreadyused) {
                sortedChannelList.push_back(tempHWRead);
                writeSDChannel[tempHWRead] = true;

            }

        }
    }
    //Sort the channels
    std::sort(sortedChannelList.begin(),sortedChannelList.end());

    return 0; //return the position of the file where data begins

}
