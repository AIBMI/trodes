TEMPLATE = subdirs

# compiles 25% faster if commented out (there are no dependencies)
#CONFIG += ordered

SUBDIRS = exportanalog \
          exportdio \
          exportLFP \
          exportMDA \
          exportOfflineSorter \
          exportPHY \
          exportspikes \
          exporttime \
          mergeSDRecording \
          sdToRec \
          exporthwtimes \
          splitRec
