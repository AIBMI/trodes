#ifndef LFPEXPORTHANDLER_H
#define LFPEXPORTHANDLER_H
#include "abstractexporthandler.h"
#include "iirFilter.h"


class LFPExportHandler: public AbstractExportHandler
{
    Q_OBJECT

public:
    LFPExportHandler(QStringList arguments);
    int processData();
    ~LFPExportHandler();

protected:

    void parseArguments();
    void printHelpMenu();

    //void printCustomMenu();
    //void parseCustomArguments(int &argumentsProcessed);

private:
    bool useOneChannelPerNTrode;

};

#endif // LFPEXPORTHANDLER_H
