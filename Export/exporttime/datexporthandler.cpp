#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    maxGapSizeInterpolation = 500;


    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    if ((maxGapSizeInterpolation > 500) || (maxGapSizeInterpolation < 0)) {
         qDebug() << "Error: interp must be between 0 and 500. Default 500.";
         argumentReadOk = false;
    }

    if (outputSamplingRate != -1) {
        qDebug() << "Error: outputrate must stay unchanged for spike processing.";
        qDebug() << outputSamplingRate;
        argumentReadOk = false;
    }

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract continuous time periods from a raw rec file and save to binary file. \n");
    printf("Usage:  exporttime -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n"
           "    -outputrate -1 (full sampling, can't be changed') \n"
           "    -interp 500 (used to define breaks in continuous periods) \n");



    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    QString invertSpikes_string = "";
    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        } else if ((argumentList.at(optionInd).compare("-invert",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            invertSpikes_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }



    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {


    qDebug() << "Exporting continuous time period data...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    createFilters();

    if (!openInputFile()) {
        return -1;
    }


    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".time"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating time directory.";
            return -1;
        }
    }

    //Pointers to the neuro files
    /*QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;*/


    QString infoLine;
    QString fieldLine;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;



    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".time.dat"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    timeFilePtrs.last()->write("<Start settings>\n");
    infoLine = QString("Description: Continuous time periods (start and end times)\n");
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Byte_order: little endian\n");
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Original_file: ") + fi.fileName() + "\n";
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Clockrate: %1\n").arg(hardwareConf->sourceSamplingRate);
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("System_time_at_creation: %1\n").arg(globalConf->systemTimeAtCreation);
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Timestamp_at_creation: %1\n").arg(globalConf->timestampAtCreation);
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    writeDefaultHeaderInfo(timeFilePtrs.last());


    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<timeperiods 2*uint32>";
    fieldLine += "\n";
    timeFilePtrs.last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs.last()->write("<End settings>\n");
    timeFilePtrs.last()->flush();





    //************************************************
    int inputFileInd = 0;

    bool firstPointInFile;

    while (inputFileInd < recFileNameList.length()) {

        firstPointInFile = true;
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;


            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }

            //We need to create a file that says if the file has a time offset
            QFile* tmpOffsetFile = new QFile();
            tmpOffsetFile->setFileName(saveLocation+fi.completeBaseName()+QString(".offset.txt"));
            if (!tmpOffsetFile->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating offset file.";
                return -1;
            }

            //Write the current offset to file
            QString offsetline = QString("%1\n").arg(startOffsetTime);
            tmpOffsetFile->write(offsetline.toLocal8Bit());

            tmpOffsetFile->flush();
            tmpOffsetFile->close();


        }

        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {
            //Read in a packet of data to make sure everything looks good
            if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
                //We have reached the end of the file
                break;
            }
            //Find the time stamp
            packetsRead++;
            bufferPtr = buffer.data()+packetTimeLocation;
            tPtr = (uint32_t *)(bufferPtr);
            currentTimeStamp = *tPtr+startOffsetTime;

            //The number of expected data points between this time stamp and the last (1 if no data is missing)
            int64_t numberOfPointsToProcess = ((int64_t)currentTimeStamp-lastTimeStamp);

            if (numberOfPointsToProcess < 1) {
                qDebug() << "Error with file: backwards timestamp by" << numberOfPointsToProcess << "at time " << currentTimeStamp << "last time:" << lastTimeStamp << "Packet: " << packetsRead;
                if (abortOnBackwardsTimestamp) {
                    qDebug() << "Aborting";
                    return -1;
                }
            }

            if (firstPointInFile) {
                //Write end and start timestamps to file
                *timeStreamPtrs.at(0) << currentTimeStamp;
                firstPointInFile = false;
            }
            if ((maxGapSizeInterpolation > -1) && (numberOfPointsToProcess > (maxGapSizeInterpolation+1))) {
                //The gap in data is too large to interpolate over, so we start a new time period in the log file

                qDebug() << "Gap of " << numberOfPointsToProcess << "found. Packet: " << packetsRead;
                numberOfPointsToProcess = 1;
                pointsSinceLastLog = -1;

                //Write end and start timestamps to file
                *timeStreamPtrs.at(0) << lastTimeStamp << currentTimeStamp;
            }

            //Print the progress to stdout
            printProgress();

            lastTimeStamp = currentTimeStamp;
            pointsSinceLastLog = (pointsSinceLastLog+numberOfPointsToProcess)%decimation;


        }

        //Write the last timestamp to file
        *timeStreamPtrs.at(0) << currentTimeStamp;

        filePtr->close();
        printf("\rDone\n");
        inputFileInd++;
    }


    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }



    return 0; //success
}

void DATExportHandler::writeSpikeData(int nTrodeNum, const QVector<int2d> *waveForm, const int *, uint32_t time){
    //A spike event was detected.
    //Write the spike waveforms plus the timestamps to the correct file


    //First, write the timestamp
    *neuroStreamPtrs[nTrodeNum] << time;

    //Then write the 40 waveform points for channel 0, then 40 points for channel 1, etc.
    for (int chInd=0; chInd < nTrodeSettings[nTrodeNum].numChannels; chInd++) {
        for (int sampleIndex = 0; sampleIndex < 40; sampleIndex++) {
            *neuroStreamPtrs[nTrodeNum] << waveForm[chInd][sampleIndex].y;
        }
    }

    /*
    for (int sampleIndex = 0; sampleIndex < 40; sampleIndex++) {
        for (int chInd=0; chInd < nTrodeSettings[nTrodeNum].numChannels; chInd++) {
            *neuroStreamPtrs[nTrodeNum] << waveForm[chInd][sampleIndex].y;
        }
    }*/

}
