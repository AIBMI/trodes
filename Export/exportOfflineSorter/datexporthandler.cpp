#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{
    //Set default behavior
    combineNeuralChannels = true;
    combineAuxChannels = false;

    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract continous data from a raw rec file and save to binary file that can be imported with Offline Sorter. \n");
    printf("Usage:  exportofflinesorter -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n"
           "    -outputrate -1 (full) \n"
           "    -usespikefilters 1 (Filter the channels with the saved spike filters)\n"
           "    -interp -1 (interpolation not supported, do not change) \n"
           "    -combinechannels 1 (combine neural channels into one file. Instead use one file per channel.) \n"
           "    -appendauxdata 0 (do not append DIO and analog data into the file)\n"
           "    -userefs 1 (Use the digital reference channels designated in the file)\n");

    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;
    QString combine_string = "";
    QString appendauxdata_string = "";
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        } else if ((argumentList.at(optionInd).compare("-combinechannels",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            combine_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-appendauxdata",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            appendauxdata_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }
    if (!combine_string.isEmpty()) {
        bool ok1;

        combineNeuralChannels = combine_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "combinechannels argument could not be resolved into an integer. Use 0 (false) or 1 (true)";
            argumentReadOk = false;
            return;
        }
    }
    if (!appendauxdata_string.isEmpty()) {
        bool ok1;

        combineAuxChannels = appendauxdata_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "appendauxdata argument could not be resolved into an integer. Use 0 (false) or 1 (true)";
            argumentReadOk = false;
            return;
        }
    }

    if (!combineNeuralChannels && combineAuxChannels) {
        qDebug() << "appendauxdata can only be set to 1 if combinechannels is also set to 1";
        argumentReadOk = false;
        return;
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "Exporting to Offline Sorter format file...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    createFilters();

    qint32 numPointsWrittenToFile = 0;

    if (!openInputFile()) {
        return -1;
    }

    int16_t tmpVal;

    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".offlinesorter"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating OfflineSorter directory.";
            return -1;
        }
    }

    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;



    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);


    QFile* infoTextFile = new QFile;
    QString infoLine;
    if (combineNeuralChannels) {
        //Create a channel info text file

        infoTextFile->setFileName(saveLocation+fileBaseName+QString(".info.txt"));
        if (!infoTextFile->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        infoLine = QString("Binary file contents\n\nData offset: 0\nDigitizing frequency: %1\nA/D Converter resolution: 16 bits\nMaximum voltage: 6.39 mV\n\n").arg(hardwareConf->sourceSamplingRate);
        infoTextFile->write(infoLine.toLocal8Bit());
    }



    //Create an output file for the neural data
    //*****************************************

    int totalChannelsInComboFile = 0;
    int totalAuxDataChannelsInComboFile = 0;

    //Create the output files
    if (combineNeuralChannels) {
        //All neural channels into one file
        neuroFilePtrs.push_back(new QFile);
        neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".os.dat"));
        if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
        neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

        totalChannelsInComboFile += spikeConf->ntrodes.length();

        for (int i=0; i<spikeConf->ntrodes.length(); i++) {
            infoLine = QString("Channel %1: NTrode %2\n").arg(i+1).arg(spikeConf->ntrodes[i]->nTrodeId);
            infoTextFile->write(infoLine.toLocal8Bit());
        }



    } else {
        //One file per ntrode
        for (int i=0; i<spikeConf->ntrodes.length(); i++) {
            neuroFilePtrs.push_back(new QFile);

            neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".os_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));

            if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                return -1;
            }
            neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
            neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);
        }
    }

    QVector<int16_t> lastValues;
    QVector<int> interleavedDataBytes;
    uint8_t *interleavedDataIDBytePtr;

    if (combineAuxChannels) {
        //Append aux channels into the file

        for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {
            lastValues.push_back(0);
            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                interleavedDataBytes.push_back(headerConf->headerChannels[auxChInd].interleavedDataIDByte);
            } else {
                interleavedDataBytes.push_back(-1);
            }
            infoLine = QString("Channel %1: Aux channel %2\n").arg(auxChInd+1+totalChannelsInComboFile).arg(headerConf->headerChannels[auxChInd].idString);
            infoTextFile->write(infoLine.toLocal8Bit());
        }
        totalAuxDataChannelsInComboFile += headerConf->headerChannels.length();
        totalChannelsInComboFile += headerConf->headerChannels.length();

    } else {
        //Create an output file for the DIO data
        //*****************************************

        /*
        QList<QFile*> DIOFilePtrs;
        QList<QDataStream*> DIOStreamPtrs;
        QList<int> DIOChannelInds;
        QList<uint8_t> DIOLastValue;



        for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::DIGITALTYPE) {
                DIOChannelInds.push_back(auxChInd);
                DIOFilePtrs.push_back(new QFile);
                DIOLastValue.push_back(2); //state will be either 1 or 0, 2 means that it's the first time point

                DIOFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".dio_%1.dat").arg(headerConf->headerChannels[auxChInd].idString));
                if (!DIOFilePtrs.last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file.";
                    return -1;
                }
                DIOStreamPtrs.push_back(new QDataStream(DIOFilePtrs.last()));
                DIOStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

            }
        }

        //Create an output file for the analog data
        //*****************************************
        QList<QFile*> analogFilePtrs;
        QList<QDataStream*> analogStreamPtrs;
        QList<int> analogChannelInds;

        //QList<uint8_t> DIOLastValue;



        for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                //This is an analog channel
                analogChannelInds.push_back(auxChInd);
                lastValues.push_back(0);
                interleavedDataBytes.push_back(headerConf->headerChannels[auxChInd].interleavedDataIDByte);

                analogFilePtrs.push_back(new QFile);

                analogFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".analog_%1.dat").arg(headerConf->headerChannels[auxChInd].idString));
                if (!analogFilePtrs.last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file.";
                    return -1;
                }
                analogStreamPtrs.push_back(new QDataStream(analogFilePtrs.last()));
                analogStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);



            }
        }*/
    }

    if (combineNeuralChannels) {
        infoTextFile->flush();
        infoTextFile->close();
    }


    //************************************************

    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;

            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }

        }




        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {
            //Read in a packet of data to make sure everything looks good
            if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
                //We have reached the end of the file
                break;
            }
            //Find the time stamp
            bufferPtr = buffer.data()+packetTimeLocation;
            tPtr = (uint32_t *)(bufferPtr);
            currentTimeStamp = *tPtr+startOffsetTime;

            //The number of expected data points between this time stamp and the last (1 if no data is missing)
            int numberOfPointsToProcess = (currentTimeStamp-lastTimeStamp);
            if (numberOfPointsToProcess < 1) {
                qDebug() << "Error with file: backwards timestamp at time " << currentTimeStamp << lastTimeStamp;
                if (abortOnBackwardsTimestamp) {
                    qDebug() << "Aborting";
                    return -1;
                } else {
                    for (int i=0; i < channelFilters.length(); i++) {
                        channelFilters[i]->resetHistory();
                    }


                    numberOfPointsToProcess = 1;
                    pointsSinceLastLog = -1;
                }
            }

            /*if ((maxGapSizeInterpolation > -1) && (numberOfPointsToProcess > (maxGapSizeInterpolation+1))) {
                //The gap in data is too large to interpolate over, so we reset the filters (maxGapSizeInterpolation of -1 means the no gap is too large)
                for (int i=0; i < channelFilters.length(); i++) {
                    channelFilters[i]->resetHistory();
                }
                numberOfPointsToProcess = 1;
                pointsSinceLastLog = -1;
            }*/

            numberOfPointsToProcess = 1; //We do not allow interpolation in this export program

            if (numberOfPointsToProcess == 1) {
                //No missing data
                if (((pointsSinceLastLog+1)%decimation) == 0) {
                    *timeStreamPtrs.at(0) << currentTimeStamp;
                    numPointsWrittenToFile++;
                }
            } else {
                //Some missing data, which we will interpolate over
                for (int p=1; p<numberOfPointsToProcess+1; p++) {
                    if (((pointsSinceLastLog+p)%decimation) == 0) {
                        *timeStreamPtrs.at(0) << lastTimeStamp+p;
                        numPointsWrittenToFile++;
                    }
                }
            }
            for (int chInd=0; chInd < channelPacketLocations.length(); chInd++) {
                bufferPtr = buffer.data()+channelPacketLocations[chInd];
                tempDataPtr = (int16_t*)(bufferPtr);
                tempDataPoint = *tempDataPtr;

                if (useRefs && refOn[chInd]) {
                    //Subtract the digital reference
                    bufferPtr = buffer.data() + refPacketLocations[chInd];
                    tempDataPtr = (int16_t*)(bufferPtr);
                    tempRefPoint = *tempDataPtr;
                    tempDataPoint -= tempRefPoint;
                }



                if (numberOfPointsToProcess == 1) {
                    //No missing data



                    if (filterOn[chInd]) {
                        tempFilteredDataPoint = channelFilters.at(chInd)->addValue(tempDataPoint);
                    }



                    if (filterOn[chInd]) {
                        if (!combineNeuralChannels) {
                            *neuroStreamPtrs.at(nTrodeIndForChannels[chInd]) << tempFilteredDataPoint; //1 file per channel
                        } else {
                            *neuroStreamPtrs.at(0) << tempFilteredDataPoint; //all channels in one file
                        }


                    } else {
                        if (!combineNeuralChannels) {
                            *neuroStreamPtrs.at(nTrodeIndForChannels[chInd]) << tempDataPoint; //1 file per channel
                        } else {
                            *neuroStreamPtrs.at(0) << tempDataPoint; //all channels in one file
                        }
                    }



                }
                dataLastTimePoint[chInd] = tempDataPoint; //remember the last unfilted data point
            }
            if (combineAuxChannels) {
                for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

                    if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                        //Analog channel
                        if (interleavedDataBytes[auxChInd] != -1) {
                            //This location in the packet has interleaved data from multiple sources that are sampled at a lower rate

                            interleavedDataIDBytePtr = (uint8_t*)(buffer.data()) + interleavedDataBytes[auxChInd];

                            if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[auxChInd].interleavedDataIDBit)) {
                                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                                bufferPtr = buffer.data()+headerConf->headerChannels[auxChInd].startByte;
                                tmpVal = (int16_t)(*bufferPtr);
                                *neuroStreamPtrs.at(0) << tmpVal;
                                lastValues[auxChInd] = tmpVal;

                            } else {
                                //Use the last data point received
                                *neuroStreamPtrs.at(0) << lastValues[auxChInd];

                            }
                        } else {
                            //No interleaving
                            bufferPtr = buffer.data()+headerConf->headerChannels[auxChInd].startByte;
                            tmpVal = (int16_t)(*bufferPtr);
                            *neuroStreamPtrs.at(0) << tmpVal;
                            lastValues[auxChInd] = tmpVal;
                        }

                    } else {
                        //Digital channel
                        bufferPtr = buffer.data()+headerConf->headerChannels[auxChInd].startByte;
                        tmpVal =  (int16_t)((*bufferPtr & (1 << headerConf->headerChannels[auxChInd].digitalBit)) >> headerConf->headerChannels[auxChInd].digitalBit);
                        *neuroStreamPtrs.at(0) << tmpVal;
                        lastValues[auxChInd] = tmpVal;

                    }
                }
            }


            /*
            for (int chInd=0; chInd < DIOChannelInds.length(); chInd++) {

                bufferPtr = buffer.data()+headerConf->headerChannels[DIOChannelInds[chInd]].startByte;
                *DIOStreamPtrs.at(chInd)  << (int16_t)((*bufferPtr & (1 << headerConf->headerChannels[DIOChannelInds[chInd]].digitalBit)) >> headerConf->headerChannels[DIOChannelInds[chInd]].digitalBit);

            }

            for (int chInd=0; chInd < analogChannelInds.length(); chInd++) {

                if (interleavedDataBytes[chInd] != -1) {
                    //This location in the packet has interleaved data from multiple sources that are sampled at a lower rate

                    interleavedDataIDBytePtr = (uint8_t*)(buffer.data()) + interleavedDataBytes[chInd];

                    if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[analogChannelInds[chInd]].interleavedDataIDBit)) {
                        //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                        bufferPtr = buffer.data()+headerConf->headerChannels[analogChannelInds[chInd]].startByte;
                        tmpVal = (int16_t)(*bufferPtr);
                        *analogStreamPtrs.at(chInd) << tmpVal;
                        lastValues[chInd] = tmpVal;

                    } else {
                        //Use the last data point received
                        *analogStreamPtrs.at(chInd) << lastValues[chInd];

                    }
                } else {
                    //No interleaving
                    bufferPtr = buffer.data()+headerConf->headerChannels[analogChannelInds[chInd]].startByte;
                    tmpVal = (int16_t)(*bufferPtr);
                    *analogStreamPtrs.at(chInd) << tmpVal;
                }

                //bufferPtr = buffer.data()+headerConf->headerChannels[analogChannelInds[chInd]].startByte;
                //tmpVal = (int16_t)(*bufferPtr);
                //*analogStreamPtrs.at(chInd) << tmpVal;

            }*/

            //Print the progress to stdout
            printProgress();

            lastTimeStamp = currentTimeStamp;
            pointsSinceLastLog = (pointsSinceLastLog+numberOfPointsToProcess)%decimation;


        }

        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }

    /*for (int i=0; i < analogFilePtrs.length(); i++) {
        analogFilePtrs[i]->flush();
        analogFilePtrs[i]->close();
    }

    for (int i=0; i < DIOFilePtrs.length(); i++) {
        DIOFilePtrs[i]->flush();
        DIOFilePtrs[i]->close();
    }*/





    return 0; //success
}
