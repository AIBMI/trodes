#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to create one or more small rec files from a large one. \n\n");
    printf("NOTE: FILTERING AND OTHER PROCESSING TURNED OFF. MODIFYING THESE IN THE INPUT FLAGS WILL BE IGNORED. RAW DATA IS SPLIT EXACTLY AS IT EXISTS IN THE ORIGINAL FILE  \n\n");
    printf("Usage:  splitrec -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "REQUIRED ARGUMENTS: You must either give a -gaps input OR both the -start and -end inputs to determine how the file should be split.\n\n"
           "Input arguments: \n"
           "-gaps <number>  When a gap in samples is encountered that is larger than or equal to this, a new split file is created. 30000 is one second. \n"
           "-start <hour:minute:second>  The start time of the output snippet. Example: 1:03:05. This argument must be accompanied by the -end argument \n"
           "-end <hour:minute:second>  The end time of the output snippet. Example: 1:31:50. \n\n");



    //AbstractExportHandler::printHelpMenu();
}

bool DATExportHandler::createNextOutputFile() {
    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    currentOutputFileNum++;

    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }


    if(outputDirectory.isEmpty()) {
        outputDirectory = fi.absolutePath();
    }

    QString saveLocation = outputDirectory+QString(QDir::separator())+fileBaseName+QString("_split%1").arg(currentOutputFileNum)+".rec"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating output directory.";
            return false;
        }
    }

    neuroFilePtrs.push_back(new QFile);
    neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString("_split%1").arg(currentOutputFileNum)+".rec");
    if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
    neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    neuroFilePtrs.last()->write(configString.toLocal8Bit());
    neuroFilePtrs.last()->flush();

    return true;


}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class
    startSplitTime = 0;
    endSplitTime = 0;
    splitDataGap = -1;

    QString datagap_string = "";
    QString starttime_string = "";
    QString endtime_string = "";
    bool startTimeEntered = false;
    bool endTimeEntered = false;
    bool gapsEntered = false;

    int optionInd = 1;
    while (optionInd < argumentList.length()) {


        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        } else if ((argumentList.at(optionInd).compare("-gaps",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            datagap_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-start",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            starttime_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;

        } else if ((argumentList.at(optionInd).compare("-end",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            endtime_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;

    }

    if (!datagap_string.isEmpty()) {
        bool ok1;

        splitDataGap = datagap_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Gaps argument could not be resolved into an integer.";
            argumentReadOk = false;
            return;
        } else {
            if (splitDataGap < 1) {
                qDebug() << "Gaps argument must be an integer greater than 1.";
                argumentReadOk = false;
                return;
            }
            if ((!starttime_string.isEmpty()) || (!endtime_string.isEmpty())) {
                qDebug() << "Gaps argument can not be used alongside the start or end arguments.";
                argumentReadOk = false;
                return;
            }
        }
        gapsEntered= true;
    }

    if (!starttime_string.isEmpty()) {

        QTime tempTime = QTime::fromString(starttime_string,"h:mm:ss");
        if (!tempTime.isValid()) {
            qDebug() << "Start and end time arguments must be in the following format:  3:01:05 for 3 hours, 1 minute, 5 seconds.";
            argumentReadOk = false;
            return;
        }
        startSplitTime = tempTime.secsTo(QTime(0,0,0))*-1; //Seconds from 0
        startTimeEntered = true;
    }

    if (!endtime_string.isEmpty()) {

        QTime tempTime = QTime::fromString(endtime_string,"h:mm:ss");
        if (!tempTime.isValid()) {
            qDebug() << "Start and end time arguments must be in the following format:  3:01:05 for 3 hours, 1 minute, 5 seconds.";
            argumentReadOk = false;
            return;
        }
        endSplitTime = tempTime.secsTo(QTime(0,0,0))*-1; //Seconds from 0
        endTimeEntered = true;
    }

    if ( !( (gapsEntered) || (startTimeEntered && endTimeEntered) ) ) {
        qDebug() << "Error: You must either give a -gaps input OR both the -start and -end inputs to determine how the file should be split.";
        return;
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {


    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    createFilters();
    currentOutputFileNum = 0;

    qint32 numPointsWrittenToFile = 0;

    if (!openInputFile()) {
        return -1;
    }

    if (splitDataGap == -1) {
        startSplitTime = startSplitTime * hardwareConf->sourceSamplingRate;
        endSplitTime = endSplitTime * hardwareConf->sourceSamplingRate;

        if (endSplitTime <= startSplitTime) {
            qDebug() << "Error: -end time must be after -start time. Aborting.";
            return -1;
        }

    }

    qDebug() << "Creating split file...";

    QVector<QVector<int16_t>* > tempDataHolder;  //Holds the data for each packet, organized in the correct order before writing to disk




    //Create an output file for the neural data
    //*****************************************
    createNextOutputFile();




    //************************************************
    if (recFileNameList.length() > 1) {
        qDebug() << "Warning: multiple rec files given as input. Only the first will be processed.";
    }

    int inputFileInd = 0;






    //Process the data and stream results to output files
    while(!filePtr->atEnd()) {
        //Read in a packet of data to make sure everything looks good
        if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
            //We have reached the end of the file
            break;
        }
        //Find the time stamp
        bufferPtr = buffer.data()+packetTimeLocation;
        tPtr = (uint32_t *)(bufferPtr);
        currentTimeStamp = *tPtr+startOffsetTime;

        //The number of expected data points between this time stamp and the last (1 if no data is missing)
        int gap = (currentTimeStamp-lastTimeStamp)-1;
        if (splitDataGap > 0) {
            if (gap >= splitDataGap) {
                //A gap longer than the one allowed occured, so start the next split file
                QTime gapTime(0,0);
                gapTime = gapTime.addSecs((currentTimeStamp/hardwareConf->sourceSamplingRate));


                qDebug() << "New file at time" << gapTime.toString("h:mm:ss");
                createNextOutputFile();
            }
            neuroStreamPtrs.last()->writeRawData(buffer.constData(),filePacketSize);
        } else {
            if ((currentTimeStamp >= startSplitTime) && ((currentTimeStamp <= endSplitTime))) {
                neuroStreamPtrs.last()->writeRawData(buffer.constData(),filePacketSize);
            } else if (currentTimeStamp > endSplitTime) {
                break;
            }
        }




        //Print the progress to stdout
        printProgress();

        lastTimeStamp = currentTimeStamp;



    }

    printf("\rDone\n");
    filePtr->close();
    inputFileInd++;


    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }





    return 0; //success
}
