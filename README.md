# Trodes #


### An open source, cross-platform software suite for neuroscience data acquisition and experimental control. 
 
-----
### Website (with screenshots) ###
[http://www.spikegadgets.com/software/trodes.html](http://www.spikegadgets.com/software/trodes.html)

### Development Team ###
http://www.spikegadgets.com/software/dev_team.html

### Wiki ###
[https://bitbucket.org/mkarlsso/trodes/wiki](https://bitbucket.org/mkarlsso/trodes/wiki)



### Required drivers ###
[FTDI D2XX Driver](http://www.ftdichip.com/Drivers/D2XX.htm)

-----
## Overview ###
Trodes is used to monitor incoming data from hardware used in neuroscience research, with a specific focus on electrophysiology. It is also being developed to allow complex control of behavioral apparatuses and neuro-perterbation devices.  Trodes is made up of "modules", each running as a separate process and serving a specialized purpose. The modules all communicate with a central graphical interface that performs the most crucial tasks associated with visualizing and saving data. With this architecture, the trodes suite is readily extendable without needing detailed understanding of existing code. 

##[Installation/Setup](https://bitbucket.org/mkarlsso/trodes/wiki/install/Trodes)
- This is a tutorial for setting up the software and data streaming via USB or Ethernet

##[Getting Started](https://bitbucket.org/mkarlsso/trodes/wiki/Getting%20Started)
- Learn how to simulate and record a signal in Trodes

##[Working With Workspaces](https://bitbucket.org/mkarlsso/trodes/wiki/Workspace)
- Learn how to create and use workspace configuration files

##[Modules](https://bitbucket.org/mkarlsso/trodes/wiki/Modules)
- Extend Trodes functionality with various modules

##[Exporting Data](https://bitbucket.org/mkarlsso/trodes/wiki/Export)
- Export data from the recording file, and then import it into Matlab or Python for further analysis

##[Release Notes](https://bitbucket.org/mkarlsso/trodes/wiki/Release%20Notes)
- Documentation of changes made in each release