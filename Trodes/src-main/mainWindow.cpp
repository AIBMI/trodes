/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "mainWindow.h"

QGLFormat qglFormat;
bool linkChangesBool;
bool exportMode;
extern QString lastDebugMsg;
extern bool unitTestMode;


BackgroundFrame::BackgroundFrame(QWidget *parent) : QFrame(parent) {
    setAttribute(Qt::WA_NoSystemBackground);
    newParent();
    setStyleSheet("background-color:white");
    QVBoxLayout *mainLayout = new QVBoxLayout();

    QLabel *newLabel = new QLabel(tr(""));
    mainLayout->addWidget(newLabel);
    setLayout(mainLayout);

}

bool BackgroundFrame::eventFilter(QObject *obj, QEvent *ev) {
    if (obj == parent()) {
        if (ev->type() == QEvent::Resize) {

            resize(static_cast<QResizeEvent*>(ev)->size());
        }
        else if (ev->type() == QEvent::ChildAdded) {
            raise();
        }
    }
    return(QFrame::eventFilter(obj, ev));
}

bool BackgroundFrame::event(QEvent *ev) {
    if (ev->type() == QEvent::ParentAboutToChange) {
        if(parent())
            parent()->removeEventFilter(this);
    }
    else if (ev->type() == QEvent::ParentChange) {
        newParent();
    }
    return(QFrame::event(ev));
}

MainWindow::MainWindow(bool audioOn)
{
    nTrodeSelectWindow = NULL;
    isAudioOn = audioOn;
    prevSelectedNTrodeIndex = 0;

    networkConf=NULL;
    moduleConf=NULL;
    nTrodeTable=NULL;
    streamConf=NULL;
    spikeConf=NULL;
    headerConf=NULL;
    hardwareConf=NULL;
    globalConf=NULL;
    //benchConfig=NULL;

    qDebug().noquote() << "Trodes Version Info:\n" << GlobalConfiguration::getVersionInfo(false); //print version info to debug log

    quitting = false;
    unitTestFlag = false;  //set to true if we are doing unit testing



  if (objectName().isEmpty())
    setObjectName(QString("Main"));

  resize(800, 600);
  exportMode = false; //Whether or not data displays should be updated

  hardwareConf = new HardwareConfiguration(NULL); // this is requried for creating the audio generator, but this object
                                                  // is overwritten when the config file is read

  sdDisp = NULL;
  spikeDisp = NULL;

  setAutoFillBackground(true);
  soundDialogOpen = false;
  recordFileOpen = false;
  dataStreaming = false;
  recording = false;
  timerTick = 0;
  channelsConfigured = false;
  visibleTime = 0;
  eventTabWasChanged = false;
  for (int i = 0; i < 64; i++) {
    eventTabsInitialized[i] = false;
  }
  eventTabsInitialized[0] = true;
  currentTrodeSelected = 0;
  singleTriggerWindowOpen = false;

  settleChannelByteInPacket = 0;
  settleChannelBit = 0;
  settleChannelTriggerState = 0;
  settleChannelDelay = 0;

  headstageSettings.autoSettleOn = false;
  headstageSettings.percentChannelsForSettle = 0;
  headstageSettings.threshForSettle = 0;
  headstageSettings.smartRefOn = false;
  headstageSettings.accelSensorOn = false;
  headstageSettings.gyroSensorOn = false;
  headstageSettings.magSensorOn = false;

  headstageSettings.smartRefAvailable = false;
  headstageSettings.autosettleAvailable = false;
  headstageSettings.accelSensorAvailable = false;
  headstageSettings.gyroSensorAvailable = false;
  headstageSettings.magSensorAvailable = false;
  headstageSettings.rfAvailable = false;

  triggerSettings = NULL;
  configChanged = false;
  //qglFormat.setVersion(3,2);
  qglFormat.setProfile(QGLFormat::CoreProfile);
  qglFormat.setDoubleBuffer(true);


  //Statusbar setup---------------------------
  statusbar = new QStatusBar(this);
  TrodesFont statusFont;
//  statusFont.setPixelSize(14);
  statusbar->setFont(statusFont);
  setStatusBar(statusbar);
  //statusbar = statusbar;
  statusbar->showMessage(tr("Not connected to device"));
  //------------------------------------------

  //Time format to be displayed
  timeFormatString = "HH:mm:ss.zzz";


  //File menu--------------------------------
  //Any shortcuts should be added in section below for workaround
  menuFile = new QMenu;
  menuFile->setTitle("File");
  menuBar()->addAction(menuFile->menuAction());

  menuConfig = new QMenu;
  menuFile->addAction(menuConfig->menuAction());

  actionLoadConfig = new QAction(this);
  actionLoadConfig->setShortcut(QKeySequence(tr("Ctrl+O")));
  menuConfig->addAction(actionLoadConfig);
  actionCloseConfig = new QAction(this);
  actionCloseConfig->setEnabled(false);
  actionCloseConfig->setShortcut(QKeySequence(tr("Shift+Ctrl+O")));
  menuConfig->addAction(actionCloseConfig);
  actionSaveConfig = new QAction(this);
  actionSaveConfig->setShortcut(QKeySequence(tr("Ctrl+S")));
  actionSaveConfig->setEnabled(false);
  menuConfig->addAction(actionSaveConfig);
  actionReConfig = new QAction(this);
  actionReConfig->setEnabled(false);
  actionReConfig->setShortcut(QKeySequence(tr("Ctrl+R")));
  menuConfig->addAction(actionReConfig);

  actionAboutConfig = new QAction(this);
  actionAboutConfig->setEnabled(false);
  actionAboutConfig->setText("About Workspace");
  menuConfig->addAction(actionAboutConfig);


  actionPlaybackOpen = new QAction(this);
  actionPlaybackOpen->setShortcut(QKeySequence(tr("Ctrl+P")));
  actionPlaybackOpen->setData(QVariant::fromValue(SourceFile));
  menuFile->addAction(actionPlaybackOpen);
  actionOpenRecordDialog = new QAction(this);
  actionOpenRecordDialog->setShortcut(QKeySequence(tr("Ctrl+N")));
  menuFile->addAction(actionOpenRecordDialog);
  actionOpenRecordDialog->setEnabled(false);
  actionCloseFile = new QAction(this);
  actionCloseFile->setShortcut(QKeySequence(tr("Ctrl+W")));
  actionCloseFile->setEnabled(false);
  menuFile->addAction(actionCloseFile);
  menuFile->addSeparator();
  actionRecord = new QAction(this);
  actionRecord->setEnabled(false);
  menuFile->addAction(actionRecord);
  actionPause = new QAction(this);
  actionPause->setEnabled(false);
  menuFile->addAction(actionPause);
  actionPlay = new QAction(this);
  actionPlay->setEnabled(false);
  menuFile->addAction(actionPlay);
  actionExport = new QAction(this);
  actionExport->setEnabled(false);
  menuFile->addAction(actionExport);


  menuFile->addSeparator();
  // Module Menu

  actionRestartModules = new QAction(this);
  menuFile->addAction(actionRestartModules);
  actionRestartModules->setEnabled(false);

  menuFile->addSeparator();

  actionQuit = new QAction(this);
  actionQuit->setShortcut(Qt::CTRL + Qt::Key_Q);
  //actionQuit->setMenuRole(QAction::QuitRole);
  menuFile->addAction(actionQuit);


  //****************************
  //Workaround for qt 5.7+ bug in linux: action shortcuts in linux do not work
  addAction(actionLoadConfig);
  addAction(actionCloseConfig);
  addAction(actionSaveConfig);
  addAction(actionPlaybackOpen);
  addAction(actionOpenRecordDialog);
  addAction(actionCloseFile);
  addAction(actionQuit);
  //****************************

  connect(actionLoadConfig, SIGNAL(triggered()), this, SLOT(loadConfig()));
  connect(actionReConfig, SIGNAL(triggered()), this, SLOT(reConfig()));
  QObject::connect(actionPlaybackOpen,SIGNAL(triggered()),this,SLOT(setSource()));
  connect(actionCloseConfig, SIGNAL(triggered()), this, SLOT(closeWorkspace()));
  connect(actionSaveConfig, SIGNAL(triggered()), this, SLOT(saveConfig()));
  connect(actionAboutConfig, SIGNAL(triggered()), this, SLOT(aboutCurConfig()));


  QObject::connect(actionOpenRecordDialog, SIGNAL(triggered()), this, SLOT(openRecordDialog()));
  QObject::connect(actionCloseFile, SIGNAL(triggered()), this, SLOT(closeFile()));
  connect(actionRecord,SIGNAL(triggered()),this,SLOT(actionRecordSelected()));
  connect(actionPause,SIGNAL(triggered()),this,SLOT(actionPauseSelected()));
  connect(actionPlay,SIGNAL(triggered()),this,SLOT(actionPlaySelected()));
  connect(actionExport,SIGNAL(triggered()),this,SLOT(openExportDialog()));
  connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
  connect(actionRestartModules, SIGNAL(triggered()), this, SLOT(checkRestartModules()));


  //----------------------------------------------



  //Connection menu--------------------------------
  menuSystem = new QMenu;
  menuSystem->setTitle("Connection");
  menuBar()->addAction(menuSystem->menuAction());
  //mainMenuBar->addAction(menuSystem->menuAction());
  //actionReconfigure = new QAction(this);
  //menuSystem->addAction(actionReconfigure);
  sourceMenu = new QMenu;
  menuSystem->addAction(sourceMenu->menuAction());
  menuSystem->addSeparator();
  menuSimulationSource = new QMenu;
  menuSpikeGadgetsSource = new QMenu;
  actionSourceNone = new QAction(this);
  actionSourceFake = new QAction(this);
  actionSourceFakeSpikes = new QAction(this);
  menuSimulationSource->addAction(actionSourceFake);
  menuSimulationSource->addAction(actionSourceFakeSpikes);
  actionSourceFile = new QAction(this);
  actionSourceEthernet = new QAction(this);
  actionSourceUSB = new QAction(this);
  menuSpikeGadgetsSource->addAction(actionSourceUSB);
  menuSpikeGadgetsSource->addAction(actionSourceEthernet);
  actionSourceRhythm = new QAction(this);
  actionSourceNone->setCheckable(true);
  actionSourceNone->setChecked(true);
  actionSourceFake->setCheckable(true);
  actionSourceFake->setChecked(false);
  actionSourceFakeSpikes->setCheckable(true);
  actionSourceFakeSpikes->setChecked(false);
  actionSourceFile->setCheckable(true);
  actionSourceFile->setChecked(false);
  actionSourceUSB->setCheckable(true);
  actionSourceUSB->setChecked(false);
  actionSourceRhythm->setCheckable(true);
  actionSourceRhythm->setChecked(false);
  actionSourceEthernet->setCheckable(true);
  actionSourceEthernet->setChecked(false);
  sourceMenu->addAction(actionSourceNone);
  //sourceMenu->addAction(actionSourceFake);
  //sourceMenu->addAction(actionSourceFakeSpikes);
  sourceMenu->addAction(actionSourceFile);
  //sourceMenu->addAction(actionSourceEthernet);
  //sourceMenu->addAction(actionSourceUSB);
  sourceMenu->addAction(menuSimulationSource->menuAction());
  sourceMenu->addAction(menuSpikeGadgetsSource->menuAction());
  sourceMenu->addAction(actionSourceRhythm);
  actionSourceNone->setData(QVariant::fromValue(SourceNone));
  actionSourceFake->setData(QVariant::fromValue(SourceFake));
  actionSourceFakeSpikes->setData(QVariant::fromValue(SourceFakeSpikes));
  actionSourceFile->setData(QVariant::fromValue(SourceFile));
  actionSourceEthernet->setData(QVariant::fromValue(SourceEthernet));
  actionSourceUSB->setData(QVariant::fromValue(SourceUSBDAQ));
  actionSourceRhythm->setData(QVariant::fromValue(SourceRhythm));
#ifndef RHYTHM
    actionSourceRhythm->setEnabled(false);
#endif
  actionConnect = new QAction(this);
  menuSystem->addAction(actionConnect);
  actionConnect->setEnabled(false);
  actionDisconnect = new QAction(this);
  menuSystem->addAction(actionDisconnect);
  actionDisconnect->setEnabled(false);
  actionClearBuffers = new QAction(this);
  menuSystem->addAction(actionClearBuffers);
  actionClearBuffers->setEnabled(false);
  actionSendSettle = new QAction(this);
  menuSystem->addAction(actionSendSettle);
  actionSendSettle->setEnabled(false);
  menuSystem->addSeparator();
  actionOpenGeneratorDialog = new QAction(this);
  menuSystem->addAction(actionOpenGeneratorDialog);
  actionOpenGeneratorDialog->setEnabled(false);




  QObject::connect(actionSourceNone,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFake,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFakeSpikes,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFile,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceEthernet,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceUSB,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceRhythm,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionConnect, SIGNAL(triggered()), this, SLOT(connectToSource()));
  QObject::connect(actionDisconnect, SIGNAL(triggered()), this, SLOT(disconnectFromSource()));
  QObject::connect(actionSendSettle, SIGNAL(triggered()), this, SLOT(sendSettleCommand()));

  QObject::connect(actionOpenGeneratorDialog, SIGNAL(triggered()), this, SLOT(openGeneratorDialog()));

  //-----------------------------------------------------



  //Display menu-------------------------------------------
  menuDisplay = new QMenu;
  menuDisplay->setTitle("View");
  menuBar()->addAction(menuDisplay->menuAction());
  //mainMenuBar->addAction(menuDisplay->menuAction());
  actionShowCurrentTrode = new QAction(this);
  actionShowCurrentTrode->setEnabled(false);
  menuDisplay->addAction(actionShowCurrentTrode);
  actionUncoupleDisplay = new QAction(this);
  actionUncoupleDisplay->setCheckable(true);
  actionUncoupleDisplay->setChecked(false);
  actionUncoupleDisplay->setShortcut(QKeySequence(Qt::Key_Space));
  menuDisplay->addAction(actionUncoupleDisplay);
  connect(actionUncoupleDisplay,SIGNAL(toggled(bool)),this,SLOT(uncoupleDisplay(bool)));

  menuEEGDisplay = new QMenu;
  menuDisplay->addAction(menuEEGDisplay->menuAction());
  menuSetTLength = new QMenu(this);
  actionSetTLength0_2 = new QAction(this);
  actionSetTLength0_2->setData(0.2);
  actionSetTLength0_2->setCheckable(true);
  actionSetTLength0_2->setChecked(false);
  actionSetTLength0_5 = new QAction(this);
  actionSetTLength0_5->setData(0.5);
  actionSetTLength0_5->setCheckable(true);
  actionSetTLength0_5->setChecked(false);
  actionSetTLength1_0 = new QAction(this);
  actionSetTLength1_0->setData(1.0);
  actionSetTLength1_0->setCheckable(true);
  actionSetTLength1_0->setChecked(true);
  actionSetTLength2_0 = new QAction(this);
  actionSetTLength2_0->setData(2.0);
  actionSetTLength2_0->setCheckable(true);
  actionSetTLength2_0->setChecked(false);
  actionSetTLength5_0 = new QAction(this);
  actionSetTLength5_0->setData(5.0);
  actionSetTLength5_0->setCheckable(true);
  actionSetTLength5_0->setChecked(false);
  menuSetTLength->addAction(actionSetTLength0_2);
  menuSetTLength->addAction(actionSetTLength0_5);
  menuSetTLength->addAction(actionSetTLength1_0);
  menuSetTLength->addAction(actionSetTLength2_0);
  menuSetTLength->addAction(actionSetTLength5_0);
  menuEEGDisplay->addMenu(menuSetTLength);
  streamFilterMenu = new QMenu;
  menuEEGDisplay->addAction(streamFilterMenu->menuAction());
  streamLFPFiltersOn = new QAction(this);
  streamLFPFiltersOn->setCheckable(true);
  streamLFPFiltersOn->setChecked(false);
  streamSpikeFiltersOn = new QAction(this);
  streamSpikeFiltersOn->setCheckable(true);
  streamSpikeFiltersOn->setChecked(true);
  streamNoFiltersOn = new QAction(this);
  streamNoFiltersOn->setCheckable(true);
  streamNoFiltersOn->setChecked(false);
  streamFilterMenu->addAction(streamLFPFiltersOn);
  streamFilterMenu->addAction(streamSpikeFiltersOn);
  streamFilterMenu->addAction(streamNoFiltersOn);
  actionSound = new QAction(this);
  menuDisplay->addAction(actionSound);
  actionDisplayAllPETHs = new QAction(this);
  actionDisplayAllPETHs->setText("Open All PETH plots");
  menuDisplay->addAction(actionDisplayAllPETHs);

  connect(actionDisplayAllPETHs, &QAction::triggered, this, &MainWindow::openallpeths);

  connect(actionSetTLength0_2,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength0_5,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength1_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength2_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength5_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  QObject::connect(actionShowCurrentTrode,SIGNAL(triggered()),this,SLOT(openTrodeWindow()));
  QObject::connect(streamLFPFiltersOn, SIGNAL(triggered()), this, SLOT(lfpFiltersOn()));
  QObject::connect(streamSpikeFiltersOn, SIGNAL(triggered()), this, SLOT(spikeFiltersOn()));
  QObject::connect(streamNoFiltersOn, &QAction::triggered, this, &MainWindow::noFiltersOn);
  if (isAudioOn)
    QObject::connect(actionSound, SIGNAL(triggered()), this, SLOT(openSoundDialog()));

  //--------------------------------------------------------
  //Settings menu
  menuSettings = new QMenu;
  menuBar()->addAction(menuSettings->menuAction());

  //nTrode settings menu-------------------------------------------
  menuNTrode = new QMenu;
  menuSettings->addAction(menuNTrode->menuAction());
  //mainMenuBar->addAction(menuNTrode->menuAction());
//  menuLinkChanges = new QMenu;
//  menuNTrode->addAction(menuLinkChanges->menuAction());
//  actionLinkChanges = new QAction(this);
//  actionLinkChanges->setCheckable(true);
//  actionLinkChanges->setChecked(false);
//  actionUnLinkChanges = new QAction(this);
//  actionUnLinkChanges->setCheckable(true);
//  actionUnLinkChanges->setChecked(true);
//  menuLinkChanges->addAction(actionUnLinkChanges);
//  menuLinkChanges->addAction(actionLinkChanges);
  linkChangesBool = false;

  clearAllNTrodes = new QAction(this);
  menuNTrode->addAction(clearAllNTrodes);

  menuHeadstage = new QMenu;
  menuSettings->addAction(menuHeadstage->menuAction());
  actionHeadstageSettings = new QAction(this);
  actionHeadstageSettings->setEnabled(false);
  menuHeadstage->addAction(actionHeadstageSettings);
  actionSettleDelay = new QAction(this);
  menuHeadstage->addAction(actionSettleDelay);



//  QObject::connect(actionUnLinkChanges, SIGNAL(triggered()), this, SLOT(unLinkChanges()));
//  QObject::connect(actionLinkChanges, SIGNAL(triggered()), this, SLOT(linkChanges()));
  QObject::connect(clearAllNTrodes, SIGNAL(triggered()), this , SLOT(clearAll()));
  connect(actionHeadstageSettings,SIGNAL(triggered()),this,SLOT(openHeadstageDialog()));
  connect(actionSettleDelay, SIGNAL(triggered()),this,SLOT(openSettleChannelDelayDialog()));
  //-----------------------------------------------------

  //Select menu ----------------------------------------
  menuSelect = new QMenu;
  menuBar()->addAction(menuSelect->menuAction());
  actionSelectAll = new QAction(this);
  actionSelectAll->setShortcut(QKeySequence(tr("Ctrl+A")));
  actionSelectByTag = new QAction(this);
  actionSelectByTag->setShortcut(QKeySequence(tr("Ctrl+F")));
  menuSelect->setTitle("Select");
  actionSelectAll->setText("All");
  actionSelectByTag->setText("By Grouping Tag");
  menuSelect->setEnabled(true);
  actionSelectAll->setEnabled(true);
  actionSelectByTag->setEnabled(true);
  menuSelect->addAction(actionSelectAll);
  menuSelect->addAction(actionSelectByTag);

  connect(actionSelectAll, SIGNAL(triggered(bool)), this, SLOT(selectAllNTrodes()));
  connect(actionSelectByTag, SIGNAL(triggered(bool)), this, SLOT(openSelectionDialog()));

  //----------------------------------------------------

  //Debug menu -----------------------------------------
  menuDebug = new QMenu;
  menuBar()->addAction(menuDebug->menuAction());
  actionBenchmarkingSettings = new QAction(this);
  actionBenchmarkingSettings->setShortcut(QKeySequence(tr("Ctrl+B")));
  addAction(actionBenchmarkingSettings);
  menuDebug->setTitle("Debug");
  actionBenchmarkingSettings->setText("Benchmarking");
  menuDebug->setEnabled(true);
  actionBenchmarkingSettings->setEnabled(true);
  menuDebug->addAction(actionBenchmarkingSettings);

  connect(actionBenchmarkingSettings, SIGNAL(triggered()), this, SLOT(openBenchmarkingDialog()));

  //----------------------------------------------------

  //Help menu-----------------------------------------

  actionAbout = new QAction(this);
  actionAbout->setMenuRole(QAction::AboutRole);
  actionAboutQT = new QAction(this);
  actionAboutQT->setMenuRole(QAction::AboutQtRole);
  actionAboutHardware = new QAction(this);
  actionAboutHardware->setMenuRole(QAction::ApplicationSpecificRole);
  actionAboutVersion = new QAction(this);
  actionAboutVersion->setMenuRole(QAction::ApplicationSpecificRole);
  actionAboutVersion->setText("Check for updates");

  menuHelp = new QMenu;
  menuBar()->addAction(menuHelp->menuAction());
  //mainMenuBar->addAction(menuHelp->menuAction());
  menuHelp->addAction(actionAbout);
  menuHelp->addAction(actionAboutQT);
  menuHelp->addAction(actionAboutHardware);
  menuHelp->addAction(actionAboutVersion);

  QObject::connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));
  QObject::connect(actionAboutHardware, SIGNAL(triggered()), this, SLOT(aboutHardware()));
  QObject::connect(actionAboutQT, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
  QObject::connect(actionAboutVersion, SIGNAL(triggered()), this, SLOT(aboutVersion()));

  //----------------------------------------------------



  //Layouts and tabs-----------------------------------------
  mainLayout =  new QGridLayout();
  mainLayout->setContentsMargins(QMargins(10,0,10,0));
  mainLayout->setVerticalSpacing(3);
  TrodesFont dispFont;
  tabs = new QTabWidget(this);
  tabs->setAttribute(Qt::WA_NoSystemBackground);
  //tabs->addTab(eventTabs,tr("nTrodes"));
  //tabs->addTab(eegDisp,tr("Streaming"));
  tabs->setStyleSheet("QTabWidget::pane {margin: 0px}") ;
  tabs->setTabPosition(QTabWidget::West);
  tabs->setFont(dispFont);

  mainLayout->addWidget(tabs,3,0);

//  tabs->addTab(splashScreen, "Intro");
//  tabs->setTab



  //---------------------------------------------------------

  //Top control panel setup----------------------------------
  headerLayout = new QGridLayout(); //contains the buttons and clock at the top of the screen
  headerLayout->setContentsMargins(QMargins(1,1,1,1));
  headerLayout->setHorizontalSpacing(3);



  int totalRightItems = 10;
  int totalLeftItems = 3;
  //Sound settings button
  soundSettingsButton = new TrodesButton;
  soundSettingsButton->setText(tr("Audio"));
  //soundSettingsButton->setFont(buttonFont);
  soundSettingsButton->setCheckable(false);
  if (isAudioOn)
    connect(soundSettingsButton,SIGNAL(pressed()),this,SLOT(soundButtonPressed()));
  soundSettingsButton->setFixedSize(70,20);
  headerLayout->addWidget(soundSettingsButton,0,totalLeftItems+9);

  trodeSettingsButton = new TrodesButton;
  trodeSettingsButton->setText(tr("nTrode"));
  //trodeSettingsButton->setFont(buttonFont);
  trodeSettingsButton->setCheckable(true);
  connect(trodeSettingsButton,SIGNAL(toggled(bool)),this,SLOT(trodesButtonToggled(bool)));
  trodeSettingsButton->setFixedSize(70,20);
  headerLayout->addWidget(trodeSettingsButton,0,totalLeftItems+8);

  spikesButton = new TrodesButton;
  spikesButton->setText(tr("Spikes"));
  //spikesButton->setFont(buttonFont);
  spikesButton->setCheckable(false);
  connect(spikesButton,SIGNAL(clicked()),this,SLOT(spikesButtonPressed()));
  spikesButton->setFixedSize(70,20);
  headerLayout->addWidget(spikesButton,0,totalLeftItems+7);

  videoButton = new TrodesButton;
  videoButton->setText(tr("Video"));
  //videoButton->setFont(buttonFont);
  videoButton->setCheckable(false);
  connect(videoButton,SIGNAL(clicked()),this,SLOT(videoButtonPressed()));
  videoButton->setFixedSize(70,20);
  headerLayout->addWidget(videoButton,0,totalLeftItems+6);

  statescriptButton = new TrodesButton;
  statescriptButton->setText(tr("StateScript"));
  //statescriptButton->setFont(buttonFont);
  statescriptButton->setCheckable(false);
  connect(statescriptButton,SIGNAL(clicked()),this,SLOT(statescriptButtonPressed()));
  statescriptButton->setFixedSize(80,20);
  headerLayout->addWidget(statescriptButton,0,totalLeftItems+5);


  commentButton = new TrodesButton;
  commentButton->setText(tr("Annotate"));
  //commentButton->setFont(buttonFont);
  commentButton->setCheckable(false);
  connect(commentButton,SIGNAL(pressed()),this,SLOT(commentButtonPressed()));
  commentButton->setFixedSize(70,20);
  headerLayout->addWidget(commentButton,0,totalLeftItems+4);


//  linkChangesButton = new TrodesButton;
//  linkChangesButton->setText(tr("Link nTrodes"));
//  //linkChangesButton->setFont(buttonFont);
//  linkChangesButton->setCheckable(true);
//  connect(linkChangesButton,SIGNAL(toggled(bool)),this,SLOT(linkChanges(bool)));
//  linkChangesButton->setFixedSize(90,20);
//  headerLayout->addWidget(linkChangesButton,0,totalLeftItems+3);

  uncoupleDisplayButton = new TrodesButton;
  uncoupleDisplayButton->setText(tr("Freeze display"));
  //linkChangesButton->setFont(buttonFont);
  uncoupleDisplayButton->setCheckable(true);
  connect(uncoupleDisplayButton,SIGNAL(toggled(bool)),this,SLOT(uncoupleDisplay(bool)));
  uncoupleDisplayButton->setFixedSize(90,20);
  headerLayout->addWidget(uncoupleDisplayButton,0,totalLeftItems+2);

  streamDisplayButton = new TrodesButton;
  streamDisplayButton->setText(tr("Stream display"));
  connect(streamDisplayButton, &TrodesButton::pressed, this, &MainWindow::streamdisplaybuttonPressed);
  streamDisplayButton->setFixedSize(90,20);
  headerLayout->addWidget(streamDisplayButton, 0, totalLeftItems+1);

  //Time display
  QTime mainClock(0,0,0,0);
  QFont labelFont;
  labelFont.setPixelSize(20);
  labelFont.setFamily("Console");
  timeLabel  = new QLabel;
  timeLabel->setText(mainClock.toString(timeFormatString));
  timeLabel->setFont(labelFont);
  timeLabel->setMinimumWidth(100);
  timeLabel->setAlignment(Qt::AlignLeft);
  pullTimer = new QTimer(this);
  connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
  pullTimer->start(100); //update timer every 100 ms
  headerLayout->addWidget(timeLabel,0,totalRightItems+totalLeftItems);

    //Record, pause, and play buttons
    recordButton = new TrodesButton;
    pauseButton = new TrodesButton;
    playButton = new TrodesButton;
    //Using QT Resource System
    QPixmap playPixmap(":/buttons/playImage.png");
    QPixmap pausePixmap(":/buttons/pauseImage.png");
    QPixmap recordPixmap(":/buttons/recordImage.png");

    QIcon recordButtonIcon(recordPixmap);
    QIcon pauseButtonIcon(pausePixmap);
    QIcon playButtonIcon(playPixmap);
    recordButton->setIcon(recordButtonIcon);
    recordButton->setRedDown(true);
    pauseButton->setIcon(pauseButtonIcon);
    playButton->setIcon(playButtonIcon);
    recordButton->setIconSize(QSize(15, 15));
    pauseButton->setIconSize(QSize(10, 10));
    playButton->setIconSize(QSize(15, 15));
    recordButton->setFixedSize(50, 20);
    pauseButton->setFixedSize(50, 20);
    playButton->setFixedSize(50, 20);
    recordButton->setToolTip(tr("Record"));
    pauseButton->setToolTip(tr("Pause"));
    playButton->setToolTip(tr("Play file"));
    recordButton->setEnabled(false);
    pauseButton->setEnabled(false);
    playButton->setEnabled(false);
    //recordButton->setCheckable(true);
    //pauseButton->setCheckable(true);
    //playButton->setCheckable(true);
    headerLayout->addWidget(recordButton, 0, 0);
    headerLayout->addWidget(pauseButton, 0, 1);
    headerLayout->addWidget(playButton, 0, 2);
    connect(recordButton, SIGNAL(pressed()), this, SLOT(recordButtonPressed()));
    connect(pauseButton, SIGNAL(pressed()), this, SLOT(pauseButtonPressed()));
    connect(playButton, SIGNAL(pressed()), this, SLOT(playButtonPressed()));
    connect(recordButton, SIGNAL(released()), this, SLOT(recordButtonReleased()));
    connect(pauseButton, SIGNAL(released()), this, SLOT(pauseButtonReleased()));
    connect(playButton, SIGNAL(released()), this, SLOT(playButtonReleased()));


    //--------------------------------------------------------------------
    playbackStartTime = 0;
    playbackEndTime = 0;
    playbackStartTimeLabel = new QLabel("--:--:--.---");
    playbackEndTimeLabel = new QLabel("--:--:--.---");
    playbackSlider = new QSlider(Qt::Horizontal);
    playbackLayout = new QGridLayout();

    labelFont.setPixelSize(15);

    playbackStartTimeLabel->setFont(labelFont);
    playbackEndTimeLabel->setFont(labelFont);
    playbackStartTimeLabel->setMinimumWidth(100);
    playbackEndTimeLabel->setMinimumWidth(100);

    playbackSlider->setMinimum(0);
    playbackSlider->setMaximum(1000);
    playbackSlider->setTracking(false);
    playbackSlider->setTickInterval(1);

    playbackLayout->setContentsMargins(QMargins(1,1,1,1));
    playbackLayout->setHorizontalSpacing(3);
    playbackLayout->addWidget(playbackStartTimeLabel, 0, 0);
    playbackLayout->addWidget(playbackSlider, 0, 1);
    playbackLayout->addWidget(playbackEndTimeLabel, 0, 2);
    playbackLayout->setColumnStretch(1, 1);

    playbackSlider->setEnabled(false);
    playbackStartTimeLabel->setEnabled(false);
    playbackEndTimeLabel->setEnabled(false);
    playbackSlider->setVisible(false);
    playbackStartTimeLabel->setVisible(false);
    playbackEndTimeLabel->setVisible(false);

    mainLayout->addLayout(playbackLayout, 2,0);

//    connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateSlider()));
    connect(playbackSlider, SIGNAL(sliderPressed()), this, SLOT(sliderisPressed()));
    connect(playbackSlider, SIGNAL(sliderReleased()),this, SLOT(sliderIsReleased()));
    connect(playbackSlider, SIGNAL(actionTriggered(int)), this, SLOT(movingSlider(int)));
    connect(playbackSlider, SIGNAL(sliderMoved(int)), this, SLOT(updateTimeFromSlider(int)));
//    connect(playbackSlider, SIGNAL(valueChanged(int)), this, SLOT(jumpFileTo(int)));
//    connect()

    //--------------------------------------------------------------------

    //File name label

    QGridLayout* headerLayout2 = new QGridLayout(); //contains the buttons and clock at the top of the screen
    headerLayout2->setContentsMargins(QMargins(1,1,1,1));
    headerLayout2->setHorizontalSpacing(3);
    fileLabel = new QLabel;
    fileLabel->setMinimumWidth(200);
    QString FileLabelColor("gray");
    QString FileLabelText("No file open");
    fileLabel->setFont(dispFont);
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileString = fileLabelTextTemplate.arg(FileLabelColor, FileLabelText);
    fileLabel->setText(fileString);
    //fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
    //headerLayout->addWidget(fileLabel,0,3);
    headerLayout2->addWidget(fileLabel, 0, 0);

    fileStatusColorIndicator = new QLabel;
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    //setStyleSheet("border:2px solid grey; border-radius: 5px;background-color: transparent;");
    //fileStatusColorIndicator->setFrameStyle(QFrame::Panel | QFrame::);
    //fileStatusColorIndicator->setLineWidth(2);
    fileStatusColorIndicator->setFont(dispFont);
    fileStatusColorIndicator->setAlignment(Qt::AlignCenter);
    fileStatusColorIndicator->setVisible(false);
    //QPalette palette;
    //palette.setColor( backgroundRole(), QColor( 0, 0, 255 ) );
    //palette.setColor( foregroundRole(), QColor( 0, 0, 255 ) );
    //palette.setColor(QPalette::Background,Qt::red);
    //fileStatusColorIndicator->setPalette( palette );
    //fileStatusColorIndicator->setAutoFillBackground( true );
    headerLayout2->addWidget(fileStatusColorIndicator, 0, 1);

    streamStatusColorIndicator = new QLabel;
    streamStatusColorIndicator->setStyleSheet("QLabel { background-color : white; color : black; border-radius: 5px}");
    streamStatusColorIndicator->setFixedWidth(200);
    streamStatusColorIndicator->setFont(dispFont);
    streamStatusColorIndicator->setText("STATUS: No source");
    streamStatusColorIndicator->setAlignment(Qt::AlignCenter);
//    streamStatusColorIndicator->setVisible(true);
    statusErrorBlinkTimer = new QTimer();
    connect(statusErrorBlinkTimer,SIGNAL(timeout()),this,SLOT(errorBlinkTmrFunc()));
    headerLayout2->addWidget(streamStatusColorIndicator, 0, 2);

    headerLayout->setColumnStretch(totalLeftItems, 1);
    headerLayout2->setColumnStretch(1, 1);
    mainLayout->addLayout(headerLayout, 0, 0);
    mainLayout->addLayout(headerLayout2,1,0);
    //---------------------------------------------

    //when the state of the source stream changes, the menus need to reflect that
    sourceControl = new SourceController(NULL);
    connect(sourceControl, SIGNAL(stateChanged(int)), this, SLOT(setSourceMenuState(int)));
    connect(sourceControl,SIGNAL(setTimeStamps(uint32_t,uint32_t)), this, SLOT(setTimeStampLabels(uint32_t,uint32_t)));
    connect(sourceControl, SIGNAL(updateSlider(qreal)), this, SLOT(updateSlider(qreal)));
    connect(sourceControl,SIGNAL(packetSizeError(bool)),this,SLOT(sourcePacketSizeError(bool)));
    if (isAudioOn) {
        soundOut = new AudioController();
        soundOut->setChannel(-1); //set the audio channel to listen to
        //soundOut->updateAudio();
        connect(this, SIGNAL(setAudioChannel(int)), soundOut, SLOT(setChannel(int)));
        connect(this, SIGNAL(updateAudio()), soundOut, SLOT(updateAudio()));
        //connect(this,SIGNAL(endAllThreads()),soundOut,SLOT(endAudio()));
        connect(this, SIGNAL(endAudioThread()),soundOut,SLOT(endAudio()));
        connect(sourceControl, SIGNAL(acquisitionStarted()), soundOut, SLOT(startAudio()));
        connect(sourceControl, SIGNAL(acquisitionStopped()), soundOut, SLOT(stopAudio()));
        connect(sourceControl, SIGNAL(acquisitionPaused()), soundOut, SLOT(stopAudio()));
    }
//    connect(playbackSlider, SIGNAL(sliderPressed()),sourceControl, SIGNAL(acquisitionPaused()));
//    connect(playbackSlider, SIGNAL(sliderReleased()), sourceControl, SIGNAL(acquisitionStarted()));
    connect(this, SIGNAL(jumpFileTo(qreal)), sourceControl, SIGNAL(jumpFileTo(qreal)));
    connect(sourceControl, SIGNAL(headstageSettingsReturned(HeadstageSettings)),this, SLOT(headstageSettingsChanged(HeadstageSettings)));
    connect(sourceControl, SIGNAL(controllerSettingsReturned(HardwareControllerSettings)),this, SLOT(controllerSettingsChanged(HardwareControllerSettings)));

    //MARK: Cur

    tabsBackground = new BackgroundFrame(tabs);
    tabsBackground->setVisible(true);
    tabsBackground->raise();



    //splash intro screen

    splashScreen = new TrodesSplashScreen(this);
    connect(splashScreen,SIGNAL(sig_createNewWorkspaceButtonPressed()),this,SLOT(raiseWorkspaceGui()));
    connect(splashScreen,SIGNAL(sig_loadTemplateWorkspace(QString)),this,SLOT(loadFileInWorkspaceGui(QString)));
    connect(splashScreen,SIGNAL(sig_loadWorkspaceButtonPressed()),this,SLOT(loadConfig()));
    connect(splashScreen,SIGNAL(sig_loadWorkspace(QString)),this,SLOT(loadConfig(QString)));
    connect(splashScreen,SIGNAL(sig_loadWorkspace(QString)),this,SLOT(saveWorkspacePathToSystem(QString)));
    connect(splashScreen,SIGNAL(sig_loadFileForPlaybackButtonPressed()),this,SLOT(openPlaybackFile()));
    connect(splashScreen,SIGNAL(sig_initializeFilePlayback(QString)),this,SLOT(openPlaybackFile(QString)));
    connect(splashScreen,SIGNAL(sig_initializeFilePlayback(QString)),this,SLOT(savePlaybackFilePathToSystem(QString)));
    connect(splashScreen,SIGNAL(sig_fadeAnimationCompleted()),this,SLOT(setBackgroundFrameVisibility()));
    connect(this, &MainWindow::gotversion, splashScreen, &TrodesSplashScreen::versionChecker);
    splashScreen->initializeAnimations();
    splashScreen->setVisible(true);
    splashScreen->raise();
    isSplashScreenVisible = true;

    //mark: loading screen -- disabled for now because it won't work without significant rewrite of loadConfig (onto separate thread)
//    loadingScreen = new LoadingScreen(this);
//    connect(this,SIGNAL(sig_fadeInLoadscreen()),loadingScreen,SLOT(fadeIn()));
//    connect(this,SIGNAL(sig_fadeOutLoadscreen()),loadingScreen,SLOT(fadeOut()));
//    lsThread = new QThread();
//    lsThread->setObjectName("LoadingScreenThread");
//    connect(lsThread, SIGNAL(finished()), loadingScreen, SLOT(deleteLater()));
////    connect(this, SIGNAL(endAllThreads()), lsThread, SLOT(quit()));
//    loadingScreen->moveToThread(lsThread);
//    lsThread->start();

    //workspaceGUI
    workspaceEditor = new WorkspaceEditorDialog();
    connect(workspaceEditor,SIGNAL(sig_openTempWorkspace(QString)),this,SLOT(openTempWorkspace(QString)));
    connect(this, SIGNAL(closeAllWindows()), workspaceEditor, SLOT(deleteLater()));

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);


    setCentralWidget(window);
    QMetaObject::connectSlotsByName(this);
    retranslateUi();


    //check if multiple instances of trodes are open
    checkMultipleInstances("Warning: An active Trodes server host was detected.  You will not be able to open a workspace file until closing all other instances of Trodes.");


    //create the benchmarking control panel
    benchmarkingControlPanel = new BenchmarkWidget();

    checkForUpdate();

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();

    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
        resize(tempPosition.width(),tempPosition.height());

    }
    settings.endGroup();
}

MainWindow::~MainWindow()
{
    if (triggerSettings != NULL)
        delete triggerSettings;

    quitModules();
}

void MainWindow::retranslateUi()
{
    setWindowTitle(QApplication::translate("Main", "Trodes", 0));
    //menuFile->setTitle(QApplication::translate("Main", "File", 0));
    menuConfig->setTitle(QApplication::translate("Main", "Workspace", 0));
    actionLoadConfig->setText(QApplication::translate("Main", "Open...", 0));
    actionCloseConfig->setText(QApplication::translate("Main", "Close", 0));
    actionSaveConfig->setText(QApplication::translate("Main", "Save workspace as...", 0));
    actionReConfig->setText(QApplication::translate("Main", "Reconfigure...", 0));
    actionOpenRecordDialog->setText(QApplication::translate("Main", "New recording...", 0));
    actionPlaybackOpen->setText(QApplication::translate("Main", "Playback...", 0));
    actionCloseFile->setText(QApplication::translate("Main", "Close file", 0));
    actionExport->setText(QApplication::translate("Main", "Export...", 0));
    actionRecord->setText(QApplication::translate("Main", "Record", 0));
    actionPause->setText(QApplication::translate("Main", "Pause", 0));
    actionPlay->setText(QApplication::translate("Main", "Play file", 0));
    menuSimulationSource->setTitle(QApplication::translate("Main", "Simulation", 0));
    menuSpikeGadgetsSource->setTitle(QApplication::translate("Main", "SpikeGadgets", 0));
    actionSourceNone->setText(QApplication::translate("Main", "None", 0));
    actionSourceUSB->setText(QApplication::translate("Main", "USB", 0));
    actionSourceRhythm->setText(QApplication::translate("Main", "Rhythm", 0));
    actionSourceFake->setText(QApplication::translate("Main", "Signal generator", 0));
    actionSourceFakeSpikes->setText(QApplication::translate("Main", "Signal generator (w/spikes)", 0));
    actionSourceFile->setText(QApplication::translate("Main", "&File", 0));
    actionSourceEthernet->setText(QApplication::translate("Main", "Ethernet", 0));
    //actionReconfigure->setText(QApplication::translate("Main", "Reconfigure...", 0));
    actionConnect->setText(QApplication::translate("Main", "Stream from source", 0));
    actionDisconnect->setText(QApplication::translate("Main", "Disconnect", 0));
    actionClearBuffers->setText(QApplication::translate("Main", "Clear buffers", 0));
    actionSendSettle->setText(QApplication::translate("Main", "Settle amplifiers", 0));

    actionOpenGeneratorDialog->setText(QApplication::translate("Main", "Generator controls...", 0));
    actionSound->setText(QApplication::translate("Main", "Audio window", 0));
    actionRestartModules->setText(QApplication::translate("Main", "Restart modules", 0));

    actionQuit->setText(QApplication::translate("Main", "Exit", 0));

    actionAbout->setText(QApplication::translate("Main", "About", 0));
    actionAboutHardware->setText(QApplication::translate("Main", "About Hardware", 0));
    actionAboutQT->setText(QApplication::translate("Main", "About QT", 0));

    //menuSystem->setTitle(QApplication::translate("Main", "Connection", 0));
    sourceMenu->setTitle(QApplication::translate("Main", "Source", 0));
    menuHelp->setTitle(QApplication::translate("Main", "Help", 0));
    //menuDisplay->setTitle(QApplication::translate("Main", "Display", 0));
    menuEEGDisplay->setTitle(QApplication::translate("Main", "Streaming", 0));
    menuSetTLength->setTitle(QApplication::translate("Main", "Trace length", 0));
    actionSetTLength0_2->setText(QApplication::translate("Main", "0.2 Seconds", 0));
    actionSetTLength0_5->setText(QApplication::translate("Main", "0.5 Seconds", 0));
    actionSetTLength1_0->setText(QApplication::translate("Main", "1.0 Seconds", 0));
    actionSetTLength2_0->setText(QApplication::translate("Main", "2.0 Seconds", 0));
    actionSetTLength5_0->setText(QApplication::translate("Main", "5.0 Seconds", 0));
    streamFilterMenu->setTitle(QApplication::translate("Main", "Filters", 0));
    streamLFPFiltersOn->setText(QApplication::translate("Main", "LFP filters", 0));
    streamSpikeFiltersOn->setText(QApplication::translate("Main", "Spike filters", 0));
    streamNoFiltersOn->setText(QApplication::translate("Main", "No filters", 0));


    menuSettings->setTitle(QApplication::translate("Main", "Settings", 0));
    menuNTrode->setTitle(QApplication::translate("Main", "nTrodes", 0));
    actionShowCurrentTrode->setText(QApplication::translate("Main", "nTrode trigger window", 0));
//    menuLinkChanges->setTitle(QApplication::translate("Main", "Link changes", 0));
//    actionLinkChanges->setText(QApplication::translate("Main", "Link changes across nTrodes", 0));
//    actionUnLinkChanges->setText(QApplication::translate("Main", "Change settings independently", 0));
    actionUncoupleDisplay->setText(QApplication::translate("Main", "Freeze display", 0));

    clearAllNTrodes->setText(QApplication::translate("Main", "Clear all scatterplots", 0));
    menuHeadstage->setTitle(QApplication::translate("Main", "Headstage", 0));
    actionHeadstageSettings->setText(QApplication::translate("Main", "Settings...", 0));
    actionSettleDelay->setText(QApplication::translate("Main", "Settle delay...", 0));


}

void MainWindow::checkForUpdate(){
    //Check for latest version. If no internet access or network reply, dont continue.
    //https://forum.qt.io/topic/50200/solved-qnetworkaccessmanager-crash-related-to-ssl/7
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("MainWindow"));
    int updatechecksuccess = settings.value(QLatin1String("updatecheck"), -1).toInt();
    if(updatechecksuccess == 0){
        //Trodes didn't check successfully last time, skip.
        qDebug() << "Trodes could not check for an update!";
        return;
    }

    //Set update check success to be false first, so that if trodes crashes then it knows next time to skip
    settings.setValue(QLatin1String("updatecheck"), 0);

    //Check for update
    QLoggingCategory::setFilterRules("qt.network.ssl.warning=false");
    QNetworkAccessManager manager;
    if(manager.networkAccessible()==1){
        QNetworkRequest request = QNetworkRequest(QUrl("http://www.spikegadgets.com/software/latest_trodes_version.xml"));
        request.setRawHeader( "User-Agent" , "Mozilla Firefox" );
        QNetworkReply *response = manager.get(request);
        QEventLoop event;
        QTimer timeout;
        timeout.start(1000);
        connect(response,SIGNAL(finished()),&event,SLOT(quit()));
        connect(&timeout, SIGNAL(timeout()), &event, SLOT(quit()));
        event.exec();
        if(manager.networkAccessible()==1 && response->error() == QNetworkReply::NoError){
            QString str = QString(response->readAll());
            QDomDocument xml;
            xml.setContent(str);
            QString version = xml.elementsByTagName("VersionInfo").at(0).toElement().attribute("version");
            QString versioncpy(version);
            //If version is empty, something went wrong with getting the info. No warning will be made to the user
            if(version.replace(".", "").toInt() > TRODES_VERSION && !version.isEmpty()){
                isLatestVersion = false;
            }
            else{
                isLatestVersion = true;
            }
            emit gotversion(isLatestVersion, versioncpy);
        }
        delete response;
    }
    //Set value back to 1
    settings.setValue(QLatin1String("updatecheck"), 1);
}
void MainWindow::startThreads()
{
    //Threads are started after the main window is created and showing

    //Audio thread
    if (isAudioOn) {
        QThread* audioThread = new QThread();

        connect(audioThread, SIGNAL(started()), soundOut, SLOT(startAudio()));
        connect(soundOut, SIGNAL(finished()), audioThread, SLOT(quit()));
        connect(soundOut, SIGNAL(finished()), soundOut, SLOT(deleteLater()));
        connect(audioThread, SIGNAL(finished()), audioThread, SLOT(deleteLater()));
        soundOut->moveToThread(audioThread);
        audioThread->start();
    }
}

void MainWindow::settingsWarning(){
    if(configChanged){
        QMessageBox::StandardButton answer = QMessageBox::question(
                    0, "Settings were changed",
                    "Warning! You have unsaved changes to your nTrode settings. Would you like to save them in a Trodes config file (.trodesconf)? ",
                    QMessageBox::Yes|QMessageBox::No);
        //messageBox.setFixedSize(500,200);
        if (answer == QMessageBox::Yes) {
            saveConfig();
        }
        else {
            //"No" or messagebox closed
            //Do nothing extra
        }
        configChanged = false;
    }
}

void MainWindow::saveConfig()
{
    QString fname = "";
    if(configChanged){
        if(recordFileOpen){
            fname = recordFileName;
        }
        else if(playbackFileOpen){
            fname = playbackFile;
        }
        else{
            fname = currentConfigFileName;
        }
    }
    else{
        fname = currentConfigFileName;
    }
    saveConfig(fname);
}

void MainWindow::saveConfig(QString fname){
    //qDebug() << QDir::currentPath();
    //qDebug() << QCoreApplication::applicationDirPath();
    //qDebug() << currentConfigFileName;

    QFileInfo fI = QFileInfo(fname);
    QString configPath = fI.absolutePath();
    QString configDefaultName = fI.completeBaseName();


    QStringList filenames;
    QString filename;
    QFileDialog dialog(this, "Save configuration as");

    //dialog.selectFile(QString("test.xml"));
    dialog.setDirectory(configPath);
    dialog.setDefaultSuffix("trodesconf");
    dialog.selectFile(configDefaultName);
    //dialog.selectFile(configDefaultName);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setOption(QFileDialog::DontConfirmOverwrite, false);
    //dialog.setOption(QFileDialog::DontUseNativeDialog);
    if (dialog.exec()) {
        filenames = dialog.selectedFiles();
    }
    if (filenames.size() == 1) {
        filename = filenames.first();
    }
    if (!filename.isEmpty()) {
        if (!writeTrodesConfig(filename)) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Unable to save file.");
            messageBox.setFixedSize(500, 200);
        }
        else {
            configChanged = false;
        }
    }
}

void MainWindow::closeWorkspace() {
    //this function is a wrapper for closeConfig(), used to allow us to customize certain behavior
    //In this case, we want the closeWorkspace command to close a playbackFile and the workspace config if a .rec file is open in Trodes
    if (playbackFileOpen) {
        closeFile();
        return;
    }
    closeConfig();
}

void MainWindow::closeConfig(bool openSplashScreen)
{  
    if (channelsConfigured) //only save this setting if a config had been open when closeConfig is called
        saveTrodeSettingPanelVisible(trodeSettingsButton->isChecked());

    trodeSettingsButton->setChecked(false);
    //If ntrode Settings have been changed since opening the file, ask if they want to save the trodes config
    settingsWarning();
    if (openSplashScreen) {
        swapSplashScreenAndTabView(true);
    }

    if (channelsConfigured) {

        quitModules();


        disconnectFromSource();

        if (isAudioOn)
            soundOut->setChannel(-1); //set the audio channel to -1 (off)

        //this if statement prevents an infinite loop from occuring if switching from playback source
        if (!playbackFileOpen) {
            setSource(SourceNone);
        }


        //delete the trodesNet->tcpServer if it was started

        emit endAllThreads();
        //recordOut->deleteLater(); //We need to add the proper thread shutdown to the record thread (currently
                                    //thread quitting in endAllThreads()

        if (triggerSettings != NULL) {
            delete triggerSettings;
            triggerSettings = NULL;
        }

        //remove each display tab
        while (tabs->count() > 0) {
            tabs->removeTab(0);
        }


        delete eegDisp;



        if (sdDisp != NULL) {
            delete sdDisp;
            sdDisp = NULL;
        }


        //delete soundOut;





        //Mark: to do - this is a direct call to end separate threads, causing race conditions
        streamManager->removeAllProcessors();




        delete streamManager;



        //streamManager->deleteLater();

        delete spikeDisp;
        spikeDisp = NULL;



        //ntrodeDisplayWidgetPtrs.clear();

        delete nTrodeTable;
        delete streamConf;
        delete spikeConf;
        delete headerConf;
        delete moduleConf;
        delete networkConf;




        networkConf=NULL;
        moduleConf=NULL;
        nTrodeTable=NULL;
        streamConf=NULL;
        spikeConf=NULL;
        headerConf=NULL;
        //hardwareConf=NULL;
        //globalConf=NULL;


        //delete hardwareConf;




        //sourceControl->clearBuffers();

        channelsConfigured = false;

        actionCloseConfig->setEnabled(false);
        actionSaveConfig->setEnabled(false);
        actionReConfig->setEnabled(false);
        actionLoadConfig->setEnabled(true);
        actionAboutConfig->setEnabled(false);
        actionHeadstageSettings->setEnabled(false);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        actionOpenRecordDialog->setEnabled(false);
        actionShowCurrentTrode->setEnabled(false);
        currentConfigFileName = "";
    }

}

void MainWindow::reConfig()
{
    int returnVal = -1;
    //Save the current workspace to tempWorkspace
     QString tempFilePath = QString("%1/tempWorkspace.trodesconf").arg(QCoreApplication::applicationDirPath());
    if (writeTrodesConfig(tempFilePath)) { //save current settings to the tempFile
        configChanged = false; // the save was successful, so no unsaved changes
        if (currentConfigFileName != "") {
            closeConfig(false);
        }
        workspaceEditor->loadFileIntoWorkspaceGui(tempFilePath);
        returnVal = workspaceEditor->openReconfigEditor();
    }
    else {
        qDebug() << "Error: saving tempFile failed. (MainWindow::reConfig)";
    }

    if (returnVal != QDialog::Accepted) { //if the user pressed cancel, open the old workspace
        loadConfig(tempFilePath);
    }
}

void MainWindow::loadConfig()
{
//    if (currentConfigFileName != "") {
//        closeConfig(false); //automatically close configuration files if they are open, also don't bring up splashScreen
//    }
    QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
//    if (!unitTestFlag && checkMultipleInstances(multInstError))
//        return;

    //Used the saved system settings from the last session as the default folder
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));
    QStringList tempPathList = settings.value(QLatin1String("prevWorkspacePaths")).toStringList();
    QString tempPath;
    if(tempPathList.isEmpty())
        tempPath = QDir::currentPath();
    else
        tempPath = tempPathList.first();
    settings.endGroup();

    QString fileName = QFileDialog::getOpenFileName(this, QString("Open configuration file"), tempPath, "Trodes config files (*.trodesconf *.xml)");
    if (!fileName.isEmpty()) {
        //Save the folder in system setting for the next session
        QFileInfo fi(fileName);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("configPath"), fi.absoluteFilePath());
        settings.endGroup();
        //save the configuration path the previously used workspaces list
        saveWorkspacePathToSystem(fi.absoluteFilePath());
        //Load the config file
        loadConfig(fileName);

    }
}

int MainWindow::loadConfig(QString fileName)
{
    //mark: loading screen
//    if (currentConfigFileName != "") {
//        emit sig_fadeInLoadscreen();
////        loadingScreen->fadeIn();
//    }

    closeConfig(false); //automatically close configuration files if they are open, also don't bring up splashScreen
    // This is used to load a configuration and create all the control/display widgets
    // according to the settings in the config file
    QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
    if (!unitTestFlag && checkMultipleInstances(multInstError)) {
        swapSplashScreenAndTabView(true); //return to the splash screen if load failed
        return -1;
    }
//    splashScreen->hide();
    loadedConfigFile = fileName;
    if (!channelsConfigured) {
        //Read the config .xml file; the second argument specifies that this is being called within trodes
        int parseCode = nsParseTrodesConfig(fileName);
        if (parseCode < 0) {
            //Show error dialog and abort load
            showErrorMessage(lastDebugMsg);
            //Make sure the user can still try to load another file
            splashScreen->setCanLoadFile(true);
            return parseCode;
        } else if (parseCode == 100) {
            //Show a warning but don't abort load.
            showWarningMessage(lastDebugMsg);
        }
        currentConfigFileName = fileName;
        qDebug() << "[MainWindow::loadConfig] Number of ntrodes" << nTrodeTable->ntrodes.length();
        qDebug() << "[MainWindow::loadConfig] Number of ntrodes (spikeConf)" << spikeConf->ntrodes.length();

        if (unitTestFlag) {
            unitTestMode = true;
        } else {
            unitTestMode = false;
        }

        configChanged = false;

        //Set up the stream controller
        streamManager = new StreamProcessorManager(0);
        connect(streamManager, SIGNAL(bufferOverrun()), this, SLOT(bufferOverrunHandler())); //In case data rates are too fast, use an emergency stop signal
        connect(streamManager, SIGNAL(sourceFail_Sig(bool)), sourceControl, SLOT(noDataComing(bool)));
        connect(streamManager,SIGNAL(sourceFail_Sig(bool)),this,SLOT(sourceNoDataError(bool)));
        connect(streamManager,SIGNAL(headstageFail_Sig(bool)),this,SLOT(sourceNoHeadstageError(bool)));
        connect(this, SIGNAL(newTraceLength(double)), streamManager, SLOT(updateDataLength(double)));
        //All connections between streamconf/spikeconf and streammanager is done in streammanager's constructor
        connect(spikeConf, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
        connect(spikeConf, SIGNAL(changeAllThresh(int)), this, SLOT(setAllThresh(int)));
        connect(sourceControl, SIGNAL(acquisitionStarted()), streamManager, SLOT(startAcquisition()));
        connect(sourceControl, SIGNAL(acquisitionStopped()), streamManager, SLOT(stopAcquisition()));

        connect(streamManager, SIGNAL(functionTriggerRequest(int)), sourceControl, SLOT(sendFunctionTriggerCommand(int)));
        qDebug() << "[MainWindow::loadConfig] Set up stream manager";
        // connect the signal from the server that we need a  data stream sent to a module to the slot that
        // will cause the StreamProcessor thread to start the appropriate data client
        //connect(TrodesServer, SIGNAL(newContDataClient(quint8)), streamManager, SLOT(startContDataClient(qunit8)));
        //connect(TrodesServer, SIGNAL(newSpikeDataClient(quint8)), streamManager, SLOT(startSpikeDataClient(qunit8)));
        //connect(TrodesServer, SIGNAL(newHeaderDataClient(quint8)), streamManager, SLOT(startHeaderDataClient(qunit8)));
        //Stream display setup---------------------------



        eegDisp = new StreamDisplayManager(0, streamManager);
        spikeFiltersOn();

        int columnCount = 0;
        for (int tnum = 0; tnum < eegDisp->eegDisplayWidgets.length(); tnum++) {

            int columnsOnPage = eegDisp->columnsPerPage[tnum];
            if (!eegDisp->isHeaderDisplayPage[tnum]) {
                tabs->addTab(eegDisp->eegDisplayWidgets[tnum], QString("Tr ") + QString("%1").arg(eegDisp->nTrodeIDs[columnCount].first()) + "-" + QString("%1").arg(eegDisp->nTrodeIDs[columnCount + columnsOnPage - 1].last()));
            } else {
                if (eegDisp->isHeaderDisplayPage[tnum]) {
                    tabs->addTab(eegDisp->eegDisplayWidgets[tnum], QString("Aux"));

                    //tabs->setTabText(tnum, QString("Aux  ") + QString("%1").arg(eegDisp->streamDisplayChannels[columnCount].first() + 1) + "-" + QString("%1").arg(eegDisp->streamDisplayChannels[columnCount + columnsOnPage - 1].last() + 1));
                    //tabs->setTabText(tnum, QString("Aux"));
                }
            }
            columnCount += columnsOnPage;
        }
        qDebug() << "[MainWindow::loadConfig] Set up stream display";

        //RFDisplay
        bool rfConfigured = false;
        if (rfConfigured) {
            //sdDisp = new SDDisplay();
            sdDisp = new SDDisplayPanel();
            connect(sdDisp,SIGNAL(connectionRequest()),sourceControl,SLOT(connectToSDCard()));
            connect(sdDisp,SIGNAL(cardEnableRequest()),sourceControl,SLOT(enableSDCard()));
            connect(sdDisp,SIGNAL(reconfigureRequest(int)),sourceControl,SLOT(reconfigureSDCard(int)));
            connect(sourceControl,SIGNAL(SDCardStatus(bool,int,bool,bool)),sdDisp,SLOT(updateSDCardStatus(bool,int,bool,bool)));
            tabs->addTab(sdDisp,"SD card");
            //sdDisp->addPanel();
            //rfDisp->addPanel();

            qDebug() << "[MainWindow::loadConfig] Set up RF display";
        }


        //As of qt5.1.1, you have to switch between the tabs in order for the mousePressEvent callback in the glWidgets to work.
        tabs->setCurrentIndex(eegDisp->eegDisplayWidgets.length() - 1);
        tabs->setCurrentIndex(0);

        //------------------------------------------------
        //connect(this, SIGNAL(setAudioChannel(int)), eegDisp, SLOT(updateAudioHighlightChannel(int)));
        connect(eegDisp, SIGNAL(trodeSelected(int)), this, SLOT(selectTrode(int)));
        connect(eegDisp, SIGNAL(nTrodeClicked(int,Qt::KeyboardModifiers)), this, SLOT(nTrodeClicked(int,Qt::KeyboardModifiers)));
        connect(eegDisp, SIGNAL(sig_rightClickAction(QString)), this, SLOT(processNTrodeRightClickAction(QString)));
        if (isAudioOn)
            connect(eegDisp, SIGNAL(streamChannelClicked(int)),this,SLOT(audioChannelChanged(int)));
        connect(eegDisp, SIGNAL(newPSTHTrigger(int,bool)),streamManager,SLOT(setPSTHTrigger(int,bool)));
        connect(eegDisp, SIGNAL(newSettleControlChannel(int,quint8,quint8)),this,SLOT(setSettleControlChannel(int,quint8,quint8)));
        connect(eegDisp, SIGNAL(setNewMaxDisp(int)), this, SLOT(setMaxDisp(int)));



        /*
        spikeDisp = new SpikeDisplayWidget(NULL, &ntrodeDisplayWidgetPtrs);
        connect(spikeDisp, SIGNAL(resetAudioButtons()), this, SLOT(resetAllAudioButtons()));
        //connect(spikeDisp, SIGNAL(setAudioChannel(int)), this, SLOT(audioChannelChanged(int)));
        connect(spikeDisp, SIGNAL(setAudioChannel(int)), this, SIGNAL(setAudioChannel(int)));
        connect(spikeDisp, SIGNAL(updateAudio()), this, SIGNAL(updateAudio()));
        //connect(spikeDisp, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
        //connect(spikeDisp, SIGNAL(changeAllThresh(int)), this, SLOT(setAllThresh(int)));
        connect(spikeDisp, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
        connect(spikeDisp, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
        connect(spikeDisp, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
        connect(spikeDisp, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
        connect(spikeDisp, SIGNAL(nTrodeWindowClosed(int)), this, SLOT(removeFromOpenNtrodeList(int)));
        */
        if (hardwareConf->NCHAN > 0 && spikeConf->ntrodes.length() > 0) {
            spikeDisp = new MultiNtrodeDisplayWidget(NULL);
            spikeDisp->setStreamManagerPtr(streamManager);

            //spikeDisp->setWindowFlags(Qt::WindowStaysOnTopHint);
            //spikeDisp->show();
            singleTriggerWindowOpen = true;
            if (isAudioOn) {
                connect(eegDisp, SIGNAL(streamChannelClicked(int)),spikeDisp,SLOT(changeAudioChannel(int)));
                //connect(spikeDisp, SIGNAL(resetAudioButtons()), this, SLOT(resetAllAudioButtons()));
                connect(spikeDisp, SIGNAL(channelClicked(int)), this, SLOT(audioChannelChanged(int)));
                connect(spikeDisp,SIGNAL(channelClicked(int)),eegDisp,SLOT(updateAudioHighlightChannel(int)));
            }
            connect(spikeConf,SIGNAL(newMaxDisplay(int,int)),spikeDisp,SLOT(setMaxDisplay(int,int)));
            connect(spikeDisp, SIGNAL(sig_newMaxDisp(int)), this, SLOT(setMaxDisp(int)));
            connect(this, &MainWindow::openallpeths, spikeDisp, &MultiNtrodeDisplayWidget::openAllNTrodePETHs);
            //connect(spikeDisp, SIGNAL(updateAudio()), this, SIGNAL(updateAudio()));
            //connect(spikeDisp, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
            //connect(spikeDisp, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
            //connect(spikeDisp, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
            //connect(spikeDisp, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
            //connect(spikeDisp, SIGNAL(nTrodeWindowClosed(int)), this, SLOT(removeFromOpenNtrodeList(int)));


            qDebug() << "[MainWindow::loadConfig] Set up spike trigger display";
        }


        // Set up connections for spikeDetectors
        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
            connect(streamManager->spikeDetectors[i],
                    SIGNAL(spikeDetectionEvent(int, const QVector<int2d>*, const int*, uint32_t)),
                    spikeDisp, SLOT(receiveNewEvent(int,const QVector<int2d>*,const int*,uint32_t)), Qt::DirectConnection); //This might not be thread-safe. Needs to be double checked
        }

       //connect(eegDisp, SIGNAL(streamChannelClicked(int)), spikeDisp, SLOT(relayChangeAudioSignal(int)));

        //Audio thread setup--------------------------
        if (hardwareConf->NCHAN > 0 && spikeConf->ntrodes.length() > 0 && isAudioOn) {
            soundOut->setChannel(spikeConf->ntrodes[0]->hw_chan[0]); //set the audio channel to listen to
            soundOut->updateAudio();
        }
        //---------------------------------------------

        //Record thread setup---------------------------

        recordOut = new RecordThread();
        QThread* workerThread = new QThread();
        recordOut->moveToThread(workerThread);
        connect(workerThread, SIGNAL(started()), recordOut, SLOT(setUp()));
        connect(recordOut, SIGNAL(finished()), workerThread, SLOT(quit()));
        connect(recordOut,SIGNAL(finished()), recordOut, SLOT(deleteLater()));
        connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
        workerThread->setObjectName("Recorder");
        workerThread->start();

        connect(this, SIGNAL(endAllThreads()), recordOut, SLOT(endRecordThread()));
        // update the list of channels to save once a configuration file has been loaded
        connect(this, SIGNAL(configFileLoaded()), recordOut, SLOT(setupSaveDisplayedChan()));
        connect(recordOut,SIGNAL(writeError()),this,SLOT(errorSaving()));

        //----------------------------------------------


        channelsConfigured = true;
        actionLoadConfig->setEnabled(true);
        actionCloseConfig->setEnabled(true);
        actionSaveConfig->setEnabled(true);
        actionReConfig->setEnabled(true);
        actionAboutConfig->setEnabled(true);
        if (sourceControl->currentSource > 0) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        //actionOpenRecordDialog->setEnabled(true);
        actionShowCurrentTrode->setEnabled(true);

        // start the local TCP server


        startMainNetworkMessaging();


        // set up the network connections with the modules if modules are defined
        // set myID to -1 to indicate that this is trodes
        moduleConf->myID = TRODES_ID;
        startModules(fileName);
        //mark: restartMod

        //MARK: Event

        connect(spikeDisp, SIGNAL(broadcastEvent(TrodesEventMessage)),this,SLOT(broadcastEvent(TrodesEventMessage)));
        connect(this, SIGNAL(sendEvent(uint32_t,int,QString)),trodesNet->tcpServer, SLOT(eventOccurred(uint32_t,int,QString)));
        connect(spikeDisp, SIGNAL(broadcastNewEventReq(QString)), trodesNet->tcpServer, SLOT(addEventTypeToList(QString)));

        connect(sourceControl, &SourceController::newTimestamp, this, &MainWindow::seekPlaybackSignal, Qt::UniqueConnection);


        connect(spikeDisp, SIGNAL(broadcastEventRemoveReq(QString)), trodesNet->tcpServer, SLOT(removeEventTypeFromList(QString)));


        connect(sourceControl, SIGNAL(newTimestamp(uint32_t)), trodesNet->tcpServer, SLOT(sendCurTimeToModules(uint32_t)));
        // update the list of channels to save for the record thread

        emit configFileLoaded();

        swapSplashScreenAndTabView(false);

        uncoupleDisplay(false); //Make sure display is not in frozen mode

        //only read benchmarking settings from the config file if the user hasn't edited benchConf via either the GUI or cmdLine yet
        if (benchConfig != NULL && !benchConfig->wasEditedByUser() && !benchConfig->wasInitiatedFromCommandLine())
            benchmarkingControlPanel->getSettingsFromConfig();

        //mark: Load Screen fade out
//        emit sig_fadeOutLoadscreen();
//        loadingScreen->fadeOut();

        //open triggerSettings panel if it had previously been open
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("trodeSettingsPanel"));
        bool wasOpen = settings.value(QLatin1String("wasOpen")).toBool();
        settings.endGroup();
        if (wasOpen)
            trodeSettingsButton->setChecked(true);

        if (hardwareConf->NCHAN > 0)
            nTrodeClicked(0, Qt::NoModifier);

        return parseCode;
    }
    else {
        //mark: Load Screen fade out
//        emit sig_fadeOutLoadscreen();
//        loadingScreen->fadeOut();
        return -1;
    }
}

void MainWindow::startModules(QString configFileName)
{
    if (moduleConf->singleModuleConf.length() && !playbackFileOpen) {

        SingleModuleConf s;
        // launch the modules and start the TCPIP server to each one as we do so. This is slower than launching
        // all the modules at once, but allows us to keep their connections in order

        for (int i = 0; i < moduleConf->singleModuleConf.length(); i++) {
            s = moduleConf->singleModuleConf[i];

            // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
            QProcess *moduleProcess = new QProcess(this);
            connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
            connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

            trodesNet->startSingleModule(s, moduleProcess);
        }
    }
    // enable the Restart Modules menu item
    actionRestartModules->setEnabled(true);
}


void MainWindow::restartCrashedModule(QString modName) {
    if (modName == "Camera") {
        videoButtonPressed();
    }
}

void MainWindow::forwardProcessOutput() {
    QProcess* senderProcess = (QProcess*)sender();

    qDebug().noquote() << senderProcess->readAllStandardError();
}

void MainWindow::openSettleChannelDelayDialog() {

    bool ok;
    int newDelay = QInputDialog::getInt(0, "Enter settle delay after trigger",
                "Delay (samples):", settleChannelDelay, 0, 30000,1,&ok);
    if (ok) {
        setSettleChannelDelay(newDelay);
    }
}

void MainWindow::setSettleChannelDelay(int delay) {
    settleChannelDelay = delay;
    sourceControl->sendSettleChannel(settleChannelByteInPacket,settleChannelBit,settleChannelDelay,settleChannelTriggerState);
}

void MainWindow::setSettleControlChannel(int byteInPacket, quint8 bit, quint8 triggerState) {

    settleChannelByteInPacket = byteInPacket;
    settleChannelBit = bit;
    settleChannelTriggerState = triggerState;
    //settleChannelDelay = 0;
    sourceControl->sendSettleChannel(byteInPacket,bit,settleChannelDelay,triggerState);
}

void MainWindow::startMainNetworkMessaging(void)
{
    // Create the global trodes module network object
    trodesNet = new TrodesModuleNetwork();


    // Set the ID to the specified ID for the main program.
    trodesNet->moduleID = TRODES_ID;
    trodesNet->tcpServer = new TrodesServer();

    //The server is able to respond to small data requests from the modules.
    //Here is where we give the server access to the possible data types requested.
    //So far just time.
    trodesNet->tcpServer->setModuleTimePtr(&currentTimeStamp);
    connect(this, SIGNAL(recordFileOpened(QString)), trodesNet->tcpServer, SLOT(sendFileOpened(QString)));
    connect(this, SIGNAL(sourceConnected(QString)), trodesNet->tcpServer, SLOT(sendSourceConnect(QString)));
    connect(this, SIGNAL(recordFileClosed()), trodesNet->tcpServer, SLOT(sendFileClose()));
    connect(this, SIGNAL(recordingStarted()), trodesNet->tcpServer, SLOT(sendStartRecord()));
    connect(this, SIGNAL(recordingStopped()), trodesNet->tcpServer, SLOT(sendStopRecord()));
    connect(this, SIGNAL(endAllThreads()), trodesNet->tcpServer, SLOT(deleteServer()));
    connect(trodesNet->tcpServer, SIGNAL(trodesServerError(QString)), this, SLOT(threadError(QString)));
    connect(trodesNet->tcpServer,SIGNAL(settleCommandTriggered()),this,SLOT(sendSettleCommand()));

    //MARK: cur

    //Connection to update all modules whenever the user edits the benchmark settings
    connect(this->benchmarkingControlPanel, SIGNAL(signal_benchConfigUpdated()), trodesNet->tcpServer, SLOT(sendBenchConfigToModules()));

    //Connections for file playback, communicate with other modules playing back files
    connect(trodesNet->tcpServer,SIGNAL(playbackCommandReceived(qint8,qint32)),this,SLOT(processPlaybackCommand(qint8,qint32)));
    connect(this, SIGNAL(signal_sendPlaybackCommand(qint8,qint32)), trodesNet->tcpServer, SLOT(sendPlaybackCommand(qint8,qint32)));


    //Here is where we set up the connections for when a module is
    //asking for a dedicated data stream.  Individual thread (such as
    //the spike triggering threads) will then stream data to the socket.
    //connect(trodesNet->tcpServer,SIGNAL(newAnalogIODataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newAnalogIOHandler(TrodesSocketMessageHandler*,qint16)));
    //connect(trodesNet->tcpServer,SIGNAL(newDigitalIODataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newDigitalIOHandler(TrodesSocketMessageHandler*,qint16)));
    //connect(trodesNet->tcpServer,SIGNAL(newContinuousDataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newContinuousHandler(TrodesSocketMessageHandler*,qint16)));
    //connect(trodesNet->tcpServer,SIGNAL(newSpikeDataSocket(TrodesSocketMessageHandler*,qint16)),streamManager, SLOT(newNTrodeTriggerHandler(TrodesSocketMessageHandler*,qint16)));

    // connect slots related to data exchange among modules.  These just pass signals from a message handler through
    // tcpServer and on to trodesNet
    connect(trodesNet->tcpServer, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)), trodesNet, SLOT(sendAllDataAvailableToModule(TrodesSocketMessageHandler*)));
    connect(trodesNet->tcpServer, SIGNAL(doAddDataAvailable(DataTypeSpec*)), trodesNet, SLOT(addDataAvailable(DataTypeSpec*)));
    connect(trodesNet->tcpServer, SIGNAL(doRemoveDataAvailable(qint8)), trodesNet, SLOT(removeDataAvailable(qint8)));
    connect(trodesNet->tcpServer, SIGNAL(nameReceived(TrodesSocketMessageHandler*,QString)), trodesNet->tcpServer,
            SLOT(setNamedModuleMessageHandler(TrodesSocketMessageHandler*,QString)));


    connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), this, SLOT(setModuleDataStreaming(bool)));
    connect(this, SIGNAL(messageForModules(TrodesMessage*)), trodesNet->tcpServer, SLOT(sendMessageToModules(TrodesMessage*)));
//    connect(this, SIGNAL(clearMessageHandler()), trodesNet->tcpServer, SLOT(clearModuleMessageHandlers()));
    connect(this, SIGNAL(clearDataAvailable()), trodesNet, SLOT(clearDataAvailable()));

    connect(trodesNet, SIGNAL(messageForModule(TrodesSocketMessageHandler*, TrodesMessage*)),
            trodesNet->tcpServer, SLOT(sendMessageToModule(TrodesSocketMessageHandler*, TrodesMessage*)));

    connect(trodesNet->tcpServer, SIGNAL(restartCrashedModule(QString)), this, SLOT(restartCrashedModule(QString)));


    // add a signal for internal DataAvailable information to be added to the main list
    connect(streamManager, SIGNAL(addDataProvided(DataTypeSpec*)), trodesNet, SLOT(addDataAvailable(DataTypeSpec*)));


    //check Connection status


    // set the port for communication to the hardware
    if ((networkConf->networkConfigFound) && (networkConf->trodesHost != "")) {
        trodesNet->tcpServer->setAddress(networkConf->trodesHost);
    }
    if ((networkConf->networkConfigFound) && (networkConf->trodesPort != 0)) {
        trodesNet->tcpServer->startServer("Trodes main", networkConf->trodesPort);
    }
    else {
        //otherwise just find an available address and port
        //localhost is the last option if nothing else is available
        trodesNet->tcpServer->startServer("Trodes main");
        // set the host name and the port
        networkConf->trodesHost = QHostInfo::localHostName();
        networkConf->trodesPort = trodesNet->tcpServer->serverPort();

    }

    qDebug() << "[MainWindow::startMainNetworkMessaging] trodesHost =" << networkConf->trodesHost << "port =" << networkConf->trodesPort;

    //Move the trodesNet object to a separate thread so that networking can go on outside the GUI thread

    trodesNetThread = new QThread;
    //connect(trodesNetThread,SIGNAL(started()),trodesNet->tcpServer,SLOT());
    connect(trodesNet->tcpServer, SIGNAL(finished()), trodesNetThread, SLOT(quit()));
    connect(trodesNet->tcpServer, SIGNAL(finished()), trodesNet->tcpServer, SLOT(deleteLater()));
    trodesNet->tcpServer->moveToThread(trodesNetThread);
    connect(trodesNetThread, SIGNAL(finished()), trodesNetThread, SLOT(deleteLater()));
    trodesNetThread->start();
}

void MainWindow::sourceNoDataError(bool on) {
    //Error when no data is coming from hardware
    noDataErrFlag = on;
    updateErrorBlink();
}

void MainWindow::sourceNoHeadstageError(bool on) {
    //Error when no data is coming from hardware
    noHeadstageErrFlag = on;
    updateErrorBlink();
}

void MainWindow::sourcePacketSizeError(bool on) {
    //Error when the incoming packet size does not match what is expected from the workspace
    packetErrFlag = on;
    updateErrorBlink();
}

void MainWindow::updateErrorBlink() {
    //Used to update the STATUS indicator text and red blinking state

    if (!playbackFileOpen) {
        if (!packetErrFlag && !noDataErrFlag && !noHeadstageErrFlag) {
            toggleErrorBlink(false);
            streamStatusColorIndicator->setText("STATUS: Receiving stream ok");
        } else if (packetErrFlag) {
            streamStatusColorIndicator->setText("ERROR: Wrong incoming packet size");
            toggleErrorBlink(true);
        } else if (noDataErrFlag) {
            streamStatusColorIndicator->setText("ERROR: No data being received");
            toggleErrorBlink(true);

        } else if (noHeadstageErrFlag) {
            streamStatusColorIndicator->setText("ERROR: Headstage data empty");
            toggleErrorBlink(true);

        }
    }
}

void MainWindow::toggleErrorBlink(bool blink) {
    //Used to toggle the STATUS indicator red blinking state

    statusBlinkRed = false;
    if (blink) {
        statusErrorBlinkTimer->start(250);
    } else {
        statusErrorBlinkTimer->stop();
        streamStatusColorIndicator->setStyleSheet("QLabel { background-color : white; color : black; border-radius: 5px}");
    }

}

void MainWindow::errorBlinkTmrFunc() {
    if (statusBlinkRed) {
        //change to white
        streamStatusColorIndicator->setStyleSheet("QLabel { background-color : white; color : black; border-radius: 5px}");
        statusBlinkRed = false;
    } else {
        streamStatusColorIndicator->setStyleSheet("QLabel { background-color : red; color : black; border-radius: 5px}");
        statusBlinkRed = true;
    }
}



void MainWindow::audioChannelChanged(int hwchannel)
{
//    if (isAudioOn)
    soundOut->setChannel(hwchannel);
    //emit setAudioChannel(hwchannel);
}

void MainWindow::nTrodeClicked(int nTrodeInd, Qt::KeyboardModifiers mods) {
    int clickedID = spikeConf->ntrodes[nTrodeInd]->nTrodeId;
    int clickedIndex = nTrodeInd;
    bool deselected = false; //this bool tracks whether the clicked nTrode was deselected or selected

    QHashIterator<int, int> prevIter(selectedNTrodes);
    while (prevIter.hasNext()) { //set the labels of the previously selected nTrodes to black
        prevIter.next();
        eegDisp->setNTrodeSelected(prevIter.key(), false); //deselect all previous nTrodes
    }

    if (!(mods & Qt::ControlModifier) && !(mods & Qt::ShiftModifier)) {
        selectedNTrodes.clear(); //if neither control or shift were pressed, overwrite nTrodes
        selectedNTrodes.insert(clickedIndex, clickedID);
    }

    if (mods & Qt::ControlModifier) { //if control clicked
        if (!selectedNTrodes.contains(clickedIndex)) {//if not selected, select it
            selectedNTrodes.insert(clickedIndex, clickedID);
        }
        else {//if previously selected, deselect it
            selectedNTrodes.remove(clickedIndex);
            deselected = true;
        }
    }
    else {//if control was not clicked, overwrite all selected nTrodes
        selectedNTrodes.clear();
    }

    if (mods & Qt::ShiftModifier) { //if shift clicked
//        qDebug() << "Select all between " << spikeConf->ntrodes[prevSelectedNTrodeIndex]->nTrodeId << " and " << clickedID;
        int highInd, lowInd;
        if (prevSelectedNTrodeIndex > clickedIndex) {
            highInd = prevSelectedNTrodeIndex;
            lowInd = clickedIndex;
        }
        else {
            highInd = clickedIndex;
            lowInd = prevSelectedNTrodeIndex;
        }

        //select all nTrodes in between the selected range
        for (int i = lowInd; i <= highInd; i++) {
            int curId = spikeConf->ntrodes[i]->nTrodeId;
            if (!selectedNTrodes.contains(i)) //don't select the same nTrode twice
                selectedNTrodes.insert(i,curId);
        }

    }
    else //only overwrite prev selected nTrode if shift was not clicked
        prevSelectedNTrodeIndex = clickedIndex;


    if (!selectedNTrodes.contains(clickedIndex) && !deselected)
        selectedNTrodes.insert(clickedIndex,clickedID);

    QHashIterator<int, int> iter(selectedNTrodes); //the key is the index and the value is the nTrodeID
    while (iter.hasNext()) { //set the labels of the selected nTrodes to red
        iter.next();
        eegDisp->setNTrodeSelected(iter.key(), true);
    }
    selectedNTrodesUpdated(); //update the ntrode settings panel
}

//This function processes the actions occuring when right clicking one of the StreamWidgetGL
void MainWindow::processNTrodeRightClickAction(QString actionStr) {
    QString lfpAction = "Toggle LFP display for this nTrode";
    QString spikeAction = "Toggle Spike display for this nTrode";
    QString rawAction = "Toggle raw data display for this nTrode";
    QString ntrodeColorAction = "Change nTrode color...";
    QString backgroundColorAction = "Change background color...";

    if (actionStr == "nTrode settings...") {

    } else if (actionStr == ntrodeColorAction) {

        QHashIterator<int, int> iter(selectedNTrodes);
        iter.next();
        int trodeNum = iter.key();
        QColor iniColor = spikeConf->ntrodes.at(trodeNum)->color;
        QColor newColor = QColorDialog::getColor(iniColor, this, "Select New Channel Color");
        if (newColor.isValid()) {
            spikeConf->setColor(trodeNum, newColor);
            emit sig_channelColorChanged(newColor);
            while (iter.hasNext()) {
                iter.next();
                trodeNum = iter.key();
                spikeConf->setColor(trodeNum, newColor);
            }

        }

//        showNtrodeColorSelector(channel);
    } else if (actionStr == backgroundColorAction) {
        QColor newColor = QColorDialog::getColor(streamConf->backgroundColor, this, "Select New Background Color");
        if (newColor.isValid())
            streamConf->setBackgroundColor(newColor);

    } else if(actionStr == lfpAction){
        QHashIterator<int, int> iter(selectedNTrodes);
        while (iter.hasNext()) {
            iter.next();
            int trodeNum = iter.key();
            streamConf->setLFPModeOn(trodeNum, true);
            streamConf->setSpikeModeOn(trodeNum, false);
        }

    } else if(actionStr == spikeAction){
        QHashIterator<int, int> iter(selectedNTrodes);
        while (iter.hasNext()) {
            iter.next();
            int trodeNum = iter.key();
            streamConf->setLFPModeOn(trodeNum, false);
            streamConf->setSpikeModeOn(trodeNum, true);
        }
    } else if(actionStr == rawAction){
        QHashIterator<int, int> iter(selectedNTrodes);
        while (iter.hasNext()) {
            iter.next();
            int trodeNum = iter.key();
            streamConf->setLFPModeOn(trodeNum, false);
            streamConf->setSpikeModeOn(trodeNum, false);
        }
    }
}

void MainWindow::selectTrode(int nTrode)
{
    if (nTrode != currentTrodeSelected) {
        currentTrodeSelected = nTrode;
        if (singleTriggerWindowOpen) {
            spikeDisp->setShownNtrode(currentTrodeSelected);
        }
    }
}

int MainWindow::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    Q_ASSERT(!configFileName.isEmpty());

    file.setFileName(configFileName);
    if (!file.open(QIODevice::ReadOnly)) {
        return -1;
    }

    QFileInfo fi(configFileName);
    QString ext = fi.suffix();
    if (ext.compare("rec") == 0) {
        //this is a rec file with a configuration in the header

        QString configContent;
        QString configLine;
        bool foundEndOfConfig = false;

        while (file.pos() < 1000000) {
            configLine += file.readLine();
            configContent += configLine;
            if (configLine.indexOf("</Configuration>") > -1) {
                foundEndOfConfig = true;
                break;
            }
            configLine = "";
        }

        if (foundEndOfConfig) {
            filePos = file.pos();
        }
    }
    file.close();
    return filePos;
}

bool MainWindow::openPlaybackFile() {
    //Used the saved system settings from the last session as the default folder
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));
    QStringList tempPathList = settings.value(QLatin1String("prevPlaybackPaths")).toStringList();
    QString tempPath;
    if(tempPathList.isEmpty())
        tempPath = QDir::currentPath();
    else
        tempPath = tempPathList.first();
    settings.endGroup();

    QString pFileName = QFileDialog::getOpenFileName(this, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"));
    if (!pFileName.isEmpty()) {
        //Save the folder in system setting for the next session
        QFileInfo fi(pFileName);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("playbackPath"), fi.absoluteFilePath());
        settings.endGroup();
        savePlaybackFilePathToSystem(fi.absoluteFilePath());

        //Load the config file
        return(openPlaybackFile(pFileName));
    }
    return(false);
}

bool MainWindow::openPlaybackFile(const QString fileName)
{
    qDebug() << "[MainWindow::openPlaybackFile] Opening file" << fileName;


    if ((!playbackFileOpen) && (!recordFileOpen)) {
        int filePos;
        bool usingExternalWorkspace = false;

        QFileInfo fI(fileName);

        QString baseName = fI.completeBaseName();
        QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";

        QFileInfo workspaceFile(workspaceCheckName);
        if (workspaceFile.exists()) {
            qDebug() << "[MainWindow::openPlaybackFile] Using the following workspace file: " << workspaceFile.fileName();
            usingExternalWorkspace = true;
            filePos = findEndOfConfigSection(fileName);
        }


        //Open up the configuration settings used when the
        //data were recorded. x current workspace.
        if (channelsConfigured) {
            closeConfig();
        }

        int loadReturn;
        playbackFileOpen = true; //we set this gloval flag before opening the config file, because that way we can use it to stop opening modules
        if (usingExternalWorkspace) {
            loadReturn = loadConfig(workspaceFile.absoluteFilePath());
            if (loadReturn != 0) {
                playbackFileOpen = false;
                return false;
            }
        } else {
           //No external workspace with the same name found, so try to load the settings
           //embedded in the recording file
           filePos = loadConfig(fileName);
        }

        if (filePos < 0) {
            //QMessageBox messageBox;
            //messageBox.critical(0, "Error", "The configuration settings associated with this recording could not be parsed.");
            showErrorMessage("The configuration settings associated with this recording could not be parsed.");

            actionSourceNone->setChecked(true);
            sourceControl->setSource(SourceNone);
            playbackFileOpen = false;
            return false;
        }




        //int packetBytes = 2*hardwareConf->NCHAN+(2*hardwareConf->headerSize)+4;

        //int32_t recTimeInSec = (fI.size()-filePos)/(packetBytes*hardwareConf->sourceSamplingRate);

        actionCloseFile->setEnabled(true);
        playbackFile = fileName;
        fileString = fI.fileName();
        fileLabel->setText(fileString);
        fileString = fI.absoluteFilePath();

        if(playbackFileOpen && !unitTestFlag){
            QFileInfo file(fileName);
            QDir dir = file.absoluteDir();
            QStringList filters;
            filters << "*.h264";

            QFileInfoList files = dir.entryInfoList(filters);
            for (int i = 0; i < files.size(); ++i) {
                QFileInfo fileInfo = files.at(i);
                if(QString::compare(fileInfo.absoluteFilePath().replace(QRegExp("(\\.\\d)*\\.h264"), ".rec"), file.absoluteFilePath()) == 0){
                    videoButtonPressed(fileInfo.absoluteFilePath());
//                    break;
                }
            }
        }

        fileDataPos = filePos;
        actionSourceFile->setChecked(true);
        sourceControl->setSource(SourceFile);
        actionPlay->setEnabled(true);
        playButton->setEnabled(true);
        pauseButton->setEnabled(true);
        actionExport->setEnabled(true);
        actionSourceNone->setChecked(false);
        actionSourceFake->setChecked(false);
        actionSourceFakeSpikes->setChecked(false);
        actionSourceUSB->setChecked(false);
        actionSourceRhythm->setChecked(false);
        actionSourceEthernet->setChecked(false);
        actionOpenGeneratorDialog->setEnabled(false);

        //we could in priciple record data from the playback, but this seems like an odd thing
        //to do.  If we don't allow this, we can use the pause button for both playback and record.
        actionOpenRecordDialog->setEnabled(false);
        swapSplashScreenAndTabView(false);
    }

    return true;
}

void MainWindow::threadError(QString errorString) {
    // show a box with the error message
    if (!unitTestFlag) {
        QMessageBox::warning(this, "Error", errorString);
    }
}

void MainWindow::openRecordDialog()
{
    //dialog to create a new record file
    QString dataDir = globalConf->filePath;
    QString fileName;
    QString defaultFileName;
    QDateTime fileCreateTime = QDateTime::currentDateTime();

    if (!QDir(dataDir).exists()) {
        dataDir = QDir::homePath();
    }
    else {
        dataDir = QDir(dataDir).absolutePath();
    }


    //defaultFileName = globalConf->filePrefix + fileCreateTime.toString("yyyy.MM.dd").replace(".", "") +
    //                  tr("_") + fileCreateTime.toString("hh.mm.ss").replace(".", "") + tr(".rec");

    defaultFileName = globalConf->filePrefix + fileCreateTime.toString("yyyy.MM.dd").replace(".", "") +
                      tr("_") + fileCreateTime.toString("hh.mm.ss").replace(".", "");
    dataDir = QDir().toNativeSeparators(dataDir + "/" + defaultFileName);
    qDebug() << "[MainWindow::openRecordDialog] Default file " << dataDir;


    //This is the name of the folder to be created
    fileName = QFileDialog::getSaveFileName(this, tr("Create File"), dataDir);

    //Find the base name of the folder
    QFileInfo filenameInfo(fileName);
    QString basename = filenameInfo.baseName();

    //Create the folder
    QDir dir;
    dir.mkpath(fileName);

    //The filename will be the same as the folder name
    fileName = fileName + "/" + basename;


    if (!(fileName == "")) {
        if (!fileName.endsWith(".rec")) {
            fileName += ".rec";
        }

        //HeadstageSettings hsSettings = sourceControl->getHeadstageSettings();
        //HardwareControllerSettings hcSettings = sourceControl->getControllerSettings();

        int fileOpenStatus = recordOut->openFile(fileName, controllerSettings, headstageSettings); //creates the file and writes the current config info
        if (fileOpenStatus == -1) {
            QMessageBox::information(0, "error", tr("File already exists. Please rename existing file first."));
            return;
        }
        if (fileOpenStatus == -2) {
            QMessageBox::information(0, "error", tr("Error: File could not be created."));
            return;
        }

        recordFileOpen = true;
        recordFileName = fileName;
        QFileInfo fI(fileName);
        fileString = fI.fileName();
        fileLabel->setText(fileString + tr("   (0 MB)")); //append the current size of the file in the display

        fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
        fileStatusColorIndicator->setText("Recording Paused");
        fileStatusColorIndicator->setVisible(true);
        actionOpenRecordDialog->setEnabled(false);
        actionCloseFile->setEnabled(true);
        actionCloseConfig->setEnabled(false);
        actionLoadConfig->setEnabled(false);
        actionReConfig->setEnabled(false);
        actionRestartModules->setEnabled(false);
        statescriptButton->setEnabled(false);

        if (dataStreaming) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }
        recordTimer = new QTime;
        msecRecorded = 0;
        emit recordFileOpened(fileName);
    }
}

void MainWindow::exportData(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting)
{

    //TODO: many of the inputs are not yet used.  Don't delete!!

    if (playbackFileOpen && !dataStreaming) {
        qDebug() << "[MainWindow::exportData] Exporting: " << "Spikes: " << spikesOn << " ModuleData: " << ModuleDataon;
        QFileInfo fileInfo;
        fileInfo.setFile(playbackFile);
        QString baseName = fileInfo.completeBaseName();
        QDir fileDir = fileInfo.absoluteDir();
        if (spikesOn) {
            QString spikeDirName = baseName + "_Spikes";
            fileDir.mkdir(spikeDirName);
            if (!fileDir.cd(spikeDirName)) {
                qDebug() << "[MainWindow::exportData] Error making spike directory";
                return;
            }
            streamManager->createSpikeLogs(fileDir.absolutePath());
        }
        qDebug() << "[MainWindow::exportData] Path: " << fileDir.path() << " Name: " << fileInfo.completeBaseName();

        disconnectFromSource();
        exportMode = true;
        connectToSource();
    }
}

void MainWindow::cancelExport()
{

    disconnectFromSource();
    exportMode = false;
}

void MainWindow::bufferOverrunHandler()
{
    if (!exportMode) {
        //disconnectFromSource();
        //qDebug() << "Data streaming stopped-- data rate is too fast";
        qDebug() << "[MainWindow::bufferOverrunHandler] Buffer overrun!";
    }
    else {
        sourceControl->waitForThreads();
    }
}

void MainWindow::showWarningMessage(QString msg) {
    //Multi-purpose error dialog
    if (!unitTestFlag) {
        QMessageBox messageBox;
        messageBox.warning(0,"Warning",msg);
        messageBox.setFixedSize(500,200);
    }
}

void MainWindow::showErrorMessage(QString msg) {
    //Multi-purpose error dialog
    if (!unitTestFlag) {
        QMessageBox messageBox;
        messageBox.critical(0,"Error",msg);
        messageBox.setFixedSize(500,200);
    }
}

void MainWindow::errorSaving() {
    //pauseButtonPressed();
    showErrorMessage(QString("Serious error writing to disk.  A brief period of data was lost because the hard drive became unresponive. See debug log for more information. Please report to SpikeGadgets support team."));
}

void MainWindow::closeFile()
{
    if (playbackFileOpen) {
        setSource(SourceNone);
    } else {
        pauseButtonPressed();
        settingsWarning();
        recordOut->closeFile();
        recordFileOpen = false;
        QString FileLabelColor("gray");
        QString FileLabelText("No file open");
        QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
        fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");

        fileStatusColorIndicator->setText("Recording Paused");
        fileStatusColorIndicator->setVisible(false);
        fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
        actionOpenRecordDialog->setEnabled(true);
        actionCloseFile->setEnabled(false);
        actionRecord->setEnabled(false);
        recordButton->setEnabled(false);
        actionPause->setEnabled(false);
        pauseButton->setEnabled(false);
        actionRestartModules->setEnabled(true);
        statescriptButton->setEnabled(true);

        if (channelsConfigured) {
            actionCloseConfig->setEnabled(true);
            actionLoadConfig->setEnabled(true);
        }
        emit recordFileClosed();
    }
}

void MainWindow::recordButtonPressed()
{
    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionRecord->setEnabled(false);
    recordOut->startRecord();
    actionCloseFile->setEnabled(false);
    recording = true;
    actionDisconnect->setEnabled(false);
    statusbar->showMessage(tr("Started recording at ") + calcTimeString(currentTimeStamp));
    qDebug() << statusbar->currentMessage();
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : lightgreen; color : black; border-radius: 5px}");

    fileStatusColorIndicator->setText("Recording Active");
    emit recordingStarted();
    recordTimer->restart();
    videoButton->setEnabled(false);//disable video button when recording
}
void MainWindow::recordButtonReleased()
{
    recordButton->setDown(true);
}
void MainWindow::actionRecordSelected()
{
    recordButton->setDown(true);
    recordButtonPressed();
}


void MainWindow::pauseButtonPressed()
{
    if (recordFileOpen) {
        recordButton->setDown(false);
        actionPause->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionCloseFile->setEnabled(true);
        recordOut->pauseRecord();
        recording = false;
        actionRecord->setEnabled(true);
        statusbar->showMessage(tr("Paused recording at ") + calcTimeString(currentTimeStamp));
        fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
        qDebug() << statusbar->currentMessage();
        fileStatusColorIndicator->setText("Recording Paused");
        emit recordingStopped();
        msecRecorded += recordTimer->elapsed();
        recordTimer->restart();
        videoButton->setEnabled(true);
    }
    else if (playbackFileOpen) {
        actionPause->setEnabled(false);
        actionPlay->setEnabled(true);
        playButton->setDown(false);
        sourceControl->pauseSource();
        actionExport->setEnabled(true);
        emit signal_sendPlaybackCommand(PC_PAUSE, currentTimeStamp);
    }
}
void MainWindow::pauseButtonReleased()
{
    pauseButton->setDown(true);
}
void MainWindow::actionPauseSelected()
{
    pauseButton->setDown(true);
    pauseButtonPressed();
}

void MainWindow::playButtonPressed()
{
    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionPlay->setEnabled(false);
    actionExport->setEnabled(false); //We don't allow exporting when the file is being played back
    filePlaybackSpeed = 1; //Normal speed
    exportMode = false;

    connectToSource();
    emit signal_sendPlaybackCommand(PC_PLAY, currentTimeStamp);
}
void MainWindow::playButtonReleased()
{
    playButton->setDown(true);
}
void MainWindow::actionPlaySelected()
{
    playButton->setDown(true);
    playButtonPressed();
}

void MainWindow::sendSettleCommand() {
    sourceControl->sendSettleCommand();
}

bool MainWindow::disconnectFromSource()
{
    if (recordFileOpen) {
        if (recordOut->getBytesWritten() > 0) {
            //There is a record file open with data in it
            //This needs to be closed
            //If No pressed, then return and ignore closeEvent event

//            QMessageBox messageBox;
//            int answer = messageBox.question(0, "File is open", "The record file will be closed. Proceed?");
            QMessageBox::StandardButton answer = QMessageBox::question(0, "File is open", "Closing Trodes before closing the recording file (File->Close File) may cause unintended behavior. Proceed?",  QMessageBox::Yes|QMessageBox::No);
            //messageBox.setFixedSize(500,200);
            if (answer == QMessageBox::Yes) {
                closeFile();
            }
            else {
                //"No" or messagebox closed
                return false;
            }
        }
    }

    if (playbackFileOpen) {
        actionPlay->setEnabled(true);
        actionPause->setEnabled(false);
        pauseButton->setDown(true);
        playButton->setDown(false);
        actionExport->setEnabled(false);
    }

    //disconnect from source
    sourceControl->disconnectFromSource();
    return true;
}

void MainWindow::connectToSource()
{
    //Pointless routing right now, but probably good to retain this step in case we
    //want menus to change, etc.

    sourceControl->connectToSource();

}

void MainWindow::setSource()
{
    //source selected with menu ( wrapper for setSource(int) )
    QAction* action = (QAction*)sender();

    setSource(action->data().value<DataSource>());
}

void MainWindow::setSource(DataSource source)
{
    //changes the source of the data stream

    if (source != sourceControl->currentSource) {
        //set the menu state
        actionSourceNone->setChecked(false);
        actionSourceFake->setChecked(false);
        actionSourceFakeSpikes->setChecked(false);
        actionSourceFile->setChecked(false);
        actionSourceUSB->setChecked(false);
        actionSourceRhythm->setChecked(false);
        actionSourceEthernet->setChecked(false);
        actionOpenGeneratorDialog->setEnabled(false);

        bool needToClosePlaybackConfig = false;
        if (playbackFileOpen && (source != SourceFile)) {
            needToClosePlaybackConfig = true;

        }

        //Used the saved system settings from the last session as the default folder
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        QString tempPath = settings.value(QLatin1String("playbackPath")).toString();
        settings.endGroup();
        QString pFileName;

        switch (source) {
        case SourceNone:

            actionSourceNone->setChecked(true);
            sourceControl->setSource(source);
            actionSourceNone->setEnabled(true);
            actionSourceFake->setEnabled(true);
            actionSourceFile->setEnabled(true);
            actionPlaybackOpen->setEnabled(true);
            actionSourceFakeSpikes->setEnabled(true);
            actionSourceEthernet->setEnabled(true);
            actionSourceUSB->setEnabled(true);
            actionSourceRhythm->setEnabled(true);
            if (!recordFileOpen) {
                actionCloseFile->setEnabled(false);
            }
            playbackSlider->setVisible(false);
            playbackSlider->setEnabled(false);
            playbackSlider->setValue(0);
            playbackStartTimeLabel->setVisible(false);
            playbackEndTimeLabel->setVisible(false);
            break;

        case SourceFake:
            actionSourceFake->setChecked(true);
            actionOpenGeneratorDialog->setEnabled(true);
            sourceControl->setSource(source);
            break;

        case SourceFakeSpikes:
            actionSourceFakeSpikes->setChecked(true);
            //actionOpenGeneratorDialog->setEnabled(true);
            sourceControl->setSource(source);
            break;

        case SourceFile:


            pFileName = QFileDialog::getOpenFileName(this, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"));
            if (!pFileName.isEmpty()) {
                //Save the folder in system setting for the next session
                QFileInfo fi(pFileName);
                QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
                settings.beginGroup(QLatin1String("paths"));
                settings.setValue(QLatin1String("playbackPath"), fi.absoluteFilePath());
                settings.endGroup();
                savePlaybackFilePathToSystem(fi.absoluteFilePath());

                //Load the config file
                openPlaybackFile(pFileName);
//                playButtonPressed();
//                playButtonReleased();
//                playButton->setDown(false);
//                connectToSource();
            }



            //openPlaybackFile(QFileDialog::getOpenFileName(this, tr("Open file for playback"), "", tr("Rec files (*.rec)")));

            break;

        case SourceEthernet:
            actionSourceEthernet->setChecked(true);
            sourceControl->setSource(source);
            break;

        case SourceUSBDAQ:
            actionSourceUSB->setChecked(true);
            sourceControl->setSource(source);
            break;

        case SourceRhythm:
            actionSourceRhythm->setChecked(true);
            sourceControl->setSource(source);
            break;
        }


        if (needToClosePlaybackConfig) {

            qDebug() << "[MainWindow::setSource] Closing playback config.";
            closeConfig(); //if the source was a file, we close the associated configuration

            QString FileLabelColor("gray");
            QString FileLabelText("No file open");
            QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
            fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));

            actionPlay->setEnabled(false);
            actionPause->setEnabled(false);
            playButton->setEnabled(false);
            pauseButton->setEnabled(false);
            actionExport->setEnabled(false);
        }
    }
}

void MainWindow::setTLength()
{
    QAction* action = (QAction*)sender();

    actionSetTLength0_2->setChecked(false);
    actionSetTLength0_5->setChecked(false);
    actionSetTLength1_0->setChecked(false);
    actionSetTLength2_0->setChecked(false);
    actionSetTLength5_0->setChecked(false);
    action->setChecked(true);
    double value = action->data().toDouble();

    emit newTraceLength(value);
}

void MainWindow::soundButtonPressed()
{
    soundSettingsButton->setDown(false);

    soundDialog *newSoundDialog = new soundDialog(0, 0);
    //if (channelsConfigured) {
    newSoundDialog->threshSlider->setValue(soundOut->getThresh());
    newSoundDialog->gainSlider->setValue(soundOut->getGain());
    //soundDialog *newSoundDialog = new soundDialog(soundOut->getGain(),soundOut->getThresh());
    //}
    newSoundDialog->setWindowFlags(Qt::Popup);

    newSoundDialog->setGeometry(QRect(this->geometry().x()+soundSettingsButton->x(),this->geometry().y()+soundSettingsButton->y()+soundSettingsButton->height()+this->menuBar()->height(),40,200));

/*
    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.
#if defined (__WIN32__)
    newSoundDialog->setGeometry(QRect(this->x() + soundSettingsButton->x() + 8, this->y() + soundSettingsButton->y() + 70, 40, 200));
#else
    newSoundDialog->setGeometry(QRect(this->x() + soundSettingsButton->x(), this->y() + soundSettingsButton->y() + 43, 40, 200));
#endif

*/
    //if (channelsConfigured) {
    connect(newSoundDialog->gainSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setGain(int)));
    connect(newSoundDialog->threshSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setThresh(int)));
    //}
    connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
    connect(this, SIGNAL(closeSoundDialog()), newSoundDialog, SLOT(close()));
    newSoundDialog->show();
}

void MainWindow::selectedNTrodesUpdated() {
//    qDebug() << "nTrode either selected or deslected";
    if (triggerSettings != NULL) {
        triggerSettings->setSelectedNTrodes(selectedNTrodes);
    }
}

void MainWindow::trodesButtonToggled(bool on) {
//    qDebug() << "Trodes button turned to " << on;
    if (channelsConfigured && hardwareConf->NCHAN > 0) { //only execute if there are nTrodes to edit
        trodeSettingsButton->setDown(on);

        if (triggerSettings == NULL) { //create the trigger settings panel if it doesn't already exist
//            qDebug() << "Creating Trigger Settings Panel";
            triggerSettings = new TriggerScopeSettingsWidget(0, currentTrodeSelected);
            connect(triggerSettings, SIGNAL(configChanged()), this, SLOT(settingsChanged()));
            if (isAudioOn) {
                connect(triggerSettings, SIGNAL(updateAudioSettings()), this, SIGNAL(updateAudio()));
            }
            connect(triggerSettings, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
            connect(triggerSettings, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
            connect(triggerSettings, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
            connect(triggerSettings, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
//            connect(triggerSettings, SIGNAL(toggleLinkChanges(bool)), this, SLOT(linkChanges(bool)));
            connect(triggerSettings, SIGNAL(moduleDataChannelChanged(int, int)), this, SLOT(sendModuleDataChanToModules(int, int)));

            connect(triggerSettings, SIGNAL(toggleAllTriggers(bool)), this, SLOT(toggleAllTriggers(bool)));
            connect(triggerSettings, SIGNAL(changeAllThresholds(int)), this, SLOT(setAllThresh(int)));
            connect(triggerSettings, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
            connect(triggerSettings, SIGNAL(openSelectByDialog()), this, SLOT(openSelectionDialog()));
            connect(triggerSettings, &TriggerScopeSettingsWidget::selectAllNtrodes, this, &MainWindow::selectAllNTrodes);

            connect(triggerSettings, SIGNAL(sig_threshUpdated(int)), spikeDisp, SIGNAL(sig_newThreshRecv(int)));
            connect(triggerSettings, SIGNAL(sig_maxDisplayUpdated(int)), spikeDisp, SIGNAL(sig_newMaxDispRecv(int)));

            connect(spikeDisp, SIGNAL(sig_newThresh(int)), triggerSettings, SLOT(setThresh(int)));
            connect(this, SIGNAL(sig_channelColorChanged(QColor)), triggerSettings, SLOT(setChanColorBox(QColor)));

            connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), triggerSettings, SLOT(setEnabledForStreaming(bool)));
            connect(triggerSettings, SIGNAL(closing()), this, SLOT(trodeSettingsPanelClosed()));
            connect(triggerSettings, SIGNAL(saveGeo(QRect)), this, SLOT(saveTrodeSettingPanelPos(QRect)));
            connect(this, SIGNAL(closeAllWindows()), triggerSettings, SLOT(close()));



            //load previous position
            QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

            settings.beginGroup(QLatin1String("trodeSettingsPanel"));
            QRect panelGeo = settings.value(QLatin1String("geometry")).toRect();

            if (panelGeo.height() > 0) { //if the geometry was previously saved, load it
                triggerSettings->setPanelDim(panelGeo.width(), panelGeo.height());
                triggerSettings->setPosition(panelGeo.x(), panelGeo.y());
            }
            else { //otherwise, load default
                triggerSettings->setPanelDim(225, 485);
                triggerSettings->setPositionOffset((this->geometry().width()), (tabs->geometry().y()+fileLabel->geometry().height()));
                triggerSettings->setPosition(this->geometry().x()+this->geometry().width()-triggerSettings->geometry().width(), this->geometry().y() + (tabs->geometry().y()+fileLabel->geometry().height()));
            }
            triggerSettings->attachToWidget(this);
            settings.endGroup();

            if (selectedNTrodes.count() <= 0)
                triggerSettings->loadNTrodeIntoPanel(spikeConf->ntrodes[0]->nTrodeId); //if none selected, default load the first nTrode on startup
        }

        triggerSettings->setSelectedNTrodes(selectedNTrodes);

        triggerSettings->setEnabledForStreaming(isModuleDataStreaming());

//        triggerSettings->setWindowFlags(Qt::Popup);
//        triggerSettings->setGeometry(QRect(this->geometry().x()+trodeSettingsButton->x(),this->geometry().y()+trodeSettingsButton->y()+trodeSettingsButton->height()+this->menuBar()->height(),40,200));

//        triggerSettings->setPosition(this->geometry().x()+this->geometry().width(), this->geometry().y() + (tabs->geometry().y()+fileLabel->geometry().height()));

        triggerSettings->setVisible(on);



    }

}

void MainWindow::trodeSettingsPanelClosed() {
    trodeSettingsButton->setChecked(false);
}

void MainWindow::saveTrodeSettingPanelVisible(bool wasOpen) {
//    qDebug() << "Saving the Trodes Settings Panel's visibility panel, was it open when the config was closed?";
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("trodeSettingsPanel"));
    settings.setValue(QLatin1String("wasOpen"), wasOpen);
    settings.endGroup();

}

void MainWindow::saveTrodeSettingPanelPos(QRect geo) {
//    qDebug() << "Saving the Trode Settings Panel's last location";
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("trodeSettingsPanel"));
    settings.setValue(QLatin1String("geometry"), geo);
    settings.endGroup();
}

void MainWindow::settingsChanged(){
    configChanged = true;
}

void MainWindow::statescriptButtonPressed() {
    //open video window

    if (channelsConfigured) {
        QStringList arglist;
        QString modName;

        int modNum = moduleConf->findModule("stateScript");

        SingleModuleConf s;
        if (modNum > -1) {
            s = moduleConf->singleModuleConf[modNum];
            // launch the module with the main configuration file and the specified configuration file
            qDebug() << "[MainWindow::statescriptButtonPressed] Launching module: " << s.moduleName;
        } else {
            // Try the default location
            s.moduleName = "stateScript";
            s.sendTrodesConfig = 1;
            s.sendNetworkInfo = 1;
            qDebug() << "[MainWindow::statescriptButtonPressed] No stateScript entry in config file, launching: " << s.moduleName;
        }

        // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
        QProcess *moduleProcess = new QProcess(this);
        connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
        connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

        trodesNet->startSingleModule(s, moduleProcess);
    }
}

void MainWindow::videoButtonPressed(QString videoString) {
    //open video window
    //videoButton->setDown(false);
    if (channelsConfigured && !recording) {
        QStringList arglist;
        QString modName;

        int camModNum = moduleConf->findModule("cameraModule");

        SingleModuleConf s;
        if (camModNum > -1) {
            s = moduleConf->singleModuleConf[camModNum];
            // launch the module with the main configuration file and the specified configuration file
            qDebug() << "[MainWindow::videoButtonPressed] Launching module: " << s.moduleName;
        } else {
            // Try the default cameraModule location
            s.moduleName = "cameraModule";
            s.sendTrodesConfig = 1;
            s.sendNetworkInfo = 1;
            qDebug() << "[MainWindow::videoButtonPressed] No cameraModule entry in config file, launching: " << s.moduleName;
        }

        if(playbackFileOpen){
            s.moduleArguments.append("-playback");
            if(videoString.isEmpty()){
                QFileInfo file(playbackFile);
                QDir dir = file.absoluteDir();
                QStringList filters;
                filters << "*.h264";

                QFileInfoList files = dir.entryInfoList(filters);
                for (int i = 0; i < files.size(); ++i) {
                    QFileInfo fileInfo = files.at(i);
                    if(QString::compare(fileInfo.absoluteFilePath().replace(QRegExp("(\\.\\d)*\\.h264"), ".rec"), file.absoluteFilePath()) == 0){
                        videoString = fileInfo.absoluteFilePath();
                        break;
                    }
                }
            }
            s.moduleArguments.append(videoString);
        }

        // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
        QProcess *moduleProcess = new QProcess(this);
        connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
        connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

        trodesNet->startSingleModule(s, moduleProcess);
    }
}

void MainWindow::spikesButtonPressed()
{
    //open spike trigger window
    //spikesButton->setDown(false);
    if (channelsConfigured && hardwareConf->NCHAN > 0) {
        spikeDisp->show();
        spikeDisp->raise();
        //openTrodeWindow();
    }
}


void MainWindow::commentButtonPressed()
{
    commentButton->setDown(false);

    QString commentFileName;
    if (recordFileOpen) {
        QFileInfo fileInfo(recordFileName);
        commentFileName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + ".trodesComments";
    }
    else {
        commentFileName = "";
    }

    CommentDialog *newCommentDialog = new CommentDialog(commentFileName, this);

    newCommentDialog->setWindowFlags(Qt::Popup);
    newCommentDialog->setGeometry(QRect(this->geometry().x()+commentButton->x(),this->geometry().y()+commentButton->y()+commentButton->height()+this->menuBar()->height(),40,200));

/*
    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.
#if defined (__WIN32__)
    newCommentDialog->setGeometry(QRect(this->x() + commentButton->x() + 8, this->y() + commentButton->y() + 70, 40, 200));
#else
    newCommentDialog->setGeometry(QRect(this->x() + commentButton->x(), this->y() + commentButton->y() + 43, 40, 200));
#endif
*/

    connect(this, SIGNAL(closeAllWindows()), newCommentDialog, SLOT(close()));
    if (isAudioOn)
        connect(this, SIGNAL(closeSoundDialog()), newCommentDialog, SLOT(close()));
    newCommentDialog->show();
}

void MainWindow::openHeadstageDialog() {
    //opens the dialog used to control the signal generator for debugging without hardware connected

    headstageSettings = sourceControl->getHeadstageSettings();
    HeadstageSettingsDialog *newDialog = new HeadstageSettingsDialog(headstageSettings);

    newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    //newDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    connect(this, SIGNAL(closeAllWindows()), newDialog, SLOT(close()));
    connect(newDialog, SIGNAL(windowClosed()), this, SLOT(enableHeadstageDialogMenu()));
    connect(newDialog,SIGNAL(newSettings(HeadstageSettings)),sourceControl,SLOT(setHeadstageSettings(HeadstageSettings)));
    //connect(newDialog,SIGNAL(newSettings(HeadstageSettings)),this,SLOT(headstageSettingsChanged(HeadstageSettings)));
    newDialog->show();
    actionHeadstageSettings->setEnabled(false);
}

void MainWindow::headstageSettingsChanged(HeadstageSettings s) {
    qDebug() << "New headstage settings";
    headstageSettings = s;
}

void MainWindow::controllerSettingsChanged(HardwareControllerSettings s) {
    qDebug() << "New controller settings";
    controllerSettings = s;
}

void MainWindow::openGeneratorDialog()
{
    //opens the dialog used to control the signal generator for debugging without hardware connected
    waveformGeneratorDialog *newDialog = new waveformGeneratorDialog(sourceControl->waveGeneratorSource->getModulatorFrequency(),
                                                                     sourceControl->waveGeneratorSource->getFrequency(),
                                                                     sourceControl->waveGeneratorSource->getAmplitude(),
                                                                     sourceControl->waveGeneratorSource->getThreshold());

    newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    newDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    connect(newDialog->modulatorFreqSpinBox, SIGNAL(valueChanged(double)), sourceControl->waveGeneratorSource, SLOT(setModulatorFrequency(double)));
    connect(newDialog->freqSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setFrequency(int)));
    connect(newDialog->ampSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setAmplitude(int)));
    connect(newDialog->threshSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setThreshold(int)));
    connect(this, SIGNAL(closeAllWindows()), newDialog, SLOT(close()));
    connect(this, SIGNAL(closeWaveformDialog()), newDialog, SLOT(close()));
    connect(newDialog, SIGNAL(windowClosed()), this, SLOT(enableGeneratorDialogMenu()));
    newDialog->show();

    actionOpenGeneratorDialog->setEnabled(false);
}

void MainWindow::openTrodeSettingsWindow()
{
    //opens the settings dialog for a selected nTrode
    TriggerScopeSettingsWidget* triggerSettings = new TriggerScopeSettingsWidget(0, currentTrodeSelected);
    //mark: TODO maybe delete this after finished
    triggerSettings->setSelectedNTrodes(selectedNTrodes);
    triggerSettings->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    if (isAudioOn)
        connect(triggerSettings, SIGNAL(updateAudioSettings()), this, SIGNAL(updateAudio()));
    connect(triggerSettings, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
    connect(triggerSettings, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
    connect(triggerSettings, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
    connect(triggerSettings, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
    connect(triggerSettings, SIGNAL(moduleDataChannelChanged(int, int)), this, SLOT(sendModuleDataChanToModules(int, int)));
    connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), triggerSettings, SLOT(setEnabledForStreaming(bool)));

    triggerSettings->setEnabledForStreaming(isModuleDataStreaming());
    triggerSettings->show();
}

void MainWindow::openTrodeWindow()
{
    spikeDisp->setShownNtrode(currentTrodeSelected);
    singleTriggerWindowOpen = true;
    //spikeDisp[0]->ntrodeWidgets[currentTrodeSelected]->setFocusPolicy(Qt::FocusPolicy(0));
}

void MainWindow::removeFromOpenNtrodeList(int nTrodeNum)
{
    //TODO:  if multiple nTrode windows are open, we will use the nTrodeNum input

    if (singleTriggerWindowOpen) {
        singleTriggerWindowOpen = false;
    }
}

void MainWindow::enableHeadstageDialogMenu()
{
    actionHeadstageSettings->setEnabled(true);
}

void MainWindow::enableGeneratorDialogMenu()
{
    actionOpenGeneratorDialog->setEnabled(true);
}

void MainWindow::openExportDialog()
{
    ExportDialog *newExportDialog = new ExportDialog(this);

    newExportDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    newExportDialog->setWindowModality(Qt::WindowModal);
    //this->setEnabled(false);
    connect(newExportDialog, SIGNAL(exportCancelled()), this, SLOT(cancelExport()));
    connect(this, SIGNAL(closeAllWindows()), newExportDialog, SLOT(close()));
    connect(newExportDialog, SIGNAL(startExport(bool, bool, int, int, int, int)), this, SLOT(exportData(bool, bool, int, int, int, int)));

    newExportDialog->show();
}

void MainWindow::openSoundDialog()
{
    //opens the dialog used to control the sound output.
    if (channelsConfigured) {
        soundDialog *newSoundDialog = new soundDialog(soundOut->getGain(), soundOut->getThresh());
        newSoundDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
        connect(newSoundDialog->gainSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setGain(int)));
        connect(newSoundDialog->threshSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setThresh(int)));
        connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
        newSoundDialog->show();
    }
    else { //No config file loaded, so make a dummy sound controller
        soundDialog *newSoundDialog = new soundDialog(30, 30);
        newSoundDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
        connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
        newSoundDialog->show();
    }
}

void MainWindow::setSourceMenuState(int state)
{
    //When the state of the source changes, the state is emitted and this function is called
    //to set the menus
    if (state == SOURCE_STATE_NOT_CONNECTED) {
        if (channelsConfigured && (sourceControl->currentSource > 0)) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
            actionCloseConfig->setEnabled(true);
            actionLoadConfig->setEnabled(true);
            actionReConfig->setEnabled(true);
        }
        else {
            actionHeadstageSettings->setEnabled(false);
            actionConnect->setEnabled(false);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        statusbar->showMessage(tr("Not connected to device"));
        streamStatusColorIndicator->setText("STATUS: No source");
        dataStreaming = false;
        actionOpenRecordDialog->setEnabled(false);
        emit sourceConnected("None");
    }
    else if (state == SOURCE_STATE_CONNECTERROR) {
        //setSource(1);
        qDebug() << "[MainWindow::setSourceMenuState] Connection to source failed.";
        //QMessageBox messageBox;
        //messageBox.critical(0, "Error", "Connection to source failed.");
        //messageBox.setFixedSize(500, 200);

        showErrorMessage("Connection to source failed.");
        streamStatusColorIndicator->setText("STATUS: No source");
        setSource(SourceNone);
    }
    else if (state == SOURCE_STATE_INITIALIZED) {
        if (channelsConfigured) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
            actionCloseConfig->setEnabled(true);
            actionLoadConfig->setEnabled(true);
            actionReConfig->setEnabled(false);

        }
        else {
            actionConnect->setEnabled(false);
        }
        actionOpenRecordDialog->setEnabled(false);
        if (playbackFileOpen) {
            actionPause->setEnabled(false);
            actionPlay->setEnabled(true);
            playButton->setDown(false);
            pauseButton->setDown(true);
            actionExport->setEnabled(true);
            actionSourceFile->setEnabled(false);
            actionPlaybackOpen->setEnabled(false);
        } else {
            actionSourceFile->setEnabled(true);
            actionPlaybackOpen->setEnabled(true);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        statusbar->showMessage(tr("Connected to device. Currently not streaming."));

        //reset error flags
        sourcePacketSizeError(false);
        sourceNoDataError(false);
        sourceNoHeadstageError(false);
        streamStatusColorIndicator->setText("STATUS: Connected to source");

        dataStreaming = false;

        actionSourceNone->setEnabled(true);
        actionSourceFake->setEnabled(true);
        //actionSourceFile->setEnabled(true);
        //actionPlaybackOpen->setEnabled(true);
        actionSourceFakeSpikes->setEnabled(true);
        actionSourceEthernet->setEnabled(true);
        actionSourceUSB->setEnabled(true);
        actionSourceRhythm->setEnabled(true);


        actionRecord->setEnabled(false);
        recordButton->setEnabled(false);

        //Emit connection signal
        //Listeners:
        //TrodesServer -> modules
        switch (sourceControl->currentSource) {
        case SourceFake:
            emit sourceConnected("Generator");
            break;

        case SourceFile:
//            connectToSource();
            emit sourceConnected(QString("File: ") + playbackFile);
            break;

        case SourceEthernet:
            emit sourceConnected("Ethernet");
            break;

        case SourceUSBDAQ:
            emit sourceConnected("USB");
            break;

        case SourceRhythm:
            emit sourceConnected("Rhythm");
            break;

        case SourceFakeSpikes:
            emit sourceConnected("Spikes Generator");
            break;

        case SourceNone:
        default:
            streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
            clearAll(); // clears skipe scatter plots.
            break;
        }

        emit closeWaveformDialog();
        streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
        clearAll(); // clears skipe scatter plots.
        if (quitting) {
            closeEvent(new QCloseEvent());
            return;
        }
    }
    else if (state == SOURCE_STATE_RUNNING) {
        actionLoadConfig->setEnabled(false);
        actionCloseConfig->setEnabled(false);
        actionReConfig->setEnabled(false);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionSendSettle->setEnabled(true);
        actionSourceNone->setEnabled(false);
        actionSourceFake->setEnabled(false);
        actionSourceFakeSpikes->setEnabled(false);
        actionSourceFile->setEnabled(false);
        actionPlaybackOpen->setEnabled(false);
        actionSourceEthernet->setEnabled(false);
        actionSourceUSB->setEnabled(false);
        actionSourceRhythm->setEnabled(false);
        actionHeadstageSettings->setEnabled(false);
        statusbar->showMessage(tr("Data currently streaming from device"));
        streamStatusColorIndicator->setText("STATUS: Receiving stream ok");


        dataStreaming = true;
        //Moved these two to the block above, where state == SOURCE_STATE_INITIALIZED
//        streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
//        clearAll(); // clears skipe scatter plots.

        if (sourceControl->currentSource != SourceFile) {
            actionOpenRecordDialog->setEnabled(true);
        }

        if (recordFileOpen) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }
        if (playbackFileOpen) {
            actionPause->setEnabled(true);
            actionPlay->setEnabled(false);
            actionExport->setEnabled(false);
            playButton->setDown(true);
            pauseButton->setDown(false);
        }
    }
    else if (state == SOURCE_STATE_PAUSED) {
        dataStreaming = false;
    }
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Trodes"), tr(qPrintable(GlobalConfiguration::getVersionInfo())));
}

void MainWindow::aboutHardware()
{
    QString hardwareInfo;

    QString controllerInfo;
    controllerSettings = sourceControl->getControllerSettings();

    if (controllerSettings.valid) {
        controllerInfo = QString("-CONTROLLER-\n\nFirmware version: %1.%2\nSerial number: %3 %4\n\n").arg(controllerSettings.majorVersion).arg(controllerSettings.minorVersion).arg(controllerSettings.modelNumber,5,10,QChar('0')).arg(controllerSettings.serialNumber,5,10,QChar('0'));
    } else {
        controllerInfo = QString("-CONTROLLER-\n\nNone detected\n\n");
    }

    QString headstageInfo;
    headstageSettings = sourceControl->getHeadstageSettings();

    if (headstageSettings.valid) {
        headstageInfo = QString("-HEADSTAGE-\n\nFirmware version: %1.%2\nSerial number: %3 %4\n\n").arg(headstageSettings.majorVersion).arg(headstageSettings.minorVersion).arg(headstageSettings.hsTypeCode,5,10,QChar('0')).arg(headstageSettings.hsSerialNumber,5,10,QChar('0'));
    } else {
        headstageInfo = QString("-HEADSTAGE-\n\nNone detected\n\n");
    }


    hardwareInfo = controllerInfo + headstageInfo;


    QMessageBox messageBox;
    messageBox.about(this, tr("Hardware"), tr(qPrintable(hardwareInfo)));
    //messageBox.setFixedSize(500,200);


}

void MainWindow::aboutCurConfig()
{
    QString configInfo;
    if (strcmp(qPrintable(globalConf->trodesVersion),"0") != 0) {
        QString tv = (globalConf->trodesVersion=="-1") ? "<Not Available>" : globalConf->trodesVersion;
        QString cd = (globalConf->compileDate=="-1") ? "<Not Available>" : globalConf->compileDate;
        QString ct = (globalConf->compileTime=="-1") ? "<Not Available>" : globalConf->compileTime;
        QString qv = (globalConf->qtVersion=="-1") ? "<Not Available>" : globalConf->qtVersion;
        QString ch = (globalConf->commitHeadStr=="-1") ? "<Not Available>" : globalConf->commitHeadStr;
        configInfo = QString("Workspace Info:\n"
                        "-Trodes Version:\t%1\n"
                        "-Compiled on:\t%2 %3\n"
                        "-Qt version:\t\t%4\n"
                        "-Git commit:\t'%5'").
                arg(tv).arg(cd).arg(ct).arg(qv).arg(ch);
    }
    else
        configInfo = "No workspace information available.";

    QMessageBox::about(this, tr("About Configuration"), tr(qPrintable(configInfo)));
}

void MainWindow::aboutVersion(){
    checkForUpdate();
    if(isLatestVersion){
        QMessageBox::about(this, tr("Version"), tr("Your version is up to date."));
    }
    else{
        QMessageBox::warning(this, tr("Version"), tr("There is a more recent version available. Please check https://bitbucket.org/mkarlsso/trodes/downloads/ for the latest version"));
    }
}

bool MainWindow::checkMultipleInstances(QString errorMessage) {
    //qDebug() << "Checking if Trodes is already open...";
    TrodesClient dummyClient;
    QStringList activeTrodesHosts = dummyClient.findLocalTrodesServers();
    if (activeTrodesHosts.length() > 0) {
        qDebug() << "Error: Multiple instances of Trodes detected [" << activeTrodesHosts.length() << "]";
        QMessageBox::critical(this, tr("Multiple Instance Error"),tr(qPrintable(errorMessage)));
        return(true);
    }

    return(false); //return false if there are multiple instances of trodes detected
}


void MainWindow::saveWorkspacePathToSystem(QString workspacePath) {
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));

    QStringList prevWorkspaces;
    if (settings.contains("prevWorkspacePaths"))
        prevWorkspaces = settings.value("prevWorkspacePaths").toStringList();
    int alreadyPresentIndex = -1;
    for (int i = 0; i < prevWorkspaces.length(); i++) {
        if (prevWorkspaces.at(i) == workspacePath) {
            alreadyPresentIndex = i;
        }
    }
    if (alreadyPresentIndex > -1) {
        prevWorkspaces.removeAt(alreadyPresentIndex);
    }
    prevWorkspaces.push_front(workspacePath);
    if (prevWorkspaces.length() > 5) {
        prevWorkspaces.pop_back();
    }
    settings.setValue(QLatin1String("prevWorkspacePaths"), prevWorkspaces);

    settings.endGroup();
}

void MainWindow::savePlaybackFilePathToSystem(QString playbackFilePath) {
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));

    QStringList prevPlaybackFiles;
    if (settings.contains("prevPlaybackPaths"))
        prevPlaybackFiles = settings.value("prevPlaybackPaths").toStringList();

    int alreadyPresentIndex = -1;
    for (int i = 0; i < prevPlaybackFiles.length(); i++) {
        if (prevPlaybackFiles.at(i) == playbackFilePath) {
            alreadyPresentIndex = i;
        }
    }
    if (alreadyPresentIndex > -1) {
        prevPlaybackFiles.removeAt(alreadyPresentIndex);
    }
    prevPlaybackFiles.push_front(playbackFilePath);
    if (prevPlaybackFiles.length() > 5) {
        prevPlaybackFiles.pop_back();
    }
    settings.setValue(QLatin1String("prevPlaybackPaths"), prevPlaybackFiles);

    settings.endGroup();
}

void MainWindow::swapSplashScreenAndTabView(bool loadSplash) {
    isSplashScreenVisible = loadSplash;
    if (loadSplash) {
        tabsBackground->setVisible(true);
        splashScreen->setVisible(true);
        splashScreen->runAnimation_windowFade(200, true);
    }
    else {
        splashScreen->runAnimation_windowFade(200);
    }
}

void MainWindow::setBackgroundFrameVisibility() {
    tabsBackground->setVisible(isSplashScreenVisible);
}



QString MainWindow::timeStampToString(uint32_t tmpTimeStamp){
    QString currentTimeString("");
//    uint32_t tmpTimeStamp = currentTimeStamp;
    int hoursPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60 * 60));

    tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * hardwareConf->sourceSamplingRate);
    int minutesPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * hardwareConf->sourceSamplingRate);
    int secondsPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed * hardwareConf->sourceSamplingRate);
    int tenthsPassed = floor(((tmpTimeStamp * 10) / hardwareConf->sourceSamplingRate));

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
    currentTimeString.append(".");
    currentTimeString.append(QString::number(tenthsPassed));

    return currentTimeString;
}

void MainWindow::setTimeStampLabels(uint32_t playbackStartTimeStamp, uint32_t playbackEndTimeStamp){

//    qDebug() << "playbackstarttimestamp" << playbackStartTimeStamp;
//    qDebug() << "playbackendtimeStamp" << playbackEndTimeStamp;
    playbackSlider->setEnabled(true);
    playbackStartTimeLabel->setEnabled(true);
    playbackEndTimeLabel->setEnabled(true);
    playbackSlider->setVisible(true);
    playbackStartTimeLabel->setVisible(true);
    playbackEndTimeLabel->setVisible(true);


//    playbackStartClock = new QTime(timeStampToString(playbackStartTimeStamp));
//    playbackEndClock = new QTime(timeStampToString(playbackEndTimeStamp));

    playbackStartTimeLabel->setText(timeStampToString(playbackStartTimeStamp));
    playbackEndTimeLabel->setText(timeStampToString(playbackEndTimeStamp));

    playbackStartTime = playbackStartTimeStamp;
    playbackEndTime = playbackEndTimeStamp;
}



void MainWindow::updateTime()
{
    visibleTime++;
    if (visibleTime > 5) {
        if (eventTabWasChanged) {
            eventTabWasChanged = false;
            update();
        }
    }

    if (!exportMode && !playbackSlider->isSliderDown()) { //if slider is down, user is dragging it. so it needs to stop self-correcting
        //QTime currentTime;
        QString currentTimeString("");
        uint32_t tmpTimeStamp = currentTimeStamp;

        int hoursPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60 * 60));
        tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * hardwareConf->sourceSamplingRate);
        int minutesPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60));
        tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * hardwareConf->sourceSamplingRate);
        int secondsPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate));
        tmpTimeStamp = tmpTimeStamp - (secondsPassed * hardwareConf->sourceSamplingRate);
        //int tenthsPassed = floor(((tmpTimeStamp*10)/hardwareConf->sourceSamplingRate));
        //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

        if (hoursPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(hoursPassed));
        currentTimeString.append(":");
        if (minutesPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(minutesPassed));
        currentTimeString.append(":");
        if (secondsPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(secondsPassed));
        //currentTimeString.append(".");
        //currentTimeString.append(QString::number(tenthsPassed));


//        currentTimeString = timeStampToString(tmpTimeStamp).toString(timeFormatString);
        timeLabel->setText(currentTimeString);

        timerTick = (timerTick + 1) % 5;
        if ((recordFileOpen) && (timerTick == 0)) {
//            fileLabel->setText(fileString + QString("   (%1 MB    Available: %2 MB)").arg(recordOut->getBytesWritten() / 1000000).arg(recordOut->getBytesFree() / 1000000));
            fileLabel->setText(fileString + QString("   (Recorded: %1 MB  |  Available: %2 MB  |  %3m %4sec recorded)")
                               .arg(recordOut->getBytesWritten() / 1024/1024)
                               .arg(recordOut->getBytesFree() / 1024/1024)
                               .arg(recordOut->getPacketsWritten() / hardwareConf->sourceSamplingRate / 60)
                               .arg(recordOut->getPacketsWritten() / hardwareConf->sourceSamplingRate % 60));

                               /*.arg((msecRecorded+recordTimer->elapsed()*recordButton->isDown()) / 1000 / 60)
                               .arg((msecRecorded+recordTimer->elapsed()*recordButton->isDown()) / 1000 % 60));*/
        }
    }
}

void MainWindow::lfpFiltersOn()
{
    streamConf->setAllModesOn(false, true);
    streamLFPFiltersOn->setChecked(true);
    streamSpikeFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(false);
}

void MainWindow::spikeFiltersOn()
{
    streamConf->setAllModesOn(true, false);
    streamSpikeFiltersOn->setChecked(true);
    streamLFPFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(false);
}

void MainWindow::noFiltersOn(){
    streamConf->setAllModesOn(false, false);
    streamSpikeFiltersOn->setChecked(false);
    streamLFPFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(true);
}
//void MainWindow::linkChanges(bool link)
//{
//    linkChangesBool = link;
////    if (link) {
////        actionLinkChanges->setChecked(true);
////        actionUnLinkChanges->setChecked(false);
////        linkChangesButton->setChecked(true);
////    }
////    else {
////        actionLinkChanges->setChecked(false);
////        actionUnLinkChanges->setChecked(true);
////        linkChangesButton->setChecked(false);
////    }
//}

////link methods also separated by the true and false components
//void MainWindow::linkChanges()
//{
//    linkChangesBool = true;
////    actionLinkChanges->setChecked(true);
////    actionUnLinkChanges->setChecked(false);
////    linkChangesButton->setChecked(true);
//}

void MainWindow::uncoupleDisplay(bool uncouple)
{
    if (channelsConfigured) {
        uncoupleDisplayButton->setChecked(uncouple);
//        uncoupleDisplayButton->setRedDown(uncouple);
        eegDisp->freezeDisplay(uncouple);
        uncoupleDisplayButton->setblink(uncouple);
    }

}

void MainWindow::streamdisplaybuttonPressed(){
    streamDisplayButton->setDown(false);
    StreamDisplayOptionsDialog *dialog =
            new StreamDisplayOptionsDialog(streamLFPFiltersOn->isChecked(), streamSpikeFiltersOn->isChecked(), this);
    connect(dialog, &StreamDisplayOptionsDialog::lfpChosen, this, &MainWindow::lfpFiltersOn);
    connect(dialog, &StreamDisplayOptionsDialog::spikeChosen, this, &MainWindow::spikeFiltersOn);
    connect(dialog, &StreamDisplayOptionsDialog::rawChosen, this, &MainWindow::noFiltersOn);
    connect(this, &MainWindow::closeAllWindows, dialog, &StreamDisplayOptionsDialog::close);

    dialog->setWindowFlags(Qt::Popup);
    dialog->setGeometry(QRect(this->geometry().x()+streamDisplayButton->x(),this->geometry().y()+streamDisplayButton->y()+streamDisplayButton->height()+this->menuBar()->height(),75,100));

    dialog->show();
}

//void MainWindow::unLinkChanges()
//{
//    linkChangesBool = false;
////    actionLinkChanges->setChecked(false);
////    actionUnLinkChanges->setChecked(true);
//}

void MainWindow::toggleAllFilters(bool on) {

  linkChangesBool = false;

  for (int i = 0; i < spikeConf->ntrodes.length();i++) {
    spikeConf->setFilterSwitch(i,on);

  }
  if (isAudioOn)
      emit updateAudio();
  linkChangesBool = true;
}

void MainWindow::toggleAllRefs(bool on)
{
    linkChangesBool = false;
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setRefSwitch(i, on);
    }
    if (isAudioOn)
        updateAudio();

    linkChangesBool = true;
}

void MainWindow::toggleAllTriggers(bool on) {
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setTriggerMode(i,on);
    }
    if (isAudioOn)
        updateAudio();
}

void MainWindow::setAllMaxDisp(int newMaxDisp)
{
    linkChangesBool = false;
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setMaxDisp(i, newMaxDisp);
        /*
        for (int j = 0; j < spikeConf->ntrodes[i]->maxDisp.length(); j++) {
            spikeConf->setMaxDisp(i, j, newMaxDisp);
        }*/
    }


    linkChangesBool = true;
}

void MainWindow::setMaxDisp(int newMaxDisp) {
    QHashIterator<int, int> iter(selectedNTrodes);
    while (iter.hasNext()) {
        iter.next();
        if (iter.key() >= spikeConf->ntrodes.length() || iter.key() < 0) {
            qDebug() << "Error: Selected Index out of array range. (MainWindow::setMaxDisp)";
            continue;
        }
        spikeConf->setMaxDisp(iter.key(), newMaxDisp);
//        SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[iter.key()];
//        for (int i = 0; i < curNTrode->maxDisp.length(); i++) {
//            spikeConf->setMaxDisp(iter.key(), i, newMaxDisp);
//        }
    }
    if (triggerSettings != NULL) {
        triggerSettings->setMaxDisplay(newMaxDisp);
    }

}

void MainWindow::setAllRefs(int nTrode, int channel)
{
    linkChangesBool = false;
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setReference(i, nTrode, channel);
    }
    if (isAudioOn)
        updateAudio();

    linkChangesBool = true;
}

void MainWindow::setAllFilters(int low, int high)
{
    linkChangesBool = false;

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setLowFilter(i, low);
        spikeConf->setHighFilter(i, high);
    }
    if (isAudioOn)
        emit updateAudio();

    linkChangesBool = true;
}

void MainWindow::setAllThresh(int newThresh)
{
    linkChangesBool = false;

    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        spikeConf->setThresh(i, newThresh);
        /*
        for (int j = 0; j < spikeConf->ntrodes[i]->thresh.length(); j++) {
            spikeConf->setThresh(i, j, newThresh);
        }*/
    }

    linkChangesBool = true;
}

void MainWindow::checkRestartModules(void)
{
    int ret = QMessageBox::warning(this, tr("Restart Modules?"),
                                   tr("Are you sure you want to quit and restart the modules?"),
                                   QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);

    if (ret == QMessageBox::Yes) {
        quitModules();
        QThread::sleep(2);
        emit clearDataAvailable();
        QThread::sleep(2); //MARK: todo change this to not be a simple sleep, perhaps a threadlock until clear is finished would be best OR add clearDataAvailable to the startModule routine
        startModules(loadedConfigFile);
    }
}

void MainWindow::quitModules(void)
{
    //Send out a quit signal to all modules
    if ((channelsConfigured) && (trodesNet->tcpServer->nConnections())) {
        TrodesMessage *trodesMessage = new TrodesMessage;
        trodesMessage->messageType = TRODESMESSAGE_QUIT;
        emit messageForModules(trodesMessage);
    }

    QThread::msleep(250); //Give the modules some time to quit
}

void MainWindow::clearAll()
{

    if (spikeDisp != NULL) {
        spikeDisp->clearAllButtonPressed();
    }
    //for (int i = 0; i < ntrodeDisplayWidgetPtrs.length(); i++) {
    //    ntrodeDisplayWidgetPtrs[i]->clearButtonPressed();
    //}
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_P) {

//        loadingScreen->debug();
    }

    QMainWindow::keyPressEvent(event);
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    //Quit all threads and close all windows before accepting the close event
    qDebug() << "[MainWindow::closeEvent] Closing Trodes";
    if (dataStreaming || playbackFileOpen) {
        //stop streaming data
        //quitting = true;
        if(!disconnectFromSource()){
            event->ignore();
            return;
        }
        //event->ignore();

        //return;
        QThread::msleep(250);
    }

    sourceControl->setSource(SourceNone); //closes and deletes all source threads
    QThread::msleep(250);

    //quitModules();
    if (channelsConfigured) {
        closeConfig();
    }
    if (isAudioOn) {
        soundOut->endAudio();
        emit endAudioThread();
    }
    QThread::msleep(250);

    emit endAllThreads();
     QThread::msleep(250);

    benchmarkingControlPanel->close();

//    trodesButtonToggled(false);
    trodeSettingsButton->setChecked(false);
    emit closeAllWindows();
    event->accept();
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    if (isAudioOn)
        emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::moveEvent(QMoveEvent *)
{
    if (isAudioOn)
        emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}


void MainWindow::paintEvent(QPaintEvent *event)
{
    if (!eventTabWasChanged) {
        event->accept();
    }
    else {
        //QPixmap pixmap(size());
        QPainter painter;

        //painter.begin(&pixmap);
        //render(&painter);
        //painter.end();

        // Do processing on pixmap here

        painter.begin(this);
        //painter.drawPixmap(0, 0, pixmap);
        render(&painter);
        painter.end();
    }
}


void MainWindow::eventTabChanged(int newTab)
{
    if (eventTabsInitialized[newTab] == false) {
        eventTabWasChanged = true;
        visibleTime = 0;
        eventTabsInitialized[newTab] = true;
    }
}

void MainWindow::resetAllAudioButtons()
{
    //for (int i = 0; i < spikeDisp->ntrodeWidgets.length(); i++) {
    //    spikeDisp->ntrodeWidgets[i]->triggerScope->turnOffAudio();
    //}
}

QString MainWindow::calcTimeString(uint32_t tmpTimeStamp)
{
    QString currentTimeString("");
//    uint32_t tmpTimeStamp = currentTimeStamp;
    int hoursPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60 * 60));

    tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * hardwareConf->sourceSamplingRate);
    int minutesPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate * 60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * hardwareConf->sourceSamplingRate);
    int secondsPassed = floor(tmpTimeStamp / (hardwareConf->sourceSamplingRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed * hardwareConf->sourceSamplingRate);
//    int tenthsPassed = floor(((tmpTimeStamp * 10) / hardwareConf->sourceSamplingRate));

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
//    currentTimeString.append(".");
//    currentTimeString.append(QString::number(tenthsPassed));

    return currentTimeString;
}

void MainWindow::sendModuleDataChanToModules(int nTrode, int chan)
{
    spikeConf->ntrodes[nTrode]->moduleDataChan = chan;

    QByteArray msg;
    TrodesDataStream tmpStream(&msg, QIODevice::ReadWrite);

    tmpStream << nTrode << chan;
    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_NTRODEMODULEDATACHAN, msg, this);
    qDebug() << "[MainWindow::sendModuleDataChanToModules]: sending new moduleData chan to modules";
    emit messageForModules(tm);
    //trodesNet->tcpServer->sendMessageToModules(tm);
}

void MainWindow::setModuleDataStreaming(bool streamOn)
{
    moduleDataStreaming = streamOn;
}

bool MainWindow::isModuleDataStreaming(void)
{
    return moduleDataStreaming;
}

//MARK: event
void MainWindow::broadcastEvent(TrodesEventMessage ev) {
    //qDebug() << "Broadcasting Event: " << ev;
    //trodesNet->sendEvent(currentTimeStamp,ev);
    emit sendEvent(currentTimeStamp, ev.getTime(), ev.getEventMessage());
    //trodesNet->tcpServer->sendEvent(currentTimeStamp, ev);
}

void MainWindow::raiseWorkspaceGui() {
//    qDebug() << "Lets create some shit, YEEEEE HAWWWW";
//    workspaceEditor->exec();
//    workspaceEditor->raise();
    workspaceEditor->openEditor();
}

void MainWindow::loadFileInWorkspaceGui(QString filePath) {
    workspaceEditor->loadFileIntoWorkspaceGui(filePath);
    raiseWorkspaceGui();
}

void MainWindow::openTempWorkspace(QString path) {
//    qDebug() << " -" << path;
    loadConfig(path);
//    QFile tmpFile(path);
//    tmpFile.remove(); //don't remove temp file because if you do that will crash modules set up to 'send config info'
}

//MARK: select by dialog
void MainWindow::openSelectionDialog(void) {
//    qDebug() << "Open select by dialog";
    if (nTrodeSelectWindow == NULL) {
        nTrodeSelectWindow = new NTrodeSelectionDialog();
        connect(nTrodeSelectWindow, SIGNAL(sig_selectTags(int,QHash<GroupingTag,int>)), this, SLOT(receivedSelectByTagCommand(int,QHash<GroupingTag,int>)));
        connect(nTrodeSelectWindow, SIGNAL(sig_clearSelection()), this, SLOT(clearAllSelected()));
        connect(this, SIGNAL(closeAllWindows()), nTrodeSelectWindow, SLOT(close()));
    }

    if (nTrodeSelectWindow->isVisible()) {
        nTrodeSelectWindow->setVisible(false);
    }
    else {
        nTrodeSelectWindow->update();
        nTrodeSelectWindow->show();
        nTrodeSelectWindow->raise();
    }
}

void MainWindow::receivedSelectByTagCommand(int operation, QHash<GroupingTag, int> selectedTags) {
//    qDebug() << "receivedSelectByTagCommand";

    //clear previously selected
    QHashIterator<int, int> prevIter(selectedNTrodes);
    while (prevIter.hasNext()) { //set the labels of the previously selected nTrodes to black
        prevIter.next();
        eegDisp->setNTrodeSelected(prevIter.key(), false); //deselect all previous nTrodes
    }
    selectedNTrodes.clear();
    if (selectedTags.isEmpty()) //if no tags were selected, then return
        return;
    //possibly add in the currentTrodeSelected here to preserve the highlighted selection

    if (operation == FO_OR) {
//        qDebug() << "   performing OR";
        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[i];

            QHashIterator<GroupingTag, int> trodeIter(curNTrode->gTags);
            while (trodeIter.hasNext()) {
                trodeIter.next();
                if (selectedTags.contains(trodeIter.key())) {
                    eegDisp->setNTrodeSelected(i, true);
                    //MAYBE check selectedNTrodes for identical entries...
                    selectedNTrodes.insert(i, curNTrode->nTrodeId);
                    break;
                }
            }
        }
    }
    else if (operation == FO_AND) {
//        qDebug() << "   performing And";
        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[i];

            QHashIterator<GroupingTag, int> tagsIter(selectedTags);
            //assume that the curNTrode at index i will have all tags
            eegDisp->setNTrodeSelected(i, true);
            selectedNTrodes.insert(i, curNTrode->nTrodeId);

            while (tagsIter.hasNext()) {
                tagsIter.next();
                if (!curNTrode->gTags.contains(tagsIter.key())) { //if the curNTrode doesn't have a single one of the selected tags, deselect it
                    eegDisp->setNTrodeSelected(i, false);
                    //MAYBE check selectedNTrodes for identical entries...
                    selectedNTrodes.remove(i);
                    break;
                }
            }
        }
    }

    selectedNTrodesUpdated(); //update the ntrode settings panel

    currentTrodeSelected = selectedNTrodes.begin().key();
    spikeDisp->setShownNtrode(currentTrodeSelected);
    eegDisp->updateAudioHighlightChannel(spikeConf->ntrodes[currentTrodeSelected]->hw_chan.first());

}

void MainWindow::clearAllSelected() {
//    qDebug() << "clear all selected";
    //clear previously selected
    QHashIterator<int, int> prevIter(selectedNTrodes);
    while (prevIter.hasNext()) { //set the labels of the previously selected nTrodes to black
        prevIter.next();
        eegDisp->setNTrodeSelected(prevIter.key(), false); //deselect all previous nTrodes
    }
    selectedNTrodes.clear();

    selectedNTrodesUpdated(); //update the ntrode settings panel
}

void MainWindow::selectAllNTrodes() {
    clearAllSelected();
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[i];
        eegDisp->setNTrodeSelected(i, true);
        selectedNTrodes.insert(i, curNTrode->nTrodeId);
    }
    selectedNTrodesUpdated(); //update the ntrode settings panel

    currentTrodeSelected = selectedNTrodes.begin().key();
    spikeDisp->setShownNtrode(currentTrodeSelected);
    eegDisp->updateAudioHighlightChannel(spikeConf->ntrodes[currentTrodeSelected]->hw_chan.first());
}

//MARK: benchmarking dialog
void MainWindow::openBenchmarkingDialog() {
    //qDebug() << "open benchmarking control pannel!";
    benchmarkingControlPanel->show();
    benchmarkingControlPanel->raise();
}

void MainWindow::processPlaybackCommand(qint8 flg, qint32 timestamp) {
//    qDebug() << "playback command received: " << flg << " -- " << timestamp;
    switch(flg) {
    case PC_PAUSE: {
        pauseButtonPressed();
        pauseButtonReleased();
        break;
    }
    case PC_PLAY: {
        playButtonPressed();
        playButtonReleased();
        break;
    }
    case PC_STOP: {
        break;
    }
    case PC_SEEK:{
        approxSliderMove(timestamp);
        break;
    }
    case PC_NULL: {
        //null command
        break;
    }
    default:
        qDebug() << "Error: Invalid playback command flag [" << flg << "] received. (MainWindow::processPlaybackCommand)";
        break;
    }
}


//--------------------------------------------------------------------------------
//File Playback Slider functions

void MainWindow::movingSlider(int action){
    //action is enum value, 0 is NoAction and 7 is SliderMove. QAbstractSlider enums
    if(action > 0){
        emit jumpFileTo((qreal)playbackSlider->sliderPosition()/playbackSlider->maximum());
    }
}

void MainWindow::updateTimeFromSlider(int value){
    if(playbackSlider->isEnabled()){
        qreal pct = (qreal)value / (qreal)(playbackSlider->maximum() - playbackSlider->minimum());
        uint32_t time = (playbackEndTime - playbackStartTime)*pct + playbackStartTime;
        timeLabel->setText(calcTimeString(time));
    }
}
void MainWindow::updateSlider(qreal pct){
    if(playbackSlider->isEnabled() && !playbackSlider->isSliderDown()){
        playbackSlider->setValue(pct*playbackSlider->maximum());
    }
}

void MainWindow::sliderisPressed(){
    if(!pauseButton->isDown())
        sourceControl->pauseSource();

}

void MainWindow::sliderIsReleased(){
    if(!pauseButton->isDown())
        connectToSource();
    streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
    clearAll(); // clears skipe scatter plots.
}

void MainWindow::pausePlaybackSignal(){
    emit signal_sendPlaybackCommand(PC_PAUSE, currentTimeStamp);
}

void MainWindow::playPlaybackSignal(){
    emit signal_sendPlaybackCommand(PC_PLAY, currentTimeStamp);
}

void MainWindow::seekPlaybackSignal(uint32_t t){
    emit signal_sendPlaybackCommand(PC_SEEK, t);
}

void MainWindow::approxSliderMove(uint32_t timestamp){
    qreal pct = (qreal)(timestamp-playbackStartTime)/(playbackEndTime - playbackStartTime);
    emit jumpFileTo(pct);


}
