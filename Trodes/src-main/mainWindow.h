/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UI_MAIN_H
#define UI_MAIN_H

#include <sys/types.h>
#include <QtGui>
#include "spikeDisplay.h"
#include "streamDisplay.h"
#include "streamProcessorThread.h"
#include <QAudioOutput>
#include <QAudioFormat>
#include "audioThread.h"
#include "triggerThread.h"
#include "recordThread.h"
#include "trodesSocket.h"
#include "networkMessage.h"
#include "dialogs.h"
#include "globalObjects.h"
#include "sourceController.h"
#include "rfDisplay.h"
#include "trodesSplashScreen.h"
#include "benchmarkWidget.h"


QT_BEGIN_NAMESPACE


class Style_tweaks : public QProxyStyle
{
    public:

        void drawPrimitive(PrimitiveElement element, const QStyleOption *option,
                           QPainter *painter, const QWidget *widget) const
        {
            /* do not draw focus rectangles - this permits modern styling */
            if (element == QStyle::PE_FrameFocusRect)
                return;

            QProxyStyle::drawPrimitive(element, option, painter, widget);
        }
};

//NOTE: scaling for the backgroundFrame will sometimes be incorrect...
class BackgroundFrame : public QFrame {
    Q_OBJECT
    void newParent() {
        if (!parent()) return;
        parent()->installEventFilter(this);
        raise();
    }
public:
    explicit BackgroundFrame(QWidget *parent = 0);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);
    bool event(QEvent *ev);
};

class MainWindow : public QMainWindow
{
  Q_OBJECT

private:
    bool isSplashScreenVisible;
    bool isAudioOn;
    QHash<int, int> selectedNTrodes;
    int prevSelectedNTrodeIndex;

    NTrodeSelectionDialog *nTrodeSelectWindow;

private slots:
    void about();
    void aboutHardware();
    void aboutCurConfig();
    void aboutVersion();
    bool checkMultipleInstances(QString errorMessage);
    void saveWorkspacePathToSystem(QString workspacePath);
    void savePlaybackFilePathToSystem(QString playbackFilePath);
    void swapSplashScreenAndTabView(bool loadSplash);
    void setBackgroundFrameVisibility(void);

protected:
    void keyPressEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void paintEvent(QPaintEvent *);

public:
    int                         findEndOfConfigSection(QString filename);


    TrodesSplashScreen          *splashScreen;
    WorkspaceEditorDialog       *workspaceEditor;
    BackgroundFrame             *tabsBackground;

    LoadingScreen               *loadingScreen;
    QThread                     *lsThread; //loadingScreen thread;

    QGridLayout                 *playbackLayout;
    QSlider                     *playbackSlider;
    QLabel                      *playbackStartTimeLabel;
    QLabel                      *playbackEndTimeLabel;
//    QTime                       *playbackStartClock;
//    QTime                       *playbackEndClock;
    QString                     timeFormatString;
    uint32_t                    playbackStartTime;
    uint32_t                    playbackEndTime;

    QTabWidget                  *tabs;
    QTabWidget                  *eventTabs;
    //QList<NtrodeDisplayWidget*> ntrodeDisplayWidgetPtrs;
    QVBoxLayout                 *ultraLayout;
    QGridLayout                 *mainLayout;
    QGridLayout                 *headerLayout;
    QLabel                      *timeLabel;
    QLabel                      *fileLabel;
    QLabel                      *fileStatusColorIndicator;
    QLabel                      *streamStatusColorIndicator;
    QTimer                      *statusErrorBlinkTimer;
    TrodesButton                 *soundSettingsButton;
    TrodesButton                 *trodeSettingsButton;
    TrodesButton                 *commentButton;
    TrodesButton                 *spikesButton;
    TrodesButton                 *videoButton;
    TrodesButton                 *statescriptButton;
    TrodesButton                 *recordButton;
    TrodesButton                 *pauseButton;
    TrodesButton                 *playButton;
//    TrodesButton                 *linkChangesButton;
    TrodesButton                 *uncoupleDisplayButton;
    TrodesButton                 *streamDisplayButton;
    QString                     fileString;
    bool                        soundDialogOpen;
    bool                        recordFileOpen;
    QString                     recordFileName;
    bool                        dataStreaming;
    bool                        moduleDataStreaming;
    bool                        recording;
    int                         timerTick;
    bool                        eventTabWasChanged;
    bool                        statusBlinkRed;
    bool                        noDataErrFlag;
    bool                        packetErrFlag;
    bool                        noHeadstageErrFlag;

    QString calcTimeString(uint32_t tmpTimeStamp);
    QString                     currentConfigFileName;
    QString                     loadedConfigFile;
    int64_t                     visibleTime;
    bool                        eventTabsInitialized[64];
    int                         currentTrodeSelected;
    bool                        singleTriggerWindowOpen;
    bool                        channelsConfigured;
    QTimer                      *pullTimer;
    bool                        quitting;  //True if the program is in the process of quitting
    bool                        unitTestFlag;
    bool                        configChanged;

    int settleChannelByteInPacket;
    quint8 settleChannelBit;
    quint8 settleChannelTriggerState;
    int settleChannelDelay;


    HeadstageSettings headstageSettings;
    HardwareControllerSettings controllerSettings;

    StreamProcessorManager      *streamManager;
    SourceController            *sourceControl;
    StreamDisplayManager        *eegDisp;
    //SpikeDisplayWidget          *spikeDisp;
    MultiNtrodeDisplayWidget    *spikeDisp;
    //SDDisplay                   *sdDisp;
    SDDisplayPanel              *sdDisp;
    AudioController             *soundOut;
    RecordThread                *recordOut;
    TrodesModuleNetwork         *trodesNet;
    QThread                     *trodesNetThread;
    BenchmarkWidget             *benchmarkingControlPanel;
    TriggerScopeSettingsWidget  *triggerSettings;


    QString timeStampToString(uint32_t);


    void startThreads();

    QMenu *menuFile;
    QMenu *menuConfig;
    QAction *actionAboutConfig;
    QAction *actionLoadConfig;
    QAction *actionCloseConfig;
    QAction *actionSaveConfig;
    QAction *actionReConfig;
    QAction *actionOpenRecordDialog;
    QAction *actionPlaybackOpen;
    QAction *actionCloseFile;
    QAction *actionExport;
    QAction *actionRecord;
    QAction *actionPause;
    QAction *actionPlay;
    QAction *actionQuit;


    QMenu *menuSystem;
    QMenu *sourceMenu;

    QMenu *menuSimulationSource;
    QMenu *menuSpikeGadgetsSource;

    QAction *actionSourceNone;
    QAction *actionSourceFake;
    QAction *actionSourceFakeSpikes;
    QAction *actionSourceFile;
    QAction *actionSourceUSB;
    QAction *actionSourceRhythm;
    QAction *actionSourceEthernet;
    QAction *actionSound;



    QAction *actionConnect;
    QAction *actionDisconnect;
    QAction *actionClearBuffers; //a debugging tool
    QAction *actionSendSettle;
    QAction *actionOpenGeneratorDialog;

    QAction *actionRestartModules;


    QMenu *menuHelp;
    QAction *actionAbout;
    QAction *actionAboutQT;
    QAction *actionAboutHardware;
    QAction *actionAboutVersion;
    bool isLatestVersion;

    QMenu   *menuDisplay;
    QMenu   *menuEEGDisplay;
    QMenu   *menuSetTLength;
    QAction *actionSetTLength0_2;
    QAction *actionSetTLength0_5;
    QAction *actionSetTLength1_0;
    QAction *actionSetTLength2_0;
    QAction *actionSetTLength5_0;
    QMenu   *streamFilterMenu;
    QAction *streamLFPFiltersOn;
    QAction *streamSpikeFiltersOn;
    QAction *streamNoFiltersOn;
    QAction *actionDisplayAllPETHs;

    QMenu   *menuSettings;
    QMenu   *menuSelect;
    QAction *actionSelectAll;
    QAction *actionSelectByTag;

    QMenu   *menuDebug;
    QMenu   *menuNTrode;
    QAction *actionShowCurrentTrode;
//    QMenu   *menuLinkChanges;
//    QAction *actionLinkChanges;
//    QAction *actionUnLinkChanges;
    QAction *actionUncoupleDisplay;

    QAction *clearAllNTrodes;
    QMenu   *menuHeadstage;
    QAction *actionHeadstageSettings;
    QAction *actionSettleDelay;
    QAction *actionBenchmarkingSettings;

    QTime *recordTimer;
    int msecRecorded;
    QStatusBar *statusbar;
    QWidget *centralwidget;

    MainWindow(bool audioOn = true);
    ~MainWindow();
    void retranslateUi();
    // retranslateUi

signals:
    void gotversion(bool, QString);
    void configFileLoaded();
    void setAudioChannel(int hwchannel);
    void updateAudio();
    void closeAllWindows();
    void closeSoundDialog();
    void closeWaveformDialog();
    void endAllThreads();
    void endAudioThread();
    void newTraceLength(double);
    void newTimeStamp(uint32_t);
    void recordFileOpened(QString filename);
    void sourceConnected(QString source);
    void recordFileClosed();
    void recordingStarted();
    void recordingStopped();
    void messageForModules(TrodesMessage *tm);
    void clearDataAvailable(void); //This signal tells moduleNet to remove all non-trodes data available markers

    void sendEvent(uint32_t t, int evTime, QString eventName);

    void jumpFileTo(qreal value);
    void signal_sendPlaybackCommand(qint8 cmdFlg, qint32 ts);

    void sig_channelColorChanged(QColor color);

    void sig_fadeInLoadscreen(void);
    void sig_fadeOutLoadscreen(void);
    void openallpeths();

public slots:

    void checkForUpdate();
    void sourcePacketSizeError(bool on);
    void sourceNoDataError(bool on);
    void sourceNoHeadstageError(bool on);
    void toggleErrorBlink(bool blink);
    void updateErrorBlink();
    void errorBlinkTmrFunc();
    void audioChannelChanged(int hwchannel);
    void nTrodeClicked(int nTrodeInd, Qt::KeyboardModifiers mods);
    void processNTrodeRightClickAction(QString actionStr);
    void selectTrode(int nTrode);
    bool openPlaybackFile(void);
    bool openPlaybackFile(const QString fileName);
    void eventTabChanged(int);
    void setSourceMenuState(int);
    void resetAllAudioButtons();
    void updateTime();
    void lfpFiltersOn();
    void spikeFiltersOn();
    void noFiltersOn();
//    void linkChanges(bool link);
//    void linkChanges();
//    void unLinkChanges();
    void uncoupleDisplay(bool uncouple);
    void streamdisplaybuttonPressed();
    void openSoundDialog();
    void openGeneratorDialog();
    void openHeadstageDialog();
    void headstageSettingsChanged(HeadstageSettings s);
    void controllerSettingsChanged(HardwareControllerSettings s);
    void openSettleChannelDelayDialog();
    void setSettleChannelDelay(int delay);
    void openTrodeSettingsWindow();
    void openTrodeWindow();
    void enableHeadstageDialogMenu();
    void enableGeneratorDialogMenu();
    void soundButtonPressed();
    void commentButtonPressed();
    void spikesButtonPressed();
    void videoButtonPressed(QString videoString = "");
    void statescriptButtonPressed();
    void openExportDialog();
    void selectedNTrodesUpdated(void);
    void trodesButtonToggled(bool on);
    void trodeSettingsPanelClosed(void);
    void saveTrodeSettingPanelVisible(bool wasOpen);
    void saveTrodeSettingPanelPos(QRect geo);
    void setMaxDisp(int newMaxDisp); //this function sets all selected nTrodes max displays to the specified integer value
    void setAllRefs(int nTrode, int channel);
    void setAllMaxDisp(int newMaxDisp);
    void setAllThresh(int newThresh);
    void setAllFilters(int low, int high);
    void toggleAllFilters(bool on);
    void toggleAllRefs(bool on);
    void toggleAllTriggers(bool on);
    void setTLength();
    void setSource();
    void setSource(DataSource source);
    void connectToSource();
    bool disconnectFromSource();
    void sendSettleCommand();
    void clearAll();
    void loadConfig();
    int loadConfig(QString);
    void closeWorkspace(); //wraper binding for closeConfig
    void closeConfig(bool openSplashScreen = true);
    void settingsChanged();
    void settingsWarning();
    void saveConfig();
    void saveConfig(QString fname);
    void reConfig();
    void openRecordDialog();
    void closeFile();
    void recordButtonPressed();
    void pauseButtonPressed();
    void playButtonPressed();
    void recordButtonReleased();
    void pauseButtonReleased();
    void playButtonReleased();
    void actionRecordSelected();
    void actionPauseSelected();
    void actionPlaySelected();
    void exportData(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting);
    void cancelExport();
    void removeFromOpenNtrodeList(int nTrodeNum);
    void checkRestartModules(void);
    void quitModules(void);
    void bufferOverrunHandler();
    void showErrorMessage(QString msg);
    void showWarningMessage(QString msg);
    void errorSaving();
    void restartCrashedModule(QString modName);

    void startMainNetworkMessaging();
    void startModules(QString fileName);

    void sendModuleDataChanToModules(int nTrode, int chan);

    void setModuleDataStreaming(bool);
    bool isModuleDataStreaming(void);

    void threadError(QString errorString);
    void forwardProcessOutput();

    void setSettleControlChannel(int byteInPacket, quint8 bit, quint8 triggerState); //receives signal from streamDisplay

    void setTimeStampLabels(uint32_t playbackStartTimeStamp, uint32_t playbackEndTimeStamp);
    void updateSlider(qreal);
    void sliderisPressed();
    void sliderIsReleased();
    void movingSlider(int value);
    void updateTimeFromSlider(int value);
    void pausePlaybackSignal();
    void playPlaybackSignal();
    void seekPlaybackSignal(uint32_t t);
    void approxSliderMove(uint32_t);


    //MARK: event
    void broadcastEvent(TrodesEventMessage ev);


    void raiseWorkspaceGui(void);
    void loadFileInWorkspaceGui(QString filePath);
    void openTempWorkspace(QString path);

    //MARK: select by dialog
    void openSelectionDialog(void);
    void receivedSelectByTagCommand(int operation, QHash<GroupingTag,int> selectedTags);
    void clearAllSelected(void);
    void selectAllNTrodes(void);

    //MARK: benchmarking dialog
    void openBenchmarkingDialog(void);
    void processPlaybackCommand(qint8 flg, qint32 timestamp);


};


  QT_END_NAMESPACE

#endif // MAIN_H
