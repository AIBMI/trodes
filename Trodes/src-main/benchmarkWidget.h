#ifndef BENCHMARKWIDGET_H
#define BENCHMARKWIDGET_H

#include <QtGui>
#include <QObject>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QGroupBox>
#include "configuration.h"

class BenchmarkWidget : public QWidget {
Q_OBJECT

public:
    BenchmarkWidget(QWidget *parent=0);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    void iniSpikeGroup(void);
    void iniPositionGroup(void);
    void iniEventGroup(void);

    QVBoxLayout *MainLayout;
    QHBoxLayout *BottomGroupsLayout;
    QHBoxLayout *BottomBarLayout;

    /* **SpikeGroup defines ** */
    QGroupBox   *spikeGroup;
    QVBoxLayout *sgMainLayout;
    QVBoxLayout *sgSpikeDetectLayout;
    QVBoxLayout *sgSpikeSendLayout;
    QVBoxLayout *sgSpikeReceiveLayout;
    QHBoxLayout *sgOptionLayout;
    QHBoxLayout *sgFreqLayout;
    QCheckBox   *enableSpikeDetect;
    QCheckBox   *enableSpikeSend;
    QCheckBox   *enableSpikeReceive;
    QLineEdit   *editFreqSpikeDetect;
    QLineEdit   *editFreqSpikeSend;
    QLineEdit   *editFreqSpikeReceive;
    QLabel      *labelFreqSpikeDetect;
    QLabel      *labelFreqSpikeSend;
    QLabel      *labelFreqSpikeReceive;

    /* **PositionGroup defines ** */
    QGroupBox   *positionGroup;
    QVBoxLayout *pgMainLayout;
    QCheckBox   *enablePosition;
    QLineEdit   *editFreqPosition;
    QLabel      *labelFreqPosition;

    /* **EventGroup defines ** */
    QGroupBox   *eventGroup;
    QVBoxLayout *egMainLayout;
    QCheckBox   *enableEventSys;
    QLineEdit   *editFreqEventSys;
    QLabel      *labelFreqEventSys;


    QPushButton *buttonApply;
    QLabel      *statusText;

public slots:
    void updateBenchConfig(void);
    void getSettingsFromConfig(void);

private slots:
    bool checkFreqAreValid(void);
    void changeStatusToEdited(void);
    void changeStatusToSaved(void);

signals:
    void signal_benchConfigUpdated(void);

};

#endif // BENCHMARKWIDGET_H
