/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globalObjects.h"
#include "dialogs.h"

#include <cmath>
#include <cfloat>
#include <QtGui>
#include <QGridLayout>
#include <QtAlgorithms>


ExportDialog::ExportDialog(QWidget *) {

    QGridLayout *widgetLayout = new QGridLayout();
    QFont labelFont;
    labelFont.setPixelSize(12);
    labelFont.setFamily("Console");

    //Create the spike export controls
    spikeBox = new QGroupBox(tr("Spikes"),this);
    triggerSelector = new QComboBox();
    triggerSelector->setFont(labelFont);
    triggerSelector->addItem("Current thresholds");
    //triggerSelector->addItem("Standard deviation: 3");
    //triggerSelector->addItem("Standard deviation: 4");
    //triggerSelector->addItem("Standard deviation: 5");

    noiseRemoveSelector = new QComboBox();
    noiseRemoveSelector->setFont(labelFont);
    noiseRemoveSelector->addItem("None");
    //noiseRemoveSelector->addItem("Common events");

    triggerModeLabel = new QLabel("Trigger mode");
    triggerModeLabel->setFont(labelFont);
    noiseLabel = new QLabel("Noise exclusion");
    noiseLabel->setFont(labelFont);
    QGridLayout *spikeBoxLayout = new QGridLayout();
    spikeBoxLayout->addWidget(triggerModeLabel,0,0);
    spikeBoxLayout->addWidget(noiseLabel,0,1);
    spikeBoxLayout->addWidget(triggerSelector,1,0);
    spikeBoxLayout->addWidget(noiseRemoveSelector,1,1);
    spikeBox->setLayout(spikeBoxLayout);
    spikeBox->setCheckable(true);
    spikeBox->setChecked(true);
    //spikeBox->setFixedHeight();
    widgetLayout->addWidget(spikeBox,0,0);

    //Create the ModuleData export controls
    ModuleDataBox = new QGroupBox(tr("ModuleData"),this);
    ModuleDataChannelSelector = new QComboBox();
    ModuleDataChannelSelector->setFont(labelFont);
    ModuleDataChannelSelector->addItem("One per nTrode");
    ModuleDataChannelSelector->addItem("All channels");
    ModuleDataFilterSelector = new QComboBox();
    ModuleDataFilterSelector->setFont(labelFont);
    ModuleDataFilterSelector->setEnabled(false);
    ModuleDataChannelLabel = new QLabel("Channels");
    ModuleDataChannelLabel->setFont(labelFont);
    ModuleDataFilterLabel = new QLabel("Filter");
    ModuleDataFilterLabel->setFont(labelFont);
    QGridLayout *ModuleDataBoxLayout = new QGridLayout();
    ModuleDataBoxLayout->addWidget(ModuleDataChannelLabel,0,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterLabel,0,1);
    ModuleDataBoxLayout->addWidget(ModuleDataChannelSelector,1,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterSelector,1,1);
    ModuleDataBox->setLayout(ModuleDataBoxLayout);
    ModuleDataBox->setCheckable(true);
    ModuleDataBox->setChecked(false);
    widgetLayout->addWidget(ModuleDataBox,1,0);
    ModuleDataBox->setEnabled(false);

    //Add the buttons
    QGridLayout *buttonLayout = new QGridLayout();
    cancelButton = new QPushButton("Cancel");
    exportButton = new QPushButton("Export");
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(exportButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    widgetLayout->addLayout(buttonLayout,2,0);

    //Add the progressbar
    progressBar = new QProgressBar();
    progressBar->setVisible(false);
    widgetLayout->addWidget(progressBar,3,0);

    connect(cancelButton,SIGNAL(clicked()),this,SLOT(cancelButtonPushed()));
    connect(exportButton,SIGNAL(clicked()),this,SLOT(exportButtonBushed()));


    setLayout(widgetLayout);
    setWindowTitle(tr("Export Settings"));

}

void ExportDialog::cancelButtonPushed() {
    emit exportCancelled();
    emit closing();
    this->close();
}

void ExportDialog::exportButtonBushed() {
    progressBar->setVisible(true);
    progressBar->setMinimum(0);
    progressBar->setMaximum(playbackFileSize);
    progressBar->setValue(playbackFileCurrentLocation);
    progressCheckTimer = new QTimer(this);
    progressCheckTimer->setInterval(2000);
    connect(progressCheckTimer,SIGNAL(timeout()),this,SLOT(timerExpired()));

    filePlaybackSpeed = 10; //Fast-forward speed
    emit  startExport(spikeBox->isChecked(), ModuleDataBox->isChecked(), triggerSelector->currentIndex(), noiseRemoveSelector->currentIndex(), ModuleDataChannelSelector->currentIndex(), ModuleDataFilterSelector->currentIndex());

    progressCheckTimer->start();

}

void ExportDialog::timerExpired() {

    //filePlaybackSpeed++;
    progressBar->setValue(playbackFileCurrentLocation);
}


soundDialog::soundDialog(int currentGain, int currentThresh, QWidget *parent)
    :QWidget(parent) {

    TrodesFont dispFont;

    gainSlider = new QSlider(Qt::Vertical);
    gainSlider->setMaximum(100);
    gainSlider->setMinimum(0);
    gainSlider->setSingleStep(1);
    gainSlider->setValue(currentGain);
    gainSlider->setFont(dispFont);
    gainSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(100);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setFont(dispFont);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    gainDisplay = new QLabel;
    gainDisplay->setNum(currentGain);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    gainTitle = new QLabel(tr("Gain"));
    threshTitle = new QLabel(tr("Threshold"));
    gainTitle->setFont(dispFont);
    threshTitle->setFont(dispFont);
    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(gainTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(gainDisplay,1,0,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(gainSlider,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,1,Qt::AlignHCenter);
    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);
    connect(gainSlider,SIGNAL(valueChanged(int)),gainDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Sound"));

}


waveformGeneratorDialog::waveformGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
//waveformGeneratorDialog::waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent)
    :QWidget(parent)
{

    modulatorFreqSpinBox = new QDoubleSpinBox(this);
    modulatorFreqSpinBox->setMaximum(10);
    modulatorFreqSpinBox->setMinimum(0.0);
    modulatorFreqSpinBox->setSingleStep(0.1);
    modulatorFreqSpinBox->setValue(currentModulatorFreq);
    modulatorFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    freqSlider = new QSlider(Qt::Vertical);
    freqSlider->setMaximum(1000);
    freqSlider->setMinimum(1);
    freqSlider->setSingleStep(1);
    freqSlider->setValue(currentFreq);
    freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    ampSlider = new QSlider(Qt::Vertical);
    ampSlider->setMaximum(1000);
    ampSlider->setMinimum(0);
    ampSlider->setSingleStep(1);
    ampSlider->setValue(currentAmp);
    ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(1000);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


    freqDisplay = new QLabel;
    freqDisplay->setNum(currentFreq);
    ampDisplay = new QLabel;
    ampDisplay->setNum(currentAmp);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    modulatorFreqTitle = new QLabel(tr("Modulator (Hz)"));
    freqTitle = new QLabel(tr("Frequency (Hz)"));
    ampTitle = new QLabel(tr("Amplitude (uV)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));


    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(modulatorFreqTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

    mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

    mainLayout->addWidget(modulatorFreqSpinBox,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
    mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);

    connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
    connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Waveform generator"));

}

void waveformGeneratorDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}



spikeGeneratorDialog::spikeGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
  :QWidget(parent)

{
  modulatorFreqSpinBox = new QDoubleSpinBox(this);
  modulatorFreqSpinBox->setMaximum(10);
  modulatorFreqSpinBox->setMinimum(0.0);
  modulatorFreqSpinBox->setSingleStep(0.1);
  modulatorFreqSpinBox->setValue(currentModulatorFreq);
  modulatorFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  freqSlider = new QSlider(Qt::Vertical);
  freqSlider->setMaximum(1000);
  freqSlider->setMinimum(1);
  freqSlider->setSingleStep(1);
  freqSlider->setValue(currentFreq);
  freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  ampSlider = new QSlider(Qt::Vertical);
  ampSlider->setMaximum(1000);
  ampSlider->setMinimum(0);
  ampSlider->setSingleStep(1);
  ampSlider->setValue(currentAmp);
  ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  threshSlider = new QSlider(Qt::Vertical);
  threshSlider->setMaximum(1000);
  threshSlider->setMinimum(0);
  threshSlider->setSingleStep(1);
  threshSlider->setValue(currentThresh);
  threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


  freqDisplay = new QLabel;
  freqDisplay->setNum(currentFreq);
  ampDisplay = new QLabel;
  ampDisplay->setNum(currentAmp);
  threshDisplay = new QLabel;
  threshDisplay->setNum(currentThresh);

  modulatorFreqTitle = new QLabel(tr("Modulator (Hz)"));
  freqTitle = new QLabel(tr("Frequency (Hz)"));
  ampTitle = new QLabel(tr("Amplitude (uV)"));
  threshTitle = new QLabel(tr("Threshold (uV)"));


  QGridLayout *mainLayout = new QGridLayout;

  mainLayout->addWidget(modulatorFreqTitle,0,0,Qt::AlignCenter);
  mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
  mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
  mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

  mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
  mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
  mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

  mainLayout->addWidget(modulatorFreqSpinBox,2,0,Qt::AlignHCenter);
  mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
  mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
  mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

  mainLayout->setRowStretch(2,1);
  mainLayout->setMargin(10);

  connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
  connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
  connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

  setLayout(mainLayout);
  setWindowTitle(tr("Spikes+Waveform generator"));

}

void spikeGeneratorDialog::closeEvent(QCloseEvent *event)
{
  emit windowClosed();
  event->accept();
}


//-----------------------------------------------------
HeadstageSettingsDialog::HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent):QWidget(parent)
{
    setMinimumWidth(300);

    currentSettings = settings;
    settingsChanged = false;

    //Create the auto settle controls
    autoSettleBox = new QGroupBox(tr("Auto settle"),this);
    autoSettleBox->setCheckable(true);
    QGridLayout *autoSettleLayout = new QGridLayout;
    autoSettleBox->setToolTip("Amplifiers are automatically settled to prevent ringing if the signal comes above the set amplitude simultaneously for the set percentage of channels.");

    percentChannelsSlider = new QSlider(Qt::Horizontal);
    percentChannelsSlider->setMaximum(100);
    percentChannelsSlider->setMinimum(0);
    percentChannelsSlider->setSingleStep(1);
    //percentChannelsSlider->setValue(currentSettings.percentChannelsForSettle);
    percentChannelsSlider->setValue(round(100.0*((float)currentSettings.percentChannelsForSettle/(float)hardwareConf->NCHAN)));
    percentChannelsSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Horizontal);
    threshSlider->setMaximum(2000);
    threshSlider->setMinimum(500);
    threshSlider->setSingleStep(1);
    //threshSlider->setValue(currentSettings.threshForSettle);
    threshSlider->setValue(round( ((float)currentSettings.threshForSettle*AD_CONVERSION_FACTOR)/65536 ));
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    connect(percentChannelsSlider,SIGNAL(sliderMoved(int)),this,SLOT(percentSliderChanged(int)));
    connect(threshSlider,SIGNAL(sliderMoved(int)),this,SLOT(threshSliderChanged(int)));

    threshIndicator = new QLabel(QString().number(threshSlider->value()));
    percentIndicator = new QLabel(QString().number(percentChannelsSlider->value()));

    percentTitle = new QLabel(tr("Channels over thresh (%)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));

    autoSettleLayout->addWidget(threshTitle,0,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(threshSlider,1,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentTitle,2,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentChannelsSlider,3,0,Qt::AlignLeft);

    autoSettleLayout->addWidget(threshIndicator,1,1,Qt::AlignRight);
    autoSettleLayout->addWidget(percentIndicator,3,1,Qt::AlignRight);

    autoSettleBox->setLayout(autoSettleLayout);
    autoSettleBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);




    QGridLayout *secRowLayout = new QGridLayout;

    //Create smart ref controls
    smartReferenceBox = new QGroupBox(tr("Sampling sequence correction"),this);
    smartReferenceBox->setCheckable(false);
    QGridLayout *smartReferenceLayout = new QGridLayout;
    smartRefCheckBox = new QCheckBox();
    smartRefCheckBox->setText("Enable");
    smartReferenceBox->setToolTip("Used to correct for sequential sampling of channels if digital referencing is used. Recommended.");

    if (!currentSettings.smartRefAvailable) {
        smartRefCheckBox->setChecked(false);
        smartRefCheckBox->setEnabled(false);
    } else {
        smartRefCheckBox->setChecked(currentSettings.smartRefOn);
    }
    smartRefCheckBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    smartReferenceLayout->addWidget(smartRefCheckBox,0,0);
    smartReferenceBox->setLayout(smartReferenceLayout);
    smartReferenceBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    connect(smartRefCheckBox,SIGNAL(toggled(bool)),this,SLOT(smartRefToggled(bool)));
    secRowLayout->addWidget(smartReferenceBox,0,0,Qt::AlignCenter);

    //Create sensor controls
    sensorBox = new QGroupBox(tr("Motion sensors"),this);
    sensorBox->setToolTip("Toggles on/off available motion sensors.");
    sensorBox->setCheckable(false);
    QGridLayout *sensorLayout = new QGridLayout;
    accelCheckBox = new QCheckBox();
    accelCheckBox->setText("Accel.");
    sensorLayout->addWidget(accelCheckBox,0,0);
    gyroCheckBox = new QCheckBox();
    gyroCheckBox->setText("Gyro");
    sensorLayout->addWidget(gyroCheckBox,0,1);
    magnetCheckBox = new QCheckBox();
    magnetCheckBox->setText("Compass");
    sensorLayout->addWidget(magnetCheckBox,0,2);
    if (!currentSettings.accelSensorAvailable) {
        accelCheckBox->setChecked(false);
        accelCheckBox->setEnabled(false);
    } else {
        accelCheckBox->setChecked(currentSettings.accelSensorOn);
    }

    if (!currentSettings.gyroSensorAvailable) {
        gyroCheckBox->setChecked(false);
        gyroCheckBox->setEnabled(false);
    } else {
        gyroCheckBox->setChecked(currentSettings.gyroSensorOn);
    }

    if (!currentSettings.magSensorAvailable) {
        magnetCheckBox->setChecked(false);
        magnetCheckBox->setEnabled(false);
    } else {
        magnetCheckBox->setChecked(currentSettings.magSensorOn);
    }

    sensorBox->setLayout(sensorLayout);
    sensorBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    connect(accelCheckBox,SIGNAL(toggled(bool)),this,SLOT(accelToggled(bool)));
    connect(gyroCheckBox,SIGNAL(toggled(bool)),this,SLOT(gyroToggled(bool)));
    connect(magnetCheckBox,SIGNAL(toggled(bool)),this,SLOT(magToggled(bool)));
    secRowLayout->addWidget(sensorBox,0,2,Qt::AlignCenter);


    secRowLayout->setColumnStretch(1,1);
    secRowLayout->setMargin(20);

    QGridLayout *thirdRowLayout = new QGridLayout;
    rfBox = new QGroupBox(tr("Radio channel"),this);
    rfBox->setToolTip("Set RF channel");
    rfBox->setCheckable(false);
    QGridLayout *rfLayout = new QGridLayout;
    rfChannelSpinBox = new QSpinBox();
    rfChannelSpinBox->setMinimum(0);
    rfChannelSpinBox->setMaximum(255);
    if (!currentSettings.rfAvailable) {
        rfChannelSpinBox->setEnabled(false);
    } else {
        rfChannelSpinBox->setValue(currentSettings.rfChannel);
    }
    connect(rfChannelSpinBox,SIGNAL(valueChanged(int)),this,SLOT(rfChannelChanged(int)));

    rfLayout->addWidget(rfChannelSpinBox,0,0);

    rfBox->setLayout(rfLayout);
    rfBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    thirdRowLayout->addWidget(rfBox,0,0,Qt::AlignCenter);
    thirdRowLayout->setColumnStretch(1,1);
    thirdRowLayout->setMargin(20);

    okButton = new TrodesButton();
    okButton->setText("Apply");
    okButton->setEnabled(false);
    cancelButton = new TrodesButton();
    cancelButton->setText("Cancel");
    connect(okButton,SIGNAL(pressed()),this,SLOT(okButtonPressed()));
    connect(cancelButton,SIGNAL(pressed()),this,SLOT(cancelButtonPressed()));

    QGridLayout *buttonLayout = new QGridLayout();
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(okButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    buttonLayout->setContentsMargins(10,10,10,10);


    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setMargin(10);
    mainLayout->setContentsMargins(10,10,10,10);
    mainLayout->setVerticalSpacing(10);

    mainLayout->addWidget(autoSettleBox,0,0,Qt::AlignCenter);
    mainLayout->addLayout(secRowLayout,1,0,Qt::AlignCenter);
    mainLayout->addLayout(thirdRowLayout,2,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,3,0);

    setLayout(mainLayout);

    if (currentSettings.autosettleAvailable) {
        autoSettleBox->setChecked(currentSettings.autoSettleOn);
    } else {
        autoSettleBox->setChecked(false);
        autoSettleBox->setEnabled(false);
    }

    connect(autoSettleBox,SIGNAL(toggled(bool)),this,SLOT(autoSettleOnToggled(bool)));

    setWindowTitle(tr("Headstage settings"));

}

void HeadstageSettingsDialog::rfChannelChanged(int value) {
    currentSettings.rfChannel = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::percentSliderChanged(int value) {
    percentIndicator->setText(QString().number(value));
    currentSettings.percentChannelsForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::threshSliderChanged(int value) {
    threshIndicator->setText(QString().number(value));
    currentSettings.threshForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::autoSettleOnToggled(bool on) {
    currentSettings.autoSettleOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::smartRefToggled(bool on) {
    currentSettings.smartRefOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::accelToggled(bool on) {
    currentSettings.accelSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::gyroToggled(bool on) {
    currentSettings.gyroSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::magToggled(bool on) {
    currentSettings.magSensorOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::okButtonPressed() {
    setWindowTitle(tr("Sending to hardware..."));
    this->repaint();

    currentSettings.percentChannelsForSettle = round( ((float)percentChannelsSlider->value()/100.0)*(float)hardwareConf->NCHAN );
    currentSettings.threshForSettle = round( ((float)threshSlider->value() * 65536) / AD_CONVERSION_FACTOR );
    emit newSettings(currentSettings); //send the new settings to the source controller
    this->close();
}

void HeadstageSettingsDialog::cancelButtonPressed() {
    this->close();
}

void HeadstageSettingsDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}

void HeadstageSettingsDialog::resizeEvent(QResizeEvent *event) {
    //autoSettleBox->setGeometry(this->geometry());
    autoSettleBox->setFixedWidth(this->geometry().width()-50);
    percentChannelsSlider->setFixedWidth(this->geometry().width()-120);
    threshSlider->setFixedWidth(this->geometry().width()-120);
}



//-----------------------------------------------------
//This dialog pops up when the "Annotate" button is pressed.
//Allows the user to add comments to the recording session. All comments are
//stores in file that is separate from the main recording file, but with the same base name.

CommentDialog::CommentDialog(QString fileNameIn, QWidget *parent)
    :QWidget(parent),
    fileName(fileNameIn) {

    bool enableControls = false;
    TrodesFont dispFont;

    if (!fileName.isEmpty()) {
        enableControls = true;
        QFile commentFile(fileName);
    }

    newCommentEdit = new QLineEdit();
    newCommentEdit->setFont(dispFont);
    newCommentEdit->setMinimumWidth(200);
    newCommentEdit->setFrame(true);
    newCommentEdit->setStyleSheet("QLineEdit {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}");
    newCommentEdit->setEnabled(enableControls);

    connect(newCommentEdit,SIGNAL(returnPressed()),this,SLOT(saveCurrentComment()));
    connect(newCommentEdit,SIGNAL(textChanged(QString)),this,SLOT(somethingEntered()));

    saveButton = new TrodesButton();
    saveButton->setText("Save");
    saveButton->setEnabled(false);

    QLabel* historyLabel = new QLabel();
    historyLabel->setFont(dispFont);
    historyLabel->setText("History:");
    historyLabel->setEnabled(enableControls);
    //lastComment = new QLabel();
    lastComment = new QTextEdit();
    lastComment->setFont(dispFont);
    //sec2lastComment = new QLabel();
    lastComment->setStyleSheet("QFrame {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}"
                                "QLabel {color : gray;}");
    lastComment->setMinimumWidth(200);
    lastComment->setEnabled(enableControls);

    commentLabel = new QLabel();
    commentLabel->setFont(dispFont);
    commentLabel->setText("New comment:");
    commentLabel->setEnabled(enableControls);

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *commentLayout = new QGridLayout;
    QGridLayout *buttonLayout = new QGridLayout;

    //commentLayout->addWidget(sec2lastComment,0,0,Qt::AlignCenter);
    commentLayout->addWidget(historyLabel,0,0,Qt::AlignLeft);
    commentLayout->addWidget(lastComment,1,0,Qt::AlignCenter);

    commentLayout->setColumnStretch(0,1);
    commentLayout->setContentsMargins(0,0,0,0);

    buttonLayout->addWidget(saveButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);

    mainLayout->addLayout(commentLayout,1,0);
    //mainLayout->addWidget(historyFrame,1,0,Qt::AlignRight);
    mainLayout->addWidget(commentLabel,2,0,Qt::AlignLeft);
    mainLayout->addWidget(newCommentEdit,3,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,4,0);
    //mainLayout->setMargin(10);

    connect(saveButton,SIGNAL(pressed()),this,SLOT(saveCurrentComment()));

    setLayout(mainLayout);
    getHistory();

}

void CommentDialog::saveLine() {
    //Saves the current line to file
    if (saveButton->isEnabled()) {
        if (!fileName.isEmpty()) {
            //append the current timestamp to the comment
            QString currentComment = QString("%1  ").arg(currentTimeStamp) + newCommentEdit->text()+"\n";
            //write to the file
            QFile commentFile(fileName);
            commentFile.open(QIODevice::Append);
            commentFile.write(currentComment.toLocal8Bit());
            commentFile.close();
        }
        newCommentEdit->clear();
        //update the history box
        getHistory();
    }
}

void CommentDialog::getHistory() {
    //Read the contents of the file and populate the "history" box

    QString contents;

    if (!fileName.isEmpty()) {
        QFile commentFile(fileName);
        if (commentFile.exists()) {
            //read the entire file
            commentFile.open(QIODevice::ReadOnly);
            contents = QString(commentFile.readAll());
            commentFile.close();
        }
    }

    lastComment->setText(contents);
    //set the scrollbar to show the last few lines
    QScrollBar *bar = lastComment->verticalScrollBar();
    bar->setValue(bar->maximum());

}


void CommentDialog::saveCurrentComment() {

    //add the comment to the file
    saveLine();
    //close();
}


void CommentDialog::somethingEntered() {
    //we don't enable the save button unless something has been entered
    saveButton->setEnabled(true);
}

void CommentDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//---------------------------------------------


//-------------------------------------------
RasterPlot::RasterPlot(QWidget *parent) {

    setMinimumHeight(10);
    setMinimumWidth(200);
    minX = 0;
    maxX = 10;
    setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }

    setData(tmpValues);*/

}


void RasterPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    //xTickLabels.clear();
    //xTickLabels.append(minX);
    //xTickLabels.append(maxX);
    //xTickLabels.append((maxX-minX)/2);
    update();
}

void RasterPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void RasterPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void RasterPlot::addRaster(const QVector<qreal> &times) {
    eventTimes.push_back(times);
    //setMinimumHeight(eventTimes.length()*4);
    update();
}

void RasterPlot::setRasters(const QVector<QVector<qreal> > &times) {
    eventTimes.clear();
    for (int i=0; i<times.length(); i++) {
        eventTimes.push_back(times[i]);
    }

    //Add y axis ticks
    yTickLabels.clear();
    yTickLabels.append(0);
    yTickLabels.append(eventTimes.length()/2);
    yTickLabels.append(eventTimes.length());

    update();
}

void RasterPlot::clearRasters() {
    eventTimes.clear();
    update();
}

void RasterPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());


    qreal numRows = eventTimes.length();
    //qreal maxRange = maxY;

    int margin = 30;

    int topOfGraph = 5;
    int bottomOfGraph = height()-5;
    // int graphHeight = bottomOfGraph-topOfGraph; // UNUSED

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    // int graphWidth = rightOfGraph-leftOfGraph; // UNUSED

    int rowSpacing = 1;
    int tickHeight = ((bottomOfGraph - topOfGraph)/numRows) - (2*rowSpacing);
    if (tickHeight < 1) {
        tickHeight = 1;
    }


    if ((maxX-minX) > 0) {
        for (int r=0; r < eventTimes.length(); r++) {
            int currentRowPix = topOfGraph + rowSpacing + (r*((bottomOfGraph - topOfGraph)/numRows));
            for (int e = 0; e < eventTimes[r].length(); e++) {
                if ((eventTimes[r][e] >= minX) && (eventTimes[r][e] <= maxX)) {
                    int xLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((eventTimes[r][e]-minX)/(maxX-minX)));
                    painter.drawLine(xLoc,currentRowPix,xLoc,currentRowPix+tickHeight);
                }
            }
        }
    }



//    //Draw the axes
//    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);

//    //Display the axis labels
//    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
//    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

//    //Put unit labels on the x axis

//    for (int i = 0; i < xTickLabels.length(); i++) {
//        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*(xTickLabels[i]/(maxX-minX)));
//        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
//        painter.drawText(labelBox, Qt::AlignCenter,
//                         QString("%1").arg(xTickLabels[i]));
//        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
//    }



    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
//        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        int yTickLoc =// bottomOfGraph-((bottomOfGraph-topOfGraph)*((qreal)i/(yTickLabels.length()-1)));
        topOfGraph + (i*((bottomOfGraph - topOfGraph)/(yTickLabels.length()-1)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString("%1").arg(yTickLabels[i]));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }

    painter.end();
}

void RasterPlot::resizeEvent(QResizeEvent *event) {

    update();

}



//--------------------------------------------

HistogramPlot::HistogramPlot(QWidget *parent) {

    setMinimumHeight(100);
    setMinimumWidth(200);
    maxY = 0.0;

    //setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );



//    QVector<qreal> tmpValues;
//    for (int i = 0; i < 200; i++) {
//        tmpValues.append(i);
//    }

//    setData(tmpValues);
    errorsIndicator = false;
    binsIndicator = false;
    bin1 = new QRectF();
    bin2 = new QRectF();

    margin = 30;
    topOfGraph = 5;
    for(int i = 0; i < 4; i++)
        errBar[i] = false;

    clicking = false;
    barColor = QColor(100,100,100);
}


void HistogramPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);
    update();
}

void HistogramPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void HistogramPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void HistogramPlot::setColor(QColor c){
    barColor = c;
}

void HistogramPlot::setData(QVector<qreal> bvalues){
    //Copy values and set MaxY
    maxY = 0.0;
    barValues.clear();
    for (int i=0; i<bvalues.length(); i++) {
        barValues.append(bvalues[i]);
        if (barValues[i] > maxY) {
            maxY = barValues[i];
        }
    }
    minX = 0;
    maxX = bvalues.length();

    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);

    yTickLabels.clear();
    yTickLabels.append(maxY);

    update();
}

void HistogramPlot::clearData(){
    barValues.clear();
    update();
}

void HistogramPlot::clickingOn(bool on, int cl){
    clicking = on;
    cluster = cl;
    setMouseTracking(on);
    if(clicking){
        this->setStyleSheet("HistogramPlot:hover {border: 2px solid red;"
                            "border-radius: 4px;"
                            "padding: 2px;}");
    }
}

void HistogramPlot::setErrorBars(QVector<QVector<qreal> > hilowErrValues){
    for(int i = 2; i<hilowErrValues.length(); i+=2)
        for(int j = 0; j<hilowErrValues[i].length(); j++)
            if(errBar[i/2] && hilowErrValues[i][j] > maxY)
                maxY = hilowErrValues[i][j];

    hiloerrorBarValues.clear();
    hiloerrorBarValues = hilowErrValues;
}

void HistogramPlot::setChecked(bool checked[]){
    errorsIndicator = true;
//    for(int i = 0; i < 4; i++){
//        this->checked[i] = checked[i];
//    }
    this->errBar[1] = checked[1];
}

void HistogramPlot::setBinValues(qreal bin1Start, qreal bin1Size, qreal bin2Start, qreal bin2Size){
    this->binsIndicator = true;
    this->bin1Start = bin1Start;
    this->bin1Size = bin1Size;
    this->bin2Start = bin2Start;
    this->bin2Size = bin2Size;
}

void HistogramPlot::mousePressEvent(QMouseEvent *event){
    if(clicking){
        emit PSTHRequest(cluster);
    }
}

void HistogramPlot::mouseMoveEvent(QMouseEvent *event){
    if(clicking){

    }
}

void HistogramPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;

    QStyleOption opt;
    opt.init(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());

    qreal numBars = barValues.length();
    maxYDisplay = ceil(maxY/50)*50;
    qreal maxRange = maxY;//maxYDisplay;


    int bottomOfGraph = height()-margin-10;
    int graphHeight = bottomOfGraph-topOfGraph;

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    int graphWidth = rightOfGraph-leftOfGraph;

    Qt::GlobalColor colors[4] = {Qt::red, Qt::black, Qt::blue, Qt::magenta};


    //Draw the axes
    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);
    for (int c=0; c < barValues.length(); c++) {
        //bar values
        qreal xcorner = leftOfGraph + (c*(graphWidth/numBars));
        qreal ycorner = bottomOfGraph-((barValues[c]/maxRange)*graphHeight);
        qreal barwidth = graphWidth/numBars;
        qreal barheight = (barValues[c]/maxRange)*graphHeight;

        //Draw histogram bars
        QRectF rect = QRectF(xcorner, ycorner, barwidth, barheight);
        //QRectF rect = QRectF(c*(width()/numBars), height()-((barValues[c]/maxRange)*height()), (width()/numBars), ((barValues[c]/maxRange)*height()));
        painter.drawRect(rect);
        painter.fillRect(rect,QBrush(barColor));
    }


    //Previously, there was capability for multiple different error bars on each bar in the histogram
    //They have been disabled in the psth window class, but the code is kept here in case of possible
    //future changes/additions to the error bars.
    if(errorsIndicator){
        int numOfErrBars = 0;
        for(int i = 0; i < 4; i++)
            numOfErrBars += (int)errBar[i];

        for (int c=0; c < barValues.length(); c++) {
            int ithbar = 0;
            qreal xcorner = leftOfGraph + (c*(graphWidth/numBars));
            for(int i = 0; i < 4; i++){
                if(!errBar[i])
                    continue;
                ithbar++;
                painter.setPen(colors[i]);
                qreal xMid = xcorner + (((qreal)(ithbar)/(numOfErrBars+1))*(graphWidth/numBars));
                qreal yMidhigher = bottomOfGraph-((hiloerrorBarValues[2*i][c])*graphHeight/maxRange);
                qreal yMidlower = bottomOfGraph-((hiloerrorBarValues[2*i+1][c])*graphHeight/maxRange);

                QLine line = QLine(xMid, yMidhigher, xMid, yMidlower);
                QLine tick1 = QLine(xMid-2, yMidhigher, xMid+2, yMidhigher);
                QLine tick2 = QLine(xMid-2, yMidlower, xMid+2, yMidlower);

                painter.drawLine(line);
                painter.drawLine(tick1);
                painter.drawLine(tick2);
            }
            painter.setPen(pen);
        }
    }

    if(binsIndicator){
        //Draw bin 1 and bin 2 indicators
        QPen bin1Pen(QColor(255, 0, 0, 100));
        QPen bin2Pen(QColor(0, 0, 255, 100));
        qreal bin1Left = leftOfGraph + (bin1Start*(graphWidth)/numBars);
        qreal bin1Width = bin1Size*(graphWidth/numBars);
        QRectF bin1Rect(bin1Left, topOfGraph, bin1Width, graphHeight);
        qreal bin2Left = leftOfGraph + (bin2Start*(graphWidth)/numBars);
        qreal bin2Width = bin2Size*(graphWidth/numBars);
        QRectF bin2Rect(bin2Left, topOfGraph, bin2Width, graphHeight);
        painter.setPen(bin1Pen);
        painter.drawRect(bin1Rect);
        painter.setPen(bin2Pen);
        painter.drawRect(bin2Rect);
        painter.fillRect(bin1Rect, QColor(255, 0, 0, 15));
        painter.fillRect(bin2Rect, QColor(0, 0, 255, 15));

        painter.setPen(pen);
    }
    //Put unit labels on the x axis

    for (int i = 0; i < xTickLabels.length(); i++) {
        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((xTickLabels[i]-minX)/(maxX-minX)));
        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(xTickLabels[i], 'f', 1 ));
        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
    }

    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(yTickLabels[i], 'f', 1 ));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }


    //Display the axis labels
    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

    painter.end();
}


void HistogramPlot::resizeEvent(QResizeEvent *event) {
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    update();

}

//-------------------------------------------

PSTHDialog::PSTHDialog(QWidget *parent)
    :QWidget(parent){

    setGeometry(300,300,300,300);
    setMinimumHeight(300);
    setMinimumWidth(300);


    rasterWindow = new RasterPlot(this);
    rasterWindow->setYLabel("Trial Number");
    window = new HistogramPlot(this);
    window->setMaximumHeight(200);
    window->setXLabel("Time relative to event (sec)");
    window->setYLabel("Rate (Hz)");

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    plotLayout->addWidget(rasterWindow,0,0);
    plotLayout->addWidget(window,1,0);
    plotLayout->setRowStretch(0,3);
    plotLayout->setRowStretch(1,1);
    mainLayout->addLayout(plotLayout,0,0);

    setUpControlPanel();
    //controlLayout->setColumnStretch(0,1);
    mainLayout->addLayout(controlLayout,1,0);
    mainLayout->setRowStretch(0,1);


    setLayout(mainLayout);
    window->show();
    rasterWindow->show();

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("PSTHposition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    settings.beginGroup(QLatin1String("PSTHsettings"));
    int tempNumBins = settings.value(QLatin1String("numbins")).toInt();
    if (tempNumBins > 0) {
        numBinsSpinBox->setValue(tempNumBins);
    } else {
        numBinsSpinBox->setValue(100);
    }

    int tempMsecRange = settings.value(QLatin1String("msecRange")).toInt();
    if (tempMsecRange > 0) {
        rangeSpinBox->setValue(tempMsecRange);
    } else {
        rangeSpinBox->setValue(500);
    }

    /*absTimeRange = 1.0;
    numBins = 80; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;*/
    int tmpValue;
//    tmpValue = settings.value(QLatin1String("sdCheck")).toInt();
//    setSDChecked(tmpValue);
//    SDCheckbox->setChecked((bool)tmpValue);

    tmpValue = settings.value(QLatin1String("seCheck")).toInt();
    setSEChecked(tmpValue);
    SECheckbox->setChecked((bool)tmpValue);

//    tmpValue = settings.value(QLatin1String("rCheck")).toInt();
//    setRChecked(tmpValue);
//    RCheckbox->setChecked((bool)tmpValue);

//    tmpValue = settings.value(QLatin1String("ciCheck")).toInt();
//    setCIChecked(settings.value(QLatin1String("ciCheck")).toInt());
//    CICheckbox->setChecked((bool)tmpValue);


    tmpValue = settings.value(QLatin1String("bin1Start")).toInt();
    if(tmpValue)
        binOneStartBox->setValue(tmpValue);
    else
        binOneStartBox->setValue(-200);

    tmpValue = settings.value(QLatin1String("bin1Size")).toInt();
    if(tmpValue)
        binOneSizeBox->setValue(tmpValue);
    else
        binOneSizeBox->setValue(100);

    tmpValue = settings.value(QLatin1String("bin2Start")).toInt();
    if(tmpValue)
        binTwoStartBox->setValue(tmpValue);
    else
        binTwoStartBox->setValue(200);

    tmpValue = settings.value(QLatin1String("bin2Size")).toInt();
    if(tmpValue)
        binTwoSizeBox->setValue(tmpValue);
    else
        binTwoSizeBox->setValue(100);

    settings.endGroup();


    /*
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;

    for (uint32_t t = 5; t < 1000000; t = t+30000) {
        trialTimes.append(t);
    }

    for (uint32_t t = 5; t < 1000000; t = t+5000) {
        eventTimes.append(t);
    }

    plot(trialTimes,eventTimes);*/

}

//Creates everything for bottom of PSTH window
void PSTHDialog::setUpControlPanel(){
    rangeLabel = new QLabel("+/- msec");
    binsLabel = new QLabel("Bins");
    QFont labelFont;
    labelFont.setPixelSize(12);
    rangeLabel->setFont(labelFont);
    binsLabel->setFont(labelFont);
    rangeLabel->setMaximumHeight(25);
    binsLabel->setMaximumHeight(25);

    rangeSpinBox = new QSpinBox(this);
    rangeSpinBox->setFrame(false);
    rangeSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    rangeSpinBox->setMinimum(50);
    rangeSpinBox->setMaximum(1000);
    rangeSpinBox->setSingleStep(10);
    rangeSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    rangeSpinBox->setFocusPolicy(Qt::NoFocus);
    rangeSpinBox->setToolTip(tr("Display range (+/- msec)"));

    numBinsSpinBox = new QSpinBox(this);
    numBinsSpinBox->setFrame(false);
    numBinsSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    numBinsSpinBox->setMinimum(20);
    numBinsSpinBox->setMaximum(200);
    numBinsSpinBox->setSingleStep(1);
    numBinsSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    numBinsSpinBox->setFocusPolicy(Qt::NoFocus);
    numBinsSpinBox->setToolTip(tr("Number of bins"));

//    SDCheckbox = new QCheckBox("Std. Dev",this);
    SECheckbox = new QCheckBox("Std. Error", this);
//    RCheckbox = new QCheckBox("Range", this);
//    CICheckbox = new QCheckBox("Conf. Interval", this);
//    SDCheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: red }");
    SECheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: black }");
//    RCheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: blue }");
//    CICheckbox->setStyleSheet("QCheckBox::indicator:checked{ background: magenta }");

    binOneStartLabel = new QLabel("Bin 1 Start");
    binOneSizeLabel = new QLabel("Bin 1 Size");
    binTwoStartLabel = new QLabel("Bin 2 Start");
    binTwoSizeLabel = new QLabel("Bin 2 Size");
    binOneStartLabel->setFont(labelFont);
    binOneSizeLabel->setFont(labelFont);
    binTwoStartLabel->setFont(labelFont);
    binTwoSizeLabel->setFont(labelFont);

    binOneStartBox = new QSpinBox(this);
    binOneStartBox->setMinimum(-1000);
    binOneStartBox->setMaximum(1000);
    binOneStartBox->setSingleStep(10);
    binOneStartBox->setFrame(false);
    binOneStartBox->setStyleSheet("QSpinBox { background-color: white; }");

    binOneSizeBox = new QSpinBox(this);
    binOneSizeBox->setMinimum(0);
    binOneSizeBox->setMaximum(2*1000);
    binOneSizeBox->setSingleStep(10);
    binOneSizeBox->setFrame(false);

    binTwoStartBox = new QSpinBox(this);
    binTwoStartBox->setMinimum(-1000);
    binTwoStartBox->setMaximum(1000);
    binTwoStartBox->setSingleStep(10);
    binTwoStartBox->setFrame(false);

    binTwoSizeBox = new QSpinBox(this);
    binTwoSizeBox->setMinimum(0);
    binTwoSizeBox->setMaximum(2*1000);
    binTwoSizeBox->setSingleStep(10);
    binTwoSizeBox->setFrame(false);

    binSumStatsText = new QTextEdit();
    binSumStatsText->setText(generateSumStatsText());

    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setRange(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setNumBins(int)));
    connect(rangeSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(rangeChanged(int)));
    connect(numBinsSpinBox, SIGNAL(valueChanged(int)), this, SIGNAL(binsChanged(int)));

//    connect(SDCheckbox, SIGNAL(stateChanged(int)), this, SLOT(setSDChecked(int)));
    connect(SECheckbox, SIGNAL(stateChanged(int)), this, SLOT(setSEChecked(int)));
//    connect(RCheckbox, SIGNAL(stateChanged(int)), this, SLOT(setRChecked(int)));
//    connect(CICheckbox, SIGNAL(stateChanged(int)), this, SLOT(setCIChecked(int)));

    connect(binOneStartBox, SIGNAL(valueChanged(int)), this, SLOT(setBinOneStart(int)));
    connect(binOneSizeBox, SIGNAL(valueChanged(int)), this, SLOT(setBinOneSize(int)));
    connect(binTwoStartBox, SIGNAL(valueChanged(int)), this, SLOT(setBinTwoStart(int)));
    connect(binTwoSizeBox, SIGNAL(valueChanged(int)), this, SLOT(setBinTwoSize(int)));

    /*rangeSpinBox->setMaximumWidth(30);
    numBinsSpinBox->setMaximumWidth(30);
    rangeLabel->setMaximumWidth(30);
    binsLabel->setMaximumWidth(30);*/

    //rangeSpinBox->setMaximumHeight(30);
    //numBinsSpinBox->setMaximumHeight(30);

    controlLayout = new QGridLayout;
    controlLayout->setVerticalSpacing(2);
    controlLayout->setHorizontalSpacing(10);
    controlLayout->setContentsMargins(0,0,0,0);

    int binstatsColSpan = 4;
    generateSumStatsLabel();

//    controlLayout->addWidget(binSumStatsGrid, 0, 1);
    controlLayout->addWidget(binOneStartLabel, 0,2+binstatsColSpan);
    controlLayout->addWidget(binOneSizeLabel, 0,3+binstatsColSpan);

    controlLayout->addWidget(binOneStartBox, 1,2+binstatsColSpan);
    controlLayout->addWidget(binOneSizeBox, 1,3+binstatsColSpan);

    controlLayout->addWidget(binTwoStartLabel, 2,2+binstatsColSpan);
    controlLayout->addWidget(binTwoSizeLabel, 2,3+binstatsColSpan);

    controlLayout->addWidget(binTwoStartBox, 3,2+binstatsColSpan);
    controlLayout->addWidget(binTwoSizeBox, 3,3+binstatsColSpan);

    controlLayout->addWidget(rangeLabel,0,4+binstatsColSpan);
    controlLayout->addWidget(binsLabel,0,5+binstatsColSpan);
    controlLayout->addWidget(rangeSpinBox,1,4+binstatsColSpan);
    controlLayout->addWidget(numBinsSpinBox,1,5+binstatsColSpan);

//    controlLayout->addWidget(SDCheckbox, 0, 6);
    controlLayout->addWidget(SECheckbox, 2, 4+binstatsColSpan);
//    controlLayout->addWidget(RCheckbox, 2, 6);
//    controlLayout->addWidget(CICheckbox, 3, 6);

    controlLayout->setColumnStretch(0, 3);
    controlLayout->setColumnStretch(5, 1);
}

void PSTHDialog::generateSumStatsLabel(){
    //First quick and ugly version to get sumstats on the ui
    minLabel1 = new QLabel();
    medLabel1 = new QLabel();
    meanLabel1 = new QLabel();
    maxLabel1 = new QLabel();
    numLabel1 = new QLabel();
    minLabel2 = new QLabel();
    medLabel2 = new QLabel();
    meanLabel2 = new QLabel();
    maxLabel2 = new QLabel();
    numLabel2 = new QLabel();
    QLabel *bin1 = new QLabel("Bin 1");
    bin1->setStyleSheet("QLabel { color : red; }");
    QLabel *bin2 = new QLabel("Bin 2");
    bin2->setStyleSheet("QLabel { color : blue; }");

    controlLayout->addWidget(new QLabel("Min"), 1, 1);
    controlLayout->addWidget(new QLabel("Med"), 2, 1);
    controlLayout->addWidget(new QLabel("Mean"), 3, 1);
    controlLayout->addWidget(new QLabel("Max"), 4, 1);
    controlLayout->addWidget(new QLabel("Num"), 5, 1);
    controlLayout->addWidget(bin1, 0, 2);
    controlLayout->addWidget(bin2, 0, 4);
    controlLayout->addWidget(minLabel1, 1, 2);
    controlLayout->addWidget(medLabel1, 2, 2);
    controlLayout->addWidget(meanLabel1, 3, 2);
    controlLayout->addWidget(maxLabel1, 4, 2);
    controlLayout->addWidget(numLabel1, 5, 2);
    controlLayout->addWidget(minLabel2, 1, 4);
    controlLayout->addWidget(medLabel2, 2, 4);
    controlLayout->addWidget(meanLabel2, 3, 4);
    controlLayout->addWidget(maxLabel2, 4, 4);
    controlLayout->addWidget(numLabel2, 5, 4);

    controlLayout->setColumnMinimumWidth(1, 10);
    controlLayout->setColumnMinimumWidth(2, 10);
    controlLayout->setColumnMinimumWidth(3, 10);
}


void PSTHDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("PSTHposition"), this->geometry());
    settings.endGroup();
}

void PSTHDialog::plot(const QVector<uint32_t> &trialTimesIn, const QVector<uint32_t> &eventTimesIn) {

    trialTimes.clear();
    trialTimes.resize(trialTimesIn.length());
    for (int i=0; i < trialTimesIn.length(); i++) {
        trialTimes[i] = trialTimesIn[i];
    }

    eventTimes.clear();
    eventTimes.resize(eventTimesIn.length());
    for (int i=0; i < eventTimesIn.length(); i++) {
        eventTimes[i] = eventTimesIn[i];
    }

    calcDisplay();

}

//SLOT functions
void PSTHDialog::setNumBins(int nBins) {
    //absTimeRange = 1.0;
    numBins = nBins; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("numbins"), numBins);
    settings.endGroup();
}

void PSTHDialog::setRange(int msecRange) {
    absTimeRange = (qreal)msecRange/1000;
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("msecRange"), msecRange);
    settings.endGroup();
}

void PSTHDialog::setSEChecked(int c){
    checked[1] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("seCheck"), c);
    settings.endGroup();
}

void PSTHDialog::setSDChecked(int c){
    checked[0] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("sdCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setRChecked(int c){
    checked[2] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("rCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setCIChecked(int c){
    checked[3] = (bool)c;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("ciCheck"), c);
    settings.endGroup();
}
void PSTHDialog::setBinOneStart(int num){
    binOneStart = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin1Start"), num);
    settings.endGroup();
}
void PSTHDialog::setBinOneSize(int num){
    binOneSize = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin1Size"), num);
    settings.endGroup();
}
void PSTHDialog::setBinTwoStart(int num){
    binTwoStart = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin2Start"), num);
    settings.endGroup();
}
void PSTHDialog::setBinTwoSize(int num){
    binTwoSize = (qreal)num/1000;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("bin2Size"), num);
    settings.endGroup();
}
//Organizes trials and events according to specified settings to be passed to displays
void PSTHDialog::calcDisplay() {

    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    QVector<QVector<qreal> > trialEventTimes;
    QVector<QVector<int> > binTrialFreqs;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }
    QVector<int> binOneCounts(numTrials, 0);
    QVector<int> binTwoCounts(numTrials, 0);

    counts.clear();
    counts.fill(0, numBins);
//    for (int i=0; i < numBins; i++) {
//        counts.push_back(0);
//    }

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        QVector<qreal> relEventTimes;
        QVector<int> trialFreqs(numBins, 0);

        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((eventTimes[currentEventInd] < trialWindowStart) && (currentEventInd < eventTimes.length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((eventTimes[currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < eventTimes.length())) {

            qreal tmpRelEventTime = ((qreal)eventTimes[currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            relEventTimes.append(tmpRelEventTime);

            //Counts for all bins for histogram and for error bars
            for (int i=0; i < numBins; i++) {
                if (tmpRelEventTime <= -absTimeRange + (i*binSize)+binSize) {
                    trialFreqs[i]++;
                    counts[i]++;
                    break;
                }
            }

            //Counts for two user defined bins for sum stats
            if(tmpRelEventTime >= binOneStart && tmpRelEventTime < binOneStart+binOneSize)
                binOneCounts[trialInd]++;
            if(tmpRelEventTime >= binTwoStart && tmpRelEventTime < binTwoStart+binTwoSize)
                binTwoCounts[trialInd]++;

            trialEventInd++;
        }
        binTrialFreqs.append(trialFreqs);
        trialEventTimes.append(relEventTimes);
        binOneCounts[trialInd] /= (binOneSize);
        binTwoCounts[trialInd] /= (binTwoSize);
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < numBins; i++) {
        rates.push_back(counts[i]/(binSize*numTrials));
    }

    //Calculates all error functions so that user can pick which to display in histogramplot
    QVector<QVector<qreal> > eValues;
    eValues = calcErrorBars(binTrialFreqs, 0.95);

    calcSumStats(binOneCounts, 1);
    calcSumStats(binTwoCounts, 2);
//    binSumStatsText->clear();
//    binSumStatsText->setText(generateSumStatsText());

    window->setErrorBars(eValues);
    window->setChecked(checked);
    window->setBinValues((absTimeRange+binOneStart)/binSize,
                    binOneSize/binSize,
                    (absTimeRange+binTwoStart)/binSize,
                    binTwoSize/binSize);
    window->setData(rates);
    window->setXRange(-absTimeRange, absTimeRange);


    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    rasterWindow->setRasters(trialEventTimes);

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }
    window->setData(tmpValues);
    window->setXRange(-absTimeRange, absTimeRange);

    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    QVector<qreal> faketimes;
    faketimes.append(-1.5);
    faketimes.append(-1);
    faketimes.append(-.75);
    faketimes.append(-.1);
    faketimes.append(1.7);
    faketimes.append(3.3);
    faketimes.append(5.6);
    faketimes.append(7.7);

    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    */

}

QString PSTHDialog::generateSumStatsText(){
    QMapIterator<QString, qreal> i(binOneSumStats);
    QMapIterator<QString, qreal> j(binTwoSumStats);
    QString txt = "Stat\t\tBin 1\t\tBin 2\n";

    while(i.hasNext() && j.hasNext()){
        i.next();
        j.next();
        txt += i.key() + "\t\t" + QString::number(i.value(), 'g', 3) + "\t\t";
        txt += QString::number(j.value(), 'g', 3) + "\n";

    }
    return txt;
}

void PSTHDialog::calcSumStats(QVector<int> data, int bin){
    qreal min = DBL_MAX;
    qreal max = DBL_MIN;
    qreal median = 0;
    qreal mean = 0;

    qSort(data);
    for(int i = 0; i < data.length(); i++){
        if(data[i] > max)
            max = data[i];
        if(data[i] < min)
            min = data[i];
        if(i == data.length()/2){
            if(data.length()%2 != 0)
                median = data[i];
            else
                median = (data[i] + data[i-1])/2;
        }
        mean += data[i];
    }
    mean = mean/data.length();

    if(bin == 1){
        minLabel1->setText(QString::number(min, 'f',3));
        medLabel1->setText(QString::number(median, 'f',3));
        meanLabel1->setText(QString::number(mean, 'f', 3));
        maxLabel1->setText(QString::number(max, 'f',3));
        numLabel1->setText(QString::number(data.length()));
    }
    else if(bin==2){
        minLabel2->setText(QString::number(min, 'f',3));
        medLabel2->setText(QString::number(median, 'f', 3));
        meanLabel2->setText(QString::number(mean, 'f', 3));
        maxLabel2->setText(QString::number(max, 'f',3));
        numLabel2->setText(QString::number(data.length()));

    }

    return;
}

QVector<QVector<qreal> > PSTHDialog::calcErrorBars(QVector<QVector<int> > data, qreal pct){
    QVector<QVector<qreal> > allHiLoValues;
    allHiLoValues.append(stdDev(data));
    allHiLoValues.append(stdError(data));
    allHiLoValues.append(range(data));
    allHiLoValues.append(confInt(data, pct));
    return allHiLoValues;
}


//All of the following functions will break if variable --data-- is not of length --numTrials-- and each vector inside
//is not of length --numBins--, serves as implicit check for parameter accuracy

QVector<QVector<qreal> > PSTHDialog::stdDev(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;

    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+sqrt(sumSq/(numTrials-1)));
        loValues.push_back(avg-sqrt(sumSq/(numTrials-1)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::stdError(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+sqrt(sumSq/((numTrials-1)*numTrials)));
        loValues.push_back(avg-sqrt(sumSq/((numTrials-1)*numTrials)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::range(QVector<QVector<int> > data){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    for(int j = 0; j < numBins; j++){
        qreal min = DBL_MAX;
        qreal max = DBL_MIN;
        for(int i = 0; i < numTrials; i++){
            if(data[i][j]/binSize > max)
                max = data[i][j]/binSize;
            else if(data[i][j]/binSize < min)
                min = data[i][j]/binSize;
        }
        hiValues.push_back(max);
        loValues.push_back(min);
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}

QVector<QVector<qreal> > PSTHDialog::confInt(QVector<QVector<int> > data, qreal pct){
    QVector<QVector<qreal> > hilowValues;
    QVector<qreal> hiValues;
    QVector<qreal> loValues;
    qreal crit = 1;
    if(pct == 0.9)
        crit = 1.645;
    else if(pct==0.95)
        crit = 1.96;
    else if(pct==0.99)
        crit = 2.576;

    for(int j = 0; j < numBins; j++){
        qreal avg = 0;
        qreal sumSq = 0;
        avg = counts[j]/(binSize*numTrials);
        for(int i = 0; i < numTrials; i++){
            sumSq += pow(data[i][j]/(binSize) - avg, 2);
        }

        hiValues.push_back(avg+crit*sqrt(sumSq/((numTrials-1)*numTrials)));
        loValues.push_back(avg-crit*sqrt(sumSq/((numTrials-1)*numTrials)));
    }

    hilowValues.push_back(hiValues);
    hilowValues.push_back(loValues);
    return hilowValues;
}


//-----------------------------------------------------------------------------------

MultiPlotDialog::MultiPlotDialog(QWidget *parent, QColor c) :
    QWidget(parent)
  , plotColor(c){

    setGeometry(300,300,300,300);
    setMinimumHeight(300);
    setMinimumWidth(300);
    topHistogram = new HistogramPlot(this);
    topHistogram->setXLabel("Time relative to event (sec)");
    topHistogram->setYLabel("Rate (Hz)");
    topHistogram->setColor(plotColor);


    clusterPlotGrid = new QGridLayout();

    topPlotLabel = new QLabel(this);
    topPlotLabel->setText("All data for NTrode ");
    botPlotLabel = new QLabel(this);
    botPlotLabel->setText("Data for clusters ");

    currClInd = 0;
    newNTrode = false;
    currentNTrode = -1;
    currPage = 1;
    plotsPerPage = 8;
    setupGridSize(QString::number(plotsPerPage));


//-------------------------------------------------------------
    rangeLabel = new QLabel("+/- msec");
    binsLabel = new QLabel("Bins");
    rangeLabel->setMaximumHeight(25);
    binsLabel->setMaximumHeight(25);
//    numPlotsLabel->setMaximumHeight(25);

    rangeSpinBox = new QSpinBox(this);
    rangeSpinBox->setFrame(false);
    rangeSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    rangeSpinBox->setMinimum(50);
    rangeSpinBox->setMaximum(1000);
    rangeSpinBox->setSingleStep(10);
    rangeSpinBox->setFixedSize(50,22);
    rangeSpinBox->setFocusPolicy(Qt::NoFocus);
    rangeSpinBox->setToolTip(tr("Display range (+/- msec)"));

    numBinsSpinBox = new QSpinBox(this);
    numBinsSpinBox->setFrame(false);
    numBinsSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    numBinsSpinBox->setMinimum(20);
    numBinsSpinBox->setMaximum(200);
    numBinsSpinBox->setSingleStep(1);
    numBinsSpinBox->setFixedSize(50,22);
    numBinsSpinBox->setFocusPolicy(Qt::NoFocus);
    numBinsSpinBox->setToolTip(tr("Number of bins"));

    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setRange(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setNumBins(int)));
    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SIGNAL(rangeChanged(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SIGNAL(binsChanged(int)));

//-------------------------------------------------------------
    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *plotLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    plotLayout->addWidget(topHistogram,2,0, 1, 3);
    plotLayout->addLayout(clusterPlotGrid, 4, 0, 1, 3);
    plotLayout->addWidget(topPlotLabel, 1, 0, Qt::AlignCenter);
    plotLayout->addWidget(botPlotLabel, 3, 0, Qt::AlignCenter);
    plotLayout->setRowStretch(2, 1);
    plotLayout->setRowStretch(4, 1);
    plotLayout->setColumnStretch(0, 1);

    QGridLayout *controlLayout = new QGridLayout;
    controlLayout->addWidget(rangeLabel, 0, 1);
    controlLayout->addWidget(binsLabel, 0, 2);
    controlLayout->addWidget(rangeSpinBox, 1, 1);
    controlLayout->addWidget(numBinsSpinBox, 1, 2);
    controlLayout->setVerticalSpacing(2);
    controlLayout->setColumnStretch(0,1);
    mainLayout->addLayout(plotLayout,0,0);
    mainLayout->addLayout(controlLayout, 1,0);
//    mainLayout->setRowStretch(0,1);


    setLayout(mainLayout);
    topHistogram->show();
//-------------------------------------------------------------
    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("MPSTHPosition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    settings.beginGroup(QLatin1String("PSTHsettings"));
    int tempNumBins = settings.value(QLatin1String("numbins")).toInt();
    if (tempNumBins > 0) {
        numBinsSpinBox->setValue(tempNumBins);
    } else {
        numBinsSpinBox->setValue(100);
    }

    int tempMsecRange = settings.value(QLatin1String("msecRange")).toInt();
    if (tempMsecRange > 0) {
        rangeSpinBox->setValue(tempMsecRange);
    } else {
        rangeSpinBox->setValue(500);
    }

    settings.endGroup();
}

//SLot called with new data
void MultiPlotDialog::plot(const QVector<uint32_t> &trialTimesIn, const QVector<uint32_t> &eventTimesIn, const QVector<QVector<uint32_t> > &clusterEventTimesIn){
    trialTimes.clear();
    trialTimes.resize(trialTimesIn.length());
    for (int i=0; i < trialTimesIn.length(); i++) {
        trialTimes[i] = trialTimesIn[i];
    }

    eventTimes.clear();
    eventTimes.resize(eventTimesIn.length());
    for (int i=0; i < eventTimesIn.length(); i++) {
        eventTimes[i] = eventTimesIn[i];
    }

    clusterEventTimes.clear();
    clusterEventTimes.resize(clusterEventTimesIn.length());
    for(int i=0; i < clusterEventTimesIn.length(); i++){
        clusterEventTimes[i] = clusterEventTimesIn[i];
    }

    calcTopDisplay();
    setupClusterHistograms();
}

//Creates and adds new clusters
void MultiPlotDialog::setupClusterHistograms(){
    for(int i = 0; i < clusterEventTimes.size(); i++){
        if(!clusterEventTimes[i].empty() && !clusterPlots.contains(i)){
            HistogramPlot *tmp = new HistogramPlot(this);
            connect(tmp, SIGNAL(PSTHRequest(int)), this, SLOT(getPSTH(int)));
            tmp->clickingOn(true, i);
            clusterPlots.insert(i, tmp);
        }
    }
    if(clusterPlots.size()==0)
        botPlotLabel->hide();
    else
        botPlotLabel->show();
    displayClusterGrid();
}

//Re-displays the cluster histogram plots
void MultiPlotDialog::displayClusterGrid(){

    int ithPlot = 0;
    QList<int> keys = clusterPlots.keys();
    for(int i = 0; i < keys.size(); i++){
        HistogramPlot *ptr = clusterPlots.value(keys[i]);
        ptr->hide();

        int rownum = ithPlot/gridCols;
        int colnum = ithPlot%gridRows;
        if(plotsPerPage == 2)
            colnum = ithPlot;

        calcClusterHistogramDisplay(keys[i]);
        ptr->setXLabel("Time relative to event (sec): Cluster " + QString::number(keys[i]));
        ptr->setYLabel("Rate (Hz)");
        ptr->show();

        clusterPlotGrid->addWidget(ptr, rownum, colnum);
        ithPlot++;
    }

}

//Calculate bins for top display
void MultiPlotDialog::calcTopDisplay() {

    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }

    QVector<int> counts;//.clear();
    counts.fill(0, numBins);

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((eventTimes[currentEventInd] < trialWindowStart) && (currentEventInd < eventTimes.length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((eventTimes[currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < eventTimes.length())) {

            qreal tmpRelEventTime = ((qreal)eventTimes[currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            if(tmpRelEventTime >= -absTimeRange){
                int i = (int)floor((tmpRelEventTime+absTimeRange)/(qreal)binSize);
                if(i >= 0 && i < counts.length())
                    counts[i]++;
                else
                    qDebug() << "Error IN PSTHDIALOG";
            }
            trialEventInd++;
        }
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < counts.size(); i++) {
        rates.push_back(counts[i]/(qreal)(binSize*numTrials));
    }
    topHistogram->setData(rates);
    topHistogram->setXRange(-absTimeRange, absTimeRange);
    if(currentNTrode>-1)
        topPlotLabel->setText("All data for NTrode " + QString::number(spikeConf->ntrodes[currentNTrode]->nTrodeId));

}

//Calculates histogram data given a cluster
void MultiPlotDialog::calcClusterHistogramDisplay(int clusterInd){
    uint sampRate = hardwareConf->sourceSamplingRate;
    uint rangeInt = absTimeRange*sampRate;
    numTrials = trialTimes.length();
//    if(numTrials<=1){
//        qDebug() << "Warning: Number of trials too low!";
//        return;
//    }

    QVector<int> clCounts = QVector<int>(numBins, 0);

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((clusterEventTimes[clusterInd][currentEventInd] < trialWindowStart) && (currentEventInd < clusterEventTimes[clusterInd].length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((clusterEventTimes[clusterInd][currentEventInd+trialEventInd] < trialWindowEnd)
               && ((currentEventInd+trialEventInd) < clusterEventTimes[clusterInd].length())) {

            qreal tmpRelEventTime = ((qreal)clusterEventTimes[clusterInd][currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            if(tmpRelEventTime >= -absTimeRange){
                int i = (int)floor((tmpRelEventTime+absTimeRange)/(qreal)binSize);
                if(i >= 0 && i < clCounts.length()) clCounts[i]++;
                else
                    qDebug() << "ERROR in PSTHDIALOGclCounts";
            }
            trialEventInd++;
        }
    }

    //Bin sizes/rates calculation
    QVector<qreal> rates;
    for (int i=0; i < clCounts.size(); i++) {
        rates.push_back(clCounts[i]/(qreal)(binSize*numTrials));
    }
    clusterPlots.value(clusterInd)->setData(rates);
    clusterPlots.value(clusterInd)->setXRange(-absTimeRange, absTimeRange);
}

//Called when signal is generated by parent window
void MultiPlotDialog::update(int ntrode, QVector<uint32_t> deventTimes, QVector<uint32_t> spikeTimes, QVector<QVector<uint32_t> > clusterSpikeTimes){

    if(currentNTrode != ntrode){
        QList<int> keys = clusterPlots.keys();
        for(int i = 0; i < keys.size(); i++){
            clusterPlots.value(keys[i])->hide();
            clusterPlotGrid->removeWidget(clusterPlots.value(keys[i]));
            delete clusterPlots.value(keys[i]);
            clusterPlots.remove(keys[i]);
        }
        clusterPlots.clear();
    }
    currentNTrode = ntrode;
    this->setWindowTitle("nTrode " + QString::number(spikeConf->ntrodes[ntrode]->nTrodeId));
    plot(deventTimes, spikeTimes, clusterSpikeTimes);
}

void MultiPlotDialog::setNumBins(int nBins) {
    //absTimeRange = 1.0;
    numBins = nBins; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;
    calcTopDisplay();
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("numbins"), numBins);
    settings.endGroup();
}

void MultiPlotDialog::setRange(int msecRange) {
    absTimeRange = (qreal)msecRange/1000;
    binSize = (2*absTimeRange)/numBins;
    calcTopDisplay();
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("msecRange"), msecRange);
    settings.endGroup();
}

void MultiPlotDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("MPSTHPosition"), this->geometry());
    settings.endGroup();
}

//Tells ntrode display window to open a psth window for this cluster
void MultiPlotDialog::getPSTH(int cl){
    emit requestPSTH(cl, currentNTrode);
}

//Hard coded number of plots shown. To extend, add here and also to dropdown menu in constructor
//Was useful in old implementation, now numplots is hardcoded in constructor
void MultiPlotDialog::setupGridSize(QString numplots){
    switch (numplots.toInt()) {
    case 1:
        gridRows = 1;
        gridCols = 1;
        break;
    case 2:
        gridRows = 1;
        gridCols = 2;
        break;
    case 4:
        gridRows = 2;
        gridCols = 2;
        break;
    case 6:
        gridRows = 2;
        gridCols = 3;
    case 8:
        gridRows = 2;
        gridCols = 4;
    default:
        gridRows = 2;
        gridCols = 2;
        break;
    }
    plotsPerPage = numplots.toInt();
    currPage = 0;
    displayClusterGrid();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("MultiPlotsettings"));
    settings.setValue(QLatin1String("plotsPerPage"), plotsPerPage);
    settings.endGroup();
}

//QDialog inherits QWidget. Only useful for exec() function which blocks Trodes until closed
WorkspaceEditorDialog::WorkspaceEditorDialog(QWidget *parent) : QDialog(parent) {
    workspaceGui = new WorkspaceEditor(M_EMBEDDED);
    workspaceGui->setObjectName("editor");

    QMenu *menuSaveLoad = new QMenu();
    menuSaveLoad->setTitle("File");
    menuSaveLoad->setEnabled(true);

    QMenuBar *menuBar = new QMenuBar();
    menuBar->addAction(menuSaveLoad->menuAction());
    menuBar->setStyleSheet("background-color:rgb(0,0,0,0)");

    QMenu *menuSave = new QMenu();
    menuSave->setTitle("Save");
    menuSaveLoad->addAction(menuSave->menuAction());

    actionSave = new QAction(0);
    actionSave->setText("Save");
    actionSave->setShortcut(QKeySequence(tr("Ctrl+S")));
    addAction(actionSave);
    actionSaveAs = new QAction(0);
    actionSaveAs->setText("Save As");
    actionSaveAs->setShortcut(QKeySequence(tr("Ctrl+Shift+S")));
    addAction(actionSaveAs);
    menuSave->addAction(actionSave);
    menuSave->addAction(actionSaveAs);

    actionLoad = new QAction(0);
    actionLoad->setText("Load");
    actionLoad->setShortcut(QKeySequence(tr("Ctrl+O")));
    addAction(actionLoad);
    menuSaveLoad->addAction(actionLoad);

    connect(actionSave, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveToXML()));
    connect(actionSaveAs, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveAsToXML()));
    connect(actionLoad, SIGNAL(triggered(bool)), workspaceGui, SLOT(buttonLoadPressed()));


    mainLayout = new QVBoxLayout();

    buttonBar = new QHBoxLayout();
    buttonCancel = new QPushButton(tr("Cancel"));
    buttonAccept = new QPushButton(tr("Accept"));
    buttonCancel->setMaximumSize(buttonCancel->sizeHint());
    buttonAccept->setMaximumSize(buttonAccept->sizeHint());
    connect(buttonCancel, SIGNAL(released()),this,SLOT(buttonCancelPressed()));
    connect(buttonAccept, SIGNAL(released()),this,SLOT(buttonAcceptPressed()));

    buttonBar->addWidget(buttonCancel, 0, Qt::AlignLeft);
    buttonBar->addWidget(buttonAccept, 0, Qt::AlignRight);

    mainLayout->addWidget(menuBar, 0);
    mainLayout->addWidget(workspaceGui, 1);
    mainLayout->addLayout(buttonBar, 1);
    this->setObjectName("main");

    setStyleSheet(EDITOR_STYLE_SHEET);
    setLayout(mainLayout);

}

void WorkspaceEditorDialog::loadFileIntoWorkspaceGui(QString filename) {
    workspaceGui->loadFromXML(filename);
}

int WorkspaceEditorDialog::openEditor(void) {
    return(this->exec());
}

int WorkspaceEditorDialog::openReconfigEditor(void) {
    workspaceGui->setForReconfigure();
    return(openEditor());
}

void WorkspaceEditorDialog::clearGUI() {
    //no good alternative to deleteing the WorkspaceEditor and replacing it with a new one
    WorkspaceEditor *newEditor = new WorkspaceEditor(M_EMBEDDED);
    mainLayout->replaceWidget(workspaceGui, newEditor);
    delete workspaceGui;
    workspaceGui = newEditor;
    workspaceGui->setObjectName("editor");
    setStyleSheet(EDITOR_STYLE_SHEET);
    connect(actionSave, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveToXML()));
    connect(actionSaveAs, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveAsToXML()));
    connect(actionLoad, SIGNAL(triggered(bool)), workspaceGui, SLOT(buttonLoadPressed()));
}

void WorkspaceEditorDialog::buttonCancelPressed() {
    workspaceGui->askToSaveUnsavedChanges();
    this->hide();
    clearGUI(); //we clear the gui because we don't want previous settings loaded into the gui to persist
    QDialog::reject();
}

void WorkspaceEditorDialog::buttonAcceptPressed() {
    QString tempFilePath = workspaceGui->askToSaveUnsavedChanges();
    if(tempFilePath.isEmpty()){
        tempFilePath = QString("%1/tempWorkspace.trodesconf").arg(QCoreApplication::applicationDirPath());
    }

    if (workspaceGui->saveToXML(tempFilePath,true)) { //only execute if the save was successful

        emit sig_openTempWorkspace(tempFilePath);
        this->hide();
        clearGUI();
        QDialog::accept();
    }

}

void WorkspaceEditorDialog::closeEvent(QCloseEvent * event){
    buttonCancelPressed();
}

NTrodeSelectionDialog::NTrodeSelectionDialog(QWidget *parent) : QWidget(parent) {
    operation = FO_OR; //OR operation by default

    mainLayout = new QVBoxLayout();

    //ini categories box
    labelCategory = new QLabel(tr("Category:"));
    categorySelector = new QComboBox();
    categorySelector->addItem("All");
    connect(categorySelector, SIGNAL(activated(int)), this, SLOT(categorySelected(int)));

    //Select all tags option
    selectAllTags = new QCheckBox("Select All Tags");
    selectAllTags->setChecked(false);
    connect(selectAllTags, &QCheckBox::stateChanged, this, &NTrodeSelectionDialog::allTagsButtonSelected);

    //ini tags list
    labelTags = new QLabel(tr("Tags:"));
    tagList = new QListWidget();
    connect(tagList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(tagSelected(QListWidgetItem*)));

    //ini operation box
    labelOperation = new QLabel(tr("Operation:"));
    operationSelector = new QComboBox();
    operationSelector->addItem("nTrodes with one selected tag (OR)");
    operationSelector->addItem("nTrodes with all selected tags (AND)");
    connect(operationSelector, SIGNAL(activated(int)), this, SLOT(operationSelected(int)));

    //clear button
    buttonClear = new QPushButton();
    buttonClear->setText("Clear");
    connect(buttonClear, SIGNAL(released()), this, SLOT(buttonClearPressed()));

    mainLayout->addWidget(labelCategory, 0, Qt::AlignCenter);
    mainLayout->addWidget(categorySelector, 0, Qt::AlignCenter);
    mainLayout->addWidget(labelTags, 0, Qt::AlignCenter);
    mainLayout->addWidget(selectAllTags, 0, Qt::AlignLeft);
    mainLayout->addWidget(tagList, 0, Qt::AlignCenter);
    mainLayout->addWidget(labelOperation, 0, Qt::AlignCenter);
    mainLayout->addWidget(operationSelector, 0, Qt::AlignCenter);
    mainLayout->addWidget(buttonClear, 0, Qt::AlignCenter);

    setLayout(mainLayout);
}

void NTrodeSelectionDialog::update() {
//    qDebug() << "Updating categories and tags";
//    CategoryDictionary cataDict = spikeConf->groupingDict;
    QHash<QString, QHash<QString,QString> > categories = spikeConf->groupingDict.getCategories();
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    categorySelector->clear();

    categorySelector->addItem("All");
    while (iter.hasNext()) {
        iter.next();
        if (iter.key() == "All") //ignore the All category
            continue;

        categorySelector->addItem(iter.key());
    }

    if (categorySelector->count() > 0) {
        categorySelector->setCurrentIndex(0);
        this->categorySelected(0);
    }

    //When updating the selection dialog, make sure to uncheck all previous selections so the user experience is not befudling
    selectedTags.clear();
    for (int i = 0; i < tagList->count(); i++) {
        tagList->item(i)->setCheckState(Qt::Unchecked);
    }
}

void NTrodeSelectionDialog::categorySelected(int categoryIndex) {
//    qDebug() << "Category Selected: " << categorySelector->currentText();
//    tagSelector->clear();
    QLabel sizingLabel;
//    sizingLabel.setText(tr("All"));
//    int minimumW = sizingLabel.sizeHint().width();

    tagList->clear();

//    QListWidgetItem *firstItem = new QListWidgetItem();
//    firstItem->setText("All");
//    firstItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
//    firstItem->setCheckState(Qt::Unchecked);

//    tagList->insertItem(0, firstItem);

//    bool checkAll = true;
    CategoryDictionary cataDict = spikeConf->groupingDict;
    QString category = categorySelector->currentText();
    QList<GroupingTag> tags;
    if (category == "All") {
        tags = cataDict.getSortedAllTagList();
    }
    else {
        QHashIterator<QString,QString> iter(spikeConf->groupingDict.getCategorysTags(categorySelector->currentText()));
        while (iter.hasNext()) {
            iter.next();
            QString curTagStr = iter.key();
            if (curTagStr == "") //ignore any empty tags
                continue;

            GroupingTag curTag;
            curTag.category = category;
            curTag.tag = curTagStr;
            tags.append(curTag);
        }
    }

    for (int i = 0; i < tags.length(); i++) {
        QListWidgetItem *newTag = new QListWidgetItem();
        GroupingTag curTag = tags.at(i);
        newTag->setText(curTag.toTagString());
        newTag->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);


        if (selectedTags.contains(curTag)) {
            newTag->setCheckState(Qt::Checked);
        }
        else {
//            checkAll = false;
            newTag->setCheckState(Qt::Unchecked);
        }

        tagList->insertItem(tagList->count(), newTag);
    }
//    if (checkAll)
//        firstItem->setCheckState(Qt::Checked);

}

void NTrodeSelectionDialog::allTagsButtonSelected(int state){
    if (selectAllTags->checkState() == Qt::Unchecked) {
        //deactivate all tags
        for (int i = 0; i < tagList->count(); i++) {
            GroupingTag curTag;
            curTag.readInTagStr(tagList->item(i)->text());
            if (selectedTags.contains(curTag))
                selectedTags.remove(curTag);
            tagList->item(i)->setCheckState(Qt::Unchecked);
        }
    }
    else {
        //activate all tags
        for (int i = 0; i < tagList->count(); i++) {
            GroupingTag curTag;
            curTag.readInTagStr(tagList->item(i)->text());
            if (!selectedTags.contains(curTag))
                selectedTags.insert(curTag, 1);
            tagList->item(i)->setCheckState(Qt::Checked);
        }
    }
    emit sig_selectTags((int)operation,selectedTags);
}
void NTrodeSelectionDialog::tagSelected(QListWidgetItem *tag) {
//    qDebug() << "Tag selected: " << tag->text();

    GroupingTag selectedTag;
    selectedTag.readInTagStr(tag->text());

    if (tag->checkState() == Qt::Checked) {
        //deactivate tag
        if (selectedTags.contains(selectedTag)) {
            selectedTags.remove(selectedTag);
        }
        tag->setCheckState(Qt::Unchecked);
    }
    else {
        //activate tag
        if (!selectedTags.contains(selectedTag)) {
            selectedTags.insert(selectedTag, 1);
        }
        tag->setCheckState(Qt::Checked);
    }
    //emit signal to select the requested tags
    emit sig_selectTags((int)operation,selectedTags);

}

void NTrodeSelectionDialog::operationSelected(int opIndex) {
//    qDebug() << "Operation Selected: ";
    if (opIndex == 0) { //OR index
        operation = FO_OR;
    }
    else if (opIndex == 1) { //AND index
        operation = FO_AND;
    }
    emit sig_selectTags((int)operation,selectedTags);
}

void NTrodeSelectionDialog::buttonClearPressed() {
//    qDebug() << "Clear Button Pressed";
    //deactivate all tags
    selectAllTags->setChecked(false);
    selectedTags.clear();
    for (int i = 0; i < tagList->count(); i++) {
        tagList->item(i)->setCheckState(Qt::Unchecked);
    }
    emit sig_clearSelection(); //this could be handled by sig_selectTags, but this will be more efficient
}

StreamDisplayOptionsDialog::StreamDisplayOptionsDialog(bool lfp, bool spike, QWidget *parent){
    mainLayout = new QVBoxLayout();

    lfpbutton = new TrodesButton;
    lfpbutton->setText("LFP");
    if(lfp) lfpbutton->setDown(true);
    connect(lfpbutton, &TrodesButton::pressed, this, &StreamDisplayOptionsDialog::lfpChosen);
    connect(lfpbutton, &TrodesButton::clicked, this, &StreamDisplayOptionsDialog::lfpChosenslot);

    spikebutton = new TrodesButton;
    spikebutton->setText("Spike");
    if(spike) spikebutton->setDown(true);
    connect(spikebutton, &TrodesButton::pressed, this, &StreamDisplayOptionsDialog::spikeChosen);
    connect(spikebutton, &TrodesButton::clicked, this, &StreamDisplayOptionsDialog::spikeChosenslot);

    rawbutton = new TrodesButton;
    rawbutton->setText("Unfiltered");
    if(!lfp && !spike) rawbutton->setDown(true);
    connect(rawbutton, &TrodesButton::pressed, this, &StreamDisplayOptionsDialog::rawChosen);
    connect(rawbutton, &TrodesButton::clicked, this, &StreamDisplayOptionsDialog::rawChosenslot);

    mainLayout->addWidget(lfpbutton);
    mainLayout->addWidget(spikebutton);
    mainLayout->addWidget(rawbutton);

    setLayout(mainLayout);
}

void StreamDisplayOptionsDialog::lfpChosenslot(){
    lfpbutton->setDown(true);
    spikebutton->setDown(false);
    rawbutton->setDown(false);
}

void StreamDisplayOptionsDialog::spikeChosenslot(){
    lfpbutton->setDown(false);
    spikebutton->setDown(true);
    rawbutton->setDown(false);
}

void StreamDisplayOptionsDialog::rawChosenslot(){
    lfpbutton->setDown(false);
    spikebutton->setDown(false);
    rawbutton->setDown(true);
}


//TriggerScopeSettingsWidget displays the 'settings' tab for each nTrode.
TriggerScopeSettingsWidget::TriggerScopeSettingsWidget(QWidget *parent, int trodeNum) :
  QWidget(parent) {
    attachedMode = false;
    attachedWidget = NULL;
    posOffset.setX(0);
    posOffset.setY(0);
    panelSize.setHeight(225);
    panelSize.setWidth(485);
    multipleSelected = false;

    TrodesFont dispFont;

//    emptyLabel = new QLabel(tr(""));
//    labels.push_back(emptyLabel);

    nTrodeNumber = trodeNum;
    widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));

    widgetLayouts[0]->setSpacing(0);
    widgetLayouts[0]->setContentsMargins(7,2,7,2);

    //Create the reference controls
    iniReferenceBox(1, dispFont);

    //Create the filter controls
    iniSpikeFilterBox(3, dispFont);

    //Create the ModuleData controls
    iniLFPBox(2, dispFont);

    //Create the Spike Trigger controls
    iniSpikeTriggerBox(4, dispFont);

    //grouping tag panel
    iniGroupingTagControls(6, dispFont);

    //display settings box
    iniDispBox(5, dispFont);

    //Select buttons
    layoutSelectButtons = new QHBoxLayout();
    widgetLayouts[0]->addLayout(layoutSelectButtons, 7, 0);

    //select by button
    buttonSelectBy = new QPushButton(tr("Select..."));
    widgets.append(buttonSelectBy);
    buttonSelectBy->setMaximumSize(buttonSelectBy->sizeHint());
    buttonSelectBy->setMinimumSize(buttonSelectBy->sizeHint());
    connect(buttonSelectBy, SIGNAL(released()), this, SIGNAL(openSelectByDialog()));
    layoutSelectButtons->addWidget(buttonSelectBy, Qt::AlignLeft);

    //Select all button
    buttonSelectAll = new QPushButton(tr("Select All"), this);
    widgets.append(buttonSelectAll);
    buttonSelectAll->setMaximumSize(buttonSelectAll->sizeHint());
    buttonSelectAll->setMinimumSize(buttonSelectAll->sizeHint());
    connect(buttonSelectAll, &QPushButton::released, this, &TriggerScopeSettingsWidget::selectAllNtrodes);
    layoutSelectButtons->addWidget(buttonSelectAll, Qt::AlignRight);

    //Add a label at the top to show which nTrode this is controlling
    QFont labelFont;
    labelFont.setPixelSize(14);
    labelFont.setFamily("Arial");
    labelFont.setBold(true);
    QGridLayout* IDlayout = new QGridLayout();
    trodeLabel = new QLabel(this);
    labels.push_back(trodeLabel);
    trodeLabel->setFont(labelFont);
    trodeLabel->setText(QString("nTrode ")+QString::number(spikeConf->ntrodes[trodeNum]->nTrodeId));
//    IDlayout->addWidget(emptyLabel,0,1);
    IDlayout->addWidget(trodeLabel,0,0);
//    linkCheckBox = new QCheckBox(this);
//    widgets.append(linkCheckBox);
//    linkCheckBox->setText("Link settings");
//    linkCheckBox->setFont(dispFont);
//    if (linkChangesBool) {
//        linkCheckBox->setChecked(true);
//    }
//    connect(linkCheckBox,SIGNAL(toggled(bool)),this,SLOT(linkBoxClicked(bool)));
//    IDlayout->addWidget(linkCheckBox,0,2);
    IDlayout->setColumnStretch(1,1);
    widgetLayouts[0]->addLayout(IDlayout,0,0);

    setLayout(widgetLayouts[0]);
    setWindowTitle(tr("nTrode Settings"));

    if (attachedMode) {
        setWindowFlags(Qt::FramelessWindowHint);

        //initialize animations
        a_horizontalExpand = new QPropertyAnimation(this, "geometry");
        connect(a_horizontalExpand, SIGNAL(finished()), this, SLOT(setWidgetsVisible()));
        a_horizontalClose = new QPropertyAnimation(this, "geometry");
        a_minSize = new QPropertyAnimation(this, "minimumSize");
    }
    else {
        setWindowFlags(Qt::Tool | Qt::WindowStaysOnTopHint);
    }
    setWidgetsHidden();

}

TriggerScopeSettingsWidget::~TriggerScopeSettingsWidget() {
//    qDebug()<< "Deleting the Panel";
//    qDebug() << "   Visble? " << isVisible();
//    qDebug() << "final location: " << this->pos();

    emit saveGeo(this->geometry());

    if (attachedMode) {
        delete a_horizontalExpand;
        delete a_horizontalClose;
        delete a_minSize;
    }
}

//This function is used to pass a hash table of currently selected nTrodes to the TriggerScopeSettingsWidget object
void TriggerScopeSettingsWidget::setSelectedNTrodes(QHash<int, int> selectedNTrodes) {
//    selectedNTrodes.
    QString multiTooltip = "";
    selectedIndicis.clear();
    selectedIndicis = selectedNTrodes.keys();
    QList<int> selectedIDs = selectedNTrodes.values();
    int curLoadedNTrodeID = spikeConf->ntrodes[nTrodeNumber]->nTrodeId;
    std::sort(selectedIDs.begin(),selectedIDs.end());

    int num = 0;
    bool loadNewNTrode = true;
    for (int i = 0; i < selectedIDs.length(); i++) {
        if (selectedIDs.at(i) == curLoadedNTrodeID)
            loadNewNTrode = false;
        multiTooltip = QString("%1%2, ").arg(multiTooltip).arg(selectedIDs.at(i));
        num++;
    }
    multiTooltip = QString("%1").arg(multiTooltip.left(multiTooltip.length()-2));

    if (loadNewNTrode) { //if ntrode from which setting were previously loaded was unselected, load in new settings
        //load settings from the lowest numbered nTrode in the list of selected
        if (selectedIDs.count() > 0) {
            int newID = selectedIDs.first();
            nTrodeNumber = spikeConf->ntrodes.index(newID);
        }
        else { //no ntrodes selected
            //todo: if no ntrodes were selected, close the panel and uncheck the button;
        }

    }

    if (num > 1) {
        trodeLabel->setText("nTrode: Multiple");
        multipleSelected = true;
    }
    else {
        if (selectedIDs.length() > 0) {
//            qDebug() << "Loading label 'selectedIDs.at(0)";
            trodeLabel->setText(QString("nTrode %1").arg(selectedIDs.at(0)));
        }
        else {
//            qDebug() << "Loading label 'ntrodes[nTrodeNumber]->nTrodeId";
            trodeLabel->setText(QString("nTrode %1").arg(spikeConf->ntrodes[nTrodeNumber]->nTrodeId));
        }
        multipleSelected = false;
    }
//    qDebug() << "Panel Dimensions = " << this->geometry().width() << " x " << this->geometry().height();
    trodeLabel->setToolTip(multiTooltip);
    loadNTrodeIntoPanel(spikeConf->ntrodes.at(nTrodeNumber)->nTrodeId);
    setGroupingTagWidgetsVisibility();
    checkNTrodesForConflicts();
}

bool TriggerScopeSettingsWidget::eventFilter(QObject *obj, QEvent *ev) {
    this->raise(); //This keeps the panel's edge from being separated by a focus line
    if (attachedWidget != NULL && obj == attachedWidget) {
        if (ev->type() == QEvent::Move) {
            QPoint newPos = static_cast<QMoveEvent*>(ev)->pos();
            this->setPosition(newPos.x()+this->posOffset.x(), newPos.y()+this->posOffset.y());
//            resize(static_cast<QResizeEvent*>(ev)->size());
        }
        else if (ev->type() == QEvent::Resize) {
            QSize newSize = static_cast<QResizeEvent*>(ev)->size();
            //only offset we currently care about is the x offset
            this->setPositionOffset(newSize.width(), posOffset.y());
            this->setPosition(attachedWidget->geometry().x()+this->posOffset.x(), attachedWidget->geometry().y()+this->posOffset.y());
        }
    }
    return(QWidget::eventFilter(obj, ev));
}

bool TriggerScopeSettingsWidget::event(QEvent *ev) {
    if (ev->type() == QEvent::Close) {
        emit closing();
    }

    return(QWidget::event(ev));
}

void TriggerScopeSettingsWidget::iniReferenceBox(int columnPos, TrodesFont font) {
//    int labelIndex = labels.size()-1;
    refBox = new QGroupBox(tr("Referencing Settings"),this);
    widgets.append(refBox);
    refBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(refBox));
    RefTrodeBox = new QComboBox(this);
    widgets.append(RefTrodeBox);

    RefTrodeBox->setFont(font);
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
      RefTrodeBox->addItem(QString::number(spikeConf->ntrodes[i]->nTrodeId));
    }
    RefTrodeBox->setFixedWidth(50);
    RefTrodeBox->setMinimumHeight(RefTrodeBox->sizeHint().height());


    RefChannelBox = new QComboBox(this);
    widgets.append(RefChannelBox);
    RefChannelBox->setFont(font);
    labels.push_back(new QLabel(tr("nTrode"),this));
    REFNTRODE_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("Channel"),this));//1
    REFCHAN_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);

    spikeRefCheckBox = new QCheckBox(tr("Spikes"));
    spikeRefCheckBox->setFont(font);
    spikeRefCheckBox->setMinimumSize(spikeRefCheckBox->sizeHint());
    spikeRefCheckBox->setMinimumHeight(spikeRefCheckBox->sizeHint().height()+15);
    spikeRefCheckBox->setMinimumWidth(spikeRefCheckBox->sizeHint().width()+10);
    spikeRefCheckBox->setCheckable(true);

    lfpRefCheckBox = new QCheckBox(tr("LFP"));
    lfpRefCheckBox->setFont(font);
    lfpRefCheckBox->setMinimumSize(lfpRefCheckBox->sizeHint());
    lfpRefCheckBox->setMinimumHeight(lfpRefCheckBox->sizeHint().height()+15);
    lfpRefCheckBox->setMinimumWidth(lfpRefCheckBox->sizeHint().width()+10);
    lfpRefCheckBox->setCheckable(true);

    QHBoxLayout *checkBoxLayout = new QHBoxLayout();
    QLabel *ckboxLabel = new QLabel(tr("Apply Ref to: "));
    ckboxLabel->setMinimumWidth(ckboxLabel->sizeHint().width()+5);
    ckboxLabel->setFont(font);
    labels.push_back(ckboxLabel);//2
    checkBoxLayout->addWidget(ckboxLabel, 0, Qt::AlignLeft);
    checkBoxLayout->addWidget(spikeRefCheckBox, 0, Qt::AlignCenter);
    checkBoxLayout->addWidget(lfpRefCheckBox, 0, Qt::AlignCenter);

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[0],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[1],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(RefTrodeBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(RefChannelBox,1,1,Qt::AlignCenter);
//    widgetLayouts[groupBoxIndex]->addWidget(spikeRefCheckBox, 2, 0, 1, 1, Qt::AlignCenter);
//    widgetLayouts[groupBoxIndex]->addWidget(lfpRefCheckBox, 2, 1, 1, 1, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addLayout(checkBoxLayout, 2, 0, 5, 0, Qt::AlignCenter);

    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    refBox->setLayout(widgetLayouts[groupBoxIndex]);
//    refBox->setCheckable(true);
    refBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->refOn);
    refBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(refBox,columnPos,0);

    //Input initial values
    int refID = spikeConf->ntrodes[nTrodeNumber]->refNTrode;
    RefTrodeBox->setCurrentIndex(refID);
    RefChannelBox->clear();
    for (int i = 0; i < spikeConf->ntrodes[refID]->hw_chan.length(); i++)
        RefChannelBox->addItem(QString::number(i+1));

    RefChannelBox->setCurrentIndex(spikeConf->ntrodes[nTrodeNumber]->refChan);
    RefChannelBox->setMinimumHeight(RefChannelBox->sizeHint().height());

    //Connections
//    connect(refBox,SIGNAL(clicked(bool)),this,SLOT(updateRefSwitch(bool)));
    connect(RefTrodeBox,SIGNAL(activated(int)),this,SLOT(updateRefTrode(int)));
    connect(RefChannelBox,SIGNAL(activated(int)),this,SLOT(updateRefChan(int)));
    connect(spikeRefCheckBox,SIGNAL(clicked(bool)),this,SLOT(updateRefSwitch(bool)));
    connect(lfpRefCheckBox,SIGNAL(clicked(bool)),this,SLOT(updateLFPFilterSwitch(bool)));

//    connect(refBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(RefTrodeBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(RefChannelBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(spikeRefCheckBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(lfpRefCheckBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));

    refBox->setMaximumHeight(refBox->sizeHint().height());
    refBox->setMinimumHeight(refBox->sizeHint().height());
}

void TriggerScopeSettingsWidget::iniSpikeFilterBox(int columnPos, TrodesFont font) {
    filterBox = new QGroupBox(tr("Spike filter"),this);
    widgets.append(filterBox);
    filterBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(filterBox));
    labels.push_back(new QLabel(tr("Low (Hz)"),this));//3
    LOWFILTER_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("High (Hz)"),this));//4
    HIGHFILTER_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    lowFilterCutoffBox = new QComboBox(this);
    widgets.append(lowFilterCutoffBox);
    lowFilterCutoffBox->setFont(font);
    QList<int> lowList;
    for (int i = 200;i < 700;i+=100) {
        lowList.append(i);
        lowFilterCutoffBox->addItem(QString::number(lowList.last()));
    }
    //Set the current lower filter setting from config file (find nearest available)
    for (int i=0;i<lowList.length();i++) {
        if (lowList[i] <= spikeConf->ntrodes[nTrodeNumber]->lowFilter) {
            lowFilterCutoffBox->setCurrentIndex(i);
        }
    }
    lowFilterCutoffBox->setFixedWidth(60);
    lowFilterCutoffBox->setMinimumHeight(lowFilterCutoffBox->sizeHint().height());
    highFilterCutoffBox = new QComboBox(this);
    widgets.append(highFilterCutoffBox);
    highFilterCutoffBox->setFont(font);
    QList<int> highList;
    for (int i = 5000;i < 7000;i+=1000) {
        highList.append(i);
        highFilterCutoffBox->addItem(QString::number(highList.last()));
    }
    //Set the current upper filter setting from config file (find nearest available)
    for (int i=0;i<highList.length();i++) {
        if (highList[i] <= spikeConf->ntrodes[nTrodeNumber]->highFilter) {
            highFilterCutoffBox->setCurrentIndex(i);
        }
    }
    highFilterCutoffBox->setFixedWidth(70);
    highFilterCutoffBox->setMinimumHeight(highFilterCutoffBox->sizeHint().height());
    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[3],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[4],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(lowFilterCutoffBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(highFilterCutoffBox,1,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    filterBox->setLayout(widgetLayouts[groupBoxIndex]);
    filterBox->setCheckable(true);
    filterBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->filterOn);
    filterBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(filterBox,columnPos,0);

    //connections
    connect(filterBox,SIGNAL(clicked(bool)),this,SLOT(updateFilterSwitch(bool)));
    connect(highFilterCutoffBox,SIGNAL(activated(int)),this,SLOT(updateUpperFilter()));
    connect(lowFilterCutoffBox,SIGNAL(activated(int)),this,SLOT(updateLowerFilter()));

    connect(filterBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(highFilterCutoffBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(lowFilterCutoffBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));

    filterBox->setMaximumHeight(filterBox->sizeHint().height());
}

void TriggerScopeSettingsWidget::iniSpikeTriggerBox(int columnPos, TrodesFont font) {
    triggerBox = new QGroupBox(tr("Spike trigger"),this);
    widgets.append(triggerBox);
    triggerBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(triggerBox));
    labels.push_back(new QLabel(tr("Threshold (μV)"),this)); //7
    THRESH_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);

    threshSpinBox = new QSpinBox();
    widgets.append(threshSpinBox);
    threshSpinBox->setMinimum(10);
    threshSpinBox->setMaximum(4000);
    threshSpinBox->setSingleStep(10);
    threshSpinBox->setToolTip(tr("Trigger threshold (μV)"));
    threshSpinBox->setValue(spikeConf->ntrodes[nTrodeNumber]->thresh.at(0));
    threshSpinBox->setMinimumHeight(threshSpinBox->sizeHint().height());

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[7],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(threshSpinBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    triggerBox->setLayout(widgetLayouts[groupBoxIndex]);
//    triggerBox->setCheckable(true);
//    triggerBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->triggerOn.at(0));
    triggerBox->setFixedHeight(75);
//    connect(triggerBox, SIGNAL(clicked(bool)), this, SLOT(updateSpikeTrigger(bool)));

    widgetLayouts[0]->addWidget(triggerBox,columnPos,0);

    //Connections
//    connect(triggerBox, SIGNAL(clicked(bool)), this, SLOT(updateSpikeTrigger(bool)));
    connect(threshSpinBox, SIGNAL(valueChanged(int)), this, SLOT(updateThresh()));

//    connect(triggerBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(threshSpinBox, SIGNAL(editingFinished(int)),this,SIGNAL(configChanged()));

    triggerBox->setMaximumHeight(triggerBox->sizeHint().height());
}

void TriggerScopeSettingsWidget::iniLFPBox(int columnPos, TrodesFont font) {
    //The LFP settings are used for more than just sending to modules, so please don't change to name to
    //something like "data to modules, etc".
    ModuleDataFilterBox = new QGroupBox(tr("LFP Filter"),this);
    widgets.append(ModuleDataFilterBox);
    ModuleDataFilterBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(ModuleDataFilterBox));
    labels.push_back(new QLabel(tr("Channel"),this));//5
    MODDATACHAN_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("High cutoff (Hz)"),this)); //6
    MODDATAHIGH_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    ModuleDataHighFilterCutoffBox = new QComboBox(this);
    widgets.append(ModuleDataHighFilterCutoffBox);
    ModuleDataHighFilterCutoffBox->setFont(font);
    QList<int> ModuleDatahighList;
    //TODO avaliable filter values should be specified by filter object, not hardcoded
    for (int i = 100;i <= 500;i+=100) {
        ModuleDatahighList.append(i);
        ModuleDataHighFilterCutoffBox->addItem(QString::number(ModuleDatahighList.last()));
    }
    for (int i=0;i<ModuleDatahighList.length();i++) {
        if (ModuleDatahighList[i] <= spikeConf->ntrodes[nTrodeNumber]->moduleDataHighFilter) {
            ModuleDataHighFilterCutoffBox->setCurrentIndex(i);
        }
    }
    ModuleDataHighFilterCutoffBox->setMinimumHeight(ModuleDataHighFilterCutoffBox->sizeHint().height());
    ModuleDataChannelBox = new QComboBox(this);
    widgets.append(ModuleDataChannelBox);
    ModuleDataChannelBox->setFont(font);
    for (int i = 0; i < spikeConf->ntrodes[nTrodeNumber]->hw_chan.length(); i++)
      ModuleDataChannelBox->addItem(QString::number(i+1));

    ModuleDataChannelBox->setCurrentIndex(spikeConf->ntrodes[nTrodeNumber]->moduleDataChan);
    ModuleDataChannelBox->setMinimumHeight(ModuleDataChannelBox->sizeHint().height());
    //Disabling the ability to select main LFP channel until Issue #30 is fixed
    ModuleDataChannelBox->setEnabled(false);

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[5],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[6],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(ModuleDataChannelBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(ModuleDataHighFilterCutoffBox,1,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    ModuleDataFilterBox->setLayout(widgetLayouts[groupBoxIndex]);
    ModuleDataFilterBox->setCheckable(true);
    ModuleDataFilterBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(ModuleDataFilterBox,columnPos,0);

    //Connections
    connect(ModuleDataFilterBox, SIGNAL(clicked(bool)), SLOT(updateModuleDataFilterSwitch(bool)));
    connect(ModuleDataChannelBox,SIGNAL(activated(int)),this,SLOT(updateModuleDataChannel()));
    connect(ModuleDataHighFilterCutoffBox,SIGNAL(activated(int)),this,SLOT(updateModuleDataUpperFilter()));

    connect(ModuleDataFilterBox,SIGNAL(clicked(bool)),this,SIGNAL(configChanged()));
    connect(ModuleDataChannelBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));
    connect(ModuleDataHighFilterCutoffBox,SIGNAL(activated(int)),this,SIGNAL(configChanged()));

    ModuleDataFilterBox->setMaximumHeight(ModuleDataFilterBox->sizeHint().height());
}

void TriggerScopeSettingsWidget::iniGroupingTagControls(int columnPos, TrodesFont font) {
    //make visualizer first
    groupingTagBox = new QGroupBox(tr("Grouping Tags"), this);
    groupingTagList = new QListWidget();
    groupingTagSharedList = new QListWidget();
    groupingTagAllList = new QListWidget();

    groupingTabView = new QTabWidget();
    groupingTabView->addTab(groupingTagAllList, "All");
    groupingTabView->addTab(groupingTagSharedList, "Shared");

    buttonAddRemoveTags = new QPushButton("Add/Remove Tags");
    groupingDialog = NULL;
    buttonAddRemoveTags->setMaximumSize(buttonAddRemoveTags->sizeHint());
    widgets.append(groupingTagBox);
    widgets.append(groupingTagList);
    widgets.append(buttonAddRemoveTags);
    widgets.append(groupingTabView);

    widgetLayouts.push_back(new QGridLayout(groupingTagBox));
    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(groupingTagList, 0, 0, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(groupingTabView, 0, 0, Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(buttonAddRemoveTags, 1, 0, Qt::AlignLeft);
    widgetLayouts[0]->addWidget(groupingTagBox, columnPos, 0);

//    connect(buttonAddRemoveTags, SIGNAL(released()), this, SLOT(addRemoveTagsButtonPressed()));
    connect(buttonAddRemoveTags, SIGNAL(released()), this, SLOT(addRemoveTagsButtonPressed()));
}

void TriggerScopeSettingsWidget::iniDispBox(int columnPos, TrodesFont font) {
    displayBox = new QGroupBox(tr("Display Settings"), this);
    widgets.append(displayBox);
    displayBox->setFont(font);
    widgetLayouts.push_back(new QGridLayout(displayBox));
    labels.push_back(new QLabel(tr("Max Disp (μV)"),this));//8
    MAXDISP_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);
    labels.push_back(new QLabel(tr("Channel Color"),this));//9
    COLOR_LABEL_INDEX = labels.size()-1;
    labels.last()->setFont(font);

    maxDispSpinBox = new QSpinBox();
    widgets.append(maxDispSpinBox);
    maxDispSpinBox->setMinimum(50);
    maxDispSpinBox->setMaximum(4000);
    maxDispSpinBox->setSingleStep(25);
    maxDispSpinBox->setToolTip(tr("Spike Display Range (+/- μV)"));
    maxDispSpinBox->setValue(spikeConf->ntrodes[nTrodeNumber]->maxDisp.at(0));
    maxDispSpinBox->setMinimumHeight(maxDispSpinBox->sizeHint().height());

    buttonColorBox = new ClickableFrame();
    buttonColorBox->setFixedSize(25,25);
    buttonColorBox->setStyleSheet("background-color:#aaaaaa");

    int groupBoxIndex = widgetLayouts.length() - 1;
    widgetLayouts[groupBoxIndex]->addWidget(labels[8],0,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(labels[9],0,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(maxDispSpinBox,1,0,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->addWidget(buttonColorBox,1,1,Qt::AlignCenter);
    widgetLayouts[groupBoxIndex]->setVerticalSpacing(0);
    displayBox->setLayout(widgetLayouts[groupBoxIndex]);

    widgetLayouts[0]->addWidget(displayBox,columnPos,0);

    //Connections
    connect(buttonColorBox, SIGNAL(sig_clicked()), this, SLOT(getColorDialog()));
    connect(maxDispSpinBox, SIGNAL(valueChanged(int)), this, SLOT(updateMaxDisp()));

    connect(maxDispSpinBox, SIGNAL(editingFinished(int)),this,SIGNAL(configChanged()));

    displayBox->setMaximumHeight(displayBox->sizeHint().height());
}

void TriggerScopeSettingsWidget::attachToWidget(QWidget *obj) {
    if (!attachedMode)
        return;

    attachedWidget = obj;
    obj->installEventFilter(this);
}

void TriggerScopeSettingsWidget::loadNTrodeIntoPanel(int ntrodeID) {
//    qDebug() << "Loading ntrode " << ntrodeID << " at index " << spikeConf->ntrodes.index(ntrodeID);

    SingleSpikeTrodeConf *nTrode = spikeConf->ntrodes.at(spikeConf->ntrodes.index(ntrodeID));

    if (groupingDialog != NULL)
        groupingDialog->setVisible(false); //make the grouping dialog box disapear if it hasn't already

    loadRef(nTrode);
    loadSpikeFilter(nTrode);
    loadSpikeTrigger(nTrode);
    loadLFP(nTrode);
    loadTags(nTrode);
    loadDisplaySettings(nTrode);
}

void TriggerScopeSettingsWidget::loadRef(SingleSpikeTrodeConf *nTrode) {
//    refBox->setChecked(nTrode->refOn);
    spikeRefCheckBox->setChecked(nTrode->refOn);
    lfpRefCheckBox->setChecked(nTrode->lfpRefOn);
    RefTrodeBox->setCurrentIndex(nTrode->refNTrode);
    RefChannelBox->setCurrentIndex(nTrode->refChan);
}

void TriggerScopeSettingsWidget::loadSpikeFilter(SingleSpikeTrodeConf *nTrode) {
    filterBox->setChecked(nTrode->filterOn);
    for (int i = 0; i < lowFilterCutoffBox->count(); i++) {
        if (lowFilterCutoffBox->itemText(i).toInt() >= nTrode->lowFilter) {
            lowFilterCutoffBox->setCurrentIndex(i);
            break;
        }
        else if (i == lowFilterCutoffBox->count()-1) {
            lowFilterCutoffBox->setCurrentIndex(i);
        }
    }
    for (int i = 0; i < highFilterCutoffBox->count(); i++) {
        if (highFilterCutoffBox->itemText(i).toInt() >= nTrode->highFilter) {
            highFilterCutoffBox->setCurrentIndex(i);
            break;
        }
        else if (i == highFilterCutoffBox->count()-1) {
            highFilterCutoffBox->setCurrentIndex(i);
        }
    }
}

void TriggerScopeSettingsWidget::loadSpikeTrigger(SingleSpikeTrodeConf *nTrode) {
//    triggerBox->setChecked(nTrode->triggerOn.at(0));
    threshSpinBox->setValue(nTrode->thresh.at(0));
//    maxDispSpinBox->setValue(nTrode->maxDisp.at(0));
}

void TriggerScopeSettingsWidget::loadLFP(SingleSpikeTrodeConf *nTrode) {
    ModuleDataFilterBox->setChecked(nTrode->lfpFilterOn);
    ModuleDataChannelBox->setCurrentIndex(nTrode->moduleDataChan);
    for (int i = 0; i < ModuleDataHighFilterCutoffBox->count(); i++) {
        if (ModuleDataHighFilterCutoffBox->itemText(i).toInt() >= nTrode->moduleDataHighFilter) {
            ModuleDataHighFilterCutoffBox->setCurrentIndex(i);
            break;
        }
        else if (i == ModuleDataHighFilterCutoffBox->count()-1) {
            ModuleDataHighFilterCutoffBox->setCurrentIndex(i);
        }
    }
}

void TriggerScopeSettingsWidget::loadTags(SingleSpikeTrodeConf *nTrode) {
    allTags.clear(); //clear the selected all tags hash (to be loaded into the panel)
    if (multipleSelected) { // if multiple ntrodes were selected, load all their tags and their shared tags into the tab view
        //All tags
        groupingTagAllList->clear();
        QList<QList<QString> > inputTags;
        for (int k = 0; k < selectedIndicis.length(); k++) {
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[selectedIndicis.at(k)];

            QHashIterator<GroupingTag, int> i(curNTrode->gTags);

            while (i.hasNext()) {
                i.next();
//                QString curCata = spikeConf->groupingDict.getTagsCategory(i.key());
                QString curCata = i.key().category;
                QString curTag = i.key().tag;
//                qDebug() << "[" << curCata << "] " << curTag;
                bool createNewCata = true;
                for (int j = 0; j < inputTags.length(); j++) {
                    if (inputTags.at(j).at(0) == curCata) { //first item of each sub list will be catagory
                        createNewCata = false;
                        QList<QString> curTagList = inputTags.at(j);
                        bool tagAlreadyInList = false;
                        for (int y = 1; y < curTagList.length(); y++) { //check the list to see if the tag is already in it
                            if (curTagList.at(y) == curTag) {
                                tagAlreadyInList = true;
                                break;
                            }

                        }
                        if (!tagAlreadyInList) { //only add the tag if it's not already in the list
                            curTagList.push_back(curTag);
                            inputTags.replace(j, curTagList);
                        }
                        break;
                    }
                }
                if (createNewCata) {
                    QList<QString> newCataList;
                    newCataList.push_back(curCata);
                    newCataList.push_back(curTag);
                    inputTags.push_back(newCataList);
                }
            }
        }
        //now put tags into the groupingTagAllList
        for (int j = 0; j < inputTags.length(); j++) {
            QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
            for (int k = 1; k < inputTags.at(j).length(); k++) {
                QString curTag = inputTags.at(j).at(k);
                GroupingTag curGTag;
                curGTag.tag = curTag;
                curGTag.category = curCata;
                allTags.insert(curGTag, 1);
//                QString newRow = QString("[%1] %2").arg(curCata).arg(curTag);
                QString newRow = curGTag.toTagString();
                groupingTagAllList->addItem(newRow);
            }
        }
        //shared tags
        groupingTagSharedList->clear();
        inputTags.clear();

        for (int k = 0; k < selectedIndicis.length(); k++) {
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[selectedIndicis.at(k)];

            if (k == 0) {
                inputTags = Helper::getSortedTagList(curNTrode);
            }
            else {
                for (int i = 0; i < inputTags.length(); i++) {
                    QList<QString> curTagList = inputTags.at(i);

                    QString curCata = inputTags.at(i).at(0);
                    for (int j = 1; j < curTagList.length(); j++) {
                        QString curTag = curTagList.at(j);
                        GroupingTag curGTag;
                        curGTag.category = curCata;
                        curGTag.tag = curTag;
                        if (!curNTrode->gTags.contains(curGTag)) {
                            curTagList.removeAt(j);
                            j--;
                        }
                    }
                    inputTags.replace(i, curTagList);
                }
            }
        }
        //now put tags into the groupingTagSharedList
        for (int j = 0; j < inputTags.length(); j++) {
            QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
            for (int k = 1; k < inputTags.at(j).length(); k++) {
                QString curTag = inputTags.at(j).at(k);
                QString newRow = QString("[%1] %2").arg(curCata).arg(curTag);
                groupingTagSharedList->addItem(newRow);
            }
        }

    }
    else { // if one ntode selected, load the tags for that nTrode into the groupingTagList
        groupingTagList->clear();
        QList<QList<QString> > inputTags;
        inputTags = Helper::getSortedTagList(nTrode);

        //now put tags into the groupingTagList
        for (int j = 0; j < inputTags.length(); j++) {
            QString curCata = inputTags.at(j).at(0); //first item of each sub list is the catagorey
            for (int k = 1; k < inputTags.at(j).length(); k++) {
                QString curTag = inputTags.at(j).at(k);
                GroupingTag curGTag;
                curGTag.tag = curTag;
                curGTag.category = curCata;
                allTags.insert(curGTag, 1);
//                QString newRow = QString("[%1] %2").arg(curCata).arg(curTag);
                QString newRow = curGTag.toTagString();
                groupingTagList->addItem(newRow);
            }
        }
    }

}

void TriggerScopeSettingsWidget::loadDisplaySettings(SingleSpikeTrodeConf *nTrode) {
    maxDispSpinBox->setValue(nTrode->maxDisp.at(0));
    buttonColorBox->setStyleSheet(QString("background-color:%1").arg(nTrode->color.name()));

}

void TriggerScopeSettingsWidget::setEnabledForStreaming(bool streamOn) {
//    linkCheckBox->setEnabled(!streamOn);
    refBox->setEnabled(!streamOn);
    filterBox->setEnabled(!streamOn);
    ModuleDataFilterBox->setEnabled(!streamOn);
    RefTrodeBox->setEnabled(!streamOn);
    RefChannelBox->setEnabled(!streamOn);
    lowFilterCutoffBox->setEnabled(!streamOn);
    highFilterCutoffBox->setEnabled(!streamOn);
    ModuleDataHighFilterCutoffBox->setEnabled(!streamOn);


    //Disabling the ability to select main LFP channel until Issue #30 is fixed
    //ModuleDataChannelBox->setEnabled(false);
    ModuleDataChannelBox->setEnabled(!streamOn);
}

bool TriggerScopeSettingsWidget::checkNTrodesForConflicts() {
    resetAllLabelsToDefault();
    setAllLabelColors(QColor("black"));
    bool conflict = false;
    QString modifiedNtrodesMsg = "";
    for (int i = 0; i < selectedIndicis.length(); i++) {
        if (checkSelectedNTrodesForConflicts(spikeConf->ntrodes[selectedIndicis.at(i)])) {
            conflict = true;
            modifiedNtrodesMsg = "  *Mismatched nTrode Settings*";
        }
    }
//    labelMismatchSettingsWarning->setText(modifiedNtrodesMsg);
    QString toolTipMsg = "Settings across selected nTrodes differ.";
//    labelMismatchSettingsWarning->setToolTip(toolTipMsg);
    return(conflict);
}

//This function checks the specified 'checkedNTrode' against all other selected nTrodes
bool TriggerScopeSettingsWidget::checkSelectedNTrodesForConflicts(SingleSpikeTrodeConf* checkedNTrode) {
    bool conflict = false;
//    qDebug() << "For nTrode " << checkedNTrode->nTrodeId;

    for (int i = 0; i < selectedIndicis.length(); i++) {
        SingleSpikeTrodeConf* curNTrode = spikeConf->ntrodes[selectedIndicis.at(i)];
        if (curNTrode->nTrodeId == checkedNTrode->nTrodeId)
            continue; //no reason to check the nTrode against itself

        if (checkedNTrode->filterOn != curNTrode->filterOn) {
//            qDebug() << "Conflict (spike filter bool) ";
            QString str = filterBox->title();
            if (!str.contains("*"))
                str.append("*");
            filterBox->setTitle(str);
            Helper::setWidgetTextPaletteColor(filterBox, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->lowFilter != curNTrode->lowFilter) {
            //            qDebug() << "Conflict (spike low) ";
            QString str = labels.at(LOWFILTER_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(LOWFILTER_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(LOWFILTER_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->highFilter != curNTrode->highFilter) {
            //            qDebug() << "Conflict (spike high) ";
            QString str = labels.at(HIGHFILTER_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(HIGHFILTER_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(HIGHFILTER_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->refOn != curNTrode->refOn) {
//            qDebug() << "Conflict (Spike Ref bool) ";
            QString str = spikeRefCheckBox->text();
            if (!str.contains("*"))
                str.append("*");
            spikeRefCheckBox->setText(str);
            Helper::setWidgetTextPaletteColor(spikeRefCheckBox, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->lfpRefOn != curNTrode->lfpRefOn) {
//            qDebug() << "Conflict (LFP Ref bool) ";
            QString str = lfpRefCheckBox->text();
            if (!str.contains("*"))
                str.append("*");
            lfpRefCheckBox->setText(str);
            Helper::setWidgetTextPaletteColor(lfpRefCheckBox, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->lfpFilterOn != curNTrode->lfpFilterOn) {
//            qDebug() << "Conflict (LFP Filter) ";
            QString str = ModuleDataFilterBox->title();
            if (!str.contains("*"))
                str.append("*");
            ModuleDataFilterBox->setTitle(str);
            Helper::setWidgetTextPaletteColor(ModuleDataFilterBox, QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->refNTrodeID != curNTrode->refNTrodeID) {
            //            qDebug() << "Conflict (dig ref nTrode ID)";
            QString str = labels.at(REFNTRODE_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(REFNTRODE_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(REFNTRODE_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->refChan != curNTrode->refChan) {
            //            qDebug() << "Conflict (dig ref channel)";
            QString str = labels.at(REFCHAN_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(REFCHAN_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(REFCHAN_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->moduleDataChan != curNTrode->moduleDataChan) {
            //            qDebug() << "Conflict (LFP Channel)";
            QString str = labels.at(MODDATACHAN_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(MODDATACHAN_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(MODDATACHAN_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->moduleDataHighFilter != curNTrode->moduleDataHighFilter) {
            //            qDebug() << "Conflict (LFP high filter)";
            QString str = labels.at(MODDATAHIGH_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(MODDATAHIGH_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(MODDATAHIGH_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

//        if (checkedNTrode->triggerOn.at(0) != curNTrode->triggerOn.at(0)) {
//            //            qDebug() << "Conflict (enable trigger)";
//            QString str = triggerBox->title();
//            if (!str.contains("*"))
//                str.append("*");
//            triggerBox->setTitle(str);
//            Helper::setWidgetTextPaletteColor(triggerBox, QColor("red"));
//            conflict = true;
//        }

        if (checkedNTrode->thresh.at(0) != curNTrode->thresh.at(0)) {
            //            qDebug() << "Conflict (Thresh value)";
            QString str = labels.at(THRESH_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(THRESH_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(THRESH_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->maxDisp.at(0) != curNTrode->maxDisp.at(0)) {
            //            qDebug() << "Conflict (Max Display)";
            QString str = labels.at(MAXDISP_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(MAXDISP_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(MAXDISP_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (checkedNTrode->color != curNTrode->color) {
//            qDebug() << "Conflict (color);";
            QString str = labels.at(COLOR_LABEL_INDEX)->text();
            if (!str.contains("*"))
                str.append("*");
            labels.at(COLOR_LABEL_INDEX)->setText(str);
            Helper::setWidgetTextPaletteColor(labels.at(COLOR_LABEL_INDEX), QColor("red"));
            conflict = true;
        }

        if (groupingTagSharedList->count() != groupingTagAllList->count()){
//            qDebug() << "Conflict (Mismatched Grouping Tags)";
            QString str = groupingTagBox->title();
            if (!str.contains("*"))
                str.append("*");
            groupingTagBox->setTitle(str);
            conflict = true;
        }


    }
    return(conflict);
}

//this function resets all label text to their default values
void TriggerScopeSettingsWidget::resetAllLabelsToDefault() {
    for (int i = 0; i < labels.length(); i++) {
//        QLabel *curLabel = labels.at(i);
        QString str = labels.at(i)->text();
        if (str.contains("*"))
            str.remove("*");
        labels.at(i)->setText(str);
    }
    QString str;
    str = (QString)filterBox->title();
    if (str.contains("*"))
        str.remove("*");
    filterBox->setTitle(str);

    str = spikeRefCheckBox->text();
    if (str.contains("*"))
        str.remove("*");
    spikeRefCheckBox->setText(str);

    str = lfpRefCheckBox->text();
    if (str.contains("*"))
        str.remove("*");
    lfpRefCheckBox->setText(str);

    str = ModuleDataFilterBox->title();
    if (str.contains("*"))
        str.remove("*");
    ModuleDataFilterBox->setTitle(str);

    str = groupingTagBox->title();
    if (str.contains("*"))
        str.remove("*");
    groupingTagBox->setTitle(str);
}

//This function sets the text color of all the labels in the TriggerScopeSettingsWidget to black
void TriggerScopeSettingsWidget::setAllLabelColors(QColor newColor) {
    for (int i = 0; i < labels.length(); i++) {
        Helper::setWidgetTextPaletteColor(labels.at(i), newColor);
    }
    Helper::setWidgetTextPaletteColor(filterBox, newColor);
//    Helper::setWidgetTextPaletteColor(refBox, newColor);
    Helper::setWidgetTextPaletteColor(spikeRefCheckBox, newColor);
    Helper::setWidgetTextPaletteColor(lfpRefCheckBox, newColor);
    Helper::setWidgetTextPaletteColor(ModuleDataFilterBox, newColor);
    Helper::setWidgetTextPaletteColor(triggerBox, newColor);
//    groupingTagBox->setTitle("");
}

//void TriggerScopeSettingsWidget::linkBoxClicked(bool checked) {
//    emit toggleLinkChanges(checked);
//}

void TriggerScopeSettingsWidget::setMaxDisplay(int newMaxDisp) {
    if (maxDispSpinBox != NULL) {
        maxDispSpinBox->setValue(newMaxDisp);
        updateMaxDisp();
        emit configChanged();
    }
}

void TriggerScopeSettingsWidget::setThresh(int newThresh) {
    if (threshSpinBox != NULL) {
        threshSpinBox->setValue(newThresh);
        updateThresh();
        emit configChanged();
    }
}

void TriggerScopeSettingsWidget::setChanColorBox(QColor newColor) {
    buttonColorBox->setStyleSheet(QString("background-color:%1").arg(newColor.name()));
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateRefChan(int) {
    updateRefChan();
}


void TriggerScopeSettingsWidget::updateRefChan() {
//    qDebug() << "UpdateRefChan";
//    //qDebug() << "Update Ref Channel";
    //int newRefTrode  = RefTrodeBox->currentText().toInt()-1;

    int newRefTrode  = RefTrodeBox->currentText().toInt();

    bool foundRefTrode = false;
    int newRefTrodeInd;
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        if (spikeConf->ntrodes[i]->nTrodeId == newRefTrode) {
            newRefTrodeInd = i;
            foundRefTrode = true;
            break;
        }
    }

    if (!foundRefTrode) {
        qDebug() << "Error: could not find reference nTrode" << newRefTrode;
        return;
    }

    int newRefChannel  = RefChannelBox->currentText().toInt()-1;


    //When the ref trode menu is changed, the ref channel menu is cleared, and the
    //index is temporarity changed to -1.  We ignore the -1 signal.

    if (newRefChannel > -1) {


        if (linkChangesBool) {
            //int nTrodeIndex = RefTrodeBox->currentIndex();
            //int channelIndex = RefChannelBox->currentIndex();
            emit changeAllRefs(newRefTrodeInd,newRefChannel);
        } else {
            for (int i = 0; i < selectedIndicis.length(); i++) {
                spikeConf->setReference(selectedIndicis.at(i), newRefTrodeInd, newRefChannel);
            }
//            spikeConf->setReference(nTrodeNumber,newRefTrodeInd,newRefChannel);

        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateRefSwitch(bool on) {
//    qDebug() << "Update Spike Ref Switch " << on;
    if (linkChangesBool) {
        emit toggleAllRefs(on);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setRefSwitch(selectedIndicis.at(i),on);
        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateLFPFilterSwitch(bool on) {
//    qDebug() << "Update LFP Ref Switch " << on;
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setLFPRefSwitch(selectedIndicis.at(i),on);
    }
    emit updateAudioSettings();
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateFilterSwitch(bool on) {
    //qDebug() << "Update Filter Switch";
    if (linkChangesBool) {
        emit toggleAllFilters(on);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setFilterSwitch(selectedIndicis.at(i),on);
        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateLowerFilter() {
    int newLowCutoff = lowFilterCutoffBox->currentText().toInt();
    int newHighCutoff = highFilterCutoffBox->currentText().toInt();
    //qDebug() << "Update Lower Filter";
    if (linkChangesBool) {
        emit changeAllFilters(newLowCutoff,newHighCutoff);
    } else {
//        spikeConf->setLowFilter(nTrodeNumber,newLowCutoff);
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setLowFilter(selectedIndicis.at(i),newLowCutoff);
        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateUpperFilter() {
    int newLowCutoff = lowFilterCutoffBox->currentText().toInt();
    int newHighCutoff = highFilterCutoffBox->currentText().toInt();
    //qDebug() << "Update High Filter";
    if (linkChangesBool) {
       emit changeAllFilters(newLowCutoff,newHighCutoff);
    } else {
//        spikeConf->setHighFilter(nTrodeNumber,newHighCutoff);
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setHighFilter(selectedIndicis.at(i),newHighCutoff);
        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateModuleDataFilterSwitch(bool on) {
//    qDebug() << "Update LFP Filter Switch " << on;
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setLFPFilterSwitch(selectedIndicis.at(i),on);
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateModuleDataChannel() {
    //qDebug() << "Update Data Channel";
    int newChannel = ModuleDataChannelBox->currentText().toInt();
//    spikeConf->setModuleDataChan(nTrodeNumber,newChannel-1); //convert back to 0-based
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setModuleDataChan(selectedIndicis.at(i),newChannel-1); //convert back to 0-based
        // we also need to addEditModuleToConfig this change to the modules
        emit moduleDataChannelChanged(selectedIndicis.at(i), newChannel-1);
    }
    checkNTrodesForConflicts();
    // we also need to addEditModuleToConfig this change to the modules
//    emit moduleDataChannelChanged(nTrodeNumber, newChannel-1);
}

void TriggerScopeSettingsWidget::updateModuleDataUpperFilter() {
    //qDebug() << "Update Data Upper Filter";
    int newCutoff = ModuleDataHighFilterCutoffBox->currentText().toInt();
//    spikeConf->setModuleDataHighFilter(nTrodeNumber,newCutoff);
    for (int i = 0; i < selectedIndicis.length(); i++) {
        spikeConf->setModuleDataHighFilter(selectedIndicis.at(i),newCutoff);
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateSpikeTrigger(bool on) {
    //qDebug() << "Update Spike Trigger";
    if (linkChangesBool) {
        emit toggleAllTriggers(on);
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
//            spikeConf->setFilterSwitch(selectedIndicis.at(i),on);
            spikeConf->setTriggerMode(selectedIndicis.at(i), on);
        }
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateThresh() {
    //qDebug() << "Update Thresh";
    if (linkChangesBool) {
        emit changeAllThresholds(threshSpinBox->value());
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setThresh(selectedIndicis.at(i), threshSpinBox->value());
        }
        emit sig_threshUpdated(threshSpinBox->value());
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateMaxDisp() {
    //qDebug() << "Update Max Disp";
    if (linkChangesBool) {
        emit changeAllMaxDisp(maxDispSpinBox->value());
    } else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
//            spikeConf->setFilterSwitch(selectedIndicis.at(i),on);
            spikeConf->setMaxDisp(selectedIndicis.at(i), maxDispSpinBox->value());
        }
        emit sig_maxDisplayUpdated(maxDispSpinBox->value());
        emit updateAudioSettings();
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateChanColor() {
//    qDebug() << "Update channel color";
    QString colorCode = QString("#%1").arg(buttonColorBox->styleSheet().split("#").last());
    if (linkChangesBool) {
        for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
            spikeConf->setColor(i, QColor(colorCode));
        }
    }
    else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setColor(selectedIndicis.at(i), QColor(colorCode));
        }
    }
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateRefTrode(int notused) {
    updateRefTrode();
}


void TriggerScopeSettingsWidget::updateRefTrode() {
//    qDebug() << "UpdateRefTrode";
    int newRefTrode  = RefTrodeBox->currentText().toInt();

    bool foundRefTrode = false;
    int newRefTrodeInd;
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        if (spikeConf->ntrodes[i]->nTrodeId == newRefTrode) {
            newRefTrodeInd = i;
            foundRefTrode = true;
            break;
        }
    }

    if (!foundRefTrode) {
        qDebug() << "Error: could not find reference nTrode" << newRefTrode;
        return;
    }


    RefChannelBox->clear();
    for (int i = 0; i < spikeConf->ntrodes[newRefTrodeInd]->hw_chan.length(); i++)
        RefChannelBox->addItem(QString::number(i+1));

//    qDebug() << "Set the Ref NTrode to " << newRefTrode << " at index " << newRefTrodeInd << " chan [" << RefChannelBox->itemText(0) << "]";
    int newRefChan = RefChannelBox->itemText(0).toInt()-1;
    //save the settings
    if (linkChangesBool) {
        emit changeAllRefs(newRefTrodeInd, newRefChan);
    }
    else {
        for (int i = 0; i < selectedIndicis.length(); i++) {
            spikeConf->setReference(selectedIndicis.at(i), newRefTrodeInd, newRefChan);
        }
    }
    emit updateAudioSettings();

    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::updateTags() {
//    qDebug() << "Saving Grouping Tags";

    if (groupingDialog != NULL) {
//        QList<QString> tagsToAdd = groupingDialog->getTagList();
        QList<GroupingTag> tagsToAdd = groupingDialog->getAddedTags();
        QList<GroupingTag> tagsToRemove = groupingDialog->getRemovedTags();

        for (int i = 0; i < selectedIndicis.length(); i++) { //over all selected nTrodes
            SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[selectedIndicis.at(i)];
//            spikeConf->ntrodes[selectedIndicis.at(i)]->tags.clear();
            for (int j = 0; j < tagsToAdd.length(); j++) {
                if (!curNTrode->gTags.contains(tagsToAdd.at(j))) { //Add the tag to the nTrode if it doesn't already exist
                    curNTrode->gTags.insert(tagsToAdd.at(j),1);
                }
            }
            for (int j = 0; j < tagsToRemove.length(); j++) {
                if (curNTrode->gTags.contains(tagsToRemove.at(j))) { //remove the tag from the nTrode if it exists in the nTrode
                    curNTrode->gTags.remove(tagsToRemove.at(j));
                }
            }
        }
        groupingDialog->clearAddedTagList();
        groupingDialog->clearRemovedTagList();
    }

    loadTags(spikeConf->ntrodes[nTrodeNumber]); //reload the new tags into the view panel
    checkNTrodesForConflicts();
}

void TriggerScopeSettingsWidget::addRemoveTagsButtonPressed() {
//    qDebug() << "Summon the add/remove tags dialog!";
//    SingleSpikeTrodeConf *nTrode = spikeConf->ntrodes.at(nTrodeNumber);
    if (groupingDialog == NULL) {
        groupingDialog = new TagGroupingPanel();
        groupingDialog->setTitle("Add/Remove Tags");
        groupingDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
        connect(groupingDialog, SIGNAL(sig_categoryAdded(QString)), this, SLOT(addCategoryToDict(QString)));
        connect(groupingDialog, SIGNAL(sig_newTagAdded(QString,QString)), this, SLOT(addTagToDict(QString,QString)));
        connect(groupingDialog,SIGNAL(sig_enableApplyButtons()),this,SLOT(updateTags()));
        connect(groupingDialog,SIGNAL(sig_enableApplyButtons()),this,SIGNAL(configChanged()));
//        connect(groupingDialog, );
        connect(groupingTabView, SIGNAL(currentChanged(int)), groupingDialog, SLOT(setCurrentTab(int)));
        connect(groupingDialog, SIGNAL(sig_tabChanged(int)), groupingTabView, SLOT(setCurrentIndex(int)));
//        groupingTabView->set

        if (groupingTabView->isVisible()) { //initially we have to set the groupingDialog's index manually
            groupingDialog->setCurrentTab(groupingTabView->currentIndex());
        }
        else
            groupingDialog->setCurrentTab(0);

    }
    groupingDialog->updateDict(spikeConf->groupingDict);

    QList<SingleSpikeTrodeConf> selectedNTrodes;
    for (int i = 0; i < selectedIndicis.length(); i++) {
        SingleSpikeTrodeConf cNtr = *spikeConf->ntrodes.at(selectedIndicis.at(i));
        selectedNTrodes.append(cNtr);
    }

    if (selectedNTrodes.length() > 1)
        groupingDialog->loadMultipleNTrodeTags(selectedNTrodes);
    else
        groupingDialog->loadNTrodeTags(allTags);

//    groupingDialog->loadNTrodeTags(allTags); //load all tags from the selected nTrodes
    groupingDialog->loadCurrentCategoryTags();
    groupingDialog->setVisible(!groupingDialog->isVisible());
    groupingDialog->raise();
    groupingDialog->setFocus();
}

void TriggerScopeSettingsWidget::addCategoryToDict(QString nCata) {
//    qDebug() << "Adding new category to dictionary";
    spikeConf->groupingDict.addCategory(nCata);
}

void TriggerScopeSettingsWidget::addTagToDict(QString tCata, QString nTag) {
//    qDebug() << "Adding new tag to the dictionary";
    spikeConf->groupingDict.addTagToCategory(tCata, nTag);
}

void TriggerScopeSettingsWidget::setPanelDim(int width, int height) {
//    qDebug() << "Setting new widget dimensions to (" << width << ", " << height << ")";
    this->setGeometry(this->geometry().x(), this->geometry().y(), width, height);
    panelSize.setWidth(width);
    panelSize.setHeight(height);

}

//set the widgets offset, THIS DOES NOT EFFECT SETPOSITION, this merely keeps track of user set offsets for automatic repositioning
void TriggerScopeSettingsWidget::setPositionOffset(int xOffset, int yOffset) {
    posOffset.setX(xOffset);
    posOffset.setY(yOffset);
}

//sets the absolute position of the widget
void TriggerScopeSettingsWidget::setPosition(int xpos, int ypos) {
//    qDebug() << "Setting new widget position to (" << xpos << ", " <<  ypos << ")";
    this->setGeometry(xpos, ypos, this->geometry().width(), this->geometry().height());
}

void TriggerScopeSettingsWidget::setVisible(bool visible) {
    if (attachedMode) {
        if (visible) { //run expand animation
            runAnimation_Expand(250);
            QWidget::setVisible(visible);
        }
        else { //run close animation
            runAnimation_Close(250);
        }
    }
    else {
        QWidget::setVisible(visible);

        if (visible)
            setWidgetsVisible();
        else
            setWidgetsHidden();

    }
}

void TriggerScopeSettingsWidget::runAnimation_Expand(int time, bool expandRight) {
    if (!attachedMode)
        return;

    if (expandRight) {
        QPoint curPos;
        curPos.setX(this->geometry().x());
        curPos.setY(this->geometry().y());

        a_horizontalExpand->setDuration(time);
        a_horizontalExpand->setStartValue(QRect(curPos.x(), curPos.y(), 0, panelSize.height()));
        a_horizontalExpand->setEndValue(QRect(curPos.x(), curPos.y(), panelSize.width(), panelSize.height()));
        a_horizontalExpand->start();
    }
}

void TriggerScopeSettingsWidget::runAnimation_Close(int time, bool closeFromRight) {
    if (!attachedMode)
        return;

    if (closeFromRight) {
        setWidgetsHidden();

        QPoint curPos;
        curPos.setX(this->geometry().x());
        curPos.setY(this->geometry().y());

        a_minSize->setDuration(250);
        a_minSize->setStartValue(panelSize);
        a_minSize->setEndValue(QSize(0,panelSize.height()));
        a_minSize->start();

        a_horizontalClose->setDuration(time);
        a_horizontalClose->setStartValue(QRect(curPos.x(), curPos.y(), panelSize.width(), panelSize.height()));
        a_horizontalClose->setEndValue(QRect(curPos.x(), curPos.y(), 0, panelSize.height()));
        a_horizontalClose->start();
    }
}

void TriggerScopeSettingsWidget::getColorDialog() {
    QString colorCode = QString("#%1").arg(buttonColorBox->styleSheet().split("#").last());
    QColor newColor = QColorDialog::getColor(colorCode, this, "Select New Channel Color");
    if (newColor.isValid()) {
        buttonColorBox->setStyleSheet(QString("background-color:%1").arg(newColor.name()));
        updateChanColor();
        emit configChanged();
    }
}

void TriggerScopeSettingsWidget::setWidgetsVisible(void) {
    for (int i = 0; i < widgets.length(); i++) {
        widgets.at(i)->setVisible(true);
    }
    for (int i = 0; i < labels.length(); i++) {
        labels.at(i)->setVisible(true);
    }
    setGroupingTagWidgetsVisibility();

}

void TriggerScopeSettingsWidget::setWidgetsHidden(void) {
    for (int i = 0; i < widgets.length(); i++) {
        widgets.at(i)->setVisible(false);
    }
    for (int i = 0; i < labels.length(); i++) {
        labels.at(i)->setVisible(false);
    }
}

//This function controls which grouping tag widgets are visible.
void TriggerScopeSettingsWidget::setGroupingTagWidgetsVisibility() {
    if (!this->isVisible())
        return; //only execute if the widget is visible

    if (multipleSelected) {
        groupingTagList->setVisible(false);
        groupingTabView->setVisible(true);
        groupingTagSharedList->setVisible(true);
        groupingTagAllList->setVisible(true);

    }
    else {
        groupingTagList->setVisible(true);
        groupingTabView->setVisible(false);
        groupingTagSharedList->setVisible(false);
        groupingTagAllList->setVisible(false);
    }
}
