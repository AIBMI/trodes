/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SOUNDDIALOG_H
#define SOUNDDIALOG_H

#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>
#include <QMenuBar>
#include "../Modules/workspaceGUI/workspaceEditor.h"
#include "sharedtrodesstyles.h"

#if defined (__linux__)
#define EDITOR_STYLE_SHEET "#main {background-color:white;}"
#else
#define EDITOR_STYLE_SHEET "#main {background-color:white;} #editor::pane {border: 1px solid #e2e2e2;}"
#endif

//-----------------------------------------

//--------------------------------------

class ExportDialog : public QWidget {


Q_OBJECT

public:
    ExportDialog(QWidget *parent = 0);

    QGroupBox     *spikeBox;
    QComboBox     *triggerSelector;
    QComboBox     *noiseRemoveSelector;
    QGroupBox     *ModuleDataBox;
    QComboBox     *ModuleDataChannelSelector;
    QComboBox     *ModuleDataFilterSelector;

    QLabel        *triggerModeLabel;
    QLabel        *noiseLabel;
    QLabel        *ModuleDataChannelLabel;
    QLabel        *ModuleDataFilterLabel;

    QPushButton   *cancelButton;
    QPushButton   *exportButton;

    QProgressBar  *progressBar;
    QTimer        *progressCheckTimer;


private:

signals:

    void startExport(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting);
    void exportCancelled();
    void closing();

private slots:

    void exportButtonBushed();
    void cancelButtonPushed();
    void timerExpired();
};


class soundDialog : public QWidget
{

Q_OBJECT

public:
    soundDialog(int currentGain, int currentThresh, QWidget *parent = 0);
    QSlider *gainSlider;
    QSlider *threshSlider;
    QLabel *gainDisplay;
    QLabel *threshDisplay;
    QLabel *gainTitle;
    QLabel *threshTitle;

private:

signals:

};


class waveformGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    waveformGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *modulatorFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *modulatorFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


class spikeGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    spikeGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *modulatorFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *modulatorFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


//-------------------------------------------------

class CommentDialog : public QWidget {

Q_OBJECT

public:
    CommentDialog(QString fileName, QWidget *parent = 0);


private:

    QLineEdit* newCommentEdit;

    QPushButton* saveButton;

    QLabel* commentLabel;
    //QLabel* lastComment;
    QTextEdit* lastComment;

    QString fileName;

    void    getHistory();
    void    saveLine();


private slots:

    void saveCurrentComment();
    void somethingEntered();


public slots:


protected:
    void closeEvent(QCloseEvent* event);

signals:

    void windowOpenState(bool);
    void windowClosed();

};

class HeadstageSettings {

public:
    bool valid;

    quint8 minorVersion;
    quint8 majorVersion;

    uint16_t numberOfChannels;
    uint16_t hsTypeCode;
    uint16_t hsSerialNumber;
    uint16_t configCode;

    quint8 rfChannel;
    quint8 statusCode2;
    quint8 statusCode3;
    quint8 statusCode4;
    quint8 statusCode5;
    quint8 statusCode6;
    quint8 statusCode7;
    quint8 statusCode8;

    bool autoSettleOn;
    uint16_t percentChannelsForSettle;
    uint16_t threshForSettle;

    bool accelSensorOn;
    bool gyroSensorOn;
    bool magSensorOn;

    bool smartRefOn;

    bool smartRefAvailable;
    bool autosettleAvailable;
    bool accelSensorAvailable;
    bool gyroSensorAvailable;
    bool magSensorAvailable;

    bool rfAvailable;

    HeadstageSettings() {
        valid = false; //Indicates if values have been filled

        minorVersion=0;
        majorVersion=0;

        numberOfChannels=0;
        hsTypeCode=0;
        hsSerialNumber=0;
        configCode=0;


        autoSettleOn=false;
        percentChannelsForSettle=0;
        threshForSettle=0;

        accelSensorOn=false;
        gyroSensorOn=false;
        magSensorOn=false;

        smartRefOn=false;

        smartRefAvailable=false;
        autosettleAvailable=false;
        accelSensorAvailable=false;
        gyroSensorAvailable=false;
        magSensorAvailable=false;
        rfAvailable=false;

        rfChannel=0;
        statusCode2=0;
        statusCode3=0;
        statusCode4=0;
        statusCode5=0;
        statusCode6=0;
        statusCode7=0;
        statusCode8=0;
    }
    //Overload == to comapare the following fields:
    bool operator==(const HeadstageSettings& b) {
        return ( (this->autoSettleOn==b.autoSettleOn) &&
                 (this->percentChannelsForSettle==b.percentChannelsForSettle) &&
                 (this->threshForSettle==b.threshForSettle) &&
                 (this->accelSensorOn==b.accelSensorOn) &&
                 (this->gyroSensorOn==b.gyroSensorOn) &&
                 (this->magSensorOn==b.magSensorOn) &&
                 (this->smartRefOn==b.smartRefOn) &&
                 (this->smartRefAvailable==b.smartRefAvailable) &&
                 (this->autosettleAvailable==b.autosettleAvailable) &&
                 (this->accelSensorAvailable==b.accelSensorAvailable) &&
                 (this->gyroSensorAvailable==b.gyroSensorAvailable) &&
                 (this->magSensorAvailable==b.magSensorAvailable) &&
                 (this->rfAvailable==b.rfAvailable));

    }
};

class HardwareControllerSettings {

public:
    bool valid; //Indicates if values have been filled

    quint8 minorVersion;
    quint8 majorVersion;
    uint16_t modelNumber;
    uint16_t serialNumber;
    uint16_t packetSize; //estimated automatically based on connected devices
    quint8 statusCode1;
    quint8 statusCode2;
    quint8 statusCode3;
    quint8 statusCode4;
    quint8 statusCode5;
    quint8 statusCode6;
    quint8 statusCode7;
    quint8 statusCode8;


    HardwareControllerSettings() {
      valid = false;
      minorVersion=0;
      majorVersion=0;
      modelNumber=0;
      serialNumber=0;
      packetSize=0;
      statusCode1=0;
      statusCode2=0;
      statusCode3=0;
      statusCode4=0;
      statusCode5=0;
      statusCode6=0;
      statusCode7=0;
      statusCode8=0;
    }

};

class HeadstageSettingsDialog : public QWidget
{

Q_OBJECT

public:
    HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);


    QGroupBox       *autoSettleBox;
    QSlider         *percentChannelsSlider;
    QSlider         *threshSlider;
    QLabel          *percentIndicator;
    QLabel          *threshIndicator;
    QLabel          *percentTitle;
    QLabel          *threshTitle;

    QGroupBox       *smartReferenceBox;
    QCheckBox       *smartRefCheckBox;

    QGroupBox       *sensorBox;
    QCheckBox       *accelCheckBox;
    QCheckBox       *gyroCheckBox;
    QCheckBox       *magnetCheckBox;

    QGroupBox       *rfBox;
    QSpinBox        *rfChannelSpinBox;


    TrodesButton    *okButton;
    TrodesButton    *cancelButton;

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent* event);

private:

    HeadstageSettings currentSettings;
    bool settingsChanged;

private slots:

    void percentSliderChanged(int value);
    void threshSliderChanged(int value);
    void autoSettleOnToggled(bool on);

    void smartRefToggled(bool on);
    void accelToggled(bool on);
    void gyroToggled(bool on);
    void magToggled(bool on);

    void rfChannelChanged(int value);


    void okButtonPressed();
    void cancelButtonPressed();

signals:
    void windowClosed(void);
    void newSettings(HeadstageSettings settings);
};



//-----------------------------------
class RasterPlot : public QWidget {

    Q_OBJECT

public:
    RasterPlot(QWidget *parent);

    void addRaster(const QVector<qreal> &times);
    void setRasters(const QVector<QVector<qreal> > &times);
    void clearRasters();
    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:


    qreal maxX;
    qreal minX;
    QVector<QVector<qreal> > eventTimes;
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;


public slots:

private slots:

signals:

};
//-----------------------------------
class HistogramPlot : public QWidget {

    Q_OBJECT

public:
    HistogramPlot(QWidget *parent);

    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);
    void setColor(QColor c);
    void setData(QVector<qreal> bvalues);
    void setErrorBars(QVector<QVector<qreal> > hilowErrValues);
    void setChecked(bool errBar[4]);
    void setBinValues(qreal bin1Start,
                      qreal bin1Size,
                      qreal bin2Start,
                      qreal bin2Size);
    void clearData();
    void clickingOn(bool on, int cl);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

    //void paint()

//    void mouseReleaseEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    /*
    void mouseDoubleClickEvent(QMouseEvent *event);
    void leaveEvent(QEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *);
    */

private:

//    QPainter painter;
//    QPen pen;
    int margin;
    int topOfGraph;

    qreal maxX;
    qreal minX;
    qreal maxY;
    qreal maxYDisplay;
    QVector<qreal> barValues;
    QColor barColor;
    QVector<QVector<qreal> > hiloerrorBarValues;
    bool errBar[4];
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;
    qreal bin1Start;
    qreal bin1Size;
    qreal bin2Start;
    qreal bin2Size;
    QRectF *bin1;
    QRectF *bin2;
    bool errorsIndicator;
    bool binsIndicator;
    bool clicking;
    int cluster;

public slots:

private slots:

signals:
    void PSTHRequest(int);
    void highlightBorder(int);
};

class PSTHDialog : public QWidget {

Q_OBJECT

public:
    PSTHDialog(QWidget *parent = 0);
    void plot(const QVector<uint32_t> &trialTimes, const QVector<uint32_t> &eventTimes);


private:

    QLabel   *rangeLabel;
    QLabel   *binsLabel;
    QLabel   *binOneStartLabel;
    QLabel   *binOneSizeLabel;
    QLabel   *binTwoStartLabel;
    QLabel   *binTwoSizeLabel;
    QLabel   *minLabel1;
    QLabel   *maxLabel1;
    QLabel   *medLabel1;
    QLabel   *meanLabel1;
    QLabel   *numLabel1;
    QLabel   *minLabel2;
    QLabel   *maxLabel2;
    QLabel   *medLabel2;
    QLabel   *meanLabel2;
    QLabel   *numLabel2;

    QSpinBox *rangeSpinBox;
    QSpinBox *numBinsSpinBox;
    QSpinBox *binOneStartBox;
    QSpinBox *binOneSizeBox;
    QSpinBox *binTwoStartBox;
    QSpinBox *binTwoSizeBox;
//    QCheckBox *SDCheckbox;
    QCheckBox *SECheckbox;
//    QCheckBox *RCheckbox;
//    QCheckBox *CICheckbox;
    QTextEdit *binSumStatsText;
    QGridLayout *binSumStatsGrid;

    QGridLayout *controlLayout;
    HistogramPlot *window;
    RasterPlot *rasterWindow;

    qreal binSize;
    int numBins;
    qreal absTimeRange;
    QVector<int> counts;
    int numTrials;
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;
    bool checked[4];

    qreal binOneStart, binOneSize, binTwoStart, binTwoSize;
    QMap<QString, qreal> binOneSumStats, binTwoSumStats;

    void setUpControlPanel();
    QString generateSumStatsText();
    void generateSumStatsLabel();
    void calcDisplay();
    void calcSumStats(QVector<int> data, int bin);
    QVector<QVector<qreal> > calcErrorBars(QVector<QVector<int> > data, qreal pct);
    QVector<QVector<qreal> > stdDev(QVector<QVector<int> > data);
    QVector<QVector<qreal> > stdError(QVector<QVector<int> > data);
    QVector<QVector<qreal> > range(QVector<QVector<int> > data);
    QVector<QVector<qreal> > confInt(QVector<QVector<int> > data, qreal pct);



private slots:
    void setNumBins(int nBins);
    void setRange(int msecRange);
    void setSDChecked(int c);
    void setSEChecked(int c);
    void setRChecked(int c);
    void setCIChecked(int c);
    void setBinOneStart(int num);
    void setBinOneSize(int num);
    void setBinTwoStart(int num);
    void setBinTwoSize(int num);


public slots:


protected:
    //void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);
//    void paintEvent(QPaintEvent *event);

signals:

    void windowOpenState(bool);
    void windowClosed();
    void binsChanged(int);
    void rangeChanged(int);

};

class MultiPlotDialog : public QWidget {

Q_OBJECT

public:
    MultiPlotDialog(QWidget *parent = 0, QColor c = QColor(100,100,100));
    void plot(const QVector<uint32_t> &trialTimesIn,
              const QVector<uint32_t> &eventTimesIn,
              const QVector<QVector<uint32_t> > &clusterEventTimesIn);
    void setupClusterHistograms();

private:

    HistogramPlot *topHistogram;
    QGridLayout   *clusterPlotGrid;
    QLabel        *topPlotLabel;
    QLabel        *botPlotLabel;

    QPushButton   *updateButton;

    QLabel        *rangeLabel;
    QLabel        *binsLabel;
    QLabel        *numPlotsLabel;
    QSpinBox      *rangeSpinBox;
    QSpinBox      *numBinsSpinBox;

    QColor        plotColor;

    QMap<int, HistogramPlot*> clusterPlots;

    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;
    QVector<QVector<uint32_t> > clusterEventTimes;
    int currClInd;
    int currentNTrode;

    int plotsPerPage;
    int gridRows;
    int gridCols;
    int currPage;
    int numBins;
    int numTrials;
    qreal binSize;
    qreal absTimeRange;

    bool newNTrode;

    void calcTopDisplay();
    void calcClusterHistogramDisplay(int clusterInd);

private slots:
    void setNumBins(int nBins);
    void setRange(int msecRange);
    void getPSTH(int cl);
    void displayClusterGrid();
//    void organizeClusterGrid();
    void setupGridSize(QString numplots);


public slots:
    void update(int, QVector<uint32_t>, QVector<uint32_t> , QVector<QVector<uint32_t> >);

protected:
    //void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);
//    void paintEvent(QPaintEvent *event);

signals:

    void windowOpenState(bool);
    void windowClosed();
    void requestPSTH(int, int);
    void binsChanged(int);
    void rangeChanged(int);

};

class WorkspaceEditorDialog : public QDialog {
    Q_OBJECT
public:
    WorkspaceEditorDialog(QWidget *parent = 0);
    void loadFileIntoWorkspaceGui(QString filename);

protected:
    void closeEvent(QCloseEvent * event);

private:
    WorkspaceEditor *workspaceGui;
    QVBoxLayout     *mainLayout;
    QHBoxLayout     *buttonBar;

    QAction *actionSave;
    QAction *actionSaveAs;
    QAction *actionLoad;

    QPushButton     *buttonCancel;
    QPushButton     *buttonAccept;

public slots:
    int openEditor(void);
    int openReconfigEditor(void);

private slots:
    void clearGUI(void);
    void buttonCancelPressed(void);
    void buttonAcceptPressed(void);



signals:
    void sig_openTempWorkspace(QString path);
};

enum FilterOperation {FO_NULL, FO_OR, FO_AND};

class NTrodeSelectionDialog : public QWidget {
    Q_OBJECT
public:
    NTrodeSelectionDialog(QWidget *parent = 0);

public slots:
    void update(void); //updates categories and tags

private:
    QHash<GroupingTag, int> selectedTags;
    FilterOperation operation;


    QVBoxLayout *mainLayout;

    QLabel      *labelCategory;
    QLabel      *labelTags;
    QLabel      *labelOperation;

    QComboBox   *categorySelector;
    QComboBox   *operationSelector;

    QCheckBox   *selectAllTags;
    QListWidget *tagList;

    QPushButton *buttonClear;

private slots:
    void categorySelected(int categoryIndex);
    void allTagsButtonSelected(int state);
    void tagSelected(QListWidgetItem *tag);
    void operationSelected(int opIndex);

    void buttonClearPressed(void);

signals:
    void sig_selectTags(int op, QHash<GroupingTag,int> sTags);
    void sig_clearSelection(void);

};

class StreamDisplayOptionsDialog : public QWidget {
    Q_OBJECT

public:
    StreamDisplayOptionsDialog(bool lfp, bool spike, QWidget *parent = 0);

private:
    QVBoxLayout     *mainLayout;

    TrodesButton    *lfpbutton;
    TrodesButton    *spikebutton;
    TrodesButton    *rawbutton;
signals:
    void lfpChosen();
    void spikeChosen();
    void rawChosen();
private slots:
    void lfpChosenslot();
    void spikeChosenslot();
    void rawChosenslot();
};

//TriggerScopeSettingsWidget contains the settings controls for each nTrode
class  TriggerScopeSettingsWidget : public QWidget
{
    Q_OBJECT
    void newParent() {
        if (!parent()) return;
        qDebug() << "Installing Event Filter";
        parent()->installEventFilter(this);
        raise();
    }
public:
    TriggerScopeSettingsWidget(QWidget *parent, int trodeNum);
    ~TriggerScopeSettingsWidget();

public:

    int nTrodeNumber;
    QList<QGridLayout*>  widgetLayouts;
    QList<QLabel*> labels;
    QList<int> selectedIndicis;

    QLabel              *emptyLabel;

    QLabel              *trodeLabel;
    QCheckBox           *linkCheckBox;
    QCheckBox           *spikeRefCheckBox;
    QCheckBox           *lfpRefCheckBox;
    QGroupBox           *refBox;
    QGroupBox           *filterBox;
    QGroupBox           *triggerBox;
    QGroupBox           *ModuleDataFilterBox;
    QGroupBox           *groupingTagBox;
    QGroupBox           *displayBox;
    QListWidget         *groupingTagList; //all grouping tags for the currentNtrode
    QListWidget         *groupingTagSharedList; //shared tags among selected nTrodes
    QListWidget         *groupingTagAllList; //All selected ntrodes tags
    QTabWidget          *groupingTabView;

    QPushButton         *buttonAddRemoveTags;
    QHBoxLayout         *layoutSelectButtons;
    QPushButton         *buttonSelectBy;
    QPushButton         *buttonSelectAll;

    TagGroupingPanel    *groupingDialog;
    QSpinBox            *threshSpinBox;
    QSpinBox            *maxDispSpinBox;
    QComboBox           *RefTrodeBox;
    QComboBox           *RefChannelBox;
    QComboBox           *lowFilterCutoffBox;
    QComboBox           *highFilterCutoffBox;
    QComboBox           *ModuleDataHighFilterCutoffBox;
    QComboBox           *ModuleDataChannelBox;
    ClickableFrame      *buttonColorBox;

    void setSelectedNTrodes(QHash<int,int> selectedNTrodes);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);
    bool event(QEvent *ev);

private:
    bool attachedMode; //set whether or not the window is attached to it's parent window.  This enables/disables animations

    QWidget *attachedWidget;
    QPoint posOffset;
    QSize panelSize;
    bool multipleSelected;

    QHash<GroupingTag, int> allTags; //all tags shared by the currently selected nTrodes

    QList<QWidget*> widgets; //list of all widgets, for the purpose of hiding and revealing everything
    QPropertyAnimation *a_horizontalExpand;
    QPropertyAnimation *a_horizontalClose;
    QPropertyAnimation *a_minSize;

    int LOWFILTER_LABEL_INDEX;
    int HIGHFILTER_LABEL_INDEX;
    int REFNTRODE_LABEL_INDEX;
    int REFCHAN_LABEL_INDEX;
    int MODDATACHAN_LABEL_INDEX;
    int MODDATAHIGH_LABEL_INDEX;
    int THRESH_LABEL_INDEX;
    int MAXDISP_LABEL_INDEX;
    int COLOR_LABEL_INDEX;

    void iniReferenceBox(int columnPos, TrodesFont font);
    void iniSpikeFilterBox(int columnPos, TrodesFont font);
    void iniSpikeTriggerBox(int columnPos, TrodesFont font);
    void iniLFPBox(int columnPos, TrodesFont font);
    void iniGroupingTagControls(int columnPos, TrodesFont font);
    void iniDispBox(int columnPos, TrodesFont font);

signals:
    void updateAudioSettings();
    void changeAllRefs(int nTrode, int channel);
    void changeAllFilters(int low, int high);
    void changeAllThresholds(int thresh);
    void changeAllMaxDisp(int maxDisp);
    void toggleAllRefs(bool on);
    void toggleAllFilters(bool on);
    void toggleAllTriggers(bool on);
//    void toggleLinkChanges(bool link);
    void moduleDataChannelChanged(int nTrode, int chan);
    void configChanged();
    void closing(void);
    void saveGeo(QRect geo);
    void openSelectByDialog(void); //open select ntrode dialog
    void selectAllNtrodes();

    //value changed signals
    void sig_threshUpdated(int newThresh);
    void sig_maxDisplayUpdated(int newMaxDisp);

public slots:
    void attachToWidget(QWidget *obj);

    void loadNTrodeIntoPanel(int nTrodeID);
    void loadRef(SingleSpikeTrodeConf *nTrode);
    void loadSpikeFilter(SingleSpikeTrodeConf *nTrode);
    void loadSpikeTrigger(SingleSpikeTrodeConf *nTrode);
    void loadLFP(SingleSpikeTrodeConf *nTrode);
    void loadTags(SingleSpikeTrodeConf *nTrode);
    void loadDisplaySettings(SingleSpikeTrodeConf *nTrode);

    //Public Accessor functions
    void setMaxDisplay(int newMaxDisp);
    void setThresh(int newThresh);

    void setChanColorBox(QColor newColor);

    //Save to config and update the gui element
    void updateRefTrode();
    void updateRefTrode(int);
    void updateRefChan();
    void updateRefChan(int);
    void updateRefSwitch(bool on);
    void updateLFPFilterSwitch(bool on);
    void updateFilterSwitch(bool on);
    void updateLowerFilter();
    void updateUpperFilter();
    void updateModuleDataFilterSwitch(bool on);
    void updateModuleDataChannel();
    void updateModuleDataUpperFilter();
    void updateSpikeTrigger(bool on);
    void updateThresh();
    void updateMaxDisp();
    void updateChanColor();
    void updateTags(void);

    void addRemoveTagsButtonPressed(void);
    void addCategoryToDict(QString nCata);
    void addTagToDict(QString tCata, QString nTag);

//    void linkBoxClicked(bool checked);
    void setEnabledForStreaming(bool streamOn);  // disables everything if data streaming is on

    bool checkNTrodesForConflicts(void);
    bool checkSelectedNTrodesForConflicts(SingleSpikeTrodeConf *checkedNTrode);

    void resetAllLabelsToDefault(void);
    void setAllLabelColors(QColor newColor);

    void setPanelDim(int width, int height);
    void setPositionOffset(int xOffset, int yOffset); //sets the x and y offsets of the panel with relation to which widget it's attached to
    void setPosition(int xpos, int ypos);

    //animation stuff
    void setVisible(bool visible);

    void runAnimation_Expand(int time = 0, bool expandRight = true);
    void runAnimation_Close(int time = 0, bool closeFromRight = true);

private slots:
    void getColorDialog(void);

    void setWidgetsVisible(void);
    void setWidgetsHidden(void);
    void setGroupingTagWidgetsVisibility(void);

};
#endif // SOUNDDIALOG_H
