#ifndef SHAREDTRODESSTYLES_H
#define SHAREDTRODESSTYLES_H


#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>

class TrodesFont : public QFont {

public:
    TrodesFont();
};

class TrodesButton : public QPushButton {

Q_OBJECT

public:
    TrodesButton(QWidget *parent = 0);
    void setRedDown(bool yes);
    void setblink(bool blink, int msec = 500, QColor color = QColor("red"));
    void stopBlink();
private:
    QTimer blinkTimer;
    QColor blinkColor;
    bool blink;
private slots:
    void toggleBlink();
};

#endif // SHAREDTRODESSTYLES_H
