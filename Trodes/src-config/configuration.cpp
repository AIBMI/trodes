/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "configuration.h"
#include "trodesSocketDefines.h"
#include <math.h>

#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
//#include <QMessageBox>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <QMutex>


/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
// Global Configuration Structures - these are extern defined in globalObjects.h
NetworkConfiguration *networkConf;
ModuleConfiguration *moduleConf;
NTrodeTable *nTrodeTable;
streamConfiguration *streamConf;
SpikeConfiguration *spikeConf;
headerDisplayConfiguration *headerConf;
HardwareConfiguration *hardwareConf;
GlobalConfiguration *globalConf;
#ifdef PHOTOMETRY_CODE
PhotometryConfiguration *photometryConf;
#endif
BenchmarkConfig *benchConfig;



//info about the structure of the data stream
//these can be changed by the source

//int NCHAN; //Number of channels
//int hourceSamplingRate = 25000; //sampling rate (Hz)
//int headerSize; //for each sample cycle, a multi-purpose header is collected (digital events)
//QString filePrefix;
//QString filePath;

//Global access to link settings
extern bool linkChangesBool;
QMutex moduleDebugLock;
QString modName;

void moduleMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    moduleDebugLock.lock();
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "%s %s\n", qPrintable(modName), localMsg.constData());
        fflush(stderr);
        break;
    case QtInfoMsg:
        fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    }
    moduleDebugLock.unlock();
}

void setModuleName(QString name) {
    modName = QString("[%1]").arg(name);
}

void Helper::setWidgetTextPaletteColor(QWidget *widget, QColor newColor) {
    QPalette p = widget->palette();
    p.setColor(QPalette::WindowText, newColor);
    widget->setPalette(p);
}

void Helper::setWidgetTextPaletteColor(QCheckBox *widget, QColor newColor){
    widget->setStyleSheet("QCheckBox {border: none;color: " + newColor.name() + ";}");
}

QList<QList<QString> > Helper::getSortedTagList(SingleSpikeTrodeConf *nTrode) {
    QHashIterator<GroupingTag, int> i(nTrode->gTags);
    QList<QList<QString> > tagList;

    while (i.hasNext()) {
        i.next();
        QString curCata = i.key().category;
        QString curTag = i.key().tag;
//        qDebug() << "[" << curCata << "] " << curTag;
        bool createNewCata = true;
        for (int j = 0; j < tagList.length(); j++) {
            if (tagList.at(j).at(0) == curCata) { //first item of each sub list will be catagory
                createNewCata = false;
                QList<QString> curTagList = tagList.at(j);
                curTagList.push_back(curTag);
                tagList.replace(j, curTagList);
                break;
            }
        }

        if (createNewCata) {
            QList<QString> newCataList;
            newCataList.push_back(curCata);
            newCataList.push_back(curTag);
            tagList.push_back(newCataList);
        }
    }
    return(tagList);
}

/* --------------------------------------------------------------------- */
GlobalConfiguration::GlobalConfiguration(QObject *):
    saveDisplayedChanOnly(false),
    realTimeMode(false),
    MBPerFileChunk(-1)
{
    suppressModuleAbsPathWarning = 0;
    timestampAtCreation = -1;
    systemTimeAtCreation = -1;
    headstageSerialNumber = "-1";
    controllerSerialNumber = "-1";
    autoSettleOn = -1;
    smartRefOn = -1;
    gyroSensorOn = -1;
    accelSensorOn = -1;
    magSensorOn = -1;
    controllerFirmwareVersion = "-1";
    headstageFirmwareVersion = "-1";

    loadCurrentVersionInfo();
}

int GlobalConfiguration::loadFromXML(QDomNode &globalConfNode) {
    filePath = globalConfNode.toElement().attribute("filePath", "/");
    filePrefix = globalConfNode.toElement().attribute("filePrefix", "");
    saveDisplayedChanOnly = globalConfNode.toElement().attribute("saveDisplayedChanOnly", "0").toInt();
    realTimeMode = globalConfNode.toElement().attribute("realtimeMode", "0").toInt();
    MBPerFileChunk = globalConfNode.toElement().attribute("fileChunkSize", "-1").toInt();
    suppressModuleAbsPathWarning = globalConfNode.toElement().attribute("suppressModuleAbsPathWarning", 0).toInt();

    timestampAtCreation = globalConfNode.toElement().attribute("timestampAtCreation", "-1").toLong();
    systemTimeAtCreation = globalConfNode.toElement().attribute("systemTimeAtCreation", "-1").toLong();
    trodesVersion = globalConfNode.toElement().attribute("trodesVersion","-1");
    compileDate = globalConfNode.toElement().attribute("compileDate","-1");
    compileTime = globalConfNode.toElement().attribute("compileTime","-1");
    qtVersion = globalConfNode.toElement().attribute("qtVersion","-1");
    //exPathStr = globalConfNode.toElement().attribute("executablePath","0");
    commitHeadStr = globalConfNode.toElement().attribute("commitHead","-1");

    headstageSerialNumber = globalConfNode.toElement().attribute("headstageSerial", "-1");
    controllerSerialNumber = globalConfNode.toElement().attribute("controllerSerial", "-1");
    autoSettleOn = globalConfNode.toElement().attribute("headstageAutoSettleOn", "-1").toInt();
    smartRefOn = globalConfNode.toElement().attribute("headstageSmartRefOn", "-1").toInt();
    gyroSensorOn = globalConfNode.toElement().attribute("headstageGyroSensorOn", "-1").toInt();
    accelSensorOn = globalConfNode.toElement().attribute("headstageAccelSensorOn", "-1").toInt();
    magSensorOn = globalConfNode.toElement().attribute("headstageMagSensorOn", "-1").toInt();
    controllerFirmwareVersion = globalConfNode.toElement().attribute("controllerFirmwareVersion","-1");
    headstageFirmwareVersion = globalConfNode.toElement().attribute("headstageFirmwareVersion","-1");

    //qDebug() << "Realtime mode: " << realTimeMode;
    return 0;
}

void GlobalConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode) {
    QDomElement gconf = doc.createElement("GlobalConfiguration");

    rootNode.appendChild(gconf);
    //loadCurrentVersionInfo(); //Version info should simply pass what was in the loaded config (not the current Trodes info)

    gconf.setAttribute("filePath", filePath);
    gconf.setAttribute("filePrefix", filePrefix);
    gconf.setAttribute("saveDisplayedChanOnly", saveDisplayedChanOnly);
    gconf.setAttribute("realtimeMode", realTimeMode);
    gconf.setAttribute("fileChunkSize", MBPerFileChunk);
    gconf.setAttribute("suppressModuleAbsPathWarning", suppressModuleAbsPathWarning);
    gconf.setAttribute("timestampAtCreation", QString("%1").arg(timestampAtCreation));
    gconf.setAttribute("systemTimeAtCreation", QString("%1").arg(systemTimeAtCreation));
    gconf.setAttribute("trodesVersion", trodesVersion);
    gconf.setAttribute("compileDate", compileDate);
    gconf.setAttribute("compileTime", compileTime);
    gconf.setAttribute("qtVersion", qtVersion);
    gconf.setAttribute("headstageSerial", headstageSerialNumber);
    gconf.setAttribute("controllerSerial", controllerSerialNumber);
    gconf.setAttribute("headstageAutoSettleOn", autoSettleOn);
    gconf.setAttribute("headstageSmartRefOn", smartRefOn);
    gconf.setAttribute("headstageGyroSensorOn", gyroSensorOn);
    gconf.setAttribute("headstageAccelSensorOn", accelSensorOn);
    gconf.setAttribute("headstageMagSensorOn", magSensorOn);
    gconf.setAttribute("headstageFirmwareVersion",headstageFirmwareVersion);
    gconf.setAttribute("controllerFirmwareVersion",controllerFirmwareVersion);

    //gconf.setAttribute("executablePath", exPathStr);
    gconf.setAttribute("commitHead", commitHeadStr);
}

void GlobalConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode,quint32 currentTimeStamp) {
    QDomElement gconf = doc.createElement("GlobalConfiguration");

    rootNode.appendChild(gconf);
    loadCurrentVersionInfo(); //make sure version info is current before saving the workspace

    gconf.setAttribute("filePath", filePath);
    gconf.setAttribute("filePrefix", filePrefix);
    gconf.setAttribute("saveDisplayedChanOnly", saveDisplayedChanOnly);
    gconf.setAttribute("realtimeMode", realTimeMode);
    gconf.setAttribute("timestampAtCreation", currentTimeStamp);
    gconf.setAttribute("systemTimeAtCreation", QDateTime::currentDateTime().currentMSecsSinceEpoch());
    gconf.setAttribute("trodesVersion", trodesVersion);
    gconf.setAttribute("compileDate", compileDate);
    gconf.setAttribute("compileTime", compileTime);
    gconf.setAttribute("qtVersion", qtVersion);
    //gconf.setAttribute("executablePath", exPathStr);
    gconf.setAttribute("commitHead", commitHeadStr);
    gconf.setAttribute("headstageSerial", headstageSerialNumber);
    gconf.setAttribute("controllerSerial", controllerSerialNumber);
    gconf.setAttribute("headstageAutoSettleOn", autoSettleOn);
    gconf.setAttribute("headstageSmartRefOn", smartRefOn);
    gconf.setAttribute("headstageGyroSensorOn", gyroSensorOn);
    gconf.setAttribute("headstageAccelSensorOn", accelSensorOn);
    gconf.setAttribute("headstageMagSensorOn", magSensorOn);
    gconf.setAttribute("headstageFirmwareVersion",headstageFirmwareVersion);
    gconf.setAttribute("controllerFirmwareVersion",controllerFirmwareVersion);


}

QString GlobalConfiguration::getVersionInfo(bool withSpaces) {
    int version = TRODES_VERSION;
    int places = 0;
    int firstMod = 10;
    while (version > 0 ) {
        version = version/10;
        places++;
        if (places > 3) {
            firstMod = firstMod*10;
        }
    }
    version = TRODES_VERSION;
    int bugVersion = version%(firstMod);
    version = version/(firstMod);
    int featureVersion = version%10;
    version = (version/10)%10;
    QString versionStr = QString("Trodes version %1.%2.%3").arg(version).arg(featureVersion).arg(bugVersion);
    if(BETA_VERSION){
        versionStr = QString("%1 BETAv%2").arg(versionStr).arg(BETA_VERSION);
    }
    QString compileInfoStr = QString("Compiled on %1 at %2 with Qt v%3").arg(__DATE__).arg(__TIME__).arg(QT_VERSION_STR);
    QString pathStr = QString("Executable Location: '%1'").arg(QCoreApplication::applicationFilePath());
    QString commitStr = QString("Git Commit: %1").arg(GIT_COMMIT);
    QString aboutStr;

    if (withSpaces)
        aboutStr = QString("%1\n\n%2\n\n%3\n\n%4").arg(versionStr).arg(compileInfoStr).arg(pathStr).arg(commitStr);
    else
        aboutStr = QString("%1\n%2\n%3\n%4").arg(versionStr).arg(compileInfoStr).arg(pathStr).arg(commitStr);

    return(aboutStr);
}

void GlobalConfiguration::loadCurrentVersionInfo(void) {
    int version = TRODES_VERSION;
    int places = 0;
    int firstMod = 10;
    while (version > 0 ) {
        version = version/10;
        places++;
        if (places > 3) {
            firstMod = firstMod*10;
        }
    }
    version = TRODES_VERSION;
    int bugVersion = version%(firstMod);
    version = version/(firstMod);
    int featureVersion = version%10;
    version = (version/10)%10;

    trodesVersion = QString("%1.%2.%3").arg(version).arg(featureVersion).arg(bugVersion);
    if(BETA_VERSION){
        trodesVersion = QString("%1 BETAv%2").arg(trodesVersion).arg(BETA_VERSION);
    }
    compileDate = __DATE__;
    compileTime = __TIME__;
    qtVersion = QT_VERSION_STR;
    //exPathStr = QCoreApplication::applicationFilePath();
    commitHeadStr = GIT_COMMIT;
}


HardwareConfiguration::HardwareConfiguration(QObject *):
    sourceSamplingRate(30000),
    headerSize(0),
    headerSizeManuallyDefined(false),
    ECUConnected(false)

{

}

int HardwareConfiguration::loadFromXML(QDomNode &hardwareConfNode) {

    //Some of these items can be defined in a backwards compatible section, so
    //we need to make sure thay have not already been defines (still equal to 0).

    if (hardwareConf->NCHAN == 0) {
        NCHAN = hardwareConfNode.toElement().attribute("numChannels", "32").toInt();
        if ((NCHAN % 32) != 0) {
            qDebug() << "[ParseTrodesConfig] Error: numChannels must be a multiple of 32";
            return -6;
        }

        //qDebug() << "[ParseTrodesConfig] Number of channels: " << NCHAN;
    }

    if (hardwareConf->sourceSamplingRate == 0) {
        hardwareConf->sourceSamplingRate = hardwareConfNode.toElement().attribute("samplingRate", "30000").toInt();
        //qDebug() << "[ParseTrodesConfig] Sampling rate: " << hardwareConf->sourceSamplingRate << " Hz";
    }


    if (hardwareConf->headerSize == 0) {
        hardwareConf->headerSize = hardwareConfNode.toElement().attribute("headerSize", "0").toInt();
        if (headerSize > 0) {
            //qDebug() << "[ParseTrodesConfig] Header size: " << hardwareConf->headerSize;
            headerSizeManuallyDefined = true;
        }
    }
#ifdef PHOTOMETRY_CODE
    if (hardwareConf->NFIBER == 0) {
        NFIBER = hardwareConfNode.toElement().attribute("numFibers", "0").toInt();
    }

    if (hardwareConf->APDsamplingRate == 0) {
        hardwareConf->APDsamplingRate = hardwareConfNode.toElement().attribute("APDsamplingRate", "5000").toInt();
    }
#endif

    ECUConnected = hardwareConfNode.toElement().attribute("ECU", "0").toInt();
    //qDebug() << "[ParseTrodesConfig] ECU: " << ECUConnected;


    QDomNode n = hardwareConfNode.firstChild();
    QDomElement deviceElement;


    while (!n.isNull()) {
        deviceElement = n.toElement();
        if (!deviceElement.isNull()) {

            if (headerSizeManuallyDefined) {
                qDebug() << "[ParseTrodesConfig] Error: if device info is listed in harwareConfig, then 'headerSize' should not be defined.";
                return -1;
            }
            //qDebug() << "[ParseTrodesConfig] Device: " << deviceElement.attribute("name","");

            DeviceInfo newDevice;
            bool ok;
            newDevice.name = deviceElement.attribute("name","");
            if (newDevice.name == "ECU") {
                ECUConnected = true;
                //qDebug() << "[ParseTrodesConfig] ECU mode on";

            }
            newDevice.packetOrderPreference = deviceElement.attribute("packetOrderPreference","").toInt(&ok);
            if (!ok) {
                qDebug() << "[ParseTrodesConfig] Error: Packet order preference conversion to number failed.";
                return -1;
            }

            newDevice.numBytes = deviceElement.attribute("numBytes","").toInt(&ok);
            if (!ok) {
                qDebug() << "[ParseTrodesConfig] Error: Device numBytes field conversion to number failed.";
                return -1;
            }

            newDevice.available = deviceElement.attribute("available","").toInt(&ok);
            if (!ok) {
                qDebug() << "[ParseTrodesConfig] Error: Device available field conversion to number failed.";
                return -1;
            }

            QDomNode devChanN = n.firstChild();
            QDomElement deviceChannelElement;
            while (!devChanN.isNull()) {

                deviceChannelElement = devChanN.toElement();
                if (!deviceChannelElement.isNull()) {
                    DeviceChannel newDeviceChannel;
                    newDeviceChannel.idString = deviceChannelElement.attribute("id","");

                    QString dataTypeString = deviceChannelElement.attribute("dataType", "");

                    if (dataTypeString == "digital") {
                        newDeviceChannel.dataType = DeviceChannel::DIGITALTYPE;
                    }
                    else if (dataTypeString == "analog") {
                        newDeviceChannel.dataType = DeviceChannel::INT16TYPE;
                    }
                    else if (dataTypeString == "uint32") {
                        newDeviceChannel.dataType = DeviceChannel::UINT32TYPE;
                    }

                    else {
                        //QMessageBox::information(0, "error", QString("Config file error: Error in parsing data type: ") + dataTypeString);
                        return -2;
                    }


                    //tmpHeaderChan.port = nt.attribute("port", "1").toInt(&ok);
                    //newDeviceChannel.input = deviceChannelElement.attribute("input", "1").toInt(&ok);
                    newDeviceChannel.startByte = deviceChannelElement.attribute("startByte", "1").toInt(&ok); //add the device offset to the value
                    if (!ok) {
                        //QMessageBox::information(0, "error", QString("Config file error: Error in converting startByte to number in device configuration. Read value is '") + deviceChannelElement.attribute("startByte", "") + QString("'."));
                        return -2;
                    }
                    else if ((newDeviceChannel.startByte < 0) || (newDeviceChannel.startByte > (newDevice.numBytes-1))) {
                        //QMessageBox::information(0, "error", QString("Config file error: startByte must be between 0 and ") + QString("%1").arg(newDevice.numBytes-1) + QString(". Read value is '") + deviceChannelElement.attribute("startByte", "") + QString("'."));
                        return -2;
                    }

                    newDeviceChannel.digitalBit = deviceChannelElement.attribute("bit", "0").toInt(&ok);
                    if ((newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) && (!ok)) {
                        //QMessageBox::information(0, "error", QString("Config file error: Error in converting bit value to number in device configuration. Read value is '") + deviceChannelElement.attribute("bit", "") + QString("'."));
                        return -2;
                    }
                    else if ((newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) && ((newDeviceChannel.digitalBit < 0) || (newDeviceChannel.digitalBit > 7))) {
                        //QMessageBox::information(0, "error", QString("Config file error: bit value must be between 0 and 7. Read value is '") + deviceChannelElement.attribute("bit", "") + QString("'."));
                        return -2;
                    }

                    newDeviceChannel.interleavedDataIDByte = deviceChannelElement.attribute("interleavedDataIDByte", "-1").toInt();
                    if (newDeviceChannel.interleavedDataIDByte != -1) {
                        newDeviceChannel.interleavedDataIDByte = newDeviceChannel.interleavedDataIDByte;
                        newDeviceChannel.interleavedDataIDBit = deviceChannelElement.attribute("interleavedDataIDBit","-1").toInt(&ok);
                        if (!ok) {
                            qDebug() << "[ParseTrodesConfig] Error: An interleavedDataIDByte entry must be accompanied by an interleavedDataIDBit entry.";
                            return -1;
                        }
                    }


                    //This next section assigns a port number to each digital line. However, it assumes that there is only an ECU connected
                    //which is often not right.  MUST BE FIXED!!!
                    if (newDeviceChannel.startByte >= 0 && newDeviceChannel.startByte < 4 && newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) {
                        //Hardcoded digital input port
                        // port number determined by its relative bit position in the header (one indexed)
                        newDeviceChannel.port = (newDeviceChannel.startByte * 8 + newDeviceChannel.digitalBit) + 1;
                        newDeviceChannel.input = true;
                    }
                    else if (newDeviceChannel.startByte >= 4 && newDeviceChannel.startByte < 8 && newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) {
                        //Hardcoded digital output port
                        newDeviceChannel.port = (newDeviceChannel.startByte * 8 + newDeviceChannel.digitalBit) + 1 - 32;
                        newDeviceChannel.input = false;
                    }
                    else if (newDeviceChannel.idString.contains("Ain") && newDeviceChannel.dataType  == DeviceChannel::INT16TYPE) { //make sure analog Ains have their input set to TRUE
                        newDeviceChannel.input = true;
                    }
                    else if (newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) {
                        //QMessageBox::information(0, "error", QString("Config file error: Channel startByte 0 to 8 must be digital type."));
                        return -2;
                    }


                    // append this to the appropriate list
                    /*
                    if (newDeviceChannel.input) {
                        digInIDList.append(newDeviceChannel.idString);
                        digInPortList.append(newDeviceChannel.port);
                    }
                    else {
                        digOutIDList.append(newDeviceChannel.idString);
                        digOutPortList.append(newDeviceChannel.port);

                    }*/


                    newDevice.channels.append(newDeviceChannel);
                }

                devChanN = devChanN.nextSibling();
            }
            devices.append(newDevice);

        }
        n = n.nextSibling();
    }

    //Calculate the total length of the section of the packet that contains aux data
    int tempAuxPacketSize = 1; //in bytes. //The fist byte is reserved for sync, so we start with 1
    QVector<int> preferenceList;
    for (int dNum = 0; dNum<devices.length();dNum++) {
        if (devices[dNum].available) {
            tempAuxPacketSize = tempAuxPacketSize+devices[dNum].numBytes;
            preferenceList.append(devices[dNum].packetOrderPreference);
        }
    }

    //TODO: we need to make sure that two devices don't have the
    //same preference number.

    qSort(preferenceList.data(),preferenceList.data()+preferenceList.length()-1);


    //If the header size has not been manually defined, calculate it here
    if (hardwareConf->headerSize == 0) {
        if ((tempAuxPacketSize % 2)!=0) {
            qDebug() << "[ParseTrodesConfig] Error: Total header size must be an even number of bytes";
            return -1;
        }
        //Downstream code ussumes haederSize is in int16 steps. This will probably change.
        hardwareConf->headerSize = tempAuxPacketSize/2;
        qDebug() << "[ParseTrodesConfig] Header size: " << hardwareConf->headerSize;
    }

    //Calculate the byte offset within each packet where each device's data begins
    int currentOffset= 1; //The first byte is reserved for sync, so we start with 1
    for (int availInd = 0; availInd<preferenceList.length();availInd++) {

        for (int dNum = 0; dNum<devices.length();dNum++) {
            if (devices[dNum].packetOrderPreference == preferenceList[availInd]) {
                devices[dNum].byteOffset = currentOffset;
                //qDebug() << devices[dNum].name << currentOffset;
                for (int devCh=0; devCh<devices[dNum].channels.length();devCh++) {
                    devices[dNum].channels[devCh].startByte += currentOffset;
                    if (devices[dNum].channels[devCh].interleavedDataIDByte > -1) {
                        devices[dNum].channels[devCh].interleavedDataIDByte += currentOffset;
                    }


                }
                currentOffset += devices[dNum].numBytes;
                break;
            }
        }
    }


    return 0;
}

void HardwareConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode) {

    QDomElement hconf = doc.createElement("HardwareConfiguration");

    rootNode.appendChild(hconf);

    hconf.setAttribute("numChannels", NCHAN);
    hconf.setAttribute("samplingRate", sourceSamplingRate);
#ifdef PHOTOMETRY_CODE
    hconf.setAttribute("numFibers", NFIBER);
    hconf.setAttribute("APDsamplingRate", APDsamplingRate);
#endif
    if (headerSizeManuallyDefined) {
        hconf.setAttribute("headerSize", headerSize);
    }

    if (!headerSizeManuallyDefined) {
        for (int j = 0; j < devices.length(); j++) {
            QDomElement devElem = doc.createElement("Device");
            devElem.setAttribute("name", devices[j].name);
            devElem.setAttribute("packetOrderPreference", devices[j].packetOrderPreference);
            devElem.setAttribute("numBytes", devices[j].numBytes);
            devElem.setAttribute("available", (int)devices[j].available);

            for (int ch = 0; ch < devices[j].channels.length(); ch++) {
                QDomElement chanElem = doc.createElement("Channel");
                chanElem.setAttribute("id",devices[j].channels[ch].idString);

                if (devices[j].channels[ch].dataType == DeviceChannel::DIGITALTYPE) {
                    chanElem.setAttribute("dataType", "digital");
                }
                else if (devices[j].channels[ch].dataType == DeviceChannel::INT16TYPE) {
                    chanElem.setAttribute("dataType", "analog");
                }
                else if (devices[j].channels[ch].dataType == DeviceChannel::UINT32TYPE) {
                    chanElem.setAttribute("dataType", "uint32");
                }
                chanElem.setAttribute("startByte",devices[j].channels[ch].startByte-devices[j].byteOffset);

                chanElem.setAttribute("bit",devices[j].channels[ch].digitalBit);
                chanElem.setAttribute("input",(int)devices[j].channels[ch].input);
                //chanElem.setAttribute("port",(int)devices[j].channels[ch].port);


                //If the channel is interleaved, write the interleave info
                if (devices[j].channels[ch].interleavedDataIDByte > -1) {
                    chanElem.setAttribute("interleavedDataIDByte", devices[j].channels[ch].interleavedDataIDByte-devices[j].byteOffset);

                    chanElem.setAttribute("interleavedDataIDBit", devices[j].channels[ch].interleavedDataIDBit);
                }
                devElem.appendChild(chanElem);
            }

            hconf.appendChild(devElem);
        }
    }

}


/* Network Configuration */


NetworkConfiguration::NetworkConfiguration()
{
    dataSocketType = 0;
    trodesHost = "";
}

NetworkConfiguration::~NetworkConfiguration()
{
}

int NetworkConfiguration::loadFromXML(QDomNode &networkConfNode)
{
    trodesHost = networkConfNode.toElement().attribute("trodesHost", "");
    trodesPort = networkConfNode.toElement().attribute("trodesPort", "").toUInt();
    // trodesPort will be zero if nothing is specified
    if ((trodesPort != 0) && ((trodesPort < 1025)/* || (trodesPort > 65535)*/)) { //trodesPort>65535 literally impossible. quint16 cannot hold values > 65535
        //QMessageBox::information(0, "error", QString("Config file error: trodes port %1 must be between between 1025 and 65535.").arg(trodesPort));
        return -2;
    }

    hardwareAddress = networkConfNode.toElement().attribute("hardwareAddress", TRODESHARDWARE_DEFAULTIP);
    // Note that the ports are set in the main read configuration function

    QString tmpSocketType = networkConfNode.toElement().attribute("dataSocketType", "");
    if (tmpSocketType.toUpper() == "TCPIP") {
        dataSocketType = TRODESSOCKETTYPE_TCPIP;
    }
    else if (tmpSocketType.toUpper() == "UDP") {
        dataSocketType = TRODESSOCKETTYPE_UDP;
    }
    else if (tmpSocketType.toUpper() == "LOCAL") {
        //NOTE: not yet implemented in the rest of the code
        dataSocketType = TRODESSOCKETTYPE_LOCAL;
    }
    else if (tmpSocketType != "") {
        //QMessageBox::information(0, "error", QString("Config file error: dataSocketType must be TCPIP, UDP or LOCAL."));
        return -2;
    }

    return 0;
}

void NetworkConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement nconf = doc.createElement("NetworkConfiguration");

    rootNode.appendChild(nconf);

    nconf.setAttribute("trodesHost", trodesHost);
    nconf.setAttribute("trodesPort", QString("%1").arg(trodesPort));
    nconf.setAttribute("hardwareAddress", hardwareAddress);
    if (dataSocketType == TRODESSOCKETTYPE_TCPIP) {
        nconf.setAttribute("dataSocketType", "TCPIP");
    }
    else if (dataSocketType == TRODESSOCKETTYPE_UDP) {
        nconf.setAttribute("dataSocketType", "UDP");
    }
    else if (dataSocketType == TRODESSOCKETTYPE_LOCAL) {
        nconf.setAttribute("dataSocketType", "UDP");
    }
}




/* Module Configuration */

ModuleConfiguration::ModuleConfiguration(QObject *)
{
}

ModuleConfiguration::~ModuleConfiguration()
{
    while (!singleModuleConf.isEmpty()) {
        singleModuleConf.takeLast();
    }
}

int ModuleConfiguration::loadFromXML(QDomNode &moduleConfNode)
{
    QDomNode n = moduleConfNode.firstChild();

    QString dataToList;
    QString portString;
    bool ok;

    while (!n.isNull()) {
        QDomElement nt = n.toElement();
        if (!nt.isNull()) {
            SingleModuleConf tmpModuleConf;
            // initialize the hostName in case it is not set

            tmpModuleConf.moduleName = nt.attribute("moduleName", "");
            qDebug() << "[ParseTrodesConfig] Reading in configuration for module: " << tmpModuleConf.moduleName;
            tmpModuleConf.sendTrodesConfig = nt.attribute("sendTrodesConfig", "0").toInt(&ok);

            if (!ok) {
                /*QMessageBox::information(
                            0, "error", QString(
                                "Config file error: Error in converting "
                                "sendTrodesConfig in Module Configuration. "
                                "Read values are '") +
                            nt.attribute("sendTrodesConfig", "") + QString("'"));*/
                return -2;
            }
            tmpModuleConf.sendNetworkInfo = nt.attribute("sendNetworkInfo", "0").toInt(&ok);
            if (!ok) {
                /*QMessageBox::information(
                            0, "error", QString(
                                "Config file error: Error in converting "
                                "sendNetworkInfo to number in Module Configuration. "
                                "Read values are '") +
                            nt.attribute("sendNetorkInfo", "") + QString("'"));*/
                return -2;
            }
            tmpModuleConf.hostName = nt.attribute("hostName", "localhost");

            QDomNode argNode = n.firstChild();
            QDomElement argElement;
            while (!argNode.isNull()) {
                argElement = argNode.toElement();
                if (!argElement.isNull()) {
                    //qDebug() << "[ParseTrodesConfig]" << argElement.attribute("flag","") << " " << argElement.attribute("value", "");
                    tmpModuleConf.moduleArguments.push_back(argElement.attribute("flag", ""));
                    tmpModuleConf.moduleArguments.push_back(argElement.attribute("value", ""));
                }
                argNode = argNode.nextSibling();
            }
            moduleConf->singleModuleConf.append(tmpModuleConf);
        }
        n = n.nextSibling();
    }

    if (singleModuleConf.length() > 0) {
        modulesDefined = 1;
    }

    return 0;
}

void ModuleConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement mconf = doc.createElement("ModuleConfiguration");

    rootNode.appendChild(mconf);

    for (int i = 0; i < singleModuleConf.length(); i++) {
        QDomElement nt = doc.createElement("SingleModuleConfiguration");

        nt.setAttribute("moduleName", singleModuleConf[i].moduleName);
        //nt.setAttribute("modulePath",singleModuleConf[i].modulePath);
        //nt.setAttribute("moduleConfigFile",singleModuleConf[i].moduleConfigFile);
        nt.setAttribute("sendTrodesConfig", singleModuleConf[i].sendTrodesConfig);
        nt.setAttribute("sendNetworkInfo", singleModuleConf[i].sendNetworkInfo);
        for (int j = 0; j < singleModuleConf[i].moduleArguments.length(); j=j+2) {
            if (singleModuleConf[i].moduleArguments.length() > j+1) {
                QDomElement arg = doc.createElement("Argument");
                arg.setAttribute("flag", singleModuleConf[i].moduleArguments[j]);
                arg.setAttribute("value", singleModuleConf[i].moduleArguments[j+1]);
                nt.appendChild(arg);
            }
        }


        mconf.appendChild(nt);
    }

}

bool ModuleConfiguration::modulePresent(QString modName) {
    // returns true if the requested module is listed in the config file
    for (int i = 0; i < singleModuleConf.length(); i++) {
        if (singleModuleConf[i].moduleName.contains(modName, Qt::CaseInsensitive)) {
            return true;
        }
    }
    return false;
}

int ModuleConfiguration::findModule(QString modName) {
    // returns the index of the requested module if listed in the config file
    for (int i = 0; i < singleModuleConf.length(); i++) {
        if (singleModuleConf[i].moduleName.contains(modName, Qt::CaseInsensitive)) {
            return i;
        }
    }
    return -1;
}

/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
//Stream configuration

streamConfiguration::streamConfiguration(bool dontCreateFilter)
{
#ifdef TRODES_CODE

    //this if statement is for the workspaceGui to specifi that it doesn't need to create the data filters on initialization
    //which prevents an occassional bad_alloc crash
    if (!dontCreateFilter) {
//        spikeFilters.reserve(hardwareConf->NCHAN);
        for (int i=0; i<hardwareConf->NCHAN;i++) {
            spikeFilters.push_back(new BesselFilter());
            spikeFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);
        }
//        lfpFilters.reserve(spikeConf->ntrodes.length());
        for(int trode = 0; trode < spikeConf->ntrodes.length(); trode++){
            lfpFilters.push_back(new BesselFilter());
            lfpFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);
            lfpFilters.last()->setFilterRange(0,spikeConf->ntrodes[trode]->moduleDataHighFilter);
            spikeModeOn.push_back(true);
            lfpModeOn.push_back(false);
        }
    }
#endif

#ifdef PHOTOMETRY_CODE
    photometryFilters = new ButterworthFilter[hardwareConf->NFIBER*4];
    for (int i=0; i<hardwareConf->NFIBER;i++) {
        for (int j=0 ; j < 4; j++) {
            if(photometryConf->nfibers[i]->filterOn[j]){
                photometryFilters[i*4+j].setSamplingRate(hardwareConf->sourceSamplingRate);
                photometryFilters[i*4+j].setFilterRange(photometryConf->nfibers[i]->lowFilter[j],
                                                        photometryConf->nfibers[i]->highFilter[j]);
                qDebug() << "add filter " << i*4+j;
            }
        }
    }
#endif
    nColumns = 1;
    nTabs = 1;
    tLength = 1.0;
    FS = 25000;
}

streamConfiguration::~streamConfiguration()
{
}

void streamConfiguration::setSpikeModeOn(int ntrode, bool on){
    spikeModeOn[ntrode] = on;
    emit updatedSpikeMode();
}

void streamConfiguration::setLFPModeOn(int ntrode, bool on){
    lfpModeOn[ntrode] = on;
    emit updatedSpikeMode();
}

void streamConfiguration::setAllModesOn(bool spike, bool lfp){
    spikeModeOn.fill(spike);
    lfpModeOn.fill(lfp);
    emit updatedAllModes();
}
void streamConfiguration::setTLength(double newTLength)
{
    tLength = newTLength;

    emit updatedTLength(tLength);
}


int streamConfiguration::loadFromXML(QDomNode &eegDispConfNode)
{
    bool warnFlag = false;

    nColumns = eegDispConfNode.toElement().attribute("columns", "2").toInt();
    nTabs = eegDispConfNode.toElement().attribute("pages", "2").toInt();

    if (nColumns < 1 || nColumns > 4) {
        qDebug() << "Error - allowed range for number of stream display columns per page is 1 through 4";
        return -4;
    }
    tLength = 1.0;

    nChanConfigured = 0;

    backgroundColor.setNamedColor(eegDispConfNode.toElement().attribute("backgroundColor", "#808080")); //default color is gray (RGB in hex)

    for (int n = 0; n < hardwareConf->NCHAN; n++) {
        trodeIndexLookupByHWChan.push_back(-1);
        trodeChannelLookupByHWChan.push_back(-1);
    }
    for (int tmpTrodeIndex = 0; tmpTrodeIndex < spikeConf->ntrodes.length(); tmpTrodeIndex++) {
        for (int tmpChanNum = 0; tmpChanNum < spikeConf->ntrodes[tmpTrodeIndex]->maxDisp.length();
               tmpChanNum++) {
            //trodeIndexLookup.push_back(tmpTrodeIndex);
            //trodeNumberLookup.push_back(spikeConf->ntrodes[tmpTrodeIndex]->nTrode);
            //trodeChannelLookup.push_back(tmpChanNum);

            trodeIndexLookupByHWChan[spikeConf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpTrodeIndex;
            if (trodeChannelLookupByHWChan[spikeConf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] != -1) {
                qDebug() << "Warning - HW channel" << spikeConf->ntrodes[tmpTrodeIndex]->unconverted_hw_chan[tmpChanNum] << "is assigned more than once. NOT SUPPORTED!";

                warnFlag = true;
            }
            trodeChannelLookupByHWChan[spikeConf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpChanNum;
        }
    }
#ifdef PHOTOMETRY_CODE
    if(hardwareConf->NFIBER>0){
        for (int n = 0; n < hardwareConf->NFIBER*4; n++) {
            fiberIndexLookupByHWChan.push_back(-1);
            fiberChannelLookupByHWChan.push_back(-1);
        }
        for (int tmpFiberIndex = 0; tmpFiberIndex < photometryConf->nfibers.length(); tmpFiberIndex++) {
            for (int tmpChanNum = 0; tmpChanNum < photometryConf->nfibers[tmpFiberIndex]->maxDisp.length();
                   tmpChanNum++, nChanConfigured++) {
                fiberIndexLookupByHWChan[photometryConf->nfibers[tmpFiberIndex]->hw_chan[tmpChanNum]] = tmpFiberIndex;
                fiberChannelLookupByHWChan[photometryConf->nfibers[tmpFiberIndex]->hw_chan[tmpChanNum]] = tmpChanNum;
            }
        }
    }
#endif
    setChanToSave();
    if (warnFlag) return 100;  //100 creates a warning dialog for the last debug statement

    return 0;
}

void streamConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement streamdisp = doc.createElement("StreamDisplay");

    rootNode.appendChild(streamdisp);
    streamdisp.setAttribute("columns", nColumns);
    streamdisp.setAttribute("pages", nTabs);
    streamdisp.setAttribute("backgroundColor", backgroundColor.name());
    //streamdisp.setAttribute("tLength",tLength);
}

void streamConfiguration::setChanToSave() {
    saveHWChan = new bool[hardwareConf->NCHAN];
    // go through the channel list and find the hardware channels to be saved.  This is only
    // used if saveDisplayedChanOnly is set to true
    for (int i = 0; i < hardwareConf->NCHAN; i++) {
        if (streamConf->trodeChannelLookupByHWChan[i] != -1) {
            saveHWChan[i] = true;
            nChanConfigured++;
            //qDebug() << "saveHWChan" << i << saveHWChan[i] << streamConf->trodeChannelLookupByHWChan[i];
        }
        else {
            saveHWChan[i] = false;
        }
    }

}
void streamConfiguration::listChanToSave()
{
    for (int i = 0; i < hardwareConf->NCHAN; i++) {
        qDebug() << "[ParseTrodesConfig] saveHWChan" << i << saveHWChan[i] << streamConf->trodeChannelLookupByHWChan[i];
    }
}

void streamConfiguration::setBackgroundColor(QColor c) {
    backgroundColor = c;
    emit updatedBackgroundColor(c);
}

/* --------------------------------------------------------------------- */




headerDisplayConfiguration::headerDisplayConfiguration(QObject *)
{
}

headerDisplayConfiguration::~headerDisplayConfiguration()
{
    while (!headerChannels.isEmpty()) {
        headerChannels.takeLast();
    }
}

int headerDisplayConfiguration::maxDigitalPort(bool input) {
    int maxPort = 0;

    QList<int> portList;

    portList = (input == 1) ? digInPortList: digOutPortList;

    for (int i = 0; i < portList.length(); i++) {
        if (portList[i] > maxPort) {
            maxPort = portList[i];
        }
    }
    //qDebug() << "In maxDigitalPort, portlist" << portList << "maxPort" << maxPort;

    return maxPort;
}

int headerDisplayConfiguration::minDigitalPort(bool input) {
    int minPort = 10000000;

    QList<int> portList;

    portList = (input == 1) ? digInPortList: digOutPortList;

    for (int i = 0; i < portList.length(); i++) {
        if (portList[i] < minPort) {
            minPort = portList[i];
        }
    }
    //qDebug() << "In minDigitalPort, portlist" << portList << "minPort" << minPort;
    return minPort;
}

bool headerDisplayConfiguration::digitalPortValid(int port, bool input) {
    QList<int> portList;

    portList = (input == 1) ? digInPortList: digOutPortList;

    if (portList.indexOf(port) == -1) {
        return false;
    }
    return true;
}

bool headerDisplayConfiguration::digitalIDValid(QString ID, bool input) {

    QStringList IDList;

    IDList = (input == 1) ? digInIDList: digOutIDList;

    if (!IDList.contains(ID,Qt::CaseInsensitive)) {
        return false;
    }
    return true;
}

int headerDisplayConfiguration::loadFromXML(QDomNode &headerConfNode)
{
    QDomNode n = headerConfNode.firstChild();

    bool ok;

    //int maxDigitalInputPort = 0;
    //int maxDigitalOutputPort = 0;
    //int maxAnalogInputPort = 0;
    //int maxAnalogOutputPort = 0;

    while (!n.isNull()) {
        QDomElement nt = n.toElement();
        if (!nt.isNull()) {
            //int offset = 0;
            headerChannel tmpHeaderChan;
            bool skipChan = false;

            tmpHeaderChan.idString = nt.attribute("id", "");
            tmpHeaderChan.color.setNamedColor(nt.attribute("color", "#808080"));  //default color is gray (RGB in hex)
            tmpHeaderChan.maxDisp = nt.attribute("maxDisp", "1").toInt(&ok);
            tmpHeaderChan.storeStateChanges = nt.attribute("analyze", "0").toInt(&ok); //if true, then Trodes will store the times of every state change

            if (!hardwareConf->headerSizeManuallyDefined) {
                tmpHeaderChan.deviceName = nt.attribute("device", "");
                if (!tmpHeaderChan.deviceName.isEmpty()) {

                    if (hardwareConf != NULL) {
                        bool deviceFound = false;
                        for (int i=0; i<hardwareConf->devices.length();i++) {
                            if (hardwareConf->devices[i].name.compare(tmpHeaderChan.deviceName)==0) {
                                //offset = hardwareConf->devices[i].packetOffset;
                                deviceFound = true;
                                if (!hardwareConf->devices[i].available) {
                                    //This device has been turned off
                                    skipChan = true;
                                    break;
                                }

                                //Look up the hardware info for this channel
                                bool channelFound = false;
                                for (int devCh = 0; devCh < hardwareConf->devices[i].channels.length();devCh++) {
                                    if (hardwareConf->devices[i].channels[devCh].idString.compare(tmpHeaderChan.idString)==0) {
                                        channelFound = true;
                                        tmpHeaderChan.port = hardwareConf->devices[i].channels[devCh].port;
                                        tmpHeaderChan.dataType = hardwareConf->devices[i].channels[devCh].dataType;
                                        tmpHeaderChan.startByte = hardwareConf->devices[i].channels[devCh].startByte;
                                        tmpHeaderChan.digitalBit = hardwareConf->devices[i].channels[devCh].digitalBit;
                                        tmpHeaderChan.interleavedDataIDByte = hardwareConf->devices[i].channels[devCh].interleavedDataIDByte;
                                        tmpHeaderChan.interleavedDataIDBit = hardwareConf->devices[i].channels[devCh].interleavedDataIDBit;
                                        tmpHeaderChan.input = hardwareConf->devices[i].channels[devCh].input;
                                        break;
                                    }
                                }
                                if (!channelFound) {
                                    qDebug() << "[ParseTrodesConfig] Error: Channel name in headerdisplay not found in hardware config.";
                                    return -1;
                                }

                                break;
                            }
                        }
                        if (!deviceFound) {
                            qDebug() << "[ParseTrodesConfig] Error: Device name in headerdisplay not found in hardware config.";
                            return -1;
                        }

                    } else {
                        qDebug() << "[ParseTrodesConfig] Error: Hardware config must be defined before headerdisplay config.";
                        return -1;

                    }
                } else {
                    qDebug() << "[ParseTrodesConfig] Error: Each aux display entry must reference a device.";
                    return -1;
                }
            } else {

                //If no devices are defined, the hardware info for the channel can be defined in the display section
                QString dataTypeString = nt.attribute("dataType", "");

                if (dataTypeString == "digital") {
                    tmpHeaderChan.dataType = DeviceChannel::DIGITALTYPE;
                }
                else if (dataTypeString == "analog") {
                    tmpHeaderChan.dataType = DeviceChannel::INT16TYPE;
                }
                else if (dataTypeString == "uint32") {
                    tmpHeaderChan.dataType = DeviceChannel::UINT32TYPE;
                }
                else {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in parsing data type: ") + dataTypeString);
                    return -2;
                }
                tmpHeaderChan.port = nt.attribute("port", "1").toInt(&ok);
                tmpHeaderChan.input = nt.attribute("input", "1").toInt(&ok);
                tmpHeaderChan.startByte = nt.attribute("startByte", "1").toInt(&ok);
                //qDebug() << "CONFIGURATION: " << tmpHeaderChan.port << " " << tmpHeaderChan.input << " " << tmpHeaderChan.startByte;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting startByte to number in header display configuration. Read value is '") + nt.attribute("startByte", "") + QString("'."));
                    return -2;
                }
                else if ((tmpHeaderChan.startByte < 1) || (tmpHeaderChan.startByte >= hardwareConf->headerSize * 2)) {
                    //QMessageBox::information(0, "error", QString("Config file error: startByte must be between 1 and ") + QString("%1").arg((hardwareConf->headerSize * 2) - 1) + QString(". Read value is '") + nt.attribute("startByte", "") + QString("'."));
                    return -2;
                }

                tmpHeaderChan.digitalBit = nt.attribute("bit", "0").toInt(&ok);
                if ((tmpHeaderChan.dataType == DeviceChannel::DIGITALTYPE) && (!ok)) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting bit value to number in header display configuration. Read value is '") + nt.attribute("bit", "") + QString("'."));
                    return -2;
                }
                else if ((tmpHeaderChan.dataType == DeviceChannel::DIGITALTYPE) && ((tmpHeaderChan.digitalBit < 0) || (tmpHeaderChan.digitalBit > 7))) {
                    //QMessageBox::information(0, "error", QString("Config file error: bit value must be between 0 and 7. Read value is '") + nt.attribute("bit", "") + QString("'."));
                    return -2;
                }

                tmpHeaderChan.interleavedDataIDByte = nt.attribute("interleavedDataIDByte", "-1").toInt();
                if (tmpHeaderChan.interleavedDataIDByte != -1) {
                    tmpHeaderChan.interleavedDataIDByte = tmpHeaderChan.interleavedDataIDByte;
                    tmpHeaderChan.interleavedDataIDBit = nt.attribute("interleavedDataIDBit","-1").toInt(&ok);
                    if (!ok) {
                        qDebug() << "[ParseTrodesConfig] Error: An interleavedDataIDByte entry must be accompanied by an interleavedDataIDBit entry.";
                        return -1;
                    } else {
                        //qDebug() << "Interleaved channel: " << tmpHeaderChan.idString;
                    }
                }
            }

            if (!skipChan) {
                // append this to the appropriate list
                if (tmpHeaderChan.input) {
                    digInIDList.append(tmpHeaderChan.idString);
                    digInPortList.append(tmpHeaderChan.port);
                }
                else {
                    digOutIDList.append(tmpHeaderChan.idString);
                    digOutPortList.append(tmpHeaderChan.port);
                }

                headerChannels.append(tmpHeaderChan);
            }
        }
        n = n.nextSibling();
    }

    return 0;
}


void headerDisplayConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement hconf = doc.createElement("AuxDisplayConfiguration");

    rootNode.appendChild(hconf);

    for (int i = 0; i < headerChannels.length(); i++) {
        QDomElement nt = doc.createElement("DispChannel");

        nt.setAttribute("id", headerChannels[i].idString);

        nt.setAttribute("maxDisp", headerChannels[i].maxDisp);
        nt.setAttribute("color", headerChannels[i].color.name());
        nt.setAttribute("analyze", (int)headerChannels[i].storeStateChanges);

        if (!hardwareConf->headerSizeManuallyDefined) {
            nt.setAttribute("device", headerChannels[i].deviceName);
        } else {

            //Include the hardware info in each display entry (no hardware devices defined)
            if (headerChannels[i].dataType == DeviceChannel::DIGITALTYPE) {
                nt.setAttribute("dataType", "digital");
            } else if (headerChannels[i].dataType == DeviceChannel::INT16TYPE) {
                nt.setAttribute("dataType", "analog");
            } else if (headerChannels[i].dataType == DeviceChannel::UINT32TYPE) {
                nt.setAttribute("dataType", "uint32");
            }
            nt.setAttribute("port", headerChannels[i].port);
            nt.setAttribute("input", headerChannels[i].input);
            nt.setAttribute("bit", headerChannels[i].digitalBit);
            nt.setAttribute("startByte", headerChannels[i].startByte);

            if (headerChannels[i].interleavedDataIDByte > -1) {
                nt.setAttribute("interleavedDataIDByte", headerChannels[i].interleavedDataIDByte);
                nt.setAttribute("interleavedDataIDBit", headerChannels[i].interleavedDataIDBit);
            }
        }
        hconf.appendChild(nt);
    }
}



/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
// Spike Configuration

CategoryDictionary::CategoryDictionary(void) {
//    addCategory("All"); //add the default all category
}

bool CategoryDictionary::categoryExists(QString category) {
    return(categories.contains(category));
}

bool CategoryDictionary::tagExists(QString tag) {
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
        if (iter.value().contains(tag)) {
            return(true);
        }
    }
    return(false);
}

bool CategoryDictionary::gTagExists(GroupingTag gt) {
    return(isTagInCategory(gt.category,gt.tag));
}

bool CategoryDictionary::isTagInCategory(QString category, QString tag) {
    if (!categoryExists(category)) //if the category doesn't exist, the tag certainly isn't in it
        return(false);
    return(categories.value(category).contains(tag));
}

QString CategoryDictionary::getTagsCategory(QString tag) {
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
        if (iter.value().contains(tag) && iter.key() != "All") {
            return(iter.key());
        }
    }
    return("All"); //'All' is the system reserved category that will include all tags
}

QList<GroupingTag> CategoryDictionary::getSortedAllTagList() {
    QList<GroupingTag> retList;

    QList<QList<QString> > complexList;
    QHashIterator<GroupingTag,int> i(all);
    while(i.hasNext()) {
        i.next();
        QString curCata = i.key().category;
        QString curTag = i.key().tag;
        bool createNewCata = true;
        for (int j = 0; j < complexList.length(); j++) {
            if (complexList.at(j).at(0) == curCata) { //first item of each sub list will be catagory
                createNewCata = false;
                QList<QString> curTagList = complexList.at(j);
                bool tagAlreadyInList = false;
                for (int y = 1; y < curTagList.length(); y++) { //check the list to see if the tag is already in it
                    if (curTagList.at(y) == curTag) {
                        tagAlreadyInList = true;
                        break;
                    }

                }
                if (!tagAlreadyInList) { //only add the tag if it's not already in the list
                    curTagList.push_back(curTag);
                    complexList.replace(j, curTagList);
                }
                break;
            }
        }
        if (createNewCata) {
            QList<QString> newCataList;
            newCataList.push_back(curCata);
            newCataList.push_back(curTag);
            complexList.push_back(newCataList);
        }
    }

    //now turn the complex list into a sorted list

    for (int i = 0; i < complexList.length(); i++) {
        QList<QString> curTagList = complexList.at(i);
        QString curCata = curTagList.at(0);
        for (int j = 1; j < curTagList.length(); j++) {
            QString curTag = curTagList.at(j);
            GroupingTag curGTag;
            curGTag.category = curCata;
            curGTag.tag = curTag;
            retList.append(curGTag);
        }

    }

    return(retList);
}

QHash<GroupingTag, int> CategoryDictionary::getAllTags() {
    return(all);
}

QHash<QString,QString> CategoryDictionary::getCategorysTags(QString category) {
    return(categories.value(category));
}

QHash<QString, QHash<QString,QString> > CategoryDictionary::getCategories() {
    return(categories);
}

bool CategoryDictionary::addCategory(QString newCategory) {
    if (categoryExists(newCategory)) {
//        qDebug() << "category already exists";
        return(false);
    }
    if (newCategory == "All") {
        return(false);
    }

    QHash<QString,QString> emptyTagHash;
    categories.insert(newCategory, emptyTagHash);
    return(true);
}

//Add's the specified tag the to the specified category, supports multiple tags across different categories
bool CategoryDictionary::addTagToCategory(QString category, QString tag) {
    if (!categoryExists(category)) { //if the category doesn't exist
        qDebug() << "Error: Specified category does not exist. (CategoryDictionary::addTagToCategory)";
        return(false);
    }
    QHash<QString,QString> tagHash = categories.value(category);
    if (tagHash.contains(tag)) { //don't add duplicate tags to the same category
//        qDebug() << "Tag already added.";
        return(false);
    }

    tagHash.insert(tag,tag);
    categories.insert(category,tagHash); //should replace the previous value, if not do remove(cateogry) and insert(category, hash)
    if (category != "All") { //add all tags to the 'All' category, this will automatically ignore duplicates
//        addTagToCategory("All",tag); //todo: remove the "All" category from the categories list.  It is now stored in a separate object

        GroupingTag gt;
        gt.tag = tag;
        gt.category = category;
        addTagToALL(gt);
    }
    return(true);
}

bool CategoryDictionary::addTag(GroupingTag gt) {
    if (this->gTagExists(gt)) //don't add if it already is in the dictionary
        return(false);
    this->addCategory(gt.category); //add the category, will automatically skip if the category already has been added
    this->addTagToCategory(gt.category,gt.tag); //add the tag, will automatically skip if the tag has already been added
    //this will also automatically append the new tag to the 'All' category, skipping duplicates by default
    return true;
}

bool CategoryDictionary::addTagToALL(GroupingTag gt) {
//    qDebug() << "Adding tag to all";
    if (all.contains(gt))
        return(false);
//    qDebug() << "   Done";
    all.insert(gt, 1);
    return(true);
}

void CategoryDictionary::clear() {
    categories.clear();
    all.clear();
}

void CategoryDictionary::printAll() {
    qDebug() << "Printing Category Dictionary";

    qDebug() << "-- All";
    QHashIterator<GroupingTag, int> i(all);
    while (i.hasNext()) {
        i.next();
        qDebug() << "   -- [" << i.key().category << "] " << i.key().tag;
    }

    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
        qDebug() << "-- " << iter.key();
        QHashIterator<QString,QString> subIter(iter.value());
        while(subIter.hasNext()) {
            subIter.next();
            qDebug() << "   -- " << subIter.key();
        }
    }
}


SpikeConfiguration::~SpikeConfiguration()
{
    while (!ntrodes.isEmpty()) {
        delete ntrodes.takeLast();
    }
}

#ifdef TRODES_CODE
void SpikeConfiguration::setModuleDataSwitch(int nTrode, bool on)
{
    ntrodes[nTrode]->moduleDataOn = on;
    emit updatedModuleData();
}

void SpikeConfiguration::setRefSwitch(int nTrode, bool on)
{

    ntrodes[nTrode]->refOn = on;
    emit updatedRef();
}

void SpikeConfiguration::setLFPRefSwitch(int nTrode, bool on){
    ntrodes[nTrode]->lfpRefOn = on;
    emit updatedRef();
}

void SpikeConfiguration::setLFPFilterSwitch(int nTrode, bool on) {
    ntrodes[nTrode]->lfpFilterOn = on;
    emit updatedLFPFilter();
}

void SpikeConfiguration::setFilterSwitch(int nTrode, bool on)
{
    ntrodes[nTrode]->filterOn = on;
    emit updatedFilter();
}

void SpikeConfiguration::setModuleDataChan(int nTrode, int newChan)
{
    ntrodes[nTrode]->moduleDataChan = newChan;
    emit updatedModuleData();
}

void SpikeConfiguration::setMaxDisp(int nTrode, int newMaxDisp)
{
    if (linkChangesBool) {
        emit changeAllMaxDisp(newMaxDisp);
    }
    else if (ntrodes[nTrode]->maxDisp[0] != newMaxDisp ) {

        for (int i=0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
            ntrodes[nTrode]->maxDisp[i] = newMaxDisp;
        }

        emit newMaxDisplay(nTrode, newMaxDisp);
        emit updatedMaxDisplay();
    }
}

void SpikeConfiguration::setThresh(int nTrode, int newThresh)
{
    if (linkChangesBool) {
        emit changeAllThresh(newThresh);
    }
    else if (ntrodes[nTrode]->thresh[0] != newThresh) {
        for (int i=0; i < ntrodes[nTrode]->thresh.length(); i++) {
            ntrodes[nTrode]->thresh[i] = newThresh;
            ntrodes[nTrode]->thresh_rangeconvert[i] = (newThresh * 65536) / AD_CONVERSION_FACTOR;

        }

        emit newThreshold(nTrode, newThresh);
        //emit newThreshold(nTrode, chan, newThresh);
        emit updatedThresh();
    }
}


void SpikeConfiguration::setMaxDisp(int nTrode, int chan, int newMaxDisp)
{
    if (linkChangesBool) {
        emit changeAllMaxDisp(newMaxDisp);
    }
    else {
        ntrodes[nTrode]->maxDisp[chan] = newMaxDisp;
        emit newMaxDisplay(ntrodes[nTrode]->hw_chan[chan], newMaxDisp);
        emit updatedMaxDisplay();
    }
}

void SpikeConfiguration::setThresh(int nTrode, int chan, int newThresh)
{
    if (linkChangesBool) {
        emit changeAllThresh(newThresh);
    }
    else {
        ntrodes[nTrode]->thresh[chan] = newThresh;
        ntrodes[nTrode]->thresh_rangeconvert[chan] = (newThresh * 65536) / AD_CONVERSION_FACTOR;
        emit newThreshold(ntrodes[nTrode]->hw_chan[chan], newThresh);
        emit newThreshold(nTrode, chan, newThresh);
        emit updatedThresh();
    }
}

void SpikeConfiguration::setTriggerMode(int nTrode, bool triggerOn)
{
    for (int i = 0; i < ntrodes[nTrode]->triggerOn.length(); i++) {
        setTriggerMode(nTrode, i, triggerOn);
    }

}

void SpikeConfiguration::setTriggerMode(int nTrode, int chan, bool triggerOn)
{
    ntrodes[nTrode]->triggerOn[chan] = triggerOn;
    emit newTriggerMode(ntrodes[nTrode]->hw_chan[chan], triggerOn);
    emit newTriggerMode(nTrode, chan, triggerOn);
}

void SpikeConfiguration::setReference(int nTrode, int newRefNTrode, int newRefNTrodeChan)
{
    ntrodes[nTrode]->refNTrode = newRefNTrode;
    ntrodes[nTrode]->refNTrodeID = ntrodes[newRefNTrode]->nTrodeId;
    ntrodes[nTrode]->refChan = newRefNTrodeChan;



    emit updatedRef();
}

void SpikeConfiguration::setColor(int nTrode, QColor newColor)
{
    ntrodes[nTrode]->color = newColor;
    emit updatedTraceColor();
}


void SpikeConfiguration::setLowFilter(int nTrode, int cutoff)
{

    int hw_chan;
    ntrodes[nTrode]->lowFilter = cutoff;
    for (int i = 0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
        hw_chan = ntrodes[nTrode]->hw_chan[i];
        //streamConf->dataFilters[ntrodes[nTrode]->streamingChannelLookup[i]].setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);
        streamConf->spikeFilters[hw_chan]->setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);

    }

    emit updatedFilter();
}

void SpikeConfiguration::setHighFilter(int nTrode, int cutoff)
{
    int hw_chan;
    ntrodes[nTrode]->highFilter = cutoff;
    for (int i = 0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
        hw_chan = ntrodes[nTrode]->hw_chan[i];
        //streamConf->dataFilters[ntrodes[nTrode]->streamingChannelLookup[i]].setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);
        streamConf->spikeFilters[hw_chan]->setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);

    }
    emit updatedFilter();
}

void SpikeConfiguration::setModuleDataHighFilter(int nTrode, int cutoff)
{
    ntrodes[nTrode]->moduleDataHighFilter = cutoff;
    streamConf->lfpFilters[nTrode]->setFilterRange(0, cutoff);
    emit updatedModuleData();
}


#endif


int SpikeConfiguration::loadFromXML(QDomNode &spikeConfNode)
{
    QDomNode tetNode;
    QDomElement tetElement;
    QDomNode n = spikeConfNode.firstChild();
    int eidx = 0;
    int tetidx = 0;
    int totalChannelCount = 0;
    bool ok;

    int autoNtrodeType = 0;
    autoNtrodeType = spikeConfNode.toElement().attribute("autoPopulate", "0").toInt();

    QString cata = spikeConfNode.toElement().attribute("categories", "");
    if (!cata.isEmpty()) {
//        qDebug() << "  " << cata;
        QStringList categoryList = cata.split(";");
        for (int i = 0; i < categoryList.length(); i++) {
            QString categoryName = categoryList.at(i).split("=").first();
            groupingDict.addCategory(categoryName);

            QString tags = categoryList.at(i).split("=").last();
//            qDebug() << " category" << i+1 << ": " << categoryName;
            tags = tags.left(tags.length()-1);
            tags = tags.right(tags.length()-1);
//            qDebug() << "   -tags: " << tags;
            QStringList tagList = tags.split(",");
            for (int j = 0; j < tagList.length(); j++) {
                groupingDict.addTagToCategory(categoryName,tagList.at(j));
            }
        }
//        groupingDict.printAll();
    }
    else {
//        qDebug() << "##### No Catagories added";
        //default ALL category intialization here
    }

//    qDebug() << "Lets check some shit.";
//    qDebug() << "are tags 'HI' and 'BYE' in list? --" << groupingDict.tagExists("HI") << ", " << groupingDict.tagExists("BYE");

//    qDebug() << "print all the tags in categories 'YELLOW' and 'AREA";
//    if (groupingDict.categoryExists("YELLOW")) {

//    }
//    else
//        qDebug() << "   YELLOW does not exist\n";
//    QHash<QString,QString> tags = groupingDict.getCategorysTags("AREA");
//    QHashIterator<QString, QString> tIter(tags);
//    qDebug() << "   Tags for AREA";
//    while(tIter.hasNext()) {
//        tIter.next();
//        qDebug() << "       -" << tIter.value();
//    }

    //Does the config designate that channels should be distrubuted automatically?
    if (autoNtrodeType > 0) {
        int currentHardwareChan = 0;
        QVector<int> autoHWChanList;

        QString autoChannelOrder= spikeConfNode.toElement().attribute("channelOrder", "");
        if (!autoChannelOrder.isEmpty()) {

            //The user can give a channel list in the form of a string,
            //where the numbers are separated by spaces
            QStringList slist;
            slist = autoChannelOrder.split(" ");
            bool allOk(true);
            bool ok;
            for (int x = 0; x < slist.count(); x++) {
                autoHWChanList.append(slist.at(x).toInt(&ok));
                allOk &= ok;
            }
            if (!allOk) {
                qDebug() << "[ParseTrodesConfig] Error: The Automatic HW list could not be converted to numbers.";
                return -1;
            }
            if (autoHWChanList.length() != hardwareConf->NCHAN) {
                qDebug() << "[ParseTrodesConfig] Error: Automatic HW channel list is not the correct length!!";
                return -1;
            }
        } else {
            for (int i=0; i < hardwareConf->NCHAN; i++) {
                autoHWChanList.push_back(i);
            }
        }

        //This is the list of colors to cycle through
        QList<QColor> colorOptions;
        colorOptions.append(QColor(250,250,250));
        colorOptions.append(QColor(250,50,50));
        colorOptions.append(QColor(20,250,20));
        colorOptions.append(QColor(20,20,250));
        int currentColor = 0;
        for (int ntrNum=0;ntrNum < ntrodes.nitems; ntrNum++) {

            ntrodes.append(ntrNum+1, new SingleSpikeTrodeConf);
            for (int autoNTrodeChan=0;autoNTrodeChan<autoNtrodeType;autoNTrodeChan++) {

                ntrodes[ntrNum]->unconverted_hw_chan.push_back(currentHardwareChan);
                ntrodes[ntrNum]->hw_chan.append(convertHWchan(autoHWChanList[currentHardwareChan]));
                ntrodes[ntrNum]->nTrodeId = ntrNum+1;

                currentHardwareChan++;
                ntrodes[ntrNum]->maxDisp.append(400);
                ntrodes[ntrNum]->thresh.append(60);
                ntrodes[ntrNum]->thresh_rangeconvert.push_back((60 * 65536) / AD_CONVERSION_FACTOR);
                ntrodes[ntrNum]->streamingChannelLookup.append(400);

                ntrodes[ntrNum]->triggerOn.append(true);
                ntrodes[ntrNum]->lowFilter = 600;
                ntrodes[ntrNum]->highFilter = 6000;
                ntrodes[ntrNum]->color = colorOptions[currentColor];
                ntrodes[ntrNum]->refNTrode = 0;
                ntrodes[ntrNum]->refNTrodeID = 1;
                ntrodes[ntrNum]->refOn = false;
                ntrodes[ntrNum]->refChan = 0;
                ntrodes[ntrNum]->moduleDataChan = 0;
                ntrodes[ntrNum]->moduleDataHighFilter = 200;
                ntrodes[ntrNum]->filterOn = true;
                ntrodes[ntrNum]->moduleDataOn = false;
                ntrodes[ntrNum]->lfpRefOn = false;
                ntrodes[ntrNum]->lfpFilterOn = true;

                //currentHardwareChan++;

            }
            currentColor = (currentColor+1)%colorOptions.length();
        }
    } else {
        //No automatic channel ditribution
        int nTrodeCount = 0;
        while (!n.isNull()) {
            QDomElement nt = n.toElement();
            if (!nt.isNull()) {
                int ntidx = nt.attribute("id", "").toInt(&ok);
                ntrodes.append(ntidx, new SingleSpikeTrodeConf);

                if (!ok) {
                    qDebug() << "[ParseTrodesConfig] Error converting id to number in Spike Configuration. Read value is" << nt.attribute("id", "");
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting id to number in Spike Configuration. Read value is '") + nt.attribute("id", "") + QString("'."));
                    return -2;
                }
                nTrodeCount++;

                ntrodes[eidx]->color.setNamedColor(nt.attribute("color", "#808080")); //default color is gray (RGB in hex)
                ntrodes[eidx]->lowFilter = nt.attribute("lowFilter", "300").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting lowFilter to number in Spike Configuration. Read value is '") + nt.attribute("lowFilter", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->highFilter = nt.attribute("highFilter", "6000").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting highFilter to number in Spike Configuration. Read value is '") + nt.attribute("highFilter", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->refNTrode = nt.attribute("refNTrode", "1").toInt(&ok) - 1;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refNTrode to number in Spike Configuration. Read value is '") + nt.attribute("refNTrode", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->refNTrodeID = nt.attribute("refNTrodeID", "-1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refNTrode to number in Spike Configuration. Read value is '") + nt.attribute("refNTrode", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->refChan = nt.attribute("refChan", "1").toInt(&ok) - 1;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refChan to number in Spike Configuration. Read value is '") + nt.attribute("refChan", "") + QString("'."));
                    return -2;
                }


                //Users can either use LFPChan/LFPHighFilter filenames or moduleDataChan/moduleDataHighFilter
                ntrodes[eidx]->moduleDataChan = nt.attribute("moduleDataChan", "0").toInt(&ok) - 1;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataChan to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataChan", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->moduleDataHighFilter = nt.attribute("moduleDataHighFilter", "-1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataHighFilter to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataHighFilter", "") + QString("'."));
                    return -2;
                }

                if (ntrodes[eidx]->moduleDataChan == -1) {
                    ntrodes[eidx]->moduleDataChan = nt.attribute("LFPChan", "1").toInt(&ok) - 1;
                    if (!ok) {
                        //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataChan to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataChan", "") + QString("'."));
                        return -2;
                    }
                }

                if (ntrodes[eidx]->moduleDataHighFilter == -1) {
                    ntrodes[eidx]->moduleDataHighFilter = nt.attribute("LFPHighFilter", "200").toInt(&ok);
                    if (!ok) {
                        //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataHighFilter to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataHighFilter", "") + QString("'."));
                        return -2;
                    }
                    //qDebug() <<"[ParseTrodesConfig] Setting LFPHighFilter to" << ntrodes[eidx]->moduleDataHighFilter;
                }

                ntrodes[eidx]->refOn = nt.attribute("refOn", "1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refOn to number in Spike Configuration. Read value is '") + nt.attribute("refOn", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->filterOn = nt.attribute("filterOn", "1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting filterOn to number in Spike Configuration. Read value is '") + nt.attribute("filterOn", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->moduleDataOn = nt.attribute("moduleDataOn", "1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataOn to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataOn", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->lfpRefOn = nt.attribute("lfpRefOn", "0").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->lfpFilterOn = nt.attribute("lfpFilterOn", "1").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                //mark: grouping tags
                QString rawGroupingTagStr = nt.attribute("groupingTags", "");
                if (rawGroupingTagStr != "") {
//                    qDebug() << "*** Reading grouping tags: ";
                    QStringList gTagL = rawGroupingTagStr.split(";");
                    for (int i = 0; i < gTagL.length(); i++) {
                        QString curCata = gTagL.at(i).split("/").first();
                        QString curTag = gTagL.at(i).split("/").last();
                        if (curCata.isEmpty() || curTag.isEmpty())
                            continue;

//                        qDebug() << "   [" << curCata << "] " << curTag;
                        GroupingTag curGTag;
                        curGTag.category = curCata;
                        curGTag.tag = curTag;

                        ntrodes[eidx]->gTags.insert(curGTag, 1);
                        groupingDict.addTag(curGTag); //adds curGTag to the dictionary if it isn't already in it

                    }
//                    groupingDict.printAll();
                }
                else {
                    //LEGACY, depreciated now, only activate if rawGroupingTagStr was empty.  This ensures backwards compatability
//                    qDebug() << "LEGACY reading 1";
                    QString rawTagStr = nt.attribute("tags","");
                    if (rawTagStr != "") {
                        QStringList tagStrL = rawTagStr.split(";"); //tags are split by semi colon
                        for (int i = 0; i < tagStrL.length(); i++) {
                            //mark: to do - make the VALUE of the tag HASH be the CATEGORY it belongs to
                            if (groupingDict.tagExists(tagStrL.at(i))) {
                                //tag exists, don't do anything
                            }
                            else {
                                //tag doesn't exist, add to All
                                qDebug() << "IM not sure this is ever called, if it is, revist and rework";
//                                groupingDict.addTagToCategory("All",tagStrL.at(i));

                            }

                            GroupingTag newTag;
                            newTag.tag = tagStrL.at(i);
                            newTag.category = groupingDict.getTagsCategory(newTag.tag);
                            ntrodes[eidx]->gTags.insert(newTag, 1);
//                            ntrodes[eidx]->tags.insert(tagStrL.at(i),1);
                        }
                    }
                }
//                groupingDict.printAll();

                nTrodeTable->ntrodes.append(new NTrode);
                ntrodes[eidx]->nTrodeId = ntidx;
                tetNode = n.firstChild();
                tetidx = 0;
                while (!tetNode.isNull()) {
                    tetElement = tetNode.toElement();
                    if (!tetElement.isNull()) {
                        ntrodes[eidx]->maxDisp.push_back(tetElement.attribute("maxDisp", "400").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting maxDisp to number in SpikeChannel define. Read value is '") + tetElement.attribute("maxDisp", "") + QString("'."));
                            return -2;
                        }
                        if (ntrodes[eidx]->maxDisp.last() < 25) {
                            //QMessageBox::information(0, "error", QString("Config file error: All max display values (maxDisp) must be 25 or more. Error in SpikeNTrode %1.").arg(ntidx));
                            return -2;
                        }
                        ntrodes[eidx]->thresh.push_back(tetElement.attribute("thresh", "60").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting thresh to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }
                        if (ntrodes[eidx]->thresh.last() < 10) {
                            //QMessageBox::information(0, "error", QString("Config file error: All threshold values (thresh) must be 10 or more. Error in SpikeNTrode %1.").arg(ntidx));
                            return -2;
                        }
                        ntrodes[eidx]->triggerOn.push_back(tetElement.attribute("triggerOn", "1").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting triggerOn to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }

                        //ntrodes[eidx]->hw_chan.push_back(tetElement.attribute("hwChan", "0").toInt(&ok));
                        ntrodes[eidx]->thresh_rangeconvert.push_back((ntrodes[eidx]->thresh[tetidx] * 65536) / AD_CONVERSION_FACTOR);

                        int tempHWChanStorage = tetElement.attribute("hwChan", "0").toInt(&ok);
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting hwChan to number in SpikeChannel define. Read value is '") + tetElement.attribute("hwChan", "") + QString("'."));
                            return -2;
                        }

                        ntrodes[eidx]->unconverted_hw_chan.push_back(tempHWChanStorage);

                        if ((ntrodes[eidx]->unconverted_hw_chan.last() > hardwareConf->NCHAN) || (ntrodes[eidx]->unconverted_hw_chan.last() < 0)) {
                            qDebug() << "[ParseTrodesConfig] Error: All hardware channels (hwChan) must be between 0 and numChannels. Error in NTrode " << tetidx + 1 << "."
                               << "\n   Value" << ntrodes[eidx]->unconverted_hw_chan.last();
                            //QMessageBox::information(0, "error", QString("Config file error: All hardware channels (hwChan) must be between 0 and numChannels. Error in SpikeNTrode %1.").arg(ntidx));
                            return -2;
                        }
                        ntrodes[eidx]->hw_chan.append(convertHWchan(ntrodes[eidx]->unconverted_hw_chan.last()));  //Need to convert to interleaved form

                        nTrodeTable->ntrodes.last()->hw_chan.append(convertHWchan(ntrodes[eidx]->hw_chan.last()));
                        ntrodes[eidx]->streamingChannelLookup.push_back(totalChannelCount);
                        tetidx++;
                        totalChannelCount++;
                    }
                    tetNode = tetNode.nextSibling();
                }

                eidx++;
            }
            n = n.nextSibling();
        }
//        groupingDict.printAll();
        //convert the refNTrode field to RefNTrodeIDs
        convertNTrodeRefToIDs(); //this function only assigns refNTrodeIDs that don't already exist
        //populate the refNTrode field
        convertNTrodeIDsToRef();

        if (nTrodeCount != ntrodes.length()) {
            qDebug() << "[ParseTrodesConfig] Error: there is at least one extra nTrode node with bad formating";
            return -2;
        }
    }
    return 0;
}

void SpikeConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement spikeconf = doc.createElement("SpikeConfiguration");

    rootNode.appendChild(spikeconf);

    for (int i = 0; i < ntrodes.length(); i++) {
        QDomElement nt = doc.createElement("SpikeNTrode");

        nt.setAttribute("moduleDataOn", ntrodes[i]->moduleDataOn);
        nt.setAttribute("filterOn", ntrodes[i]->filterOn);
        nt.setAttribute("refOn", ntrodes[i]->refOn);
        nt.setAttribute("LFPHighFilter", ntrodes[i]->moduleDataHighFilter);
        nt.setAttribute("LFPChan", ntrodes[i]->moduleDataChan + 1);
        nt.setAttribute("refChan", ntrodes[i]->refChan + 1);
//        nt.setAttribute("refChanID", ntrodes[i]->refChanID);
//        nt.setAttribute("refNTrode", ntrodes[i]->refNTrode + 1);
#ifdef PHOTOMETRY_CODE
        nt.setAttribute("refNTrode", ntrodes[i]->refNTrode + 1);
#endif
        nt.setAttribute("refNTrodeID", ntrodes[i]->refNTrodeID);
        nt.setAttribute("highFilter", ntrodes[i]->highFilter);
        nt.setAttribute("lowFilter", ntrodes[i]->lowFilter);
        nt.setAttribute("color", ntrodes[i]->color.name());
        nt.setAttribute("id", ntrodes[i]->nTrodeId);
        nt.setAttribute("lfpRefOn", ntrodes[i]->lfpRefOn);
        nt.setAttribute("lfpFilterOn", ntrodes[i]->lfpFilterOn);

        //mark: grouping tags
        if (!ntrodes[i]->gTags.empty()) {
//            qDebug() << "Saving only GROUPING TAGS field";
            QHashIterator<GroupingTag, int> gIter(ntrodes[i]->gTags);
            QString groupingTagStr = "";
            while (gIter.hasNext()) {
                gIter.next();
                GroupingTag curGTag = gIter.key();
                groupingTagStr = QString("%1;%2/%3").arg(groupingTagStr).arg(curGTag.category).arg(curGTag.tag);
            }
            if (!groupingTagStr.isEmpty()) {
//                groupingTagStr = groupingTagStr.left(groupingTagStr.length()-1);
                nt.setAttribute("groupingTags",groupingTagStr);
            }
        }
        else { //DEPRCIATED, old tag saving code
            QHashIterator<QString, int> iter(ntrodes[i]->tags);
            QString tagStr = ""; //the old 'tag' string
            QString groupingTagStr = ""; //the new 'groupingTags' string, this includes tag/categor
            while(iter.hasNext()) {
                iter.next();
                QString nextCata;
                nextCata = groupingDict.getTagsCategory(iter.key());

                if (!groupingDict.tagExists(iter.key())) //add the key to the all category if it has not already been assigned to a category
                    groupingDict.addTagToCategory("All",iter.key());

                QString nextTag = QString("%1;").arg(iter.key());
                tagStr = QString("%1%2").arg(tagStr).arg(nextTag);
                groupingTagStr = QString("%1%2/%3").arg(groupingTagStr).arg(nextCata).arg(nextTag);
            }
            if (!tagStr.isEmpty()) {
                tagStr = tagStr.left(tagStr.length()-1);
                nt.setAttribute("tags",tagStr);
            }
            if (!groupingTagStr.isEmpty()) {
                groupingTagStr = groupingTagStr.left(groupingTagStr.length()-1);
                nt.setAttribute("groupingTags",groupingTagStr);
            }
        }

        for (int j = 0; j < ntrodes[i]->hw_chan.length(); j++) {
            QDomElement sCh = doc.createElement("SpikeChannel");
            //sCh.setAttribute("hwChan", ntrodes[i]->hw_chan[j]);
            sCh.setAttribute("hwChan", ntrodes[i]->unconverted_hw_chan[j]);
            sCh.setAttribute("maxDisp", ntrodes[i]->maxDisp[j]);
            sCh.setAttribute("thresh", ntrodes[i]->thresh[j]);
            sCh.setAttribute("triggerOn", ntrodes[i]->triggerOn[j]);
            nt.appendChild(sCh);
        }
        spikeconf.appendChild(nt);
    }


    QString categoryString = "";
    QHash<QString, QHash<QString,QString> > categories = groupingDict.getCategories();
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
//        qDebug() << "-- " << iter.key();
        categoryString = QString("%1%2={").arg(categoryString).arg(iter.key());
        QHashIterator<QString,QString> subIter(iter.value());
        int tags = 0;
        while(subIter.hasNext()) {
            subIter.next();
//            qDebug() << "   -- " << subIter.key();
            QString nextTag = QString("%1,").arg(subIter.key());
            categoryString = QString("%1%2").arg(categoryString).arg(nextTag);
            tags++;
        }
        if (tags > 0) {
            categoryString = categoryString.left(categoryString.length()-1);
        }
        categoryString = QString("%1};").arg(categoryString);
    }
    if (!categoryString.isEmpty())
        categoryString = categoryString.left(categoryString.length()-1);

//    qDebug() << "SAVING " << categoryString;
    spikeconf.setAttribute("categories", categoryString);


}

bool SpikeConfiguration::convertNTrodeRefToIDs() {
    bool retval = false;

    for (int i = 0; i < ntrodes.length(); i++) {
        if (ntrodes[i]->refNTrodeID == -1) {
            retval = true;
            ntrodes[i]->refNTrodeID = ntrodes[ntrodes[i]->refNTrode]->nTrodeId;
        }

    }
    return(retval);
}

void SpikeConfiguration::convertNTrodeIDsToRef() {
    for (int i = 0; i < ntrodes.length(); i++) {
        SingleSpikeTrodeConf *refNTrode = ntrodes.ID(ntrodes[i]->refNTrodeID);
        if (refNTrode != NULL) {
            ntrodes[i]->refNTrode = refNTrode->nTrodeIndex;
//            qDebug() << "Populating refNtrode field w/ " << refNTrode->nTrodeIndex;
        }
    }
}

int SpikeConfiguration::convertHWchan(int hw_chan)
{
    //When samples are collected on the hardware, the order of collection is
    //Card 0 Channel 0, Card 1 Channel 0, ..., Card N Channel 0, Card 0 Channel 1, Card 1, Channel 1, etc.
    //To minimize confusion, the config file uses 0-31 for card 0, 32-63 for card 1, etc.
    //This function converts the config file number to the actual hardware number.
    int numCards = hardwareConf->NCHAN / 32;
    int new_hw_chan = ((hw_chan % 32) * numCards) + floor(hw_chan / 32);
    return new_hw_chan;
}

#ifdef PHOTOMETRY_CODE
PhotometryConfiguration::~PhotometryConfiguration()
{
    while (!nfibers.isEmpty()) {
        delete nfibers.takeLast();
    }
}


void PhotometryConfiguration::setMaxDisp(int nTrode, int newMaxDisp)
{
    if (linkChangesBool) {
        emit changeAllMaxDisp(newMaxDisp);
    }
    else if (nfibers[nTrode]->maxDisp[0] != newMaxDisp ) {

        for (int i=0; i < nfibers[nTrode]->maxDisp.length(); i++) {
            nfibers[nTrode]->maxDisp[i] = newMaxDisp;
        }
        emit newMaxDisplay(nTrode, newMaxDisp);
        emit updatedMaxDisplay();
    }
}

void PhotometryConfiguration::setColor(int nTrode, QColor newColor)
{
    nfibers[nTrode]->color = newColor;
    emit updatedTraceColor();
}


int PhotometryConfiguration::loadFromXML(QDomNode &spikeConfNode)
{
    QDomNode tetNode;
    QDomElement tetElement;
    QDomNode n = spikeConfNode.firstChild();
    int eidx = 0;
    int tetidx = 0;
    int totalChannelCount = 0;
    bool ok;

    while (!n.isNull()) {
        QDomElement nt = n.toElement();
        if (!nt.isNull()) {

            int ntidx = nt.attribute("id", "").toInt(&ok);
            if (!ok) {
                qDebug() << "Error in converting id to number in Spike Configuration. Read value is" << nt.attribute("id", "");
                return -2;
            }

            nfibers[eidx]->nFiberId = ntidx;
            nfibers[eidx]->color.setNamedColor(nt.attribute("color", "#808080")); //default color is gray (RGB in hex)

            nfibers[eidx]->carrierFreq[0] = nt.attribute("carrierFreq1", "211").toInt(&ok);
            if (!ok) {
                return -2;
            }

            nfibers[eidx]->carrierFreq[1] = nt.attribute("carrierFreq2", "700").toInt(&ok);
            if (!ok) {
                return -2;
            }

            tetNode = n.firstChild();
            tetidx = 0;
            while (!tetNode.isNull()) {
                tetElement = tetNode.toElement();
                if (!tetElement.isNull()) {
                    nfibers[eidx]->hw_chan.push_back(tetElement.attribute("hwChan", "0").toInt(&ok));
                    if ((nfibers[eidx]->hw_chan.last() > hardwareConf->NFIBER*4) || (nfibers[eidx]->hw_chan.last() < 0)) {
                        qDebug() << "Config file error: All hardware channels (hwChan) must be between 0 and numChannels. Error in NTrode "
                                 << tetidx + 1 << ".";
                        return -2;
                    }

                    nfibers[eidx]->maxDisp.push_back(tetElement.attribute("maxDisp", "200").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->filterOn.push_back(tetElement.attribute("filterOn", "1").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->cidx.push_back(tetElement.attribute("cidx", "0").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->lowFilter.push_back(tetElement.attribute("lowFilter", "600").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->highFilter.push_back(tetElement.attribute("highFilter", "6000").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    tetidx++;
                    totalChannelCount++;
                }
                tetNode = tetNode.nextSibling();
            }

            eidx++;

        }
        n = n.nextSibling();
    }

    return 0;
}

void PhotometryConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement photometryConf = doc.createElement("PhotometryConfiguration");

    rootNode.appendChild(photometryConf);

    for (int i = 0; i < nfibers.length(); i++) {
        QDomElement nt = doc.createElement("PhotometryNFiber");

        nt.setAttribute("carrierFreq1", nfibers[i]->carrierFreq[0]);
        nt.setAttribute("carrierFreq2", nfibers[i]->carrierFreq[1]);
        nt.setAttribute("color",        nfibers[i]->color.name());
        nt.setAttribute("id",           nfibers[i]->nFiberId);

        for (int j = 0; j < nfibers[i]->hw_chan.length(); j++) {
            QDomElement sCh = doc.createElement("PhotometryChannel");
            sCh.setAttribute("maxDisp", nfibers[i]->maxDisp[j]);
            sCh.setAttribute("hwChan", nfibers[i]->hw_chan[j]);
            sCh.setAttribute("filterOn",  nfibers[i]->filterOn[j]);
            sCh.setAttribute("cidx",  nfibers[i]->cidx[j]);
            sCh.setAttribute("lowFilter", nfibers[i]->lowFilter[j]);
            sCh.setAttribute("highFilter", nfibers[i]->highFilter[j]);
            nt.appendChild(sCh);
        }
        photometryConf.appendChild(nt);
    }
}
#endif

BenchmarkConfig::BenchmarkConfig() {
    BenchmarkConfig(false, false, false, false, false, false);
}

BenchmarkConfig::BenchmarkConfig(bool recSysTime, bool pSpikeDetect, bool pSpikeSent, bool pSpikeReceived, bool pPositionStreaming, bool pEventSys) {
    userRuntimeEdited = false;
    iniFromCmdLine = false;
    recordSysTime = recSysTime;
    spikeDetect = pSpikeDetect;
    spikeSent = pSpikeSent;
    spikeReceived = pSpikeReceived;
    positionStreaming = pPositionStreaming;
    eventSys = pEventSys;

    resetDefaultFreq();
    //qDebug() << "ini freq vals to: " << qPrintable(getFreqStr());
}

void BenchmarkConfig::setAllBenchmarking(bool on) {
    userRuntimeEdited = false;
    iniFromCmdLine = false;
    recordSysTime = on;
    spikeDetect = on;
    spikeSent = on;
    spikeReceived = on;
    positionStreaming = on;
    eventSys = on;
}

int BenchmarkConfig::loadFromXML(QDomNode &globalConfNode) {
    //qDebug() << "loading Benchmarking stuff from XML";
    //qDebug() << "@@@@@ Before loading xml: " << qPrintable(this->getBoolStr());
    //qDebug() << "@@@@@ Before loading xml f: " << qPrintable(this->getFreqStr());

    resetDefaultFreq();
    int readVal = -1;
    readVal = globalConfNode.toElement().attribute("recordSysTime", "-1").toInt();
    if (readVal >= 0) {
        recordSysTime = true;
    }
    else
        recordSysTime = false;

    readVal = globalConfNode.toElement().attribute("spikeDetect", "-1").toInt();
    if (readVal >= 0) {
        spikeDetect = true;
        if (readVal != 0) {
            freqSpikeDetect = readVal;
        }
    }
    else
        spikeDetect = false;

    readVal = globalConfNode.toElement().attribute("spikeSent", "-1").toInt();
    if (readVal >= 0) {
        spikeSent = true;
        if (readVal != 0) {
            freqSpikeSent = readVal;
        }
    }
    else
        spikeSent = false;

    readVal = globalConfNode.toElement().attribute("spikeReceived", "-1").toInt();
    if (readVal >= 0) {
        spikeReceived = true;
        if (readVal != 0) {
            freqSpikeReceived = readVal;
        }
    }
    else
        spikeReceived = false;

    readVal = globalConfNode.toElement().attribute("positionStreaming", "-1").toInt();
    if (readVal >= 0) {
        positionStreaming = true;
        if (readVal != 0) {
            freqPositionStreaming = readVal;
        }
    }
    else
        positionStreaming = false;

    readVal = globalConfNode.toElement().attribute("eventSys", "-1").toInt();

    if (readVal >= 0) {
        eventSys = true;
        if (readVal != 0) {
            freqEventSys = readVal;
        }
    }
    else
        eventSys = false;

    if (spikeDetect || spikeSent || spikeReceived || positionStreaming || eventSys)
        recordSysTime = true;
    //qDebug() << "@@@@@ After loading xml: " << qPrintable(this->getBoolStr());
    //qDebug() << "@@@@@ After loading xml f: " << qPrintable(this->getFreqStr());
    return(0);
}

void BenchmarkConfig::saveToXML(QDomDocument &doc, QDomElement &rootNode) {
    QDomElement bconf = doc.createElement("BenchmarkConfiguration");

    rootNode.appendChild(bconf);

    if (spikeDetect)
        bconf.setAttribute("spikeDetect", freqSpikeDetect);
    if (spikeSent)
        bconf.setAttribute("spikeSent", freqSpikeSent);
    if (spikeReceived)
        bconf.setAttribute("spikeReceived", freqSpikeReceived);
    if (positionStreaming)
        bconf.setAttribute("positionStreaming", freqPositionStreaming);
    if(eventSys)
        bconf.setAttribute("eventSys", freqEventSys);


}

void BenchmarkConfig::resetDefaultFreq() {
    freqSpikeDetect = BENCH_FREQ_SPIKE_DETECT_DEFAULT;
    freqSpikeSent = BENCH_FREQ_SPIKE_SENT_DEFAULT;
    freqSpikeReceived = BENCH_FREQ_SPIKE_RECEIVE_DEFAULT;
    freqPositionStreaming = BENCH_FREQ_POS_STREAM_DEFAULT;
    freqEventSys = BENCH_FREQ_EVENTSYS__DEFAULT;
}


/* --------------------------------------------------------------------- */

bool writeTrodesConfig(QString configFileName)
{
    QDomDocument doc;
    QDomElement root = doc.createElement("Configuration");

    doc.appendChild(root);

    /*QDomElement globalOptions = doc.createElement("GlobalOptions");
    root.appendChild(globalOptions);
    globalOptions.setAttribute("numChannels", hardwareConf->NCHAN);
    globalOptions.setAttribute("samplingRate", hardwareConf->sourceSamplingRate);
    globalOptions.setAttribute("hardwareConf->headerSize", hardwareConf->headerSize);
    globalOptions.setAttribute("globalConf->filePath", globalConf->filePath);
    globalOptions.setAttribute("globalConf->filePrefix", globalConf->filePrefix);*/
    globalConf->saveToXML(doc, root);
    benchConfig->saveToXML(doc, root);
    hardwareConf->saveToXML(doc, root);
    moduleConf->saveToXML(doc, root);
    streamConf->saveToXML(doc, root);
    headerConf->saveToXML(doc, root);
    spikeConf->saveToXML(doc, root);

    QFile file(configFileName);
    //QFile file(QString("outConfig.xml"));
    //use QIODevice::Truncate to overwrite any previously existing file and QIODevice::Text to write newline characters using the local8bit encoding (cross platform diz)
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        QTextStream TextStream(&file);
        QString xmlString = doc.toString();
        //doc.save(TextStream, 0);
        QString vers = "<?xml version=\"1.0\"?>";
        TextStream << vers << endl << xmlString;
        file.close();
        return true;
    }
    else {
        return false;
    }
}

bool writeRecConfig(QString configFileName, quint32 currentTimestamp)
{
    QDomDocument doc;
    QDomElement root = doc.createElement("Configuration");

    doc.appendChild(root);

    /*QDomElement globalOptions = doc.createElement("GlobalOptions");
    root.appendChild(globalOptions);
    globalOptions.setAttribute("numChannels", hardwareConf->NCHAN);
    globalOptions.setAttribute("samplingRate", hardwareConf->sourceSamplingRate);
    globalOptions.setAttribute("hardwareConf->headerSize", hardwareConf->headerSize);
    globalOptions.setAttribute("globalConf->filePath", globalConf->filePath);
    globalOptions.setAttribute("globalConf->filePrefix", globalConf->filePrefix);*/
    globalConf->saveToXML(doc, root,currentTimestamp);
    hardwareConf->saveToXML(doc, root);
    moduleConf->saveToXML(doc, root);
    streamConf->saveToXML(doc, root);
    headerConf->saveToXML(doc, root);
    spikeConf->saveToXML(doc, root);

    QFile file(configFileName);
    //QFile file(QString("outConfig.xml"));

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream TextStream(&file);
        QString xmlString = doc.toString();
        //doc.save(TextStream, 0);
        QString vers = "<?xml version=\"1.0\"?>";
        TextStream << vers << endl << xmlString;
        file.close();
        return true;
    }
    else {
        return false;
    }
}


//Main config file parser
int nsParseTrodesConfig(QString configFileName)
{
    QDomDocument doc("TrodesConf");
    QFile file;
    bool isRecFile = false;
    int filePos = 0;

    if (!configFileName.isEmpty()) {
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << "[ParseTrodesConfig] Error:" << QString("File %1 not found").arg(configFileName);
            return -1;
        }
        qDebug() << "[ParseTrodesConfig] Loading from configuration file " << configFileName;
        QFileInfo fi(configFileName);
        QString ext = fi.suffix(); // ext = "gz"
        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header
            isRecFile = true;
            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (!file.atEnd()) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    //qDebug() << "End of config header found at " << file.pos();
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
                if (!doc.setContent(configContent)) {
                    file.close();
                    qDebug("[ParseTrodesConfig] Error: Config header didn't read properly.");
                    return -1;
                }
            }
        }
        else {
            //this is a normal xml config file
            if (!doc.setContent(&file)) {
                file.close();
                qDebug("[ParseTrodesConfig] Error: XML didn't read properly.");
                return -1;
            }
        }
    }
    file.close();
    QDomElement root = doc.documentElement();
    if (root.tagName() != "Configuration") {
        qDebug("[ParseTrodesConfig] Configuration not root node.");
        return -1;
    }
    nTrodeTable = new NTrodeTable();

    // create the global configuration object and recreate the hardwareConf if it exists.
    globalConf = new GlobalConfiguration(NULL);

    //If there is a hardware conf around, we delete the old one after we make a new one.
    HardwareConfiguration* oldHardwareConf;
    if (hardwareConf != NULL) {
        oldHardwareConf = hardwareConf;
        hardwareConf = new HardwareConfiguration(NULL);
        delete oldHardwareConf;
    } else {
        hardwareConf = new HardwareConfiguration(NULL);
    }


    hardwareConf->sourceSamplingRate = 0;
    hardwareConf->headerSize = 0;
    hardwareConf->NCHAN = 0;
#ifdef PHOTOMETRY_CODE
    hardwareConf->NFIBER = 0;
    hardwareConf->APDsamplingRate = 0;
#endif


    //Load global options.  Note that this is only for backwards compatibility; new files have a GlobalConfiguration section
    QDomNodeList globalOptions = root.elementsByTagName("GlobalOptions");
    if (globalOptions.length() == 1) {
        // load up the globalConf and hardwareConf options from this section
        QDomNode optionsNode = globalOptions.item(0);
        QDomElement optionElements = optionsNode.toElement();

        int tempNCHAN = optionElements.attribute("numChannels", "0").toInt();
        if (tempNCHAN > 0) {
            hardwareConf->NCHAN = tempNCHAN;
            if ((hardwareConf->NCHAN % 32) != 0) {
                qDebug() << "[ParseTrodesConfig] Error: numChannels must be a multiple of 32";
                return -6;
            }
            qDebug() << "[ParseTrodesConfig] Number of channels: " << hardwareConf->NCHAN;
        }



        int tempSampRate = optionElements.attribute("samplingRate", "0").toInt();
        if (tempSampRate > 0) {
            hardwareConf->sourceSamplingRate = tempSampRate;
            qDebug() << "[ParseTrodesConfig] Sampling rate: " << hardwareConf->sourceSamplingRate << " Hz";
        }

        int tempHeaderSize = optionElements.attribute("headerSize", "0").toInt();
        if (tempHeaderSize > 0) {
            hardwareConf->headerSize = tempHeaderSize;
            qDebug() << "[ParseTrodesConfig] Header size: " << hardwareConf->headerSize;
            hardwareConf->headerSizeManuallyDefined = true;
        }

        globalConf->filePrefix = optionElements.attribute("filePrefix", "");
        globalConf->filePath = optionElements.attribute("filePath", "");
    }
    else {
        QDomNodeList globalConfigList = root.elementsByTagName("GlobalConfiguration");
        if (globalConfigList.length() > 1) {
            qDebug() << "[ParseTrodesConfig] Config file error: multiple GlobalConfiguration sections found in configuration file.";
            return -7;
        }
        QDomNode globalnode = globalConfigList.item(0);
        int globalErrorCode = globalConf->loadFromXML(globalnode);
        if (globalErrorCode != 0) {
            qDebug() << "[ParseTrodesConfig] Error: Failed to load global configuration err: " << globalErrorCode;
            return globalErrorCode;
        }
    }


    QDomNodeList benchmarkConfigList = root.elementsByTagName("BenchmarkConfiguration");
    //qDebug() << "@@@@ " << benchmarkConfigList.lenght() << " - " << benchmarkConfigList.count() << " - " << benchmarkConfigList.item(0)
    if (benchmarkConfigList.length()> 1) {
        //qDebug() << "@@@@@@@YEEE HAWWW, benchmarking enabled, hit em with some options!";
    }
    else {
        QDomNode benchNode = benchmarkConfigList.item(0);
        if (benchConfig == NULL) {
            //qDebug() << "@@@@@@making new bench config...";
            benchConfig = new BenchmarkConfig();
            benchConfig->resetDefaultFreq();
           // qDebug() << "2 freq vals: " << benchConfig->getFreqStr();
        }
        //only load the benchmarking settings from the workspace if benchmarking was not initiated from the command line
        if (!benchConfig->wasInitiatedFromCommandLine() && !benchConfig->wasEditedByUser()) {
            //qDebug() << "@@@@@@bench config not ini from cmd line or by user, loading from workspace...";
            //qDebug() << " freq vals: " << benchConfig->getFreqStr();
            benchConfig->loadFromXML(benchNode);
        }
    }
    //TODO: add in error catch/throw message for incorrect benchmark config settings / failure to load


    //------------------------------------------------------------------

    QDomNodeList hardwareConfigList = root.elementsByTagName("HardwareConfiguration");
    if (hardwareConfigList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: Multiple HardwareConfiguration sections found in configuration file.";
        return -5;
    }
    if (hardwareConfigList.length() > 0) {
        QDomNode hardwarenode = hardwareConfigList.item(0);
        int hardwareErrorCode = hardwareConf->loadFromXML(hardwarenode);
        if (hardwareErrorCode != 0) {
            qDebug() << "[ParseTrodesConfig] Error: Failed to load hardware err: " << hardwareErrorCode;
            return hardwareErrorCode;
        }

    }
    else {
        qDebug() << "[ParseTrodesConfig] No hardware configuration found.";
    }


    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    // PARSE NETWORK CONFIGURATION
    QDomNodeList networkConfigList = root.elementsByTagName("NetworkConfiguration");
    if (networkConfigList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: multiple NetworkConfiguration sections found in configuration file.";
        return -5;
    }

    networkConf = new NetworkConfiguration();
    if (networkConfigList.length() > 0) {
        QDomNode networknode = networkConfigList.item(0);
        int networkErrorCode = networkConf->loadFromXML(networknode);
        if (networkErrorCode != 0) {
            return networkErrorCode;
        }
        networkConf->networkConfigFound = true;

        if (networkConf->trodesHost != "") {
            qDebug() << "[ParseTrodesConfig] Network configured, trodesHost = " << networkConf->trodesHost;
        }
        else {
            qDebug() << "[ParseTrodesConfig] Network configured, trodesHost not specified";
        }
    }
    else {
        qDebug() << "[ParseTrodesConfig] No network configuration found; using default hardware address" << TRODESHARDWARE_DEFAULTIP;
        networkConf->hardwareAddress = TRODESHARDWARE_DEFAULTIP;

        // TO DO: find the trodes hardware if it's address wasn't specified
    }
    /* set the fixed port values */
    networkConf->trodesPort = TRODESHARDWARE_CONTROLPORT;
    networkConf->ecuDirectPort = TRODESHARDWARE_ECUDIRECTPORT;
    networkConf->hardwarePort = TRODESHARDWARE_CONTROLPORT;

    if (networkConf->trodesHost == "") {
        // assume localhost
        networkConf->trodesHost = "127.0.0.1";
    }

    if ((networkConf->networkConfigFound) == false || (networkConf->dataSocketType == 0)) {
        // set to TCPIP if this was not specified
        networkConf->dataSocketType = TRODESSOCKETTYPE_TCPIP;
        qDebug() << "[ParseTrodesConfig] TCPIP data sockets selected";
    }

    // PARSE MODULE CONFIGURATION
    QDomNodeList moduleConfigList = root.elementsByTagName("ModuleConfiguration");
    if (moduleConfigList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: multiple ModuleConfig sections found in configuration file.";
        return -5;
    }

    moduleConf = new ModuleConfiguration(NULL);
    if (moduleConfigList.length() > 0) {
        moduleConf->trodesConfigFileName = configFileName;
        QDomNode modulenode = moduleConfigList.item(0);
        int moduleErrorCode = moduleConf->loadFromXML(modulenode);
        if (moduleErrorCode != 0) {
            return moduleErrorCode;
        }
        qDebug() << "[ParseTrodesConfig]" << moduleConf->singleModuleConf.length() << "module(s) configured.";
    }
    else {
        qDebug() << "[ParseTrodesConfig] No module configuration found.";
    }

    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
#ifdef PHOTOMETRY_CODE
    // PARSE PHOTOMETRY CONFIGURATION
    QDomNodeList fiberList = root.elementsByTagName("PhotometryConfiguration");
    if (fiberList.length() > 1) {
        qDebug() << "Config file error: multiple PhotometryConfiguration sections found in configuration file.";
        return -5;
    } else if (fiberList.length() == 0) {
        qDebug() << "No photometry configuration found.";
    }

    if (fiberList.length() > 0) {
        QDomNode photometrynode = fiberList.item(0);
         photometryConf = new PhotometryConfiguration(NULL, photometrynode.childNodes().length());
        int photometryErrorCode = photometryConf->loadFromXML(photometrynode);
        if (photometryErrorCode != 0) {
            return photometryErrorCode;
        }
        qDebug() << photometryConf->nfibers.length() << " fibers configured.";
    }
#endif

    // PARSE HEADER CONFIGURATION
    //QDomNodeList headerList = root.elementsByTagName("HeaderDisplay");
    QDomNodeList headerList = root.elementsByTagName("AuxDisplayConfiguration");
    if (headerList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: multiple AuxDisplayConfiguration sections found in configuration file.";
        return -5;
    } else if (headerList.length() == 0) {
        //Backwards compatibility-- section used to be called 'HeaderDisplay'
        headerList = root.elementsByTagName("HeaderDisplay");
        if (headerList.length() > 1) {
            qDebug() << "[ParseTrodesConfig] Error: multiple HeaderDisplay sections found in configuration file.";
            return -5;
        }
    }
    headerConf = new headerDisplayConfiguration(NULL);
    if (headerList.length() > 0) {
        QDomNode headernode = headerList.item(0);
        //headerConf = new headerDisplayConfiguration(NULL);
        int headerErrorCode = headerConf->loadFromXML(headernode);
        if (headerErrorCode != 0) {
            return headerErrorCode;
        }
        qDebug() << "[ParseTrodesConfig]" << headerConf->headerChannels.length() << " header display channels configured.";
    }
    else {
        qDebug() << "[ParseTrodesConfig] No header display configuration found.";
    }



    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    // PARSE SPIKE CONFIGURATION
    QDomNodeList list = root.elementsByTagName("SpikeConfiguration");
    /*
    if (list.isEmpty() || list.length() > 1) {
        qDebug() << "Config file error: either no or multiple SpikeConfiguration sections found in configuration file.";
        return -5;
    }*/
    QDomNode spikenode = list.item(0);
    int autoNtrodeType = 0;
    autoNtrodeType = spikenode.toElement().attribute("autoPopulate", "0").toInt();
    if (autoNtrodeType > 0) {
        qDebug() << "[ParseTrodesConfig] Using auto population of nTrodes";
        int numTrodes = hardwareConf->NCHAN/autoNtrodeType;
        spikeConf = new SpikeConfiguration(NULL, numTrodes);
    } else {
        spikeConf = new SpikeConfiguration(NULL, spikenode.childNodes().length());
    }

    int spikeErrorCode = spikeConf->loadFromXML(spikenode);

    if (spikeErrorCode != 0) {
        return spikeErrorCode;
    }

    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    // PARSE STREAMING CONFIGURATION
    list = root.elementsByTagName("StreamDisplay");
    if (list.isEmpty() || list.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: either no or multiple streamDisplay sections found in configuration file.";
        return -4;
    }
    QDomNode eegvisnode = list.item(0);
    streamConf = new streamConfiguration();
    int streamErrorCode = streamConf->loadFromXML(eegvisnode);
    if (streamErrorCode != 0) {
        return streamErrorCode;
    }


    qDebug() << "[ParseTrodesConfig] Configurations successfully loaded.";


    if (isRecFile) {
        return filePos; //return the position of the file where data begins
    }
    else {
        return 0;
    }
}

SingleSpikeTrodeConf::SingleSpikeTrodeConf(const SingleSpikeTrodeConf *s){
    this->nTrodeId = s->nTrodeId;
    this->nTrodeIndex = s->nTrodeIndex;
    this->refNTrode = s->refNTrode;
    this->refNTrodeID = s->refNTrodeID;
    this->refChan = s->refChan;
    this->refChanID = s->refChanID;
    this->lowFilter = s->lowFilter;
    this->highFilter = s->highFilter;
    this->moduleDataChan = s->moduleDataChan;
    this->moduleDataHighFilter = s->moduleDataHighFilter;
    this->refOn = s->refOn;
    this->filterOn = s->filterOn;
    this->moduleDataOn = s->moduleDataOn;
    this->lfpRefOn = s->lfpRefOn;
    this->lfpFilterOn = s->lfpFilterOn;

    this->channelSettings = s->channelSettings;

    this->tags = s->tags; this->tags.detach();
    this->gTags = s->gTags; this->gTags.detach();

    this->color = s->color;
    this->maxDisp = s->maxDisp; this->maxDisp.detach();
    this->thresh = s->thresh; this->thresh.detach();
    this->hw_chan = s->hw_chan; this->hw_chan.detach();
    this->unconverted_hw_chan = s->unconverted_hw_chan; this->unconverted_hw_chan.detach();
    this->thresh_rangeconvert = s->thresh_rangeconvert; this->thresh_rangeconvert.detach();
    this->streamingChannelLookup = s->streamingChannelLookup; this->streamingChannelLookup.detach();
    this->triggerOn = s->triggerOn; this->triggerOn.detach();
}
SpikeConfStructure::SpikeConfStructure(int n){
    hash.reserve(n);
    list.reserve(n);
    nitems = n;
}

int SpikeConfStructure::size(){
    return list.size();
}
int SpikeConfStructure::length(){
    return list.length();
}
int SpikeConfStructure::index(int id){
    return hash[id]->nTrodeIndex;
}
bool SpikeConfStructure::isEmpty(){
    return hash.isEmpty() || list.isEmpty();
}

/*
 //Defined inline in header
SingleSpikeTrodeConf* SpikeConfStructure::at(const int i){ //Access by index
    return list[i];
}
SingleSpikeTrodeConf* SpikeConfStructure::operator[](const int i){ //Access by index
    return list[i];
}
SingleSpikeTrodeConf* SpikeConfStructure::refOfIndex(int ind){ //Access the refNtrode object of the ntrode at index ind
    return list[list[ind]->refNTrode];
}
SingleSpikeTrodeConf* SpikeConfStructure::ID(const int i){ //Access by ID (may increase latency)
    return hash[i];
}
SingleSpikeTrodeConf* SpikeConfStructure::refOfID(int id){ //Access the refNtrode object of the ntrode with ID id (may increase latency)
    return list[hash[id]->refNTrode];
}*/

SingleSpikeTrodeConf* SpikeConfStructure::takeLast(){ //Removes last item from both and returns it
    return hash.take(list.takeLast()->nTrodeId);
}

void SpikeConfStructure::clear(){ //Removes all items from containers
    hash.clear();
    list.clear();
}
void SpikeConfStructure::append(int id, SingleSpikeTrodeConf* ntrodeconf){
    ntrodeconf->nTrodeIndex = list.size();
    list.append(ntrodeconf);
    hash.insert(id, ntrodeconf);
}
